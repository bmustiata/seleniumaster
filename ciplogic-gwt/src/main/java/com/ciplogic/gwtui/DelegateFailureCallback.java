package com.ciplogic.gwtui;

import com.google.gwt.core.client.Callback;

public abstract class DelegateFailureCallback<T, F> implements Callback<T, F> {
    private Callback delegate;

    public <U> DelegateFailureCallback(Callback<U, F> delegate) {
        this.delegate = delegate;
    }

    @Override
    public void onFailure(F failure) {
        delegate.onFailure(failure);
    }

    @Override
    public abstract void onSuccess(T result);
}
