package com.ciplogic.gwtui;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;

public class Dimensions {
    private int left;
    private int top;
    private int width;
    private int height;

    public Dimensions(int left, int top, int width, int height) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }

    public static Dimensions readFromWidget(Widget widget) {
        return new Dimensions(
            widget.getAbsoluteLeft(),
            widget.getAbsoluteTop(),
            widget.getOffsetWidth(),
            widget.getOffsetHeight()
        );
    }

    public static Dimensions readFromElement(Element element) {
        return new Dimensions(
            element.getAbsoluteLeft(),
            element.getAbsoluteTop(),
            element.getOffsetWidth(),
            element.getOffsetHeight()
        );
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dimensions that = (Dimensions) o;

        if (height != that.height) return false;
        if (left != that.left) return false;
        if (top != that.top) return false;
        if (width != that.width) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = top;
        result = 31 * result + width;
        result = 31 * result + left;
        result = 31 * result + height;
        return result;
    }
}
