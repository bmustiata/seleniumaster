package com.ciplogic.gwtui;

import java.util.List;

public interface ItemListHolder<T> {
    List<T> getItemList();
}
