package com.ciplogic.gwtui;

public enum ProgrammingLanguage {
    APL("text/apl"),
    ASTERISK("text/x-asterisk"),
    ASM_ARM("gas", "ARM"),
    ASM_X86("gas", "x86"),
    C("text/x-csrc"),
    CPP("text/x-c++src"),
    JAVA("text/x-java"),
    CSHARP("text/x-csharp"),
    SCALA("text/x-scala"),
    CLOJURE("text/x-clojure"),
    COFFEESCRIPT("text/x-coffeescript"),
    COMMON_LISP("text/x-common-lisp"),
    CSS("text/css"),
    D("text/x-d"),
    DIFF("text/x-diff"),
    ECL("text/x-ecl"),
    ERLANG("text/x-erlang"),
    GAS("text/x-gas"),
    GITHUB_MARKDOWN("gfm"),
    GO("text/x-go"),
    GROOVY("text/x-groovy"),
    HASKELL("text/x-haskell"),
    HAXE("text/x-haxe"),
    ASP_DOT_NET("application/x-aspx"),
    EMBEDDED_JAVASCRIPT("application/x-ejs"),
    JSP("application/x-jsp"),
    HTML("text/html"),
    HTTP("message/http"),
    JAVASCRIPT("text/javascript"),
    JSON("application/json"),
    TYPESCRIPT("application/typescript"),
    JINJA2("jinja2"),
    LESS("text/x-less"),
    LIVESCRIPT("text/x-livescript"),
    LUA("text/x-lua"),
    MARIA_DB("text/x-mariadb"),
    MARKDOWN("text/x-markdown"),
    MIRC("text/mirc"),
    NTRIPLES("text/n-triples"),
    OCAML("text/x-ocaml"),
    PASCAL("text/x-pascal"),
    PERL("text/x-perl"),
    PHP("text/x-php"),
    PHP_HTML("application/x-httpd-php"),
    PIG("text/x-pig"),
    PLAIN_TEXT("text/plain"),
    PROPERTIES_FILE("text/x-properties"),
    PYTHON("text/x-python"),
    R("text/x-rsrc"),
    RESTRUCTUREDTEXT("text/x-rst"),
    RUBY("text/x-ruby"),
    RUST("text/x-rustsrc"),
    SASS("text/x-sass"),
    SCHEME("text/x-scheme"),
    SCSS("text/x-scss"),
    SHELL("text/x-sh"),
    SIEVE("application/sieve"),
    SMALLTALK("text/x-stsrc"),
    SMARTY("text/x-smarty"),
    SPARQL("application/x-sparql-query"),
    SQL("text/x-sql"),
    STEX("text/x-stex"),
    LATEX("text/x-latex"),
    TCL("text/x-tcl"),
    TIDDLYWIKI("text/x-tiddlywiki"),
    TIKI_WIKI("text/tiki"),
    VB_NET("text/x-vb"),
    VBSCRIPT("text/vbscript"),
    VELOCITY("text/velocity"),
    VERILOG("text/x-verilog"),
    XML("application/xml"),
    XQUERY("application/xquery"),
    YAML("text/x-yaml"),
    Z80("text/x-z80"),

    TEXT("text");

    private String language;
    private String architecture;


    ProgrammingLanguage(String language) {
        this.language = language;
    }

    ProgrammingLanguage(String language, String architecture) {
        this.language = language;
        this.architecture = architecture;
    }

    public String getLanguage() {
        return language;
    }

    public String getArchitecture() {
        return architecture;
    }
}
