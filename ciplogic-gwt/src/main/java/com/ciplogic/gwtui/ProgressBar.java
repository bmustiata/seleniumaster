package com.ciplogic.gwtui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

public class ProgressBar extends Widget {
    interface MyUiBinder extends UiBinder<DivElement, ProgressBar> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    DivElement containerDiv;

    @UiField
    DivElement valueDiv;

    private int value; // 0 - 100

    private boolean loaded;

    public ProgressBar() {
        setElement(uiBinder.createAndBindUi(this));
        initializeEvents();
    }

    private void initializeEvents() {
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        adjustContainer(value);

        this.value = value;
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        loaded = true;

        adjustContainer( getValue() );
    }

    private void adjustContainer(int value) {
        if (loaded) {
            Dimensions dimensions = Dimensions.readFromElement(containerDiv);
            containerDiv.setPropertyString( "width", (dimensions.getWidth() * value / 100) + "px" );
        }
    }
}
