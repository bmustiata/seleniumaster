package com.ciplogic.gwtui.composite;

import com.ciplogic.gwtui.ItemListHolder;
import com.ciplogic.gwtui.datagrid.SingleSelectionDataGrid;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent;

public abstract class ListToListItemPicker<T> extends Composite {
    private ItemListHolder<T> itemListHolder;

    private ItemListHolder<T> availableItemsHolder;

    interface MyUiBinder extends UiBinder<SplitLayoutPanel, ListToListItemPicker> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    Button addItemButton;

    @UiField
    Button removeItemButton;

    @UiField
    Button moveUpButton;

    @UiField
    Button moveDownButton;

    @UiField(provided = true)
    SingleSelectionDataGrid<T> selectedItemsDataGrid;

    @UiField(provided = true)
    SingleSelectionDataGrid<T> availableItemsDataGrid;

    public ListToListItemPicker(ItemListHolder availableItemsHolder, ItemListHolder selectedItemsHolder) {
        this.itemListHolder = selectedItemsHolder;
        this.availableItemsHolder = availableItemsHolder;

        createAvailableItemsDataGrid();
        createSelectedItemsDataGrid();

        initWidget(uiBinder.createAndBindUi(this));

        initializeEvents();
    }

    public void updateAvailableItems() {
        availableItemsDataGrid.setRowData( availableItemsHolder.getItemList() );
    }

    private void createSelectedItemsDataGrid() {
        selectedItemsDataGrid = new SingleSelectionDataGrid<T>(itemListHolder.getItemList());

        selectedItemsDataGrid.addColumn(new TextColumn<T>() {
            @Override
            public String getValue(T object) {
                return getLabel(object);
            }
        }, "Name", "100%");

        selectedItemsDataGrid.setRowData( itemListHolder.getItemList() );
    }

    private void createAvailableItemsDataGrid() {
        availableItemsDataGrid = new SingleSelectionDataGrid<T>(availableItemsHolder.getItemList());

        availableItemsDataGrid.addColumn(new TextColumn<T>() {
            @Override
            public String getValue(T object) {
                return getLabel(object);
            }
        }, "Name", "100%");

        availableItemsDataGrid.setRowData( availableItemsHolder.getItemList() );
    }

    private void initializeEvents() {
        availableItemsDataGrid.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                addItemButton.setEnabled(isPickable(availableItemsDataGrid.getSelectedObject()));
            }
        });

        addItemButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                T selectedItem = availableItemsDataGrid.getSelectedObject();
                if (isPickable(selectedItem)) {
                    selectedItemsDataGrid.addItem(selectedItem);
                }
            }
        });

        selectedItemsDataGrid.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                refreshButtons();
            }
        });

        removeItemButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                selectedItemsDataGrid.removeSelectedElement();
                refreshButtons();
            }
        });

        moveUpButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                selectedItemsDataGrid.moveUpSelectedItem();
                refreshButtons();
            }
        });

        moveDownButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                selectedItemsDataGrid.moveDownSelectedItem();
                refreshButtons();
            }
        });
    }

    private void refreshButtons() {
        int selectedIndex = selectedItemsDataGrid.getSelectedIndex();

        removeItemButton.setEnabled(selectedItemsDataGrid.getSelectedObject() != null);
        moveUpButton.setEnabled(selectedIndex > 0);
        moveDownButton.setEnabled(selectedIndex < selectedItemsDataGrid.getRowCount() - 1 &&
                selectedIndex >= 0);
    }

    protected abstract boolean isPickable(T selectedItem);

    protected abstract String getLabel(T item);
}
