package com.ciplogic.gwtui.composite;

import com.ciplogic.gwtui.ItemListHolder;
import com.ciplogic.gwtui.datagrid.SingleSelectionDataGrid;
import com.ciplogic.gwtui.tree.AsynchronousTree;
import com.ciplogic.gwtui.tree.AsynchronousTreeModelBase;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent;

public abstract class TreeToListItemPicker<T> extends Composite {
    private ItemListHolder<T> itemListHolder;

    interface MyUiBinder extends UiBinder<SplitLayoutPanel, TreeToListItemPicker> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    Button addItemButton;

    @UiField
    Button removeItemButton;

    @UiField
    Button moveUpButton;

    @UiField
    Button moveDownButton;

    @UiField(provided = true)
    SingleSelectionDataGrid<T> itemDataGrid;

    @UiField(provided = true)
    AsynchronousTree<T> itemTree;

    public TreeToListItemPicker(ItemListHolder<T> itemListHolder, AsynchronousTreeModelBase<T> treeModel) {
        this.itemListHolder = itemListHolder;

        createItemDataGrid();
        createItemTree(treeModel);

        initWidget(uiBinder.createAndBindUi(this));

        initializeEvents();
    }

    private void createItemDataGrid() {
        itemDataGrid = new SingleSelectionDataGrid<T>(itemListHolder.getItemList());

        itemDataGrid.addColumn(new TextColumn<T>() {
            @Override
            public String getValue(T object) {
                return getLabel(object);
            }
        });
    }

    private void createItemTree(AsynchronousTreeModelBase<T> treeModel) {
        itemTree = new AsynchronousTree<T>(treeModel);
    }

    private void initializeEvents() {
        itemTree.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                addItemButton.setEnabled(isPickable(itemTree.getSelectedNode()));
            }
        });

        addItemButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                T selectedItem = itemTree.getSelectedNode();
                if (isPickable(selectedItem)) {
                    T processedItem = processAddedItem(selectedItem);
                    itemDataGrid.addItem(processedItem);
                }
            }
        });

        itemDataGrid.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                refreshButtons();
            }
        });

        removeItemButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                itemDataGrid.removeSelectedElement();
                refreshButtons();
            }
        });

        moveUpButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                itemDataGrid.moveUpSelectedItem();
                refreshButtons();
            }
        });

        moveDownButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                itemDataGrid.moveDownSelectedItem();
                refreshButtons();
            }
        });
    }

    private void refreshButtons() {
        int selectedIndex = itemDataGrid.getSelectedIndex();

        removeItemButton.setEnabled(itemDataGrid.getSelectedObject() != null);
        moveUpButton.setEnabled(selectedIndex > 0);
        moveDownButton.setEnabled(selectedIndex < itemDataGrid.getRowCount() - 1 &&
                selectedIndex >= 0);
    }

    /**
     * Returns the item that you actually want inserted.
     * @param item The item that was in the tree selection.
     * @return The item to be added in the list.
     */
    protected T processAddedItem(T item) {
        return item;
    }

    protected abstract boolean isPickable(T selectedItem);

    protected abstract String getLabel(T item);
}
