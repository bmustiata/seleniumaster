package com.ciplogic.gwtui.datagrid;

import com.ciplogic.gwtui.Dimensions;
import com.ciplogic.gwtui.resize.OnResizeListener;
import com.ciplogic.gwtui.resize.ResizeMonitor;
import com.google.gwt.user.cellview.client.DataGrid;

public class CiplogicDataGrid<T> extends DataGrid<T> implements OnResizeListener {
    public CiplogicDataGrid() {
        new ResizeMonitor(this, this).start();
        onResize();
    }

    @Override
    public void onResize(Dimensions oldDimensions, Dimensions newDimensions) {
        onResize();
    }
}
