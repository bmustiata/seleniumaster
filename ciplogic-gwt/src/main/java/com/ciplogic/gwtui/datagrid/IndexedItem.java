package com.ciplogic.gwtui.datagrid;

class IndexedItem<T> {
    private T item;
    private int index;

    IndexedItem(T item, int index) {
        this.item = item;
        this.index = index;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IndexedItem that = (IndexedItem) o;

        if (index != that.index) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return index;
    }
}
