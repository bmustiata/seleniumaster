package com.ciplogic.gwtui.datagrid;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ResizeLayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.ArrayList;
import java.util.List;

public class SingleSelectionDataGrid<T> extends Composite {
    interface MyUiBinder extends UiBinder<ResizeLayoutPanel, SingleSelectionDataGrid> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField(provided = true)
    CiplogicDataGrid<IndexedItem<T>> dataGrid;

    private List<T> targetList;

    private ArrayList<IndexedItem<T>> indexedItems = new ArrayList<IndexedItem<T>>(); // a copy kept in sync with the targetList.

    private SingleSelectionModel<IndexedItem<T>> selectionModel;

    // FIXME: change this to a holder instead?
    public SingleSelectionDataGrid(List<T> targetList) {
        dataGrid = new CiplogicDataGrid<IndexedItem<T>>();
        this.targetList = targetList;
        selectionModel = new SingleSelectionModel<IndexedItem<T>>();

        dataGrid.setSelectionModel(selectionModel);

        initWidget(uiBinder.createAndBindUi(this));

        this.setRowData(targetList);
    }

    public void addItem(T item) {
        IndexedItem<T> indexedElement = new IndexedItem<T>(item, indexedItems.size());
        addItem(indexedElement);

        updateRowData();

        selectItem(indexedElement);
    }

    public void removeSelectedElement() {
        IndexedItem<T> selectedObject = getSelectedItem();

        deselectItem(selectedObject);
        removeItem(selectedObject);
    }

    public void moveUpSelectedItem() {
        IndexedItem<T> selectedObject = getSelectedItem();
        IndexedItem<T> previousItem = getPreviousItem(selectedObject);

        deselectItem(selectedObject);
        swapItems(selectedObject, previousItem);
        selectItem(previousItem);
    }

    public void moveDownSelectedItem() {
        IndexedItem<T> selectedObject = getSelectedItem();
        IndexedItem<T> previousItem = getNextItem(selectedObject);

        deselectItem(selectedObject);
        swapItems(selectedObject, previousItem);
        selectItem(previousItem);
    }

    private void swapItems(IndexedItem<T> currentItem, IndexedItem<T> otherItem) {
        T temp = currentItem.getItem();

        currentItem.setItem( otherItem.getItem() );
        targetList.set( currentItem.getIndex(), otherItem.getItem() );

        otherItem.setItem(temp);
        targetList.set( otherItem.getIndex(), temp );
    }

    private IndexedItem<T> getPreviousItem(IndexedItem<T> selectedObject) {
        return indexedItems.get(selectedObject.getIndex() - 1);
    }

    private IndexedItem<T> getNextItem(IndexedItem<T> selectedObject) {
        return indexedItems.get(selectedObject.getIndex() + 1);
    }

    private void addItem(IndexedItem<T> indexedElement) {
        targetList.add(indexedElement.getItem());
        indexedItems.add(indexedElement);
    }

    private void removeItem(IndexedItem<T> selectedObject) {
        targetList.remove( selectedObject.getIndex() );
        setRowData(targetList);
    }

    private void updateRowData() {
        dataGrid.setRowData(indexedItems);
    }

    public void setRowData(List<T> items) {
        indexedItems = new ArrayList<IndexedItem<T>>();

        for (int i = 0; i < items.size(); i++) {
            indexedItems.add(new IndexedItem<T>(items.get(i), i));
        }

        dataGrid.setRowCount(indexedItems.size(), true);
        dataGrid.setRowData(indexedItems);
    }

    public void addColumn(final Column<T, ?> column) {
        Cell<Object> cell = (Cell<Object>) column.getCell();

        dataGrid.addColumn(new Column<IndexedItem<T>, Object>(cell) {
            @Override
            public Object getValue(IndexedItem<T> indexedItem) {
                return column.getValue(indexedItem.getItem());
            }
        });
    }

    public void addColumn(final Column<T, ?> column, String name) {
        addColumn(column, name, null);
    }

    public void addColumn(final Column<T, ?> column, String name, String width) {
        Cell<Object> cell = (Cell<Object>) column.getCell();

        Column<IndexedItem<T>, Object> newColumn = new Column<IndexedItem<T>, Object>(cell) {
            @Override
            public Object getValue(IndexedItem<T> indexedItem) {
                return column.getValue(indexedItem.getItem());
            }
        };

        dataGrid.addColumn(newColumn, name);

        if (width != null) {
            dataGrid.setColumnWidth(newColumn, width);
        }
    }

    public void addSelectionChangeHandler(SelectionChangeEvent.Handler handler) {
        selectionModel.addSelectionChangeHandler(handler);
    }

    public T getSelectedObject() {
        IndexedItem<T> selectedObject = getSelectedItem();

        return selectedObject == null ? null : selectedObject.getItem();
    }

    public int getSelectedIndex() {
        IndexedItem<T> selectedObject = getSelectedItem();

        return selectedObject == null ? -1 : selectedObject.getIndex();
    }

    private IndexedItem<T> getSelectedItem() {
        return selectionModel.getSelectedObject();
    }

    private void selectItem(IndexedItem<T> indexedElement) {
        selectionModel.setSelected(indexedElement, true);
    }

    private void deselectItem(IndexedItem<T> selectedObject) {
        selectionModel.setSelected(selectedObject, false);
    }

    public int getRowCount() {
        return dataGrid.getRowCount();
    }
}