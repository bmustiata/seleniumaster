package com.ciplogic.gwtui.dialog;

import com.ciplogic.gwtui.notifier.Notifier;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;

public class ResizableDialogBox extends DialogBox {
    interface Resources extends ClientBundle {
        static Resources INSTANCE = GWT.create(Resources.class);

        @Source("resize-handle.png")
        ImageResource resizeHandle();
    }

    public ResizableDialogBox() {
        super(false, false);

        FocusPanel resizeHandle = createResizeHandle();
        initializeEventsForResizeHandle(resizeHandle);
        addResizeHandleToDialog(resizeHandle);
    }

    private FocusPanel createResizeHandle() {
        FocusPanel resizeHandle = new FocusPanel(new Image(Resources.INSTANCE.resizeHandle() ));

        resizeHandle.setPixelSize(17, 17);
        resizeHandle.getElement().getStyle().setPosition(Style.Position.ABSOLUTE);
        resizeHandle.getElement().getStyle().setRight(0, Style.Unit.PX);
        resizeHandle.getElement().getStyle().setBottom(0, Style.Unit.PX);
        resizeHandle.getElement().getStyle().setCursor(Style.Cursor.SE_RESIZE);

        return resizeHandle;
    }

    private void addResizeHandleToDialog(FocusPanel resizeHandle) {
        Element tabElement = getCellElement(1, 1);
        tabElement.getStyle().setOverflow(Style.Overflow.VISIBLE);
        tabElement.appendChild(resizeHandle.getElement());
    }

    private boolean resizing;
    private int initialX, initialY, initialWidth, initialHeight;

    private void initializeEventsForResizeHandle(FocusPanel resizeHandle) {
        resizeHandle.addMouseDownHandler(new MouseDownHandler() {
            @Override
            public void onMouseDown(MouseDownEvent event) {
                if (!resizing) {
                    initialX = event.getRelativeX(getBodyElement());
                    initialY = event.getRelativeY(getBodyElement());
                    initialWidth = ResizableDialogBox.this.getOffsetWidth();
                    initialHeight = ResizableDialogBox.this.getOffsetHeight();
                    Notifier.showInfo("resize started");
                    resizing = true;
                }
            }
        });

        resizeHandle.addMouseMoveHandler(new MouseMoveHandler() {
            @Override
            public void onMouseMove(MouseMoveEvent event) {
                if (resizing) {
                    int currentX = event.getRelativeX(getBodyElement());
                    int currentY = event.getRelativeY(getBodyElement());
                    ResizableDialogBox.this.setWidth(initialWidth + initialX - currentX + "px");
                    ResizableDialogBox.this.setHeight(initialHeight + initialY - currentY + "px");
                }
            }
        });

        resizeHandle.addMouseUpHandler(new MouseUpHandler() {
            @Override
            public void onMouseUp(MouseUpEvent event) {
                Notifier.showInfo("resize ended");
                resizing = false;
            }
        });
    }

    private native Element getBodyElement() /*-{
        return $doc.body;
    }-*/;
}
