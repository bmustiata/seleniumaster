package com.ciplogic.gwtui.editor;

public interface CodeChangeListener {
    void onCodeChange(CodeTextEditor codeTextEditor);
}
