package com.ciplogic.gwtui.editor;

import com.ciplogic.gwtui.Dimensions;
import com.ciplogic.gwtui.ProgrammingLanguage;
import com.ciplogic.gwtui.resize.OnResizeListener;
import com.ciplogic.gwtui.resize.ResizeMonitor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.Widget;

import java.util.LinkedList;
import java.util.List;

/**
 * This is a wrapper over the CodeMirror editor in order to embed it in a single component.
 * Since it's based on codemirror, it supports all that codemirror has to offer.
 */
public class CodeTextEditor extends Widget implements OnResizeListener {
    interface MyUiBinder extends UiBinder<DivElement, CodeTextEditor> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    // the actual codemirror object.
    private JavaScriptObject editor;

    // when the code is not initialized, we still allow pre-setting the value from the editor 
    private boolean initialized;
    private String initialValue = "";

    // the generated ID for the element, used for initialization onLoad
    private ProgrammingLanguage programmingLanguage;
    private boolean showLineNumbers = true;

    private List<CodeChangeListener> codeChangeListenerList = new LinkedList<CodeChangeListener>();

    public CodeTextEditor() {
        this(ProgrammingLanguage.TEXT, true);
	}

    @UiConstructor
    public CodeTextEditor(ProgrammingLanguage programmingLanguage, boolean showLineNumbers) {
        this.programmingLanguage = programmingLanguage;
        this.showLineNumbers = showLineNumbers;

        setElement( uiBinder.createAndBindUi(this) );
    }

	@Override
	protected void onLoad() {
		super.onLoad();

        Element textArea = getElement().getFirstChildElement();
        initializeEditor(programmingLanguage.getLanguage(), programmingLanguage.getArchitecture(), showLineNumbers, textArea);
        setInternalValue(initialValue);

        initialized = true;

        new ResizeMonitor(this, this).start();
	}

    @Override
    public void onResize(Dimensions oldDimensions, Dimensions newDimensions) {
        refresh();
    }

    public void setValue(String value) {
        if (initialized) {
            setInternalValue(value);
        } else {
            initialValue = value;
        }
    }

    public String getValue() {
        if (initialized) {
            return getInternalValue();
        } else {
            return initialValue;
        }
    }

    public void addCodeChangeListener(CodeChangeListener codeChangeListener) {
        codeChangeListenerList.add(codeChangeListener);
    }

    public void notifyCodeChange() {
        for (CodeChangeListener codeChangeListener : codeChangeListenerList) {
            codeChangeListener.onCodeChange(this);
        }
    }

    public void setProgrammingLanguage(ProgrammingLanguage programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
        setInternalProgrammingLanguage(programmingLanguage.getLanguage(), programmingLanguage.getArchitecture());
    }

    public ProgrammingLanguage getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setShowLineNumbers(boolean showLineNumbers) {
        this.showLineNumbers = showLineNumbers;
        setInternalOption("lineNumbers", showLineNumbers);
    }

    public boolean isShowLineNumbers() {
        return this.showLineNumbers;
    }

    /**
     * When displayed for the first time, if is not already visible, the editor must be refreshed.
     */
    private native void refresh() /*-{
    	var editor = this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor;
    	setTimeout(function() {
			editor.refresh();
		}, 0);     
    }-*/;

    private native void initializeEditor(String programmingLanguage, String architecture, boolean showLineNumbers, Element element) /*-{
        var _this = this;
        var codeMirrorMode;

        if (!!architecture) {
            codeMirrorMode = {
                name : programmingLanguage,
                architecture: architecture
            };
        } else {
            codeMirrorMode = programmingLanguage;
        }

        var mainEditor = $wnd.CodeMirror.fromTextArea(element, {
            mode: codeMirrorMode,
            lineNumbers: showLineNumbers,
            lineWrapping: false,
            onChange: function() {
                _this.@com.ciplogic.gwtui.editor.CodeTextEditor::notifyCodeChange()();
            }
        });

        this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor = mainEditor;
    }-*/;

    private native String getInternalValue() /*-{
        return this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor.getValue();
    }-*/;

    private native void setInternalValue(String value) /*-{
        if (!!this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor) {
            this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor.setValue(value);
        }
    }-*/;

    private native void setInternalOption(String option, String value) /*-{
        if (!!this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor) {
            this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor.setOption(option, value);
        }
    }-*/;

    private native void setInternalProgrammingLanguage(String programmingLanguage, String architecture) /*-{
        if (!!this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor) {
            if (!!architecture) {
                this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor.setOption("mode", {
                    name : programmingLanguage,
                    architecture: architecture
                });
            } else {
                this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor.setOption("mode", programmingLanguage);
            }
        }
    }-*/;

    private native void setInternalOption(String option, boolean value) /*-{
        if (!!this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor) {
            this.@com.ciplogic.gwtui.editor.CodeTextEditor::editor.setOption(option, value);
        }
    }-*/;
}
