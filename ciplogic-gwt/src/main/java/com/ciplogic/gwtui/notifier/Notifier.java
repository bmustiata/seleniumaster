package com.ciplogic.gwtui.notifier;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

public class Notifier {
    static Element CONTAINER = DOM.createDiv();

    static {
        CONTAINER.getStyle().setPosition(Style.Position.ABSOLUTE);
        CONTAINER.getStyle().setZIndex(9999);
        CONTAINER.getStyle().setTop(12, Style.Unit.PX);
        CONTAINER.getStyle().setRight(12, Style.Unit.PX);

        getBodyElement().appendChild( CONTAINER );
    }

    public static void showWarning(String message) {
        showWarning(message, null);
    }

    public static void showWarning(String message, String title) {
        showMessage(NotifierElement.MessageType.WARNING, message, title);
    }

    public static void showInfo(String message) {
        showInfo(message, null);
    }

    public static void showInfo(String message, String title) {
        showMessage(NotifierElement.MessageType.INFO, message, title);
    }

    public static void showError(String message) {
        showError(message, null);
    }

    public static void showError(String message, String title) {
        showMessage(NotifierElement.MessageType.ERROR, message, title);
    }

    public static void showSuccess(String message) {
        showSuccess(message, null);
    }

    public static void showSuccess(String message, String title) {
        showMessage(NotifierElement.MessageType.SUCCESS, message, title);
    }

    public static native Element getBodyElement() /*-{
        return $doc.body;
    }-*/;

    private static void showMessage(NotifierElement.MessageType warning, String message, String title) {
        final NotifierElement notifierElement = new NotifierElement(warning, message, title);

        SimpleEffects.delay(4000.0, new Runnable() {
            @Override
            public void run() {
                SimpleEffects.fadeOut(notifierElement.getElement(), 400, new Runnable() {
                    @Override
                    public void run() {
                        notifierElement.removeFromParent();
                    }
                });
            }
        });

        CONTAINER.insertFirst( notifierElement.getElement() );
    }
}
