package com.ciplogic.gwtui.notifier;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

public class NotifierElement extends Composite {
    enum MessageType {
        WARNING,
        INFO,
        ERROR,
        SUCCESS
    }

    interface Resources extends ClientBundle {
        public static final Resources INSTANCE = GWT.create(Resources.class);

        @Source("warning.png")
        ImageResource warningIcon();

        @Source("info.png")
        ImageResource infoIcon();

        @Source("error.png")
        ImageResource errorIcon();

        @Source("success.png")
        ImageResource successIcon();
    }

    interface MyUiBinder extends UiBinder<HTMLPanel, NotifierElement> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField(provided = true)
    Image iconImage;

    @UiField(provided = true)
    Label titleLabel;

    @UiField(provided = true)
    Label messageLabel;

    public NotifierElement(MessageType messageType, String message, String title) {
        createMessageElements(messageType, message, title);

        registerEvents();
    }

    private void createMessageElements(MessageType messageType, String message, String title) {
        createIcon(messageType);
        createMessage(message);
        createTitle(title);

        this.initWidget(uiBinder.createAndBindUi(this));

        setStyleName("notificationStyles");
    }

    private void createIcon(MessageType messageType) {
        switch (messageType) {
            case ERROR: iconImage = new Image( Resources.INSTANCE.errorIcon() ); break;
            case INFO: iconImage = new Image( Resources.INSTANCE.infoIcon() ); break;
            case SUCCESS: iconImage = new Image( Resources.INSTANCE.successIcon() ); break;
            case WARNING: iconImage = new Image( Resources.INSTANCE.warningIcon() ); break;
        }
    }

    private void createMessage(String message) {
        messageLabel = new Label(message);
    }

    private void createTitle(String title) {
        titleLabel = new Label(title);
    }

    private void registerEvents() {
        addHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                NotifierElement.this.setStyleName("notificationStylesHover");
            }
        }, MouseOverEvent.getType());

        addHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                NotifierElement.this.setStyleName("notificationStyles");
            }
        }, MouseOutEvent.getType());

        addHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                NotifierElement.this.removeFromParent();
            }
        }, ClickEvent.getType());
    }
}
