package com.ciplogic.gwtui.notifier;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.Timer;

public class SimpleEffects {
    public static void fadeIn(final Element element, final double timeout, final Runnable runnable) {
        Timer timer = new Timer() {
            int passedTimeout = 0;

            @Override
            public void run() {
                passedTimeout += 50;

                element.getStyle().setOpacity( passedTimeout / timeout );

                if (passedTimeout >= timeout) {
                    element.getStyle().setOpacity(1.0);
                    runnable.run();
                    this.cancel();
                }
            }
        };

        timer.scheduleRepeating(50);
    }

    public static void delay(final double timeout, final Runnable runnable) {
        new Timer() {
            @Override
            public void run() {
                runnable.run();
            }
        }.schedule((int) timeout);
    }

    public static void fadeOut(final Element element, final double timeout, final Runnable runnable) {
        Timer timer = new Timer() {
            int passedTimeout = 0;

            @Override
            public void run() {
                passedTimeout += 50;

                element.getStyle().setOpacity( (timeout - passedTimeout) / timeout );

                if (passedTimeout >= timeout) {
                    element.getStyle().setOpacity(0.0);
                    runnable.run();
                    this.cancel();
                }
            }
        };

        timer.scheduleRepeating(50);
    }

}
