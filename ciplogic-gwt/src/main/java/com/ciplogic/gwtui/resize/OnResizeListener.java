package com.ciplogic.gwtui.resize;

import com.ciplogic.gwtui.Dimensions;

public interface OnResizeListener {
    public void onResize(Dimensions oldDimensions, Dimensions newDimensions);
}
