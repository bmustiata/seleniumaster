package com.ciplogic.gwtui.resize;

import com.ciplogic.gwtui.Dimensions;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Widget;

public class ResizeMonitor {
    private Dimensions lastDimensions;

    private Timer timer;

    public ResizeMonitor(final Widget widget, final OnResizeListener resizeListener) {
        this.lastDimensions = Dimensions.readFromWidget( widget );

        timer = new Timer() {
            @Override
            public void run() {
                if (! widget.isAttached()) {
                    timer.cancel();
                }

                Dimensions currentDimensions = Dimensions.readFromWidget(widget);
                if (! currentDimensions.equals(lastDimensions)) {
                    try {
                        resizeListener.onResize(lastDimensions, currentDimensions);
                    } finally {
                        lastDimensions = currentDimensions;
                    }
                }
            }
        };
    }

    public void start() {
        timer.scheduleRepeating(100);
    }
}
