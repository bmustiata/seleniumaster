package com.ciplogic.gwtui.tree;

import com.ciplogic.gwtui.tree.node.DataTreeNode;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.cellview.client.TreeNode;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.view.client.SelectionChangeEvent;

/**
 * An asynchronous tree is a tree that loads its nodes asynchronously.
 */
public class AsynchronousTree<T> extends Composite {
    private AsynchronousTreeViewModelAdaptor<T> modelAdaptor;
    private CellTree cellTree;
    private static final int MAX_NODE_SIZE = 10000;

    public AsynchronousTree(AsynchronousTreeModelBase<T> model) {
        modelAdaptor = new AsynchronousTreeViewModelAdaptor<T>(model, model.getRoot()); // FIXME: rootNode should be also in here.

        cellTree = new CellTreeMaxNodeSize(modelAdaptor, modelAdaptor.getRootNode());
        cellTree.setDefaultNodeSize(MAX_NODE_SIZE);
        cellTree.setStyleName("overflowScroll");

        addOnOpenNodeLoadContentHandler(cellTree);
        addOnClickHandler(cellTree);

        initWidget(cellTree);

        modelAdaptor.loadDataAsync(modelAdaptor.getRootNode());
    }

    private void addOnClickHandler(CellTree cellTree) {
        cellTree.addHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                // FIXME:
            }
        }, ClickEvent.getType());
    }

    private void addOnOpenNodeLoadContentHandler(CellTree cellTree) {
        cellTree.addOpenHandler(new OpenHandler<TreeNode>() {
            @Override
            public void onOpen(OpenEvent<TreeNode> event) {
                TreeNode item = event.getTarget();
                DataTreeNode<T> treeNode = (DataTreeNode<T>) item.getValue();

                if (!treeNode.isContentLoaded()) {
                    modelAdaptor.loadDataAsync(treeNode);
                }
            }
        });
    }

    public T getSelectedNode() {
        return modelAdaptor.getSelection();
    }

    public void setSelectedNode(T node) {
        modelAdaptor.setSelection(node);
    }

    public void addClickHandler(ClickHandler clickHandler) {
        cellTree.addHandler(clickHandler, ClickEvent.getType());
    }

    public void addSelectionChangeHandler(SelectionChangeEvent.Handler handler) {
        modelAdaptor.getSelectionModel().addSelectionChangeHandler(handler);
    }

    /**
     * This code needs to exist since the tree gets confused whenever both a select and a refresh
     * are ran simultaneously.
     *
     * @param parentNode
     */
    public void selectAndRefresh(final T parentNode) {
        final HandlerRegistration[] handler = new HandlerRegistration[1];

        handler[0] = modelAdaptor.addSelectionChangeListener(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                new Timer() {
                    @Override
                    public void run() {
                        modelAdaptor.refreshNode(parentNode);
                        handler[0].removeHandler();
                    }
                }.schedule(1);
            }
        });

        modelAdaptor.setSelection(parentNode);
    }
}
