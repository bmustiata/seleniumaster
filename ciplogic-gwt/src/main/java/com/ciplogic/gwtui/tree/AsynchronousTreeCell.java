package com.ciplogic.gwtui.tree;

import com.ciplogic.gwtui.tree.node.AsynchronousTreeNode;
import com.ciplogic.gwtui.tree.node.DataTreeNode;
import com.ciplogic.gwtui.tree.node.LoadingFailedTreeNode;
import com.ciplogic.gwtui.tree.node.LoadingLabelTreeNode;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

// FIXME: IconCellDecorator + TextCell?
// AbstractCell is also good, just needs the image as a resource somehow.
public class AsynchronousTreeCell<T> extends AbstractCell<AsynchronousTreeNode<T>> {
    private AsynchronousTreeModel<T> model;
    private static Template templates = GWT.create(Template.class);

    interface Template extends SafeHtmlTemplates {
        @Template("<div>" +
                "<img src='{0}' style='width : 16px; height : 16px; float : left' />" +
                "<div style='margin-left: 16px; height : 16px; padding-left : 2px'>{1}</div>" +
            "</div>")
        SafeHtml iconLabel(String icon, String label);
    }

    public AsynchronousTreeCell(AsynchronousTreeModel<T> model) {
        super();

        this.model = model;
    }

    @Override
    public void render(Context context, AsynchronousTreeNode<T> node, SafeHtmlBuilder safeHtmlBuilder) {

        if (node instanceof LoadingLabelTreeNode) {
            safeHtmlBuilder.append(
                templates.iconLabel("images/icons/ajax-loader.gif", "loading data...")
            );
        } else if (node instanceof DataTreeNode) {
            DataTreeNode<T> dataNode = (DataTreeNode<T>) node;
            safeHtmlBuilder.append(
                    templates.iconLabel(model.getIcon( dataNode.getItem() ), model.getLabel(dataNode.getItem()) )
            );
        } else if (node instanceof LoadingFailedTreeNode) {
            safeHtmlBuilder.append(
                templates.iconLabel("images/icons/ajax-loader.gif", "loading data failed :(")
            );
        }
    }
}
