package com.ciplogic.gwtui.tree;

import com.google.gwt.core.client.Callback;

import java.util.List;

public interface AsynchronousTreeModel<T> {
    T getRoot();

    void getChildren(T parent, Callback<List<T>, Exception> callback);

    String getLabel(T item);

    String getIcon(T item);

    /**
     * Returns a unique / separated key of the item that identifies the position of the node in the tree.
     * @param item
     * @return
     */
    String getKey(T item);

    boolean isLeaf(T item);
}
