package com.ciplogic.gwtui.tree;

import java.util.ArrayList;
import java.util.List;

public abstract class AsynchronousTreeModelBase<T> implements AsynchronousTreeRefresher<T>, AsynchronousTreeModel<T> {
    private List<AsynchronousTreeRefresher<T>> refreshers = new ArrayList<AsynchronousTreeRefresher<T>>();
    private T rootNode;

    protected AsynchronousTreeModelBase(T rootNode) {
        this.rootNode = rootNode;
    }

    public void addRefresher(AsynchronousTreeRefresher<T> refresherModel) {
        refreshers.add(refresherModel);
    }

    @Override
    public void refreshNode(final T node) {
        for (AsynchronousTreeRefresher<T> refresher : refreshers) {
            refresher.refreshNode(node);
        }
    }

    @Override
    public T getRoot() {
        return rootNode;
    }
}
