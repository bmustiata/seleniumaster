package com.ciplogic.gwtui.tree;

public interface AsynchronousTreeRefresher<T> {
    public void refreshNode(T node);
}
