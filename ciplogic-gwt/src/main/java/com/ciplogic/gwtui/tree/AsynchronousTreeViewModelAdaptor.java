package com.ciplogic.gwtui.tree;

import com.ciplogic.gwtui.DelegateFailureCallback;
import com.ciplogic.gwtui.tree.node.AsynchronousTreeNode;
import com.ciplogic.gwtui.tree.node.DataTreeNode;
import com.ciplogic.gwtui.tree.node.LoadingFailedTreeNode;
import com.google.gwt.core.client.Callback;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.gwt.view.client.TreeViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @param <U> Type of items that are stored in the tree model.
 */
public class AsynchronousTreeViewModelAdaptor<U> implements TreeViewModel, AsynchronousTreeRefresher<U> {
    private AsynchronousTreeModelBase<U> model;
    private AsynchronousTreeCell treeCell;
    private SingleSelectionModel<AsynchronousTreeNode<U>> selectionModel;

    private DataTreeNode<U> asyncRootNode;
    private ReverseAsyncTreeNode<U> reverseAsyncTreeNode;

    public AsynchronousTreeViewModelAdaptor(AsynchronousTreeModelBase<U> model, U rootNode) {
        asyncRootNode = new DataTreeNode<U>(null, rootNode);
        reverseAsyncTreeNode = new ReverseAsyncTreeNode<U>(model.getKey(rootNode), asyncRootNode);

        this.model = model;
        treeCell = new AsynchronousTreeCell<U>(model);

        this.selectionModel = new SingleSelectionModel<AsynchronousTreeNode<U>>();

        model.addRefresher(this); // FIXME: remove refresher when the tree is gone?
    }

    // FIXME something is very wrong with the (T item) and then a cast to (DataTreeNode<T>).
    @Override
    public <T> NodeInfo<?> getNodeInfo(T item) {
        return new DefaultNodeInfo(
            ((DataTreeNode<T>) item).getDataProvider(),
            treeCell,
            selectionModel,
            null
        );
    }

    @Override
    public boolean isLeaf(Object nodeObject) {
        if (nodeObject instanceof DataTreeNode) {
            DataTreeNode<U> node = (DataTreeNode<U>) nodeObject;
            return model.isLeaf(node.getItem());
        }

        return true;
    }

    public U getSelection() {
        if (selectionModel.getSelectedObject() instanceof DataTreeNode) {
            return ((DataTreeNode<U>)selectionModel.getSelectedObject()).getItem();
        }

        return null;
    }

    void setSelection(U item) {
        String nodeKey = model.getKey(item);
        ReverseAsyncTreeNode<U> node = reverseAsyncTreeNode.findKey(nodeKey);

        selectionModel.setSelected(node.getAsyncNode(), true);
    }

    public SingleSelectionModel<AsynchronousTreeNode<U>> getSelectionModel() {
        return selectionModel;
    }

    @Override
    public void refreshNode(U item) {
        String nodeKey = model.getKey(item);
        ReverseAsyncTreeNode<U> node = reverseAsyncTreeNode.findKey(nodeKey);

        node.refresh();
        loadDataAsync(node.getAsyncNode());
    }

    public DataTreeNode<U> getRootNode() {
        return asyncRootNode;
    }

    public void loadDataAsync(final DataTreeNode<U> parentNode) {
        getChildren(parentNode, new Callback<List<DataTreeNode<U>>, Exception>() {
            @Override
            public void onFailure(Exception e) {
                ListDataProvider<AsynchronousTreeNode<U>> dataProvider = parentNode.getDataProvider();

                dataProvider.getList().addAll((List) Collections.singletonList(new LoadingFailedTreeNode(parentNode)));
                dataProvider.getList().remove(0);
            }

            @Override
            public void onSuccess(List<DataTreeNode<U>> treeNodeList) {
                parentNode.setContentLoaded(true);
                ListDataProvider<AsynchronousTreeNode<U>> dataProvider = parentNode.getDataProvider();

                dataProvider.getList().addAll(treeNodeList);
                dataProvider.getList().remove(0);
            }
        });
    }

    private void getChildren(final DataTreeNode<U> item, final Callback<List<DataTreeNode<U>>, Exception> callback) {
        model.getChildren(item.getItem(), new DelegateFailureCallback<List<U>, Exception>(callback) {
            @Override
            public void onSuccess(List<U> us) {
                List<DataTreeNode<U>> result = new ArrayList<DataTreeNode<U>>();

                for (U u : us) {
                    DataTreeNode<U> treeNode = new DataTreeNode<U>(item, u);
                    result.add(treeNode);
                    reverseAsyncTreeNode.addKey(model.getKey(u), treeNode);
                }

                callback.onSuccess(result);
            }
        });
    }

    public HandlerRegistration addSelectionChangeListener(SelectionChangeEvent.Handler handler) {
        return selectionModel.addSelectionChangeHandler(handler);
    }
}
