package com.ciplogic.gwtui.tree;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.view.client.TreeViewModel;

/**
 * This abomination exists because the default GWT tree can't set its default node size before starting to create stuff.
 */
public class CellTreeMaxNodeSize extends CellTree {
    private static final int MAX_NODE_SIZE = 10000;

    private static final String CIPLOGIC_TREE_CSS = "ciplogic-tree.css";


    interface CssStyling extends CellTree.Resources {
        @Override
        @Source(CIPLOGIC_TREE_CSS)
        Style cellTreeStyle();
    }

    public <T> CellTreeMaxNodeSize(TreeViewModel viewModel, T rootValue) {
        super(viewModel, rootValue, (Resources) GWT.create(CssStyling.class));
    }

    @Override
    public int getDefaultNodeSize() {
        return MAX_NODE_SIZE;
    }
}
