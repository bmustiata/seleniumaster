package com.ciplogic.gwtui.tree;

import com.ciplogic.gwtui.tree.node.DataTreeNode;

import java.util.LinkedHashMap;
import java.util.Map;

public class ReverseAsyncTreeNode<T> {
    public static final String CHILD_SEPARATOR = "/";

    private String key;
    private DataTreeNode<T> asyncNode;
    private Map<String, ReverseAsyncTreeNode<T>> children = new LinkedHashMap<String, ReverseAsyncTreeNode<T>>();

    public ReverseAsyncTreeNode(String key, DataTreeNode<T> asyncNode) {
        this.key = key;
        this.asyncNode = asyncNode;
    }

    /**
     * Searches in its structure for the node that has that key.
     * @param nodeKey
     * @return
     */
    public ReverseAsyncTreeNode<T> findKey(String nodeKey) {
        if (this.key.equals(nodeKey)) {
            return this;
        } else {
            return children.get( getDirectChild(getChildSubPath(nodeKey)) ).findKey(nodeKey);
        }
    }

    public void refresh() {
        children.clear();
        asyncNode.refresh();
    }

    public void addKey(String key, DataTreeNode<T> treeNode) {
        String childSubPath = getChildSubPath(key);
        if (childSubPath.contains(CHILD_SEPARATOR)) {
            children.get( getDirectChild(childSubPath) ).addKey(key, treeNode);
        } else {
            children.put( key, new ReverseAsyncTreeNode<T>(key, treeNode) );
        }
    }

    private String getChildSubPath(String key) {
        if (key.startsWith(this.key + CHILD_SEPARATOR)) {
            return key.substring(this.key.length() + CHILD_SEPARATOR.length());
        }

        throw new IllegalArgumentException("Invalid key. Parent: " + this.key + ", child: " + key);
    }

    private String getDirectChild(String childSubPath) {
        if (childSubPath.contains(CHILD_SEPARATOR)) {
            return this.key + CHILD_SEPARATOR + childSubPath.substring( 0, childSubPath.indexOf(CHILD_SEPARATOR) );
        } else {
            return this.key + CHILD_SEPARATOR + childSubPath;
        }
    }

    public DataTreeNode<T> getAsyncNode() {
        return asyncNode;
    }
}
