package com.ciplogic.gwtui.tree.node;

import com.google.gwt.view.client.ListDataProvider;

import java.util.ArrayList;
import java.util.List;

public class DataTreeNode<T> extends AsynchronousTreeNode<T> {
    private T item;
    private ListDataProvider<AsynchronousTreeNode<T>> dataProvider;

    private boolean contentLoaded;

    public DataTreeNode(AsynchronousTreeNode parentNode, T item) {
        super(parentNode);

        if (item == null) {
            throw new IllegalArgumentException("Node item can not be null.");
        }

        this.item = item;
        this.dataProvider = new ListDataProvider<AsynchronousTreeNode<T>>( createSingleList( createLoadingNode(parentNode)) );
    }

    private List<AsynchronousTreeNode<T>> createSingleList(AsynchronousTreeNode<T> node) {
        ArrayList<AsynchronousTreeNode<T>> result = new ArrayList<AsynchronousTreeNode<T>>();

        result.add(node);

        return result;
    }

    private LoadingLabelTreeNode<T> createLoadingNode(AsynchronousTreeNode parentNode) {
        return new LoadingLabelTreeNode<T>(parentNode);
    }

    public ListDataProvider<AsynchronousTreeNode<T>> getDataProvider() {
        return dataProvider;
    }

    public boolean isContentLoaded() {
        return contentLoaded;
    }

    public void setContentLoaded(boolean contentLoaded) {
        this.contentLoaded = contentLoaded;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public void refresh() {
        setContentLoaded(false);
        dataProvider.setList( createSingleList( createLoadingNode(parentNode)) );
    }
}
