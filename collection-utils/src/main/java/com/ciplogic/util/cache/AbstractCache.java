package com.ciplogic.util.cache;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Map;

/**
 * The base implementation for various caches (SimpleCache,
 * TransientCache, etc).
 */
abstract class AbstractCache implements Cache, Serializable {
    private boolean reThrowExceptions = true;

    private static Logger log = Logger.getLogger(AbstractCache.class);

    public AbstractCache() {
    }

    public AbstractCache(boolean reThrowExceptions) {
        this.reThrowExceptions = reThrowExceptions;
    }

    /**
     * Reads data associated with a key, using a null dataFetcher. If there is no value in the cache for that key,
     * null is considered to be the result, and is also registered as the cached value for that key.
     * @param key
     * @param <T>
     * @return
     */
    @Override
    public <T> T get(String key) {
        return (T) get(key, null);
    }

    /**
     * <p>
     *     Attempts to read the data associated with a key. If no entry exists associated with that key,
     * it will call the dataFetcher and assign the value returned to the given key. Null is a valid value
     * to be associated to the given key.
     * </p>
     * <p>
     *     If the dataFetcher throws an exception, depending on the reThrowExceptions, the exception will be rethrown
     * or swallowed. If it will be swallowed, null will be considered as the value for the key.
     * </p>
     * @param key
     * @param dataFetcher
     * @param <T>
     * @return
     */
    @Override
    public <T> T get(String key, DataFetcher dataFetcher) {
        if (! getCacheItems().containsKey(key)) {
            Object result = null;
            try {
                if (dataFetcher != null) {
                    result = dataFetcher.fetch();
                }
            } catch (Exception e) {
                log.warn("Unable to read property " + key);
                log.debug("Error is: ", e);

                if (reThrowExceptions) {
                    throw new CacheReadingException(e);
                }
            }
            getCacheItems().put(key, result);
        }

        return (T) getCacheItems().get(key);
    }

    protected abstract Map<String, Object> getCacheItems();
}
