package com.ciplogic.util.cache;

public interface Cache {
    public <T> T get(String key);
    public <T> T get(String key, DataFetcher dataFetcher);
}
