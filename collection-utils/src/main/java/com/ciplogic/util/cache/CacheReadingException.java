package com.ciplogic.util.cache;

public class CacheReadingException extends RuntimeException {
    public CacheReadingException() {
    }

    public CacheReadingException(String message) {
        super(message);
    }

    public CacheReadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public CacheReadingException(Throwable cause) {
        super(cause);
    }
}
