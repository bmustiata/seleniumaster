package com.ciplogic.util.cache;

/**
 * This class is used to fetch data for caches.
 */
public abstract class DataFetcher {
    public abstract Object fetch() throws Exception;
}
