package com.ciplogic.util.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple cache that is able to fetch the data from a DataFetcher if a value associated for a key does not exists.
 */
public class SimpleCache extends AbstractCache {
    private Map<String, Object> cacheItems = new HashMap<String, Object>();

    public SimpleCache() {
    }

    public SimpleCache(boolean reThrowExceptions) {
        super(reThrowExceptions);
    }

    @Override
    protected Map<String, Object> getCacheItems() {
        return cacheItems;
    }
}
