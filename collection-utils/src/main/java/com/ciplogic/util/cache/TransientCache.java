package com.ciplogic.util.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is a cache that has the property that it gets cleared whenever it gets serialized.<br/>
 * Thus it is particulary useful on places that perform this, and we need to invalidate the cache when such a
 * serialization of state occurs (for example on application servers with session replication, view scopes in JSF
 * & co that serialize their data to the client, etc.)
 */
public class TransientCache extends AbstractCache {
    private transient Map<String, Object> cacheItems = new HashMap<String, Object>();

    public TransientCache() {
    }

    public TransientCache(boolean reThrowExceptions) {
        super(reThrowExceptions);
    }

    @Override
    protected Map<String, Object> getCacheItems() {
        if (cacheItems == null) {
            cacheItems = new HashMap<String, Object>();
        }
        return cacheItems;
    }
}
