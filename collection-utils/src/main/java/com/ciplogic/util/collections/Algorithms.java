package com.ciplogic.util.collections;

import java.util.ArrayList;
import java.util.List;

public final class Algorithms {
    private Algorithms() {}

    public static <T, U> List<T> collect(List<U> items, Function<T, U> function) {
        List<T> result = new ArrayList<T>();

        for (U item : items) {
            result.add(function.call(item));
        }
        
        return result;
    }
}
