package com.ciplogic.util.collections;

public class ArrayUtils {
    public static <T> T[] cloneArray(T[] source) {
        if (source == null) {
            return null;
        }

        return source.clone();
    }
}
