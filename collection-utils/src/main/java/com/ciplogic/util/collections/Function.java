package com.ciplogic.util.collections;

public interface Function<T,U> {
    public T call(U item);
}
