package com.ciplogic.util.collections;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.addAll;

public final class ListMaker {
    private ListMaker() {}

    public static <T> List<T> list(T ... items) {
        List<T> result = new ArrayList<T>();
        addAll(result, items);

        return result;
    }
}
