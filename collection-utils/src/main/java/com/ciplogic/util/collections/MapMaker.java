package com.ciplogic.util.collections;

import com.ciplogic.util.collections.util.MapEntry;

import java.util.HashMap;
import java.util.Map;

/**
 * A class with a simpler API to readily create maps from Java.
 */
public class MapMaker {
    private MapMaker() {}

    public static Map<String, Object> map(MapEntry<String, Object> ... items) {
        Map<String, Object> result = new HashMap<String, Object>();

        for (MapEntry<String, Object> item : items) {
            result.put(item.getKey(), item.getValue());
        }

        return result;
    }

    public static MapEntry<String, Object> entry(String key, Object value) {
        return new MapEntry<String, Object>(key, value);
    }
}
