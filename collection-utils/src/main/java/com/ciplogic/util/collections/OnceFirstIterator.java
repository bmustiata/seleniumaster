package com.ciplogic.util.collections;

import java.util.Iterator;

/**
 * <p>
 * This iterator will return first the <code>firstValue</code> and from that moment on it will
 * endlessly return the <code>nextValue</code>.
 * </p>
 * <p>
 *      This is useful for scenarios like "where"/"and", ""/"." joins, etc. where the loop
 *      logic will be otherwise forced to contain an if is first condition, then do this.
 * </p>
 * @param <T>
 */
public class OnceFirstIterator<T> implements Iterator<T> {
    private T firstValue;
    private T nextValue;
    private boolean firstReturned;

    public OnceFirstIterator(T firstValue, T nextValue) {
        this.firstValue = firstValue;
        this.nextValue = nextValue;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public T next() {
        if (firstReturned) {
            return nextValue;
        } else {
            firstReturned = true;
            return firstValue;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
