package com.ciplogic.util.guice;

import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;

public class PostConstructMatcher extends AbstractMatcher<TypeLiteral<?>> {
    @Override
    public boolean matches(TypeLiteral<?> typeLiteral) {
        Class<?> targetClass = typeLiteral.getRawType();

        for (Method method : targetClass.getMethods()) {
            if (method.getParameterTypes().length > 0) {
                continue;
            }

            PostConstruct postConstruct = method.getAnnotation(PostConstruct.class);
            if (postConstruct != null) {
                return true;
            }
        }

        return false;
    }
}
