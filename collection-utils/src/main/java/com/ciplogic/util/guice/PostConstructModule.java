package com.ciplogic.util.guice;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;

public class PostConstructModule extends AbstractModule {
    @Override
    protected void configure() {
        bindListener(new PostConstructMatcher(), new TypeListener() {
            public <I> void hear(TypeLiteral<I> iTypeLiteral, TypeEncounter<I> iTypeEncounter) {
                iTypeEncounter.register(new InjectionListener<I>() {
                    public void afterInjection(I i) {
                        invokePostConstructMethod(i);
                    }
                });
            }
        });
    }

    private void invokePostConstructMethod(Object object) {
        for (Method method : object.getClass().getMethods()) {
            if (method.getParameterTypes().length > 0) {
                continue;
            }

            PostConstruct postConstruct = method.getAnnotation(PostConstruct.class);
            if (postConstruct != null) {
                try {
                    method.invoke(object);
                } catch (Exception e) {
                    throw new IllegalArgumentException("Unable to initialize object " + object, e);
                }
            }
        }
    }
}
