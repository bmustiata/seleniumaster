package com.ciplogic.util.io;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.String.format;

public class ClassStream {
    private static Logger log = Logger.getLogger(ClassStream.class);

    private ClassStream() {}

    public static String getResourceAsString(String resourceName) {
        return getResourceAsString(ClassStream.class, resourceName);
    }

    public static String getResourceAsString(Class clazz, String resourceName) {
        log.debug("Attempting to load " + resourceName + " from classpath.");
        try {
            StringBuilder resultString = new StringBuilder("");

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(clazz.getResourceAsStream(resourceName))
            );

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                resultString.append(line);
            }
            bufferedReader.close();

            return resultString.toString();
        } catch (IOException e) {
            log.error("Unable to read resource with name: " + resourceName, e);
            return null;
        }
    }
    
    public static String getResourceAsStringForClass(Class clazz, String name) {
        String basePath = clazz.getPackage().getName().replace(".", "/");
        String resourceName = format("/%s/%s", basePath, name);

        return getResourceAsString(clazz, resourceName);
    }
}
