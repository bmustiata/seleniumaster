package com.ciplogic.util.io;

import java.io.ByteArrayOutputStream;

public class MemoryOutputStream extends ByteArrayOutputStream {
    public byte[] getBytes() {
        return buf;
    }
}
