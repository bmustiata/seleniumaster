package com.ciplogic.util.io;

/**
 * This assumes all the paths given are UNIX paths.
 */
public final class PathUtil {
    public static String concatPath(String baseFolder, String otherFolder) {
        if (baseFolder == null) {
            return otherFolder;
        }

        if (otherFolder == null) {
            return baseFolder;
        }

        if (baseFolder.endsWith("/")) {
            return baseFolder + otherFolder;
        } else {
            return baseFolder + "/" + otherFolder;
        }
    }
}
