package com.ciplogic.util.lang;

public class StringUtils {
    private String value;

    private StringUtils(String value) {
        if (value == null) {
            throw new IllegalArgumentException("Value can not be null.");
        }

        this.value = value;
    }

    public static StringUtils value(String name) {
        return new StringUtils(name);
    }

    public static boolean isEmpty(String value) {
        return value == null || "".equals(value);
    }

    public boolean startsWithAny(String ... testValues) {
        for (String testValue : testValues) {
            if (value.startsWith(testValue)) {
                return true;
            }
        }

        return false;  
    }

    public boolean equalsAny(String ... testValues) {
        for (String testValue : testValues) {
            if (value.equals(testValue)) {
                return true;
            }
        }

        return false;
    }
}
