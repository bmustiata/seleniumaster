package com.ciplogic.util.reflect;

import com.ciplogic.util.reflect.exception.BeanRuntimeException;
import com.ciplogic.util.reflect.util.MethodDistanceComparator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static org.apache.commons.lang.ArrayUtils.isEmpty;
import static org.apache.commons.lang.ArrayUtils.subarray;
import static org.apache.commons.lang.StringUtils.capitalize;
import static org.apache.commons.lang.StringUtils.join;

/**
 * Provides operations on top of classes and objects using reflection.
 */
public class BeanUtils {
    public static final Pattern MULTIPLE_PROPERTIES_PATTERN = Pattern.compile("^(.*)\\.(.*?)$");

    private BeanUtils() {}

    public static <T> T invokeVarargMethod(Object object, String methodName, Object ... arguments) {
        try {
            Method method = BeanUtils.findVarargsMethod(object.getClass(), methodName, arguments);
            if (method != null) {
                return (T) method.invoke(object, subarray(arguments, 0, method.getParameterTypes().length));
            }
        } catch (InvocationTargetException e) {
            throw new BeanRuntimeException("Unable to invoke method. It threw exception.", e.getTargetException());
        } catch (Exception e) {
            throw new BeanRuntimeException("Unable to invoke method.", e);
        }
        throw new BeanRuntimeException(
                format("Unable to find method %s on object %s with class %s, and params (%s).",
                        methodName, object, object.getClass(), join(arguments, ", ")));
    }

    /**
     * Dinamically calls a method on the given object, with the given arguments.
     * @param object
     * @param methodName
     * @param arguments
     * @param <T>
     * @return
     */
    public static <T> T invokeMethod(Object object, String methodName, Object... arguments) throws BeanRuntimeException {
        try {
            Method method = findMethod(object.getClass(), methodName, arguments);
            if (method != null) {
                return (T) method.invoke(object, arguments);
            }
        } catch (Exception e) {
            throw new BeanRuntimeException("Unable to invoke method.", e);
        }
        throw new BeanRuntimeException(
                format("Unable to find method %s on object %s with class %s, and params (%s).",
                        methodName, object, object.getClass(), join(arguments, ", ")));
    }
    
    /**
     * Creates an instance of the class <b>target</b> calling the constructor
     * that accepts the arguments given.
     * @param target The class we want to create an instance to.
     * @param arguments Constructor arguments.
     * @param <T>
     * @return Brand new instance of type T.
     * @throws BeanRuntimeException If there is no constructor that matches the given arguments, or
     *              an exception happened during the constructor calling.
     */
    public static <T> T create(Class<T> target, Object ... arguments) {
        try {
            if (isEmpty(arguments)) {
                return createUsingDefaultConstructor(target);
            }

            Constructor<T> foundConstructor = findConstructor(target, arguments);

            if (foundConstructor != null) {
                return createUsingConstructorAndArguments(foundConstructor, arguments);
            }

            throw new BeanRuntimeException("No constructor was found for arguments: " + arguments + " for class " + target);
        } catch (Exception e) {
            throw new BeanRuntimeException(e);
        }
    }

    /**
     * Sets a property with the given <b>name</b> on the object <b>instance</b> given
     * as argument with the value passed as a parameter.
     * @param instance Object to set property to.
     * @param name Name of the property.
     * @param value Value of the property.
     */
    public static void setProperty(Object instance, String name, Object value) {
        if (name.contains(".")) { // multiple properties
            Matcher matcher = MULTIPLE_PROPERTIES_PATTERN.matcher(name);
            matcher.matches();

            instance = getProperty(instance, matcher.group(1));
            name = matcher.group(2);
        }

        String methodName = "set" + capitalize(name);

        for (Method method : instance.getClass().getMethods()) {
            if (method.getName().equals(methodName) &&
                    method.getParameterTypes().length == 1 &&
                    isValueAssignable(method.getParameterTypes()[0], value)) {
                try {
                    method.invoke(instance, value);
                    break;
                } catch (Exception e) {
                    throw new BeanRuntimeException("Unable to set value " + value + " on " + instance + " ." + name, e);
                }
            }
        }
    }

    /**
     * Gets the property with the given <b>name</b> from the object <b>instance</b> given
     * as argument.
     * @param instance
     * @param name
     * @return
     */
    public static Object getProperty(Object instance, String name) {
        try {
            if (name == null || name.isEmpty()) {
                return instance;
            }

            String[] propertyChain = name.split("\\.");
            Object result = instance;

            for (String simpleProperty : propertyChain) {
                result = getSimplePropertyValue(result, simpleProperty);
            }

            return result;
        } catch (Exception e) {
            throw new BeanRuntimeException(e);
        }
    }

    private static <T> T createUsingDefaultConstructor(Class<T> target) {
        try {
            return target.newInstance();
        } catch (Exception e) {
            throw new BeanRuntimeException(e);
        }
    }

    private static <T> Constructor<T> findConstructor(Class<T> target, Object[] arguments) {
        Constructor foundConstructor = null;

        NextConstructor:
        for (Constructor constructor : target.getConstructors()) {
            Class[] parameterTypes = constructor.getParameterTypes();

            if (parameterTypes.length != arguments.length) {
                continue;
            }

            for (int i = 0; i < parameterTypes.length; i++) {
                Class parameterType = parameterTypes[i];
                if (!isValueAssignable(parameterType, arguments[i])) {
                    continue NextConstructor;
                }
            }

            foundConstructor = constructor;
            break;
        }
        return foundConstructor;
    }


    private static Method findMethod(Class target, String methodName, Object[] arguments) {
        NextMethod:
        for (Method method : target.getMethods()) {
            if (!method.getName().equals(methodName)) {
                continue;
            }

            Class[] parameterTypes = method.getParameterTypes();

            if (parameterTypes.length != arguments.length) {
                continue;
            }

            for (int i = 0; i < parameterTypes.length; i++) {
                Class parameterType = parameterTypes[i];
                if (!isValueAssignable(parameterType, arguments[i])) {
                    continue NextMethod;
                }
            }

            return method;
        }

        return null;
    }

    public static Method findVarargsMethod(Class target, String methodName, final Object[] arguments) {
        Set<Method> foundMethods = new TreeSet<Method>(new MethodDistanceComparator(arguments));

        NextMethod:
        for (Method method : target.getMethods()) {
            if (!method.getName().equals(methodName)) {
                continue;
            }

            Class[] parameterTypes = method.getParameterTypes();

            if (parameterTypes.length > arguments.length) {
                continue;
            }

            for (int i = 0; i < parameterTypes.length; i++) {
                Class parameterType = parameterTypes[i];
                if (!isValueAssignable(parameterType, arguments[i])) {
                    continue NextMethod;
                }
            }

            foundMethods.add(method);
        }

        if (foundMethods.isEmpty()) {
            return null;
        }

        return foundMethods.iterator().next();
    }

    private static <T> T createUsingConstructorAndArguments(Constructor<T> foundConstructor, Object[] arguments) throws InstantiationException, IllegalAccessException, InvocationTargetException {
        return (T) foundConstructor.newInstance(arguments);
    }

    static boolean isValueAssignable(Class parameterType, Object argument) {
        if (argument == null && !parameterType.isPrimitive()) {
            return true;
        }

        if (argument == null) {
            return false;
        }

        if (parameterType.isPrimitive() && argument instanceof Number) {
            return true;
        }

        return parameterType.isAssignableFrom(argument.getClass());
    }

    private static Object getSimplePropertyValue(Object result, String property) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String methodName = "get" + capitalize(property);

        Method method = result.getClass().getMethod(methodName, (Class<?>[]) null);

        return method.invoke(result, (Object[])null);
    }
}
