package com.ciplogic.util.reflect.exception;

public class BeanRuntimeException extends RuntimeException {
    public BeanRuntimeException() {
    }

    public BeanRuntimeException(String message) {
        super(message);
    }

    public BeanRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeanRuntimeException(Throwable cause) {
        super(cause);
    }
}