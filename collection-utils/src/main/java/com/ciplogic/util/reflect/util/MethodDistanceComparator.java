package com.ciplogic.util.reflect.util;

import java.lang.reflect.Method;
import java.util.Comparator;

/**
 * Computes the distance between two methods by the number of parameters. The closer the better.
 */
public class MethodDistanceComparator implements Comparator<Method> {
    private int argumentsLength;
    
    public MethodDistanceComparator(Object[] arguments) {
        this.argumentsLength = arguments.length;
    }

    public int compare(Method o1, Method o2) {
        int distance1 = Math.abs(argumentsLength - o1.getParameterTypes().length);
        int distance2 = Math.abs(argumentsLength - o2.getParameterTypes().length);

        return distance1 - distance2;
    }
}
