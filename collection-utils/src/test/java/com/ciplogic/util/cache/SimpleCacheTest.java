package com.ciplogic.util.cache;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SimpleCacheTest {
    @Test
    public void testDataFetchingHappensOnlyOnce() {
        Cache cache = new SimpleCache();

        DataFetcher dataFetcher = new DataFetcher() {
            boolean alreadyCalled = false;

            @Override
            public Object fetch() {
                assertFalse("The data fetcher should not be invoked again, " +
                        "since the result should be cashed for the same key", alreadyCalled);
                alreadyCalled = true;
                return null;
            }
        };

        cache.get("key", dataFetcher);
        cache.get("key", dataFetcher);
    }

    @Test
    public void testCachedDataShouldBeReturned() {
        Cache cache = new SimpleCache();

        cache.get("key", new DataFetcher() {
            @Override
            public Object fetch() {
                return "value";
            }
        });

        assertEquals("value", cache.get("key"));
    }
}
