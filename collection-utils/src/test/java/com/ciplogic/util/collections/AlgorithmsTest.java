package com.ciplogic.util.collections;

import org.junit.Test;

import java.util.List;

import static com.ciplogic.util.collections.Algorithms.collect;
import static com.ciplogic.util.collections.ListMaker.list;
import static java.lang.Integer.parseInt;
import static org.junit.Assert.assertEquals;

public class AlgorithmsTest {
    @Test
    public void testCollect() {
        List<String> items = list("1", "2", "3");
        List<Integer> result = collect(items, new Function<Integer, String>() {
            public Integer call(String item) {
                return parseInt(item);
            }
        });

        assertEquals(list(1,2,3), result);
    }
}
