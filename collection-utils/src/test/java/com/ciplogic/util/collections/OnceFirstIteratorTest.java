package com.ciplogic.util.collections;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OnceFirstIteratorTest {
    @Test(expected = UnsupportedOperationException.class)
    public void testOnceFirstIterator() {
        Iterator<String> onceFirstIterator = new OnceFirstIterator<String>("where", " and");
        String builtValue = "";

        for (int i = 0; i < 4; i++) {
            assertTrue(onceFirstIterator.hasNext());
            builtValue = builtValue + onceFirstIterator.next();
        }

        assertEquals("where and and and", builtValue);
        assertTrue(onceFirstIterator.hasNext());
        onceFirstIterator.remove();
    }
}
