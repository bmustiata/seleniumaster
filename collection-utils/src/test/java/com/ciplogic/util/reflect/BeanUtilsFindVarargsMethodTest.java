package com.ciplogic.util.reflect;

import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class BeanUtilsFindVarargsMethodTest {
    @Test
    public void testFindMethodWithMoreArguments() {
        Method method = BeanUtils.findVarargsMethod(User.class, "setName", new Object[]{
                "1", "2", "3"
        });

        assertNotNull("There should be a method that is setName with two string arguments.", method);
        assertEquals("The method should have 2 arguments", 2, method.getParameterTypes().length);
        assertEquals("The first argument should be a string.", String.class, method.getParameterTypes()[0]);
        assertEquals("The second argument should be a string.", String.class, method.getParameterTypes()[1]);
    }

    @Test
    public void testFindMethodWithExactNumberOfArguments() {
        Method method = BeanUtils.findVarargsMethod(User.class, "setName", new Object[]{
                "1"
        });

        assertNotNull("There should be a method that is setName with one string argument since it's a better match.", method);
        assertEquals("The method should have 1 argument", 1, method.getParameterTypes().length);
        assertEquals("The argument should be a string.", String.class, method.getParameterTypes()[0]);
    }

    @Test
    public void testFailFindMethodSinceTypesNotAssignable() {
        Method method = BeanUtils.findVarargsMethod(User.class, "setName", new Object[]{
                5
        });

        assertNull("There should be no such method setName with a int parameter.", method);
    }
}
