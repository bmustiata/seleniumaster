package com.ciplogic.util.reflect;

import com.ciplogic.util.reflect.exception.BeanRuntimeException;
import org.junit.Test;

import static com.ciplogic.util.reflect.BeanUtils.isValueAssignable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class BeanUtilsTest {
    @Test
    public void testIsValueAssignable() throws Exception {
        assertTrue(isValueAssignable(Integer.class, 3));
        assertTrue(isValueAssignable(String.class, "Name: "));
        assertTrue(isValueAssignable(int.class, 3.0));
    }

    @Test
    public void testGetterReading() throws Exception {
        Company company = new Company("companyname");
        User user = new User("username", company);

        assertSame(user, BeanUtils.getProperty(user, ""));
        assertSame(company, BeanUtils.getProperty(user, "company"));
        assertEquals("username", BeanUtils.getProperty(user, "name"));
        assertEquals("companyname", BeanUtils.getProperty(user, "company.name"));
    }

    @Test
    public void testInvokeMethod() throws Exception {
        Company company = new Company("companyname");

        BeanUtils.invokeMethod(company, "setName", "foo");
        assertEquals("foo", BeanUtils.invokeMethod(company, "getName"));
    }

    @Test(expected = BeanRuntimeException.class)
    public void testInvokeVarargMethodAsSimpleMethodShouldFail() throws Exception {
        Company company = new Company("companyname");

        BeanUtils.invokeMethod(company, "setName", "foo", "moo", "boo");
        assertEquals("foo", BeanUtils.invokeMethod(company, "getName", "foo", "boo", "moo"));
    }

    @Test
    public void testInvokeVarargMethod() throws Exception {
        Company company = new Company("companyname");

        BeanUtils.invokeVarargMethod(company, "setName", "foo", "moo", "boo");
        assertEquals("foo", BeanUtils.invokeVarargMethod(company, "getName", "foo", "boo", "moo"));
    }

    @Test
    public void testSetSimpleProperty() throws Exception {
        Company company = new Company("companyname");
        User user = new User("username", company);

        BeanUtils.setProperty(user, "company", new Company("othercompany"));

        assertEquals("othercompany", user.getCompany().getName());
    }

    @Test
    public void testSetDeepProperty() {
        Company company = new Company("companyname");
        User user = new User("username", company);

        BeanUtils.setProperty(user, "company.name", "othercompany");

        assertEquals("othercompany", user.getCompany().getName());
    }
}
