package com.ciplogic.util.reflect;

public class Company {
    private String name;

    public Company(String companyname) {
        this.name = companyname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
