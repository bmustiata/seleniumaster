package com.ciplogic.util.reflect;

public class User {
    private Company company;
    private String name;

    public User(String name, Company company) {
        this.name = name;
        this.company = company;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name, String surname) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void setName(String name, int age) {
        this.name = name;
    }
}
