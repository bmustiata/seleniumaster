package com.ciplogic.germanium.test;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;

@ManagedBean
@RequestScoped
public class AjaxBean implements Serializable {
    private String lastMethodCalled = "none";
    
    public String fastAjax() {
        lastMethodCalled = "fastAjax";
        return null;
    }
    
    public String slowAjax() throws InterruptedException {
        lastMethodCalled = "slowAjax";
        Thread.sleep(2000);

        return null;
    }
    
    public String reallySlowAjax() throws InterruptedException {
        lastMethodCalled = "reallySlowAjax";
        Thread.sleep(4000);

        return null;
    }

    public String getLastMethodCalled() {
        return lastMethodCalled;
    }
}
