package com.ciplogic.seleniumaster.tests;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseJettyServerTest {
    private static Server server;
    private final static String DEFAULT_SITE = "http://localhost:8080/";

    @BeforeClass
    public static void fireUpServer() throws Exception {
        server = new Server(8080);

        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/germaniumtest");
        webapp.setWar("target/germanium-regression-tests-1.0.0-SNAPSHOT.war");
        server.setHandler(webapp);

        server.start();
    }

    @AfterClass
    public static void shutdownServer() throws Exception {
        server.stop();
        server.join();
    }

    protected String getTargetSite() {
        String targetSite = System.getProperty("targetSite");

        if (targetSite != null) {
            return targetSite;
        }

        return DEFAULT_SITE;
    }
}
