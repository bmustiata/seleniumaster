package com.ciplogic.seleniumaster.tests;

import com.ciplogic.germanium.test.GermaniumExecutionFailed;
import com.ciplogic.germanium.test.GermaniumSeleneseTest;
import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class FullIntegrationTest extends BaseJettyServerTest {
    // FIXME: Targetsite should be by default localhost, or allow the targetSite variable to be set.
    // FIXME: these tests should be moved to different classes, with a suite that brings up the jetty container.

    @Test
    public void testDeployment() throws GermaniumExecutionFailed {
        new GermaniumSeleneseTest().runClasspathSuites("/tests/app-goes-up/enhance-selenium-test-suite.html",
                "/tests/app-goes-up/click-and-wait-test-suite.html")
            .withExtensions("userextension-do-some-click.js")
            .onTargetSite(getTargetSite())
            .onProfiles("ie")
            .now();
    }

    @Test
    public void testCustomLocators() throws GermaniumExecutionFailed {
        new GermaniumSeleneseTest().runClasspathSuites("/tests/app-goes-up/custom-locator-user-extension.html")
                .withExtensions("userextension-find-by-my-id.js", "userextension-do-some-click.js")
                .onTargetSite(getTargetSite())
                .now();
    }

    @Test
    public void testUserScriptsForJsf2AndAjax() throws GermaniumExecutionFailed {
        new GermaniumSeleneseTest().runFileSuites("src/test/resources/tests/app-goes-up/user-extensions-test-suite2.html")
                .withExtensions("jsf2-ajax.js")
                .onTargetSite(getTargetSite())
                .now();
    }

    @Test
    public void testScreenshotTaking() {
        BrowserProfileManager browserProfileManager = new BrowserProfileManager();
        browserProfileManager.initialize();

        try {
            new GermaniumSeleneseTest().runFileSuites("src/test/resources/tests/app-goes-up/screenshot-when-command-fails-test-suite.html")
                    .withExtensions("jsf2-ajax.js", "userextension-do-some-click.js")
                    .onTargetSite(getTargetSite())
                    .now();

            throw new IllegalStateException("Since the test is supposed to file, we shouldn't get here.");
        } catch (GermaniumExecutionFailed executionFailed) {
        }

        String screenshotFileName = "target/result-" + browserProfileManager.getDefaultProfile().getId() +
                "-screenshot-when-command-fails-test-suite.png";

        assertTrue(
            "Since the test failed, a screenshot should have been created for it.",
            new File(screenshotFileName).exists()
        );
        assertTrue(
            "The screenshot file should not be empty.",
            new File(screenshotFileName).length() > 0
        );
    }

    @Test
    public void testSeleniumVariables() throws GermaniumExecutionFailed {
        new GermaniumSeleneseTest().runClasspathSuites("/tests/app-goes-up/selenium-vars-test-suite.html")
                .withExtensions("jsf2-ajax.js", "userextension-do-some-click.js")
                .onTargetSite(getTargetSite())
                .now();
    }


    @Test
    public void testGetGermaniumProgramaticaly() {
        /*
        new GermaniumSeleneseTest().getGermanium()
                .withExtensions("userextension-simple-locator.js")
                .onTargetSite(targetSite)
                .onProfiles("ie")
                .now();
        */
    }

    @Ignore
    @Test
    public void testExternalIE() throws GermaniumExecutionFailed {
        new GermaniumSeleneseTest().runClasspathSuites("/tests/app-goes-up/user-extensions-test-suite.html")
                .withExtensions("jsf2-ajax.js", "userextension-do-some-click.js", "userextension-find-by-my-id.js")
                .onTargetSite(getTargetSite())
                .now();
    }

    @Ignore
    @Test
    public void keepTheServerUp() throws InterruptedException {
        Thread.sleep(1000000000);
    }

}