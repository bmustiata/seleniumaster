package com.ciplogic.seleniumaster.tests;

import com.ciplogic.germanium.test.GermaniumProgrammaticTest;
import com.ciplogic.seleniumaster.germanium.Germanium;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProgrammaticGermaniumIntegrationTest extends BaseJettyServerTest {
    @Test
    public void testProgrammaticGermanium() {
        Germanium germanium = null;
        try {
            germanium = new GermaniumProgrammaticTest().getGermanium()
                    .withExtensions("userextension-find-by-my-id.js",
                                    "jsf2-ajax.js",
                                    "userextension-do-some-click.js")
                    .onTargetSite(getTargetSite())
                    .now();

            germanium.open("/germaniumtest/ajaxTest.html");

            assertTrue("The text of the page should be there.", germanium.isTextPresent("call fast ajax"));
            assertFalse("The AJAX method was not called yet.", germanium.isTextPresent("fastAjax"));

            germanium.seleneseCommand("wireJsfMonitoring");

            germanium.seleneseCommand("click", "css=.fastAjaxClass");

            germanium.seleneseCommand("waitForActionComplete");

            assertTrue("The AJAX method was called, but the result is not there.", germanium.isTextPresent("fastAjax"));
        } finally {
            if (germanium != null) {
                germanium.getWrappedDriver().quit();
            }
        }
    }
}
