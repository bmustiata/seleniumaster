package com.ciplogic.seleniumaster.formatter;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.ciplogic.velocity.formatter.CiplogicVelocityClasspathResourceLoader;
import com.ciplogic.velocity.formatter.VelocityUtils;

import java.io.OutputStream;

import static com.ciplogic.util.collections.MapMaker.entry;
import static com.ciplogic.util.collections.MapMaker.map;
import static com.ciplogic.velocity.formatter.VelocityTemplateRenderer.renderTemplate;

public class OutputResultFormatter implements ResultFormatter {
    public void writeToStream(SeleniumTest seleniumTest, OutputStream outputStream, String type) {
        CiplogicVelocityClasspathResourceLoader.setBasePath("/formatter/" + type + "/");

        renderTemplate("SingleTestPage.vtl", outputStream, map(
                entry("seleniumTest", seleniumTest),
                entry("x", new VelocityUtils())
        ));
    }

    public void writeToStream(SeleniumTestSuite seleniumTestSuite, OutputStream outputStream, String type) {
        CiplogicVelocityClasspathResourceLoader.setBasePath("/formatter/" + type + "/");

        renderTemplate("TestSuite.vtl", outputStream, map(
                entry("seleniumSuite", seleniumTestSuite),
                entry("x", new VelocityUtils())
        ));
    }
}
