package com.ciplogic.seleniumaster.formatter;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;

import java.io.OutputStream;

public interface ResultFormatter {
    /**
     * Render a test result to the output stream given, using the module passed as the parameter.
     * @param seleniumTest
     * @param outputStream
     * @param type
     */
    public void writeToStream(SeleniumTest seleniumTest, OutputStream outputStream, String type);

    /**
     * Render a test suite result to the given output stream, using the module given as the parameter.
     * @param seleniumTestSuite
     * @param outputStream
     * @param type
     */
    public void writeToStream(SeleniumTestSuite seleniumTestSuite, OutputStream outputStream, String type);
}
