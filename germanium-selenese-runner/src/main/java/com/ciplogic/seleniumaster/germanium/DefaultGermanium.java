package com.ciplogic.seleniumaster.germanium;

import com.ciplogic.seleniumaster.germanium.events.UserExtensionsLoadListener;
import com.ciplogic.seleniumaster.javascript.JavascriptExecutorOverWebDriver;
import com.ciplogic.seleniumaster.javascript.JavascriptRunner;
import com.ciplogic.seleniumaster.runner.userextensions.Script;
import com.ciplogic.seleniumaster.runner.userextensions.ScriptLoader;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.google.inject.Inject;
import com.thoughtworks.selenium.DefaultSelenium;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.internal.seleniumemulation.AlertOverride;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.util.io.ClassStream.getResourceAsString;
import static java.lang.String.format;

/**
 * Germanium is a regular extended Selenium object that uses WebDriver in order to execute its commands.
 * Our object provides in addition user extensions, and also implements the JavascriptRunner interface that runs
 * javascript code in an isolated environment (catching exceptions).
 */
public class DefaultGermanium extends DefaultSelenium implements Germanium, WrapsDriver, JavascriptRunner {
    private List<Script> userExtensionScripts = new ArrayList<Script>();
    private JavascriptExecutorOverWebDriver javascriptExecutor;

    private static final Logger log = Logger.getLogger(Germanium.class);
    private List<UserExtensionsLoadListener> userExtensionsLoadListeners = new ArrayList<UserExtensionsLoadListener>();
    private ScriptLoader scriptLoader;

    private SeleneseExecutor seleneseExecutor;

    private ScreenshotSaver screenshotSaver;

    @Inject
    public DefaultGermanium(WebDriver webDriver,
                     SeleneseExecutor seleneseExecutor,
                     @TestBaseSite String baseUrl,
                     @UserExtensions List<Script> userExtensionScripts,
                     ScreenshotSaver screenshotSaver,
                     ScriptLoader scriptLoader) {
        super(new WebDriverCommandProcessor(baseUrl, webDriver));

        if (!(webDriver instanceof JavascriptExecutor)) {
            throw new IllegalArgumentException("The WebDriver instance must be also a javascript executor.");
        }

        this.screenshotSaver = screenshotSaver;
        this.seleneseExecutor = seleneseExecutor;
        javascriptExecutor = new JavascriptExecutorOverWebDriver((JavascriptExecutor) webDriver);
        this.userExtensionScripts = new ArrayList<Script>(userExtensionScripts);
        this.scriptLoader = scriptLoader;
    }

    @Override
    public void waitForPageToLoad(String timeout) {
        super.waitForPageToLoad(timeout);
        wireInUserExtensions();
    }

    @Override
    public void open(String url) {
        super.open(url);
        wireInUserExtensions();
    }

    @Override
    public void open(String url, String ignoreResponseCode) {
        super.open(url, ignoreResponseCode);
        wireInUserExtensions();
    }

    public WebDriver getWrappedDriver() {
        return ((WrapsDriver) commandProcessor).getWrappedDriver();
    }

    public void rewireUserExtensionsIfMissing() {
        if (! isSeleniumJsObjectDefined()) {
            wireInUserExtensions();
        }
    }

    @Override
    public String getEval(String script) {
        return (String) seleneseExecutor.execute("eval", script, null);
    }

    private boolean isSeleniumJsObjectDefined() {
        Object result = null;

        for (int i = 0; i < 5; i++) {
            try {
                result = new Script("return typeof selenium !== 'undefined'").execute(this);
            } catch (Exception e) {
                log.debug("An exception occured while querying for selenium. Waiting 400 ms for the browser to settle.");
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e1) {
                }
            }
        }

        return result != null && "true".equals(result.toString());
    }

    private void wireInUserExtensions() {
        overrideAlerts();
        if (isUserExtensionsDefined()) {
            runAllScriptsAsUserExtensions();
            addUserExtensionsLocatorStrategies();
            notifyUserExtensionsLoaded();
        }
    }

    private boolean isUserExtensionsDefined() {
        return ! userExtensionScripts.isEmpty();
    }

    private void overrideAlerts() {
        // so we do support delayed alerts even if they are not made with user interaction
        // FIXME: apparently there is API now to handle alerts.
        new AlertOverride(true).replaceAlertMethod(getWrappedDriver());
    }

    private void runAllScriptsAsUserExtensions() {
        if (! userExtensionScripts.isEmpty()) {
            scriptLoader.loadFromTemplate("selenium-init.js.vtl").execute(this);
            for (Script script : userExtensionScripts) {
                script.execute(this); // FIXME : check and log errors.
            }
            scriptLoader.loadFromTemplate("selenium-post.js.vtl").execute(this);
        }
    }

    private void addUserExtensionsLocatorStrategies() {
        String[] locatorNames = findLocatorNames();

        for (String locatorName : locatorNames) {
            String locatorImplementation = format("return selenium.pagebot.locateElementBy%s(locator, inDocument, inWindow);\n", locatorName);

            addLocationStrategy(locatorName.toLowerCase(), locatorImplementation);
        }
    }

    private String[] findLocatorNames() {
        String names = findLocatorNamesFromJavaScript();

        return names.split(",");
    }

    private String findLocatorNamesFromJavaScript() {
        String findLocatorNamesFunction = getResourceAsString(Germanium.class, "find-locator-names.js");
        return (String) callScript(findLocatorNamesFunction);
    }

    public Object callScript(String scriptCode) {
        return javascriptExecutor.callScript(scriptCode);
    }

    public void addOnUserExtensionsLoadListener(UserExtensionsLoadListener userExtensionsLoadListener) {
        userExtensionsLoadListeners.add(userExtensionsLoadListener);
    }

    private void notifyUserExtensionsLoaded() {
        for (UserExtensionsLoadListener listener : userExtensionsLoadListeners) {
            listener.onUserExtensionsLoad();
        }
    }

    @Override
    public Object seleneseCommand(String action, String... arguments) {
        if (arguments.length == 0) {
            return seleneseExecutor.execute(action, null, null);
        } else if (arguments.length == 1) {
            return seleneseExecutor.execute(action, arguments[0], null);
        } else {
            return seleneseExecutor.execute(action, arguments[0], arguments[1]);
        }
    }

    @Override
    public void captureScreenshot(String filename) {
        screenshotSaver.saveScreenshot(filename);
    }

    @Override
    public String captureScreenshotToString() {
        return ((TakesScreenshot) getWrappedDriver()).getScreenshotAs(OutputType.BASE64);
    }

    public WebElement findElement(String locator) {
        return (WebElement) seleneseExecutor.execute("findElement", locator, null);
    }

    public SeleneseExecutor getSeleneseExecutor() {
        return seleneseExecutor;
    }

    @Override
    public void setBaseUrl(String baseUrl) {
        this.commandProcessor = new WebDriverCommandProcessor(baseUrl, getWrappedDriver());
    }

    public ScreenshotSaver getScreenshotSaver() {
        return screenshotSaver;
    }

    public void setScreenshotSaver(ScreenshotSaver screenshotSaver) {
        this.screenshotSaver = screenshotSaver;
    }
}
