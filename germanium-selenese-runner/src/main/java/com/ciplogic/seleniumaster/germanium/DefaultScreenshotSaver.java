package com.ciplogic.seleniumaster.germanium;

import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;

public class DefaultScreenshotSaver implements ScreenshotSaver {
    private Logger log = Logger.getLogger(DefaultScreenshotSaver.class);

    private final WebDriver webDriver;

    @Inject
    public DefaultScreenshotSaver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    @Override
    public void saveScreenshot(String fileName) {
        try {
            FileOutputStream outputFile = new FileOutputStream(fileName);
            byte[] content = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
            IOUtils.copy(new ByteArrayInputStream(content), outputFile);

            outputFile.close();
        } catch (Exception e) {
            log.error("captureScreenshot failed.", e);
            throw new IllegalArgumentException(e);
        }
    }
}
