package com.ciplogic.seleniumaster.germanium;

import com.ciplogic.seleniumaster.germanium.events.UserExtensionsLoadListener;
import com.ciplogic.seleniumaster.javascript.JavascriptRunner;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsDriver;

/**
 * Germanium is a regular extended Selenium object that uses WebDriver in order to execute its commands.
 * Our object provides in addition user extensions, and also implements the JavascriptRunner interface that runs
 * javascript code in an isolated environment (catching exceptions).
 */
public interface Germanium extends WrapsDriver, JavascriptRunner, Selenium {
    void addOnUserExtensionsLoadListener(UserExtensionsLoadListener userExtensionsLoadListener);

    void rewireUserExtensionsIfMissing();

    Object seleneseCommand(String action, String... arguments);

    WebElement findElement(String locator);

    SeleneseExecutor getSeleneseExecutor();

    void setBaseUrl(String baseSite);
}
