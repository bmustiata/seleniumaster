package com.ciplogic.seleniumaster.germanium;

public interface ScreenshotSaver {
    void saveScreenshot(String fileName);
}
