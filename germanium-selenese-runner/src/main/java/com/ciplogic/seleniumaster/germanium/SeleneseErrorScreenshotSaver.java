package com.ciplogic.seleniumaster.germanium;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;

// FIXME: remove this. Only ScreenshotSaver should remain, eventually with some custom naming strategies.
public interface SeleneseErrorScreenshotSaver {
    /**
     *
     * @param suite The suite that failed.
     * @param test The test that failed.
     * @param command The command that failed.
     */
    public void saveScreenshot(SeleniumTestSuite suite, SeleniumTest test, SeleniumCommand command);
}
