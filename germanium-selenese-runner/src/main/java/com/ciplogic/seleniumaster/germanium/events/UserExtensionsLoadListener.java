package com.ciplogic.seleniumaster.germanium.events;

public interface UserExtensionsLoadListener {
    void onUserExtensionsLoad();
}
