var result = "";

for (var property in PageBot.prototype) {
    var matcher = /^locateElementBy(.*)$/.exec(property);
    if (matcher) {
        if (result.length) {
            result = result + "," + matcher[1];
        } else {
            result = matcher[1];
        }
    }
}

return result;