package com.ciplogic.seleniumaster.javascript;

public class AlertOpen {
    private String text;

    public AlertOpen(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "AlertOpen{" +
                "text='" + text + '\'' +
                '}';
    }
}
