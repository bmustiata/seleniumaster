package com.ciplogic.seleniumaster.javascript;

import static java.lang.String.format;

/**
 * The exception thrown in case a JS exception happened.
 */
public class JavaScriptExecutionException extends RuntimeException {
    private String name;
    private String message;
    private String description;

    public JavaScriptExecutionException() {
    }

    public JavaScriptExecutionException(String name, String message, String description) {
        this.name = name;
        this.message = message;
        this.description = description;
    }

    public JavaScriptExecutionException(String message) {
        super(message);
    }

    public JavaScriptExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public JavaScriptExecutionException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        if (name == null) {
            return super.toString();
        } else {
            return format("Failed executing JavaScript code. Name: `%s`, message: `%s`, description: `%s`",
                    name, message, description);
        }
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
