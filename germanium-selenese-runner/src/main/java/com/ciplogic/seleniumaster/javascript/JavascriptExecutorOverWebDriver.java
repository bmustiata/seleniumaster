package com.ciplogic.seleniumaster.javascript;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Map;

import static com.ciplogic.util.io.ClassStream.getResourceAsStringForClass;
import static java.lang.String.format;

public class JavascriptExecutorOverWebDriver implements JavascriptRunner {
    private static Logger log = Logger.getLogger(JavascriptExecutorOverWebDriver.class);
    private JavascriptExecutor javascriptExecutor;

    private static String jscallTemplate = getResourceAsStringForClass(
            JavascriptExecutorOverWebDriver.class,
            "jscall-template.js");

    public JavascriptExecutorOverWebDriver(JavascriptExecutor javascriptExecutor) {
        this.javascriptExecutor = javascriptExecutor;
    }
    
    @Override
    public Object callScript(String scriptCode) {
        log.debug("Executing raw script: " + scriptCode);
        String script = format(jscallTemplate, scriptCode);

        try {
            Object result = javascriptExecutor.executeScript(script);

            // if an alert is opened in the browser, selenium hijacks the JS call, and just returns its text.
            if (result instanceof String) {
                return new AlertOpen((String) result);
            }

            Map<String, Object> resultMap = (Map<String, Object>) result;

            if ("ERROR".equals(resultMap.get("status"))) {
                throw new JavaScriptExecutionException(stringValue(resultMap, "name"),
                        stringValue(resultMap, "message"),
                        stringValue(resultMap, "description"));
            } else if (resultMap.get("text") != null) {
                throw new JavaScriptExecutionException("Alert open.", "An alert was opened.", "An alert was opened.");
            } else {
                return resultMap.get("result");
            }

        } catch (Exception e) {
            if (e instanceof JavaScriptExecutionException) {
                throw (JavaScriptExecutionException)e;
            }

            String errorMessage = String.format("Failed executing the script:\n`%s`\n\n", script);

            log.error(errorMessage);

            throw new JavaScriptExecutionException(errorMessage, e);
        }
    }

    private String stringValue(Map<String, Object> resultMap, String name) {
        Object actualValue = resultMap.get(name);
        if (actualValue == null) {
            return "null";
        }

        return actualValue.toString();
    }

    private static String get(Map<String, Object> resultMap, String key) {
        Object result = resultMap.get(key);
        if (result != null) {
            return result.toString();
        }

        return "null";
    }
}
