package com.ciplogic.seleniumaster.javascript;

public interface JavascriptRunner {
    /**
     * Calls the javascript code, and returns whatever the script returns. If the script throws an exception a
     * JavascriptExecutionException is being thrown. If the script is blocked because an alert is present, an
     * <code>AlertOpen</code> object is returned.
     *
     * @see JavaScriptExecutionException
     * @see AlertOpen
     *
     * @param scriptCode
     * @return
     */
    public Object callScript(String scriptCode) throws JavaScriptExecutionException;
}
