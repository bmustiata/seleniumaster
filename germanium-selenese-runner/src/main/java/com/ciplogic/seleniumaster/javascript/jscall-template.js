return (function() {
    var callResult;
    try {
        callResult = {
            status : "SUCCESS",
            result : (function() {
                %s
            })()
        };
    } catch (e) {
        callResult = {
            status : "ERROR",
            result : e.message,
            message : e.message,
            description : e.description,
            name : e.name
        };
    }

    return callResult;
})();