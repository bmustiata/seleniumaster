package com.ciplogic.seleniumaster.javascript.util;

import com.ciplogic.seleniumaster.selenese.UserExtensionsEnhancer;
import com.ciplogic.util.io.ClassStream;

import static com.ciplogic.util.io.ClassStream.getResourceAsString;
import static java.lang.String.format;
import static org.apache.commons.lang.StringUtils.capitalize;

public class JavaScriptParser {
    private JavaScriptParser() {}

    /**
     * Escape all the quotes of the string.
     */
    public static String escapeAsJSString(String javascriptString) {
        if (javascriptString == null) {
            return "";
        }
        return javascriptString.replace("'", "\\'").replace("\"", "\\\"");
    }

    public static String seleniumCallString(String methodPrefix, String propertyName, String target, String value) {
        return format("selenium.%s%s('%s', '%s')",
                methodPrefix, capitalize(propertyName),
                escapeAsJSString(target), escapeAsJSString(value));
    }

    public static String userExtensionTryCatch(String javascriptCode) {
        return format(getResourceAsString(UserExtensionsEnhancer.class, "user-extension-call.js"), javascriptCode);
    }
}
