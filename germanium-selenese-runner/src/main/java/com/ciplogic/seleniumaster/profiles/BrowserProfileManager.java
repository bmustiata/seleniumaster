package com.ciplogic.seleniumaster.profiles;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfileConfig;
import com.ciplogic.seleniumaster.profiles.webdriver.DriverBuilderFactory;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

import static java.lang.String.format;
import static org.apache.commons.lang.StringUtils.isEmpty;

@Singleton
public class BrowserProfileManager {
    public static final String PROFILES_JSON = "profiles.json";

    private static Logger log = Logger.getLogger(BrowserProfileManager.class);

    private Map<String, BrowserProfile> profileMap;
    private BrowserProfile defaultProfile;

    private String profilesJsonName = PROFILES_JSON;

    private DriverBuilderFactory driverBuilderFactory;

    @Inject
    public BrowserProfileManager(DriverBuilderFactory driverBuilderFactory) {
        this.driverBuilderFactory = driverBuilderFactory;
    }

    public WebDriver getWebDriver(String profileName) {
        if (! profileMap.containsKey(profileName)) {
            String message = "Unable to find profile `" + profileName + "` in the list of available profiles: " +
                    StringUtils.join(profileMap.keySet(), ", ") + ".";
            log.error(message);
            throw new IllegalArgumentException(message);
        }
        
        BrowserProfile browserProfile = profileMap.get(profileName);
        return getWebDriver(browserProfile);
    }

    public WebDriver getWebDriver(BrowserProfile browserProfile) {
        return driverBuilderFactory.createDriverBuilder(browserProfile).getWebDriver();
    }

    public WebDriver getDefaultWebDriver() {
        return getWebDriver(defaultProfile);
    }

    public BrowserProfile getDefaultProfile() {
        return defaultProfile;
    }

    @Inject(optional = true)
    public void setProfilesJsonName(@ProfilesJsonName String profilesJsonName) {
        this.profilesJsonName = profilesJsonName;
    }

    @PostConstruct
    public void initialize() {
        reloadProfiles();
    }

    public synchronized void reloadProfiles() {
        profileMap = new HashMap<String, BrowserProfile>();
        defaultProfile = null;

        List<BrowserProfile> profiles = new ArrayList<BrowserProfile>();
        String defaultProfileName = null;

        try {
            List<URL> profileUrls = Collections.list(Thread.currentThread().getContextClassLoader().getResources(profilesJsonName));
            profileUrls.add(0, new File(profilesJsonName).toURI().toURL());

            for (URL url : profileUrls) {
                InputStream inputStream = null;
                try {
                    log.debug("Reading profiles file: " + url);
                    inputStream = url.openStream();

                    BrowserProfileConfig streamProfiles = new BrowserProfileReader().readBrowserProfiles(inputStream);
                    profiles.addAll(streamProfiles.getProfiles());

                    if (! isEmpty(streamProfiles.getDefaultProfile())) {
                        defaultProfileName = streamProfiles.getDefaultProfile();
                    }

                    inputStream.close();
                } catch (Exception e) {
                    //e.printStackTrace(); // FIXME: use log
                    log.warn("Unable to read profile file: " + url.toString());
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();  //FIXME: use log
        }

        for (BrowserProfile profile : profiles) {
            profileMap.put(profile.getId(), profile);
        }

        // FIXME: funny bug can happen if the same name as the predefined profiles is defined, since they override the user
        // ones.
        if (defaultProfileName != null) {
            defaultProfile = profileMap.get(defaultProfileName);
        }

        if (defaultProfile == null && profiles.size() > 0) {
            defaultProfile = profiles.get(0);
        }

        if (defaultProfile != null) {
            log.debug(format("Default profile is: %s (%s)",
                    defaultProfile.getId(), defaultProfile.getBrowser()));
        }
    }

    public Collection<BrowserProfile> getProfiles() {
        return profileMap.values();
    }

    public BrowserProfile getProfile(String profileId) {
        return profileMap.get(profileId);
    }
}
