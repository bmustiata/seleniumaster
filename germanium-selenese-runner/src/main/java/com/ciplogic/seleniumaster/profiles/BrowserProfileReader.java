package com.ciplogic.seleniumaster.profiles;


import com.ciplogic.seleniumaster.profiles.client.BrowserProfileConfig;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

public class BrowserProfileReader {
    public BrowserProfileConfig readBrowserProfiles(InputStream inputStream) {
        try {
            return new ObjectMapper().readValue(inputStream, BrowserProfileConfig.class);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
