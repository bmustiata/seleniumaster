package com.ciplogic.seleniumaster.profiles;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfileConfig;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.Writer;

public class BrowserProfileWriter {
    public void writeBrowserProfiles(Writer writer, BrowserProfileConfig config) {
        try {
            new ObjectMapper().writeValue(writer, config);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }
}
