package com.ciplogic.seleniumaster.profiles;

import com.ciplogic.seleniumaster.profiles.webdriver.DriverBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;

public class ProfilesModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new FactoryModuleBuilder().build(DriverBuilder.class));
    }
}
