package com.ciplogic.seleniumaster.profiles.client;

import com.ciplogic.util.collections.OnceFirstIterator;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.util.lang.StringUtils.isEmpty;

/**
 * Represents a browser profile that can be edited in the configuration file.
 */
public class BrowserProfile {
    private String id;
    private String browser;

    private String remoteIp; // if null, then it's local
    private Long remotePort = 4444L;

    private List<Capability> capabilities = new ArrayList<Capability>();
    private List<String> userExtensions = new ArrayList<String>();

    private String version;

    private BrowserProxy proxy;

    public BrowserProfile(String browser, String id) {
        this.browser = browser;
        this.id = id;
    }

    public BrowserProfile() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    @JsonIgnore
    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemote(String remoteIp) {
        if (! isEmpty(remoteIp)) {
            String[] items = remoteIp.split(":");

            this.remoteIp = computeIp(items);
            String port = computePort(items);
            if (! isEmpty(port)) {
                this.remotePort = Long.valueOf(port);
            }
        }
    }

    private String computePort(String[] items) {
        if (items.length >= 2) {
            return items[ items.length - 1 ];
        }

        return null;
    }

    private String computeIp(String[] items) {
        // FIXME: some RegExp form be here, but it must also work under GWT.
        OnceFirstIterator<String> colon = new OnceFirstIterator<String>("", ":");
        StringBuilder result = new StringBuilder("");

        if (items.length == 1) {
            return items[0];
        } else {
            for (int i = 0; i < items.length - 1; i++) {
                result.append( colon.next() ).append( items[i] );
            }
        }

        return result.toString();
    }

    public String getRemote() {
        if (isEmpty(remoteIp)) {
            return null;
        }

        if (remotePort == null) {
            return remoteIp;
        }

        return remoteIp + ":" + remotePort;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    @JsonIgnore
    public Long getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(Long remotePort) {
        this.remotePort = remotePort;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Capability> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<Capability> capabilities) {
        this.capabilities = capabilities;
    }

    public BrowserProxy getProxy() {
        return proxy;
    }

    public void setProxy(BrowserProxy proxy) {
        this.proxy = proxy;
    }

    @JsonIgnore
    public boolean isLocalHost() {
        return isEmpty(remoteIp);
    }

    @JsonIgnore
    public boolean isRemoteHost() {
        return ! isLocalHost();
    }

    public List<String> getUserExtensions() {
        return userExtensions;
    }

    public void setUserExtensions(List<String> userExtensions) {
        this.userExtensions = userExtensions;
    }
}
