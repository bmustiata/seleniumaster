package com.ciplogic.seleniumaster.profiles.client;

import com.ciplogic.util.collections.Function;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.util.collections.Algorithms.collect;
import static com.ciplogic.util.lang.StringUtils.isEmpty;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.join;

public class BrowserProfileConfig {
    private final List<BrowserProfile> profiles;
    private String defaultProfile;

    public BrowserProfileConfig() {
        profiles = new ArrayList<BrowserProfile>();
    }

    public BrowserProfileConfig(List<BrowserProfile> profiles) {
        this.profiles = profiles;
    }


    public List<BrowserProfile> getProfiles() {
        return profiles;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public BrowserProfile getProfile(String profileName) {
        if (isEmpty(profileName)) {
            throw new IllegalArgumentException("The profile name can't be empty: " + profileName);
        }

        for (BrowserProfile profile : profiles) {
            if (profile.getId().equals(profileName)) {
                return profile;
            }
        }

        throw new IllegalArgumentException(format("Profile `%s` was not found in the list of the available profiles: %s.",
                profileName, getProfileIdsAsString()));
    }

    private String getProfileIdsAsString() {
        return join(collect(profiles, new Function<String, BrowserProfile>() {
            @Override
            public String call(BrowserProfile item) {
                return item.getId();
            }
        }), ", ");
    }
}
