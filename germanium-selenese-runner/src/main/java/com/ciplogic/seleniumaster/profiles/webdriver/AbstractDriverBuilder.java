package com.ciplogic.seleniumaster.profiles.webdriver;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.Capability;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import static com.ciplogic.germanium.selenium.Browser.*;
import static org.openqa.selenium.remote.DesiredCapabilities.internetExplorer;

public abstract class AbstractDriverBuilder {
    protected BrowserProfile profile;

    protected AbstractDriverBuilder(BrowserProfile profile) {
        this.profile = profile;
    }

    public abstract WebDriver getWebDriver();

    protected DesiredCapabilities getBrowserCapabilities() {
        // FIXME: cache these.
        DesiredCapabilities defaultCapabilities = getDefaultCapabilities();
        applyCustomCapabilities(defaultCapabilities);

        return defaultCapabilities;
    }

    private DesiredCapabilities getDefaultCapabilities() {
        if (ANDROID.equals(profile.getBrowser())) {
            return DesiredCapabilities.android();
        } else if (CHROME.equals(profile.getBrowser())) {
            return DesiredCapabilities.chrome();
        } else if (FIREFOX.equals(profile.getBrowser())) {
            return DesiredCapabilities.firefox();
        } else if (HTML_UNIT.equals(profile.getBrowser())) {
            return DesiredCapabilities.htmlUnit();
        } else if (IE.equals(profile.getBrowser())) {
            return withScreenshot(internetExplorer());
        } else if (IPHONE.equals(profile.getBrowser())) {
            return DesiredCapabilities.iphone();
        } else if (OPERA.equals(profile.getBrowser())) {
            return DesiredCapabilities.opera();
        }

        return null;
    }

    private DesiredCapabilities withScreenshot(DesiredCapabilities capabilities) {
        capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);

        return capabilities;
    }

    private void applyCustomCapabilities(DesiredCapabilities defaultCapabilities) {
        for (Capability capability : profile.getCapabilities()) {
            defaultCapabilities.setCapability(capability.getName(), capability.getValue());
        }
    }
}
