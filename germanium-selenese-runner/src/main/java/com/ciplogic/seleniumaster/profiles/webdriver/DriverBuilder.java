package com.ciplogic.seleniumaster.profiles.webdriver;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

public interface DriverBuilder {
    FirefoxDriverBuilder createFirefoxDriverBuilder(BrowserProfile profile);

    SimpleCapabilitiesDriverBuilder createSimpleCapabilitiesDriverBuilder(BrowserProfile browserProfile);
}
