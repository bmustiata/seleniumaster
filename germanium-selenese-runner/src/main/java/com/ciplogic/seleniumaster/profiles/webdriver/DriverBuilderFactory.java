package com.ciplogic.seleniumaster.profiles.webdriver;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.util.lang.StringUtils;
import com.google.inject.Inject;

import static com.ciplogic.germanium.selenium.Browser.*;

public class DriverBuilderFactory {
    private final DriverBuilder driverBuilder;

    @Inject
    public DriverBuilderFactory(DriverBuilder driverBuilder) {
        this.driverBuilder = driverBuilder;
    }

    public AbstractDriverBuilder createDriverBuilder(BrowserProfile profile) {
        if (profile == null) {
            return null;
        }

        if (FIREFOX.equals(profile.getBrowser())) {
            return driverBuilder.createFirefoxDriverBuilder(profile);
        }
        if (isKnownBrowser(profile.getBrowser())) {
            return driverBuilder.createSimpleCapabilitiesDriverBuilder(profile);
        }

        return null;
    }

    private static boolean isKnownBrowser(String browser) {
        return StringUtils.value(browser)
                .equalsAny(ANDROID, CHROME, FIREFOX, HTML_UNIT, IE, IPHONE, OPERA);
    }
}
