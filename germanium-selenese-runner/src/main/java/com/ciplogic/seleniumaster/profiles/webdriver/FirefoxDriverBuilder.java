package com.ciplogic.seleniumaster.profiles.webdriver;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import static java.lang.String.format;

public class FirefoxDriverBuilder extends AbstractDriverBuilder {
    @Inject
    protected FirefoxDriverBuilder(@Assisted BrowserProfile profile) {
        super(profile);
    }

    @Override
    public WebDriver getWebDriver() {
        if (profile.getProxy() == null) {
            return new FirefoxDriver();
        } else {
            Proxy proxyPreferences = new Proxy();

            proxyPreferences.setProxyType(Proxy.ProxyType.MANUAL);
            proxyPreferences.setHttpProxy(format(
                    "%s:%d", profile.getProxy().getHost(), profile.getProxy().getPort()
            ));

            FirefoxProfile firefoxProfile = new FirefoxProfile();
            firefoxProfile.setProxyPreferences(proxyPreferences);
            firefoxProfile.setEnableNativeEvents(false); // FIXME: read this from capabilities.

            return new FirefoxDriver(firefoxProfile);
        }
    }
}
