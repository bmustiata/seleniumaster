package com.ciplogic.seleniumaster.profiles.webdriver;

import com.ciplogic.germanium.selenium.BinaryDriver;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.android.AndroidDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.iphone.IPhoneDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

import static com.ciplogic.germanium.selenium.Browser.*;

public class SimpleCapabilitiesDriverBuilder extends AbstractDriverBuilder {
    private final BinaryDriver binaryDriver;

    @Inject
    public SimpleCapabilitiesDriverBuilder(@Assisted BrowserProfile profile, BinaryDriver binaryDriver) {
        super(profile);

        this.binaryDriver = binaryDriver;
    }

    @Override
    public WebDriver getWebDriver() {
        try {
            if (profile.isRemoteHost()) {
                return createRemoteDriver();
            } else {
                return createLocalDriver();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private WebDriver createLocalDriver() {
        binaryDriver.ensureLoaded(profile.getBrowser(), null);

        if (CHROME.equals(profile.getBrowser())) {
            return new ChromeDriver(getBrowserCapabilities());
        } else if (FIREFOX.equals(profile.getBrowser())) {
            return new FirefoxDriver(getBrowserCapabilities()); // normally the FirefoxDriverBuilder should built this one
        } else if (HTML_UNIT.equals(profile.getBrowser())) {
            return new HtmlUnitDriver(getBrowserCapabilities());
        } else if (IE.equals(profile.getBrowser())) {
            return new InternetExplorerDriver(getBrowserCapabilities());
        } else if (OPERA.equals(profile.getBrowser())) {
            return new RemoteWebDriver(getBrowserCapabilities()); // XXX: obviously not working.
        }
        return null;
    }

    private WebDriver createRemoteDriver() throws Exception {
        URL remoteUrl = new URL(String.format("http://%s:%d/wd/hub", profile.getRemoteIp(), profile.getRemotePort()));

        if (CHROME.equals(profile.getBrowser())) {
            return new ChromeDriver(getBrowserCapabilities());
        } else if (FIREFOX.equals(profile.getBrowser())) {
            return new FirefoxDriver(getBrowserCapabilities()); // normally the FirefoxDriverBuilder should built this one
        } else if (OPERA.equals(profile.getBrowser())) {
            return new RemoteWebDriver(getBrowserCapabilities()); // XXX: obviously not working.
        } else if (ANDROID.equals(profile.getBrowser())) {
            return new AndroidDriver(remoteUrl, getBrowserCapabilities());
        } else if (IPHONE.equals(profile.getBrowser())) {
            return new IPhoneDriver(remoteUrl);
        }

        return new TakesScreenshotRemoteDriver(remoteUrl, getBrowserCapabilities());
    }
}
