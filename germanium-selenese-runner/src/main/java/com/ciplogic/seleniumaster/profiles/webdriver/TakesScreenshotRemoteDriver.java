package com.ciplogic.seleniumaster.profiles.webdriver;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class TakesScreenshotRemoteDriver extends RemoteWebDriver implements TakesScreenshot {
    public TakesScreenshotRemoteDriver() {
    }

    public TakesScreenshotRemoteDriver(CommandExecutor executor, Capabilities desiredCapabilities, Capabilities requiredCapabilities) {
        super(executor, desiredCapabilities, requiredCapabilities);
    }

    public TakesScreenshotRemoteDriver(CommandExecutor executor, Capabilities desiredCapabilities) {
        super(executor, desiredCapabilities);
    }

    public TakesScreenshotRemoteDriver(Capabilities desiredCapabilities) {
        super(desiredCapabilities);
    }

    public TakesScreenshotRemoteDriver(URL remoteAddress, Capabilities desiredCapabilities, Capabilities requiredCapabilities) {
        super(remoteAddress, desiredCapabilities, requiredCapabilities);
    }

    public TakesScreenshotRemoteDriver(URL remoteAddress, Capabilities desiredCapabilities) {
        super(remoteAddress, desiredCapabilities);
    }


    @Override
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        String base64 = execute(DriverCommand.SCREENSHOT).getValue().toString();
        return target.convertFromBase64Png(base64);
    }
}
