package com.ciplogic.seleniumaster.runner;

import com.ciplogic.seleniumaster.germanium.SeleneseErrorScreenshotSaver;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.google.inject.Inject;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

/**
 * The selenese runner runs a set of selenese commands over a selenium test, or a selenium test suite. The commands
 * are built using a SeleneseRunnerEnhancer, that actually creates the selenese commands into the SeleneseExecutor.
 */
public class DefaultSeleneseExecutor implements SeleneseExecutor {
    private static Logger log = Logger.getLogger(DefaultSeleneseExecutor.class);

    private Map<String, SeleneseCommandBase> commandMap = new HashMap<String, SeleneseCommandBase>();
    private Map<String, Object> storedVars = new HashMap<String, Object>();

    private SeleneseErrorScreenshotSaver seleneseScreenshotSaver;

    private static final String SELENIUM_REGEXP_STRING = ".*\\$\\{([\\d\\w]+?)\\}.*";
    private static final Pattern SELENIUM_ARGUMENT = Pattern.compile(SELENIUM_REGEXP_STRING);

    private static final Pattern SELENIUM_JAVASCRIPT_ARGUMENT = Pattern.compile("javascript\\{(.*)\\}");

    @Inject
    public DefaultSeleneseExecutor(SeleneseErrorScreenshotSaver seleneseErrorScreenshotSaver) {
        this.seleneseScreenshotSaver = seleneseErrorScreenshotSaver;
    }

    public SeleniumCommand executeCommand(SeleniumTestSuite suite, SeleniumTest test, SeleniumCommand command) {
        log.info(format("Executing command: %s(%s, %s)", command.getAction(), command.getTarget(), command.getValue()));
        
        long startTime = new Date().getTime();
        try {
            command.setCommandResult(new SeleniumCommandResult(startTime));
            command.getCommandResult().setStatus(SeleniumCommandResult.Status.SUCCESS);

            if (isCommandExisting(command.getAction())) {
                execute(command.getAction(), command.getTarget(), command.getValue());
            } else {
                log.error(format("Unable to find selenese command `%s`.", command.getAction()));
                throw new IllegalArgumentException("Unable to find selenese command " + command.getAction());
            }
        } catch (Exception e) {
            log.warn(format("Command %s(%s, %s) failed with exception.", command.getAction(), command.getTarget(), command.getValue()), e);
            command.setCommandResult(new SeleniumCommandResult(startTime));
            command.getCommandResult().setStatus(SeleniumCommandResult.Status.FAILED);
            command.getCommandResult().setErrorName(e.getMessage());
            command.getCommandResult().setErrorDescription( getDetailedMessage(e) );
            command.getCommandResult().setErrorStackTrace( ExceptionUtils.getStackTrace(e) );

            try {
                seleneseScreenshotSaver.saveScreenshot(suite, test, command);
            } catch (Exception e2) {
                log.error("Error capturing screenshot.", e2);
            }
        } finally {
            command.getCommandResult().setEndTime( new Date().getTime() );
        }

        log.info(format("Command status: %s.", command.getCommandResult().getStatus()));
        
        return command;
    }

    private String getDetailedMessage(Exception e) {
        return String.format("Root cause: %s", getRootCause(e).getMessage());
    }

    private Throwable getRootCause(Throwable e) {
        while (e.getCause() != e && e.getCause() != null) {
            e = e.getCause();
        }

        return e;
    }

    public Object execute(String commandName, String target, String value) {
        SeleneseCommandBase command = getCommandMap().get(commandName);

        if (command == null) {
            throw new IllegalArgumentException("Command `" + commandName + "` does not exist.");
        }

        target = parseSeleniumArgument(target);
        value = parseSeleniumArgument(value);

        return command.execute(target, value);
    }

    private String parseSeleniumArgument(String argument) {
        if (argument == null) {
            return null;
        }

        argument = parseSeleniumVarsIfAny(argument);
        argument = parseJavascriptEvalIfAny(argument);

        return argument;
    }

    private String parseSeleniumVarsIfAny(String argument) {
        boolean foundMatch = true;
        while (foundMatch) {
            foundMatch = false;

            Matcher matcher = SELENIUM_ARGUMENT.matcher(argument);

            if (matcher.matches()) {
                foundMatch = true;
                String variableName = matcher.group(1);
                argument = argument.replaceAll("\\$\\{" + variableName + "\\}", getStoredVariable(variableName));
            }
        }
        return argument;
    }

    private String parseJavascriptEvalIfAny(String argument) {
        Matcher matcher = SELENIUM_JAVASCRIPT_ARGUMENT.matcher(argument);
        if (matcher.matches()) {
            return getStringValue( commandMap.get("eval").execute(matcher.group(1), null) );
        }

        return argument;
    }

    // FIXME: this returns only strings.
    private String getStringValue(Object object) {
        if (object == null) {
            return "";
        }

        return object.toString();
    }

    private String getStoredVariable(String variableName) {
        Object variable = storedVars.get(variableName);

        if (variable == null) {
            return "";
        }

        return variable.toString();
    }

    private boolean isCommandExisting(String action) {
        return commandMap.containsKey(action);
    }

    /**
     * This method changes the test object itself by adding the command results.
     * @param suite
     * @param test
     */
    public SeleniumTest executeTest(SeleniumTestSuite suite, SeleniumTest test) {
        for (SeleniumCommand command : test.getCommandList()) {
            if (executeCommand(suite, test, command).getCommandResult().getStatus() == SeleniumCommandResult.Status.FAILED) {
                log.error(String.format("Failed executing command: %s(%s, %s). Error: %s\nDescription:%s",
                        command.getAction(),
                        command.getTarget(),
                        command.getValue(),
                        command.getCommandResult().getErrorName(),
                        command.getCommandResult().getErrorDescription()
                ));
                break;
            }
        }

        return test;
    }

    public SeleniumTestSuite executeSuite(SeleniumTestSuite suite) {
        for (SeleniumTest test : suite.getTestList()) {
            if (executeTest(suite, test).getCommandResult().getStatus() == SeleniumCommandResult.Status.FAILED) {
                break;
            }
        }
        return suite;
    }

    @Override
    public Map<String, SeleneseCommandBase> getCommandMap() {
        return commandMap;
    }

    @Override
    public SeleneseCommandBase command(String methodName) {
        return commandMap.get(methodName);
    }

    @Override
    public void storeVariable(String variableName, Object variableValue) {
        storedVars.put(variableName, variableValue);
    }

    @Override
    public Map<String, Object> getStoredVars() {
        return storedVars;
    }
}
