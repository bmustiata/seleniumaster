package com.ciplogic.seleniumaster.runner.exception;

public class SeleniumCommandFailed extends RuntimeException {
    public SeleniumCommandFailed() {
    }

    public SeleniumCommandFailed(String message) {
        super(message);
    }

    public SeleniumCommandFailed(String message, Throwable cause) {
        super(message, cause);
    }

    public SeleniumCommandFailed(Throwable cause) {
        super(cause);
    }
}
