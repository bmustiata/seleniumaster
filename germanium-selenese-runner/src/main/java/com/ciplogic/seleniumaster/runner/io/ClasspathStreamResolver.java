package com.ciplogic.seleniumaster.runner.io;

import com.google.inject.Inject;

import java.io.InputStream;

public class ClasspathStreamResolver implements StreamResolver {
    private String basePath;

    @Inject
    public ClasspathStreamResolver(@StreamResolverBasePath String basePath) {
        this.basePath = basePath;
    }

    public InputStream getInputStream(String resourceName) {
        InputStream inputStream = getClass().getResourceAsStream(basePath + resourceName);

        if (inputStream == null) {
            throw new IllegalArgumentException("Unable to find classpath resource: `" + basePath + resourceName + "`");
        }

        return inputStream;
    }
}
