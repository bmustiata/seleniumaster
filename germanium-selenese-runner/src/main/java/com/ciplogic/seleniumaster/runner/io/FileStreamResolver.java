package com.ciplogic.seleniumaster.runner.io;

import com.google.inject.Inject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FileStreamResolver implements StreamResolver {
    private File baseFile;

    @Inject
    public FileStreamResolver(@StreamResolverBasePath String baseFile) {
        this.baseFile = new File(baseFile);

        if (! this.baseFile.isDirectory()) {
            this.baseFile = this.baseFile.getAbsoluteFile().getParentFile();
        }
    }

    public InputStream getInputStream(String resourceName) {
        try {
            return new FileInputStream(baseFile.getAbsolutePath() + File.separator + resourceName);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to find the file: " + resourceName, e);
        }
    }
}
