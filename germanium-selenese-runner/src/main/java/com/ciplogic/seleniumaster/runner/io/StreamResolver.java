package com.ciplogic.seleniumaster.runner.io;

import java.io.InputStream;

public interface StreamResolver {
    InputStream getInputStream(String resourceName);
}
