package com.ciplogic.seleniumaster.runner.userextensions;

import com.ciplogic.seleniumaster.runner.userextensions.scriptfetchers.GermaniumJSFetcher;
import com.ciplogic.seleniumaster.runner.userextensions.scriptfetchers.SeleniumJSFetcher;
import com.ciplogic.velocity.formatter.CiplogicVelocityClasspathResourceLoader;
import com.ciplogic.velocity.formatter.VelocityTemplateRenderer;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.ciplogic.util.collections.MapMaker.entry;
import static com.ciplogic.util.collections.MapMaker.map;

public class ClasspathScriptLoader implements ScriptLoader {
    @Inject
    private SeleniumJSFetcher seleniumJSFetcher;

    @Inject
    private GermaniumJSFetcher germaniumJSFetcher;

    @Override
    public Script load(String scriptName) {
        try {
            String code = IOUtils.toString(ScriptLoader.class.getResourceAsStream("/user-extensions/" + scriptName));
            return new Script(code);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Unable to load script " + scriptName, e);
        }
    }

    @Override
    public Script loadFromTemplate(String scriptName) {
        return loadFromTemplate(scriptName, Collections.<String, Object>emptyMap());
    }

    @Override
    public Script loadFromTemplate(String scriptName, Map<String, Object> velocityArguments) {
        CiplogicVelocityClasspathResourceLoader.setBasePath("/user-extensions/");

        Map<String, Object> velocityObjects = new HashMap<String, Object>(velocityArguments);

        velocityObjects.putAll(map(
            entry("seleniumJS", seleniumJSFetcher),
            entry("germaniumJS", germaniumJSFetcher)
        ));

        String code = VelocityTemplateRenderer.renderTemplate(scriptName, velocityObjects);

        return new Script(code);
    }
}
