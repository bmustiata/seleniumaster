package com.ciplogic.seleniumaster.runner.userextensions;

import com.ciplogic.seleniumaster.exceptions.NotImplementedException;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class FileScriptLoader implements ScriptLoader {
    public Script load(String scriptName) {
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(scriptName);
            return new Script( IOUtils.toString(inputStream) );
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    public Script loadFromTemplate(String scriptName) {
        throw new NotImplementedException("Loading templates is not supported for files.");
    }

    @Override
    public Script loadFromTemplate(String scriptName, Map<String, Object> velocityArguments) {
        throw new NotImplementedException("Loading templates is not supported for files.");
    }
}
