package com.ciplogic.seleniumaster.runner.userextensions;

import com.ciplogic.seleniumaster.javascript.JavascriptRunner;

import static java.lang.String.format;

/**
 * Script represents a JavaScript call. The reason why this delegates the execution to another class, is
 * for allowing custom building scripts not only from code, but with extensions. (e.g. locators)
 */
public class Script {
    private String code;

    public Script(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object execute(JavascriptRunner javascriptRunner) {
        // FIXME: not really needed, a simple callScript on the javascriptRunner should suffice.
        return javascriptRunner.callScript(code);
    }
}
