package com.ciplogic.seleniumaster.runner.userextensions;

import com.ciplogic.seleniumaster.runner.userextensions.scriptfetchers.GermaniumJSFetcher;
import com.ciplogic.seleniumaster.runner.userextensions.scriptfetchers.SeleniumJSFetcher;
import com.ciplogic.velocity.formatter.CiplogicVelocityClasspathResourceLoader;
import com.ciplogic.velocity.formatter.VelocityTemplateRenderer;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Map;

import static com.ciplogic.util.collections.MapMaker.*;

public interface ScriptLoader {
    Script load(String scriptName);

    Script loadFromTemplate(String scriptName);

    /**
     * Load a script from a velocity template. This will define the seleniumJS, and the germaniumJS
     * as script fetchers (from Selenium and Germanium respectively).
     * @param scriptName
     * @param velocityArguments
     * @return
     */
    Script loadFromTemplate(String scriptName, Map<String, Object> velocityArguments);
}
