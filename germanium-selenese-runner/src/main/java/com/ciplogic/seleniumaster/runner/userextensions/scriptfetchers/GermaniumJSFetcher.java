package com.ciplogic.seleniumaster.runner.userextensions.scriptfetchers;

import com.ciplogic.seleniumaster.runner.userextensions.ScriptLoader;
import com.google.inject.Inject;

/**
 * Germanium JS Fetcher fetches scripts that are also templates as well.
 */
public class GermaniumJSFetcher {
    private ScriptLoader scriptLoader;

    @Inject
    public GermaniumJSFetcher(ScriptLoader scriptLoader) {
        this.scriptLoader = scriptLoader;
    }

    public String load(String name) {
        return scriptLoader.loadFromTemplate(name).getCode();
    }
}
