package com.ciplogic.seleniumaster.runner.userextensions.scriptfetchers;

import static com.ciplogic.util.io.ClassStream.getResourceAsString;

/**
 * Loads scripts directly from inside selenium. Sometimes certain scripts (like disabling alerts, etc) are better
 * to be taken from the Selenium implementation itself.
 */
public class SeleniumJSFetcher {
    public String load(String name) {
        return getResourceAsString("/org/openqa/selenium/internal/seleniumemulation/selenium_atoms/" + name + ".js");
    }
}
