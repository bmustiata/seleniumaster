package com.ciplogic.seleniumaster.runner.vo;

import com.ciplogic.seleniumaster.runner.io.StreamResolver;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.ciplogic.seleniumaster.xml.XmlUtil;
import com.ciplogic.util.collections.Function;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ciplogic.seleniumaster.xml.XmlUtil.xpathEvalList;
import static com.ciplogic.seleniumaster.xml.XmlUtil.xpathValue;
import static com.ciplogic.util.collections.Algorithms.collect;
import static org.apache.commons.lang.StringUtils.join;

public class SeleniumTestReader {
    private static Logger log = Logger.getLogger(SeleniumTestReader.class);

    private StreamResolver streamResolver;

    private Provider<SeleniumTest> seleniumTestProvider;

    private Pattern TEST_SUITE_NAME_BREAKER = Pattern.compile("^(.*[\\\\/])?(.*?)(\\.[\\d\\w]*?)?$");

    @Inject
    public SeleniumTestReader(StreamResolver streamResolver,
                              Provider<SeleniumTest> seleniumTestProvider) {
        this.streamResolver = streamResolver;
        this.seleniumTestProvider = seleniumTestProvider;
    }

    public SeleniumTest readTest(String name) {
        InputStream inputStream = streamResolver.getInputStream(name);
        Document xmlInput = readXmlFromStream(inputStream);

        SeleniumTest result = seleniumTestProvider.get();

        result.setName(getSeleniumTitle(xmlInput));
        result.setBaseSite(getSeleniumBaseSite(xmlInput));
        result.setCommandList(getSeleniumCommands(xmlInput));

        return result;
    }

    private Document readXmlFromStream(InputStream stream) {
        return XmlUtil.getDocumentFromStream(stream);
    }

    private List<SeleniumCommand> getSeleniumCommands(Document xmlInput) {
        List<Node> items = xpathEvalList(xmlInput, "//tbody/tr");
        return collect(items, new Function<SeleniumCommand, Node>() {
            @Override
            public SeleniumCommand call(Node item) {
                SeleniumCommand seleniumCommand = new SeleniumCommand();

                seleniumCommand.setAction( xpathValue(item, "./td[1]/text()") );
                seleniumCommand.setTarget(xpathValue(item, "./td[2]/text()"));
                seleniumCommand.setValue(xpathValue(item, "./td[3]/text()"));

                return seleniumCommand;
            }
        });
    }

    private String getSeleniumTitle(Document document) {
        return xpathValue(document, "/html/head/title/text()");
    }

    private String getSeleniumBaseSite(Document document) {
        return xpathValue(document, "/html/head/link[@rel='selenium.base']/@href");
    }

    public SeleniumTestSuite readTestSuite(String name) {
        InputStream inputStream = streamResolver.getInputStream(name);
        Document xmlInput = readXmlFromStream(inputStream);

        SeleniumTestSuite seleniumTestSuite = new SeleniumTestSuite();

        seleniumTestSuite.setName( getFilenameWithoutExtension(name) );
        seleniumTestSuite.setTestList(getSeleniumTests(basePath(name), xmlInput));

        return seleniumTestSuite;
    }

    private String basePath(String name) {
        if ("/".equals(name)) {
            return "/";
        } else if (name.contains("/")) {
            String[] splittedName = name.split("/");
            return join(splittedName, "/", 0, splittedName.length - 1) + "/";
        } else {
            return "";
        }
    }

    private List<SeleniumTest> getSeleniumTests(final String basePath, Document document) {
        return collect(xpathEvalList(document, "//tbody/tr/td/a"),
            new Function<SeleniumTest, Node>() {
                @Override
                public SeleniumTest call(Node item) {
                    return readTest(basePath + xpathValue(item, "./@href"));
                }
            });
    }

    private String getFilenameWithoutExtension(String name) {
        Matcher matcher = TEST_SUITE_NAME_BREAKER.matcher(name);

        if (!matcher.matches()) {
            throw new IllegalArgumentException("Name is not a valid file name: `" + name +"`.");
        }

        return matcher.group(2);
    }

}
