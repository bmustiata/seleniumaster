package com.ciplogic.seleniumaster.runner.vo.client;

public class SeleniumCommand {
    private String action;
    private String target;
    private String value;

    private SeleniumCommandResult commandResult;

    public SeleniumCommand() {
    }

    public SeleniumCommand(String action, String target, String value) {
        this.action = action;
        this.target = target;
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SeleniumCommandResult getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(SeleniumCommandResult commandResult) {
        this.commandResult = commandResult;
    }

    @Override
    public String toString() {
        return "SeleniumCommand{" +
                "'" + action + '\'' +
                ", '" + target + '\'' +
                ", '" + value + '\'' +
                '}';
    }
}
