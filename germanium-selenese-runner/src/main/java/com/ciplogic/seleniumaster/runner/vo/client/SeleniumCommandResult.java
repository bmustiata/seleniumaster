package com.ciplogic.seleniumaster.runner.vo.client;

public class SeleniumCommandResult {
    public enum Status {
        SUCCESS,
        ERROR,  // an error means the command failed, but the script can continue.
        FAILED  // a failed status indicates that the script should stop.
    }

    private Status status = Status.SUCCESS;

    private Long startTime;
    private Long endTime;

    // these are only filled if the status is ERROR or FAILED
    private String errorName;
    private String errorDescription;
    private String errorStackTrace;

    public SeleniumCommandResult () {
    }

    public SeleniumCommandResult(Long startTime) {
        this.startTime = startTime;
    }

    public SeleniumCommandResult(Status status) {
        this.status = status;
    }

    public SeleniumCommandResult(Status status, String errorName) {
        this.status = status;
        this.errorName = errorName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getErrorName() {
        return errorName;
    }

    public void setErrorName(String errorName) {
        this.errorName = errorName;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorStackTrace() {
        return errorStackTrace;
    }

    public void setErrorStackTrace(String errorStackTrace) {
        this.errorStackTrace = errorStackTrace;
    }
}
