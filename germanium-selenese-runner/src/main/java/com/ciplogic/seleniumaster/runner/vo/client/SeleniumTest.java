package com.ciplogic.seleniumaster.runner.vo.client;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

public class SeleniumTest {
    private String name;
    private String description;
    private String baseSite;

    private List<SeleniumCommand> commandList;

    @JsonIgnore
    public SeleniumCommandResult getCommandResult() {
        SeleniumCommandResult result = null;

        for (SeleniumCommand command : commandList) {
            if (command.getCommandResult().getStatus() == SeleniumCommandResult.Status.FAILED) {
                return command.getCommandResult();
            }
            if (command.getCommandResult().getStatus() == SeleniumCommandResult.Status.ERROR) {
                result = command.getCommandResult();
            }
        }

        if (result != null) {
            return result;
        }

        return commandList.get(commandList.size() - 1).getCommandResult();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBaseSite() {
        return baseSite;
    }

    public void setBaseSite(String baseSite) {
        this.baseSite = baseSite;
    }

    public List<SeleniumCommand> getCommandList() {
        return commandList;
    }

    public void setCommandList(List<SeleniumCommand> commandList) {
        this.commandList = commandList;
    }
}
