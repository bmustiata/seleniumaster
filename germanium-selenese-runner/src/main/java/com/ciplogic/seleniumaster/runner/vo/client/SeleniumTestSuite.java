package com.ciplogic.seleniumaster.runner.vo.client;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.ERROR;
import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.FAILED;

public class SeleniumTestSuite {
    private String name;
    private List<SeleniumTest> testList = new ArrayList<SeleniumTest>();

    @JsonIgnore
    public String getBaseSite() {
        return testList.size() > 0 ?
                testList.get(0).getBaseSite() : null;
    }

    @JsonIgnore
    public SeleniumCommandResult getCommandResult() {
        SeleniumCommandResult result = null;

        for (SeleniumTest test : testList) {
            if (test.getCommandResult().getStatus() == FAILED) {
                return test.getCommandResult();
            }
            if (test.getCommandResult().getStatus() == ERROR) {
                result = test.getCommandResult();
            }
        }

        if (result != null) {
            return result;
        }

        if (testList.isEmpty()) {
            return new SeleniumCommandResult(FAILED, "(Germanium) Test list is empty.");
        }

        return testList.get(testList.size() - 1).getCommandResult();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SeleniumTest> getTestList() {
        return testList;
    }

    public void setTestList(List<SeleniumTest> testList) {
        this.testList = testList;
    }
}
