package com.ciplogic.seleniumaster.selenese;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.ciplogic.seleniumaster.selenese.commands.factories.*;
import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;
import com.ciplogic.seleniumaster.selenese.conditions.factories.CheckNotConditionFactory;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.log4j.Logger;

import static org.apache.commons.lang.StringUtils.capitalize;

public abstract class EnhancerBase {
    public static final int WAIT_FOR_STEPS = 400;
    public static final int WAIT_FOR_STEP_DELAY = 100;

    public static final String COMMAND_PREFIX_GET = "get";
    public static final String COMMAND_PREFIX_STORE = "store";
    public static final String COMMAND_PREFIX_ASSERT = "assert";
    public static final String COMMAND_PREFIX_ASSERT_NOT = "assertNot";
    public static final String COMMAND_PREFIX_VERIFY = "verify";
    public static final String COMMAND_PREFIX_VERIFY_NOT = "verifyNot";
    public static final String COMMAND_PREFIX_WAIT_FOR = "waitFor";
    public static final String COMMAND_PREFIX_WAIT_FOR_NOT = "waitForNot";
    public static final String COMMAND_PREFIX_DO = "do";
    public static final String COMMAND_PREFIX_IS = "is";

    public static final String COMMAND_POSTFIX_NOT = "Not";
    public static final String COMMAND_POSTFIX_AND_WAIT = "AndWait";

    private final static Logger log = Logger.getLogger(EnhancerBase.class);

    protected SeleneseExecutor seleneseExecutor;

    @Inject
    private Provider<WaitForCheckingConditionSeleneseCommandFactory> waitForCheckingConditionSeleneseCommandFactoryProvider;
    @Inject
    private Provider<CheckNotConditionFactory> checkNotConditionFactoryProvider;
    @Inject
    private Provider<EvaluateCheckConditionSeleneseCommandFactory> evaluateCheckConditionSeleneseCommandFactoryProvider;
    @Inject
    private Provider<StoreVariableActionCommandFactory> storeVariableActionCommandFactoryProvider;
    @Inject
    private Provider<EmptySeleneseCommandFactory> emptySeleneseCommandFactoryProvider;
    @Inject
    private Provider<ExistingAndWaitSeleneseCommandFactory> existingAndWaitSeleneseCommandFactoryProvider;

    @Inject
    public EnhancerBase(SeleneseExecutor seleneseExecutor) {
        this.seleneseExecutor = seleneseExecutor;
    }

    protected void generateCommandsForProperty(String propertyName, SeleneseCommandBase commandImplementation, CheckConditionBase checkingCondition) {
        String capitalizedName = capitalize(propertyName);

        putCommand(COMMAND_PREFIX_STORE + capitalizedName, storeResult(commandImplementation));
        putCommand(COMMAND_PREFIX_ASSERT + capitalizedName, evaluateCheckingCondition(checkingCondition, SeleniumCommandResult.Status.FAILED));
        putCommand(getNotName(COMMAND_PREFIX_ASSERT, capitalizedName), evaluateNotCheckingCondition(checkingCondition, SeleniumCommandResult.Status.FAILED));
        putCommand(COMMAND_PREFIX_VERIFY + capitalizedName, evaluateCheckingCondition(checkingCondition, SeleniumCommandResult.Status.ERROR));
        putCommand(getNotName(COMMAND_PREFIX_VERIFY, capitalizedName), evaluateNotCheckingCondition(checkingCondition, SeleniumCommandResult.Status.ERROR));
        putCommand(COMMAND_PREFIX_WAIT_FOR + capitalizedName, waitForCheckingCondition(checkingCondition));
        putCommand(getNotName(COMMAND_PREFIX_WAIT_FOR, capitalizedName), waitForNotCheckingCondition(checkingCondition));
    }

    private String getNotName(String assertName, String methodName) {
        if (methodName.endsWith("Present")) {
            return assertName + methodName.substring(0, methodName.length() - "Present".length()) + "NotPresent";
        }
        return assertName + COMMAND_POSTFIX_NOT + methodName;
    }

    private SeleneseCommandBase waitForCheckingCondition(final CheckConditionBase checkingCondition) {
        return waitForCheckingConditionSeleneseCommandFactoryProvider.get().create(checkingCondition);
    }

    private SeleneseCommandBase waitForNotCheckingCondition(final CheckConditionBase checkingCondition) {
        CheckConditionBase notCheckingCondition = checkNotConditionFactoryProvider.get().create(checkingCondition);
        return waitForCheckingCondition(notCheckingCondition);
    }

    private SeleneseCommandBase evaluateCheckingCondition(final CheckConditionBase checkingCondition, final SeleniumCommandResult.Status statusOnError) {
        return evaluateCheckConditionSeleneseCommandFactoryProvider.get().create(checkingCondition, statusOnError);
    }

    private SeleneseCommandBase storeResult(final SeleneseCommandBase checkConditionBase) {
        return storeVariableActionCommandFactoryProvider.get().create(checkConditionBase);
    }

    private SeleneseCommandBase evaluateNotCheckingCondition(final CheckConditionBase checkingCondition, final SeleniumCommandResult.Status statusOnError) {
        CheckConditionBase notCheckingCondition = checkNotConditionFactoryProvider.get().create(checkingCondition);
        return evaluateCheckingCondition(notCheckingCondition, statusOnError);
    }

    private SeleneseCommandBase emptyClosure() {
        return emptySeleneseCommandFactoryProvider.get().create();
    }

    void putCommand(String name, SeleneseCommandBase implementation) {
        log.debug("Adding command: `" + name + "` to the selenese runner.");
        seleneseExecutor.getCommandMap().put(name, implementation);
    }

    SeleneseCommandBase getCommand(String name) {
        return seleneseExecutor.getCommandMap().get(name);
    }

    protected void createSingleAndWaitMethod(final String methodName) {
        SeleneseCommandBase actionAndWait = existingAndWaitSeleneseCommandFactoryProvider.get().create(methodName);
        putCommand(methodName + SeleneseRunnerEnhancer.COMMAND_POSTFIX_AND_WAIT, actionAndWait);
    }
}
