package com.ciplogic.seleniumaster.selenese;

import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;

import java.util.Map;

public interface SeleneseExecutor {
    Map<String, SeleneseCommandBase> getCommandMap();

    SeleneseCommandBase command(String methodName);

    Object execute(String command, String target, String value);

    void storeVariable(String variableName, Object variableValue);

    Map<String,Object> getStoredVars();
}
