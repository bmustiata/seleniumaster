package com.ciplogic.seleniumaster.selenese;

import com.ciplogic.seleniumaster.germanium.DefaultGermanium;
import com.ciplogic.seleniumaster.javascript.JavascriptRunner;
import com.ciplogic.seleniumaster.selenese.commands.*;
import com.ciplogic.seleniumaster.selenese.commands.factories.*;
import com.ciplogic.seleniumaster.selenese.conditions.*;
import com.ciplogic.seleniumaster.selenese.conditions.factories.*;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.thoughtworks.selenium.Selenium;

import static com.google.inject.assistedinject.FactoryProvider.newFactory;

public class SeleneseModule extends AbstractModule {
    @Override
    protected void configure() {
        // checking properties binding.
        bind(CheckProperty0ArrayFactory.class)
                .toProvider(newFactory(CheckProperty0ArrayFactory.class, CheckProperty0Array.class))
                .in(Singleton.class);

        bind(CheckProperty1ArrayFactory.class)
                .toProvider(newFactory(CheckProperty1ArrayFactory.class, CheckProperty1Array.class))
                .in(Singleton.class);

        bind(CheckProperty0Factory.class)
                .toProvider(newFactory(CheckProperty0Factory.class, CheckProperty0.class))
                .in(Singleton.class);

        bind(CheckProperty1Factory.class)
                .toProvider(newFactory(CheckProperty1Factory.class, CheckProperty1.class))
                .in(Singleton.class);

        bind(CheckBooleanConditionFactory.class)
                .toProvider(newFactory(CheckBooleanConditionFactory.class, CheckBooleanCondition.class))
                .in(Singleton.class);

        bind(CheckNotConditionFactory.class)
                .toProvider(newFactory(CheckNotConditionFactory.class, NotCondition.class))
                .in(Singleton.class);

        bind(UserExtensionCheckConditionFactory.class)
                .toProvider(newFactory(UserExtensionCheckConditionFactory.class, UserExtensionCheckCondition.class))
                .in(Singleton.class);

        // selenese commands bindings.
        bind(EmptySeleneseCommandFactory.class)
                .toProvider(newFactory(EmptySeleneseCommandFactory.class, EmptySeleneseCommand.class))
                .in(Singleton.class);

        bind(EvaluateCheckConditionSeleneseCommandFactory.class)
                .toProvider(newFactory(EvaluateCheckConditionSeleneseCommandFactory.class, EvaluateCheckConditionSeleneseCommand.class))
                .in(Singleton.class);

        bind(ExistingAndWaitSeleneseCommandFactory.class)
                .toProvider(newFactory(ExistingAndWaitSeleneseCommandFactory.class, ExistingAndWaitSeleneseCommand.class))
                .in(Singleton.class);

        bind(ExistingSeleneseCommandFactory.class)
                .toProvider(newFactory(ExistingSeleneseCommandFactory.class, ExistingSeleneseCommand.class))
                .in(Singleton.class);

        bind(PauseSeleneseCommandFactory.class)
                .toProvider(newFactory(PauseSeleneseCommandFactory.class, PauseSeleneseCommand.class))
                .in(Singleton.class);

        bind(StoreVariableActionCommandFactory.class)
                .toProvider(newFactory(StoreVariableActionCommandFactory.class, StoreVariableActionCommand.class))
                .in(Singleton.class);

        bind(StoreSeleneseCommandFactory.class)
                .toProvider(newFactory(StoreSeleneseCommandFactory.class, StoreSeleneseCommand.class))
                .in(Singleton.class);

        bind(EvalSeleneseCommandFactory.class)
                .toProvider(newFactory(EvalSeleneseCommandFactory.class, EvalSeleneseCommand.class))
                .in(Singleton.class);

        bind(WaitForCheckingConditionSeleneseCommandFactory.class)
                .toProvider(newFactory(WaitForCheckingConditionSeleneseCommandFactory.class, WaitForCheckingConditionSeleneseCommand.class))
                .in(Singleton.class);

        bind(Selenium.class)
                .to(DefaultGermanium.class)
                .in(Singleton.class);

        bind(JavascriptRunner.class)
                .to(DefaultGermanium.class)
                .in(Singleton.class);

        bind(UserExtensionActionFactory.class)
                .toProvider(newFactory(UserExtensionActionFactory.class, UserExtensionAction.class))
                .in(Singleton.class);

        bind(UserExtensionPropertyFactory.class)
                .toProvider(newFactory(UserExtensionPropertyFactory.class, UserExtensionProperty.class))
                .in(Singleton.class);
    }
}
