package com.ciplogic.seleniumaster.selenese;

import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.ciplogic.seleniumaster.selenese.commands.factories.EvalSeleneseCommandFactory;
import com.ciplogic.seleniumaster.selenese.commands.factories.ExistingSeleneseCommandFactory;
import com.ciplogic.seleniumaster.selenese.commands.factories.PauseSeleneseCommandFactory;
import com.ciplogic.seleniumaster.selenese.commands.factories.StoreSeleneseCommandFactory;
import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;
import com.ciplogic.seleniumaster.selenese.conditions.factories.*;
import com.google.inject.Inject;
import com.thoughtworks.selenium.Selenium;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

public class SeleneseRunnerEnhancer extends EnhancerBase {
    private static Logger log = Logger.getLogger(SeleneseRunnerEnhancer.class);
    private ExistingSeleneseCommandFactory existingSeleneseCommandFactory;

    @Inject
    private CheckProperty0ArrayFactory checkProperty0ArrayFactory;
    @Inject
    private CheckProperty1ArrayFactory checkProperty1ArrayFactory;
    @Inject
    private CheckProperty0Factory checkProperty0Factory;
    @Inject
    private CheckProperty1Factory checkProperty1Factory;
    @Inject
    private CheckBooleanConditionFactory checkBooleanConditionFactory;
    @Inject
    private PauseSeleneseCommandFactory pauseSeleneseCommandFactory;
    @Inject
    private StoreSeleneseCommandFactory storeSeleneseCommandFactory;
    @Inject
    private EvalSeleneseCommandFactory evalSeleneseCommandFactory;

    @Inject
    public SeleneseRunnerEnhancer(SeleneseExecutor seleneseExecutor,
                                  ExistingSeleneseCommandFactory existingSeleneseCommandFactory) {
        // needed for creating a child injector by loading the SeleneseModule into it.
        super(seleneseExecutor);
        this.existingSeleneseCommandFactory = existingSeleneseCommandFactory;
    }

    @PostConstruct
    public void initialize() {
        this.enhanceCommandMap();
    }

    public void enhanceCommandMap() {
        createMethodsFromSeleniumClass();

        createGetMethodsFromSeleniumClass();
        createIsMethodsFromSeleniumClass();
        createAndWaitForAllVoidSeleniumMethods();

        createPauseMethod();
        createStoreMethod();
        createEvalMethod();
    }

    private void createMethodsFromSeleniumClass() {
        for (Method method : Selenium.class.getMethods()) {
            SeleneseCommandBase seleneseCommand = existingSeleneseCommandFactory.create(method.getName());
            putCommand(method.getName(), seleneseCommand);
        }
    }

    int parameterCount(Class clazz, String methodName) {
        for (Method method : clazz.getMethods()) {
            if (method.getName().equals(methodName)) {
                return method.getParameterTypes().length;
            }
        }

        throw new IllegalArgumentException(format("Method %s not found on class %s.", methodName, clazz.getCanonicalName()));
    }

    boolean isResultArray(Class clazz, String methodName) {
        for (Method method : clazz.getMethods()) {
            if (method.getName().equals(methodName)) {
                return method.getReturnType().isArray();
            }
        }

        throw new IllegalArgumentException(format("Method %s not found on class %s.", methodName, clazz.getCanonicalName()));
    }

    private String getNotName(String assertName, String methodName) {
        if (methodName.endsWith("Present")) {
            return assertName + methodName.substring(0, methodName.length() - "Present".length()) + "NotPresent";
        }
        return assertName + COMMAND_POSTFIX_NOT + methodName;
    }
    
    private void createGetMethodsFromSeleniumClass() {
        List<String> propertiesList = findMethodsByPrefix(COMMAND_PREFIX_GET);

        for (String propertyName : propertiesList) {
            CheckConditionBase checkingCondition;

            /*
             * Depending if the method returns an array or not, and on the number of parameters (1 or 2), there are
             * different ways to call the method verifications.
             */
            if (isResultArray(Selenium.class, COMMAND_PREFIX_GET + propertyName)) {
                if (parameterCount(Selenium.class, COMMAND_PREFIX_GET + propertyName) == 0) {
                    checkingCondition = checkProperty0ArrayFactory.create(propertyName);
                } else {
                    checkingCondition = checkProperty1ArrayFactory.create(propertyName);
                }
            } else {
                if (parameterCount(Selenium.class, COMMAND_PREFIX_GET + propertyName) == 0) {
                    checkingCondition = checkProperty0Factory.create(propertyName);
                } else {
                    checkingCondition = checkProperty1Factory.create(propertyName);
                }
            }

            // Build all the methods from the condition.
            generateCommandsForProperty(propertyName,
                    getCommand(COMMAND_PREFIX_GET + propertyName),
                    checkingCondition);
        }
    }

    private List<String> findMethodsByPrefix(String prefix) {
        List<String> propertiesList = new ArrayList<String>();
        for (Method method : Selenium.class.getMethods()) {
            if (method.getName().startsWith(prefix)) {
                propertiesList.add(method.getName().substring(prefix.length()));
            }
        }
        return propertiesList;
    }
    
    private List<String> findVoidMethods() {
        List<String> propertiesList = new ArrayList<String>();
        for (Method method : Selenium.class.getMethods()) {
            if (method.getReturnType() == void.class) {
                propertiesList.add(method.getName());
            }
        }
        return propertiesList;
    }


    private void createIsMethodsFromSeleniumClass() {
        /**
         * Depending on the number of arguments (none, one or two), there are different ways to call the
         * underlying method.
         */
        List<String> propertiesList = findMethodsByPrefix(COMMAND_PREFIX_IS);

        CheckConditionBase checkingCondition = null;

        for (String propertyName : propertiesList) {
            checkingCondition = checkBooleanConditionFactory.create(propertyName);
            generateCommandsForProperty(
                    propertyName,
                    getCommand(COMMAND_PREFIX_IS + propertyName),
                    checkingCondition);
        }
    }

    private void createAndWaitForAllVoidSeleniumMethods() {
        for (final String methodName : findVoidMethods()) {
            createSingleAndWaitMethod(methodName);
        }
    }

    private void createPauseMethod() {
        SeleneseCommandBase pauseCommand = pauseSeleneseCommandFactory.create();
        putCommand("pause", pauseCommand);
    }

    private void createStoreMethod() {
        SeleneseCommandBase storeCommand = storeSeleneseCommandFactory.create();
        putCommand("store", storeCommand);
    }

    private void createEvalMethod() {
        SeleneseCommandBase storeCommand = evalSeleneseCommandFactory.create();
        putCommand("eval", storeCommand);
    }
}
