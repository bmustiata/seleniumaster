package com.ciplogic.seleniumaster.selenese;

import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.germanium.events.UserExtensionsLoadListener;
import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.ciplogic.seleniumaster.selenese.commands.factories.UserExtensionActionFactory;
import com.ciplogic.seleniumaster.selenese.commands.factories.UserExtensionPropertyFactory;
import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;
import com.ciplogic.seleniumaster.selenese.conditions.factories.UserExtensionCheckConditionFactory;
import com.ciplogic.util.lang.StringUtils;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

import static com.ciplogic.util.io.ClassStream.getResourceAsString;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.uncapitalize;

public class UserExtensionsEnhancer extends EnhancerBase implements UserExtensionsLoadListener {
    public final static String SELENIUM_NOT_LOADED = "SeleniumNotLoaded";

    private final static Logger log = Logger.getLogger(UserExtensionsEnhancer.class);

    private Germanium germanium;
    private boolean userExtensionsReaded;

    @Inject
    private UserExtensionCheckConditionFactory userExtensionCheckConditionFactory;
    @Inject
    private UserExtensionPropertyFactory userExtensionPropertyFactory;
    @Inject
    private UserExtensionActionFactory userExtensionActionFactory;

    @Inject
    public UserExtensionsEnhancer(SeleneseExecutor seleneseExecutor, Germanium germanium) {
        super(seleneseExecutor);
        this.germanium = germanium;

        germanium.addOnUserExtensionsLoadListener(this);
    }

    public void onUserExtensionsLoad() {
        // we need to create the commands only once, since even if the browser reloads the pages, the commands are still there
        if (!userExtensionsReaded) {
            log.debug("Creating commands for the user extensions.");

            createUserExtensionsCommands();

            userExtensionsReaded = true;
        }
    }

    private void createUserExtensionsCommands() {
        String[] propertyList = findPropertiesFromJavascript();
        generateCommandsForProperties(propertyList);
    }

    private String[] findPropertiesFromJavascript() {
        String scriptCode = getResourceAsString(UserExtensionsEnhancer.class, "find-all-selenium-properties.js");
        String allProperties = germanium.callScript(scriptCode).toString();
        String[] propertyList = allProperties.split(",");

        log.debug("Found these selenium.* user-extensions property list: " + allProperties);

        return propertyList;
    }

    private void generateCommandsForProperties(String[] propertyList) {
        for (String seleniumJavascriptProperty : propertyList) {
            if (isAGetterMethod(seleniumJavascriptProperty)) {
                generateGetterDerivedMethods(seleniumJavascriptProperty);
            } else if (isAnActionMethod(seleniumJavascriptProperty)) {
                generateActionDerivedMethods(seleniumJavascriptProperty);
            }
        }
    }

    private void generateGetterDerivedMethods(String javascriptSeleniumProperty) {
        String methodPrefix = getMethodPrefix(javascriptSeleniumProperty);
        String propertyName = getPropertyName(javascriptSeleniumProperty);

        CheckConditionBase checkConditionBase = userExtensionCheckConditionFactory.create(
            methodPrefix, propertyName
        );
        SeleneseCommandBase commandImplementation = userExtensionPropertyFactory.create(
            methodPrefix, propertyName
        );

        putCommand(javascriptSeleniumProperty, commandImplementation);
        generateCommandsForProperty(propertyName, commandImplementation, checkConditionBase);
    }

    private void generateActionDerivedMethods(String javascriptSeleniumProperty) {
        String propertyName = getPropertyName(javascriptSeleniumProperty);

        SeleneseCommandBase commandImplementation = userExtensionActionFactory.create(
                propertyName
        );

        putCommand(propertyName, commandImplementation);
        createSingleAndWaitMethod(propertyName);
    }

    private String getPropertyName(String name) {
        for (String propertyName : asList(COMMAND_PREFIX_GET, COMMAND_PREFIX_IS, COMMAND_PREFIX_DO)) {
            if (getPropertyNameForPrefix(name, propertyName) != null) {
                return getPropertyNameForPrefix(name, propertyName);
            }
        }

        return null;
    }

    private String getMethodPrefix(String name) {
        for (String prefix : asList(COMMAND_PREFIX_GET, COMMAND_PREFIX_IS, COMMAND_PREFIX_DO)) {
            if (name.startsWith(prefix)) {
                return prefix;
            }
        }

        return null;
    }

    private String getPropertyNameForPrefix(String name, String prefix) {
        if (name.startsWith(prefix)) {
            return uncapitalize( name.substring(prefix.length()) );
        }
        return null;
    }

    private boolean isAGetterMethod(String name) {
        return StringUtils.value(name).startsWithAny(
                COMMAND_PREFIX_IS,
                COMMAND_PREFIX_GET);
    }

    private boolean isAnActionMethod(String name) {
        return name.startsWith(COMMAND_PREFIX_DO);
    }
}
