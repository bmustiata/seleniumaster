package com.ciplogic.seleniumaster.selenese.commands;

import org.apache.log4j.Logger;

public class EmptySeleneseCommand extends SeleneseCommandBase {
    private static final Logger log = Logger.getLogger(EmptySeleneseCommand.class);

    @Override
    public Object execute(String target, String value) {
        log.warn("Called an empty closure");
        return null;
    }
}
