package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.javascript.JavascriptRunner;
import com.ciplogic.seleniumaster.runner.userextensions.Script;
import com.ciplogic.seleniumaster.runner.userextensions.ScriptLoader;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.google.inject.Inject;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.Map;

import static com.ciplogic.util.collections.MapMaker.entry;
import static com.ciplogic.util.collections.MapMaker.map;

public class EvalSeleneseCommand extends SeleneseCommandBase {
    private SeleneseExecutor seleneseExecutor;
    private JavascriptRunner javascriptExecutor;
    private ScriptLoader scriptLoader;

    @Inject
    public EvalSeleneseCommand(SeleneseExecutor seleneseExecutor,
                               JavascriptRunner javascriptExecutor,
                               ScriptLoader scriptLoader) {
        this.seleneseExecutor = seleneseExecutor;
        this.javascriptExecutor = javascriptExecutor;
        this.scriptLoader = scriptLoader;
    }

    @Override
    public Object execute(String script, String value) {
        Script actualScript = scriptLoader.loadFromTemplate("germanium-eval.js.vtl", map(
                entry("storedVars", getAsJsonString( seleneseExecutor.getStoredVars() )),
                entry("script", script)
        ));

        return javascriptExecutor.callScript(actualScript.getCode()).toString();
    }

    private String getAsJsonString(Map<String, Object> storedVars) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(storedVars);
        }catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
