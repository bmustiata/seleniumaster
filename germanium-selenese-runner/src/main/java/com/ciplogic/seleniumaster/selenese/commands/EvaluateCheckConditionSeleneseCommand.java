package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import static java.lang.String.format;

public class EvaluateCheckConditionSeleneseCommand extends SeleneseCommandBase {
    private final CheckConditionBase checkingCondition;
    private final SeleniumCommandResult.Status statusOnError;

    @Inject
    public EvaluateCheckConditionSeleneseCommand(@Assisted CheckConditionBase wrappedCondition,
                                                 @Assisted SeleniumCommandResult.Status statusOnError) {
        this.checkingCondition = wrappedCondition;
        this.statusOnError = statusOnError;
    }

    @Override
    public Object execute(String target, String value) {
        if (checkingCondition.test(target, value)) {
            return new SeleniumCommandResult(SeleniumCommandResult.Status.SUCCESS);
        }
        return new SeleniumCommandResult(statusOnError,
                format("Check failed for target: %s, value: %s", target, value));
    }
}
