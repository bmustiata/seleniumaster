package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

/**
 * Calls an existing command and waits the page to load.
 */
public class ExistingAndWaitSeleneseCommand extends SeleneseCommandBase {
    private final String methodName;
    private SeleneseExecutor seleneseExecutor;

    @Inject
    public ExistingAndWaitSeleneseCommand(SeleneseExecutor seleneseExecutor, @Assisted String methodName) {
        this.methodName = methodName;
        this.seleneseExecutor = seleneseExecutor;
    }

    @Override
    public Object execute(String target, String value) {
        Object result = seleneseExecutor.execute(methodName, target, value);
        seleneseExecutor.execute("waitForPageToLoad", "60000", null);

        return result;
    }
}
