package com.ciplogic.seleniumaster.selenese.commands;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.thoughtworks.selenium.Selenium;

import static com.ciplogic.util.reflect.BeanUtils.invokeVarargMethod;
import static org.apache.commons.lang.StringUtils.isEmpty;

public class ExistingSeleneseCommand extends SeleneseCommandBase {
    private Selenium selenium;
    private String actionName;

    @Inject
    public ExistingSeleneseCommand(Selenium selenium, @Assisted String actionName) {
        this.selenium = selenium;
        this.actionName = actionName;
    }

    @Override
    public Object execute(String target, String value) {
        if (!isEmpty(value)) {
            return invokeVarargMethod(selenium, actionName, target, value);
        } else {
            return invokeVarargMethod(selenium, actionName, target);
        }
    }
}
