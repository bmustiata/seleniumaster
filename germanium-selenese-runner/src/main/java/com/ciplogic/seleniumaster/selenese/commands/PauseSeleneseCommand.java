package com.ciplogic.seleniumaster.selenese.commands;

public class PauseSeleneseCommand extends SeleneseCommandBase {
    @Override
    public Object execute(String target, String value)  {
        try {
            Thread.sleep(Long.parseLong(target));
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }
}
