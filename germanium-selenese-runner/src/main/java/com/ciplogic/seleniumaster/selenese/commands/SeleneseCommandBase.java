package com.ciplogic.seleniumaster.selenese.commands;

/**
 * A normal selenese comand.
 */
public abstract class SeleneseCommandBase {
    public abstract Object execute(String target, String value);
}
