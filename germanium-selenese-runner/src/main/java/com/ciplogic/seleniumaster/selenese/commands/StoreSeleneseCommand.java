package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.google.inject.Inject;

public class StoreSeleneseCommand extends SeleneseCommandBase {
    private SeleneseExecutor seleneseExecutor;

    @Inject
    public StoreSeleneseCommand(SeleneseExecutor seleneseExecutor) {
        this.seleneseExecutor = seleneseExecutor;
    }

    @Override
    public Object execute(String target, String value)  {
        seleneseExecutor.storeVariable(value, target);
        return null;
    }
}
