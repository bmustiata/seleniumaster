package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

/**
 * Stores the variable that is the result of the action.
 */
public class StoreVariableActionCommand extends SeleneseCommandBase {
    private SeleneseCommandBase action;
    private SeleneseExecutor seleneseExecutor;

    @Inject
    public StoreVariableActionCommand(SeleneseExecutor seleneseExecutor, @Assisted SeleneseCommandBase action) {
        this.seleneseExecutor = seleneseExecutor;
        this.action = action;
    }

    @Override
    public Object execute(String target, String value) {
        seleneseExecutor.storeVariable(value, action.execute(target, null));
        return new SeleniumCommandResult(SeleniumCommandResult.Status.SUCCESS);
    }
}
