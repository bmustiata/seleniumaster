package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.javascript.JavaScriptExecutionException;
import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import static com.ciplogic.seleniumaster.javascript.util.JavaScriptParser.seleniumCallString;
import static com.ciplogic.seleniumaster.selenese.UserExtensionsEnhancer.SELENIUM_NOT_LOADED;
import static org.apache.commons.lang.StringUtils.capitalize;

public class UserExtensionAction extends SeleneseCommandBase {
    private Germanium germanium;
    private final String jsActionName;

    @Inject
    public UserExtensionAction(Germanium germanium, @Assisted String jsActionName) {
        this.jsActionName = jsActionName;
        this.germanium = germanium;
    }

    @Override
    public Object execute(String target, String value) {
        String scriptCode = "return " + seleniumCallString(SeleneseRunnerEnhancer.COMMAND_PREFIX_DO,
                capitalize(jsActionName), target, value);

        try {
            return germanium.callScript(scriptCode);
        } catch (JavaScriptExecutionException e) {
            if (SELENIUM_NOT_LOADED.equals(e.getName())) {
                germanium.rewireUserExtensionsIfMissing();
                return germanium.callScript(scriptCode);
            }
            throw e;
        }
    }
}
