package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.javascript.JavaScriptExecutionException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import static com.ciplogic.seleniumaster.javascript.util.JavaScriptParser.seleniumCallString;
import static com.ciplogic.seleniumaster.javascript.util.JavaScriptParser.userExtensionTryCatch;
import static com.ciplogic.seleniumaster.selenese.UserExtensionsEnhancer.SELENIUM_NOT_LOADED;

public class UserExtensionProperty extends SeleneseCommandBase {
    private Germanium germanium;
    private final String methodPrefix;
    private final String propertyName;

    @Inject
    public UserExtensionProperty(Germanium germanium,
                                 @Assisted String methodPrefix,
                                 @Assisted("propertyName") String propertyName) {
        this.germanium = germanium;
        this.methodPrefix = methodPrefix;
        this.propertyName = propertyName;
    }

    @Override
    public Object execute(String target, String value) {
        String scriptCode = userExtensionTryCatch("return " + seleniumCallString(methodPrefix, propertyName, target, value));

        try {
            return germanium.callScript(scriptCode);
        } catch (JavaScriptExecutionException e) {
            if (SELENIUM_NOT_LOADED.equals(e.getName())) {
                germanium.rewireUserExtensionsIfMissing();
                return germanium.callScript(scriptCode);
            }
            throw e;
        }
    }

}
