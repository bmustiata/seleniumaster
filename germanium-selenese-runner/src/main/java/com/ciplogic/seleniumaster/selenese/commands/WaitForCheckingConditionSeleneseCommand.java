package com.ciplogic.seleniumaster.selenese.commands;

import com.ciplogic.seleniumaster.runner.exception.SeleniumCommandFailed;
import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;

public class WaitForCheckingConditionSeleneseCommand extends SeleneseCommandBase {
    private static final Logger log = Logger.getLogger(WaitForCheckingConditionSeleneseCommand.class);

    private final CheckConditionBase checkingCondition;

    @Inject
    public WaitForCheckingConditionSeleneseCommand(@Assisted CheckConditionBase checkingCondition) {
        this.checkingCondition = checkingCondition;
    }

    @Override
    public Object execute(String target, String value) {
        log.debug("Wait for condition");
        for (int second = 0;; second++) {
            if (second >= SeleneseRunnerEnhancer.WAIT_FOR_STEPS) {
                fail("timeout");
            }
            try {
                if (checkingCondition.test(target, value)) {
                    break;
                }
                Thread.sleep(SeleneseRunnerEnhancer.WAIT_FOR_STEP_DELAY);
            } catch (Exception e) {
                // We expect it to fail several times.
            }
        }

        return null;
    }

    private void fail(String text) {
        throw new SeleniumCommandFailed(text);
    }
}
