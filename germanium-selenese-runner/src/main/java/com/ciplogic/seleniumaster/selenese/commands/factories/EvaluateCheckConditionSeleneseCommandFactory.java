package com.ciplogic.seleniumaster.selenese.commands.factories;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;

public interface EvaluateCheckConditionSeleneseCommandFactory {
    SeleneseCommandBase create(CheckConditionBase checkingCondition, SeleniumCommandResult.Status statusOnError);
}
