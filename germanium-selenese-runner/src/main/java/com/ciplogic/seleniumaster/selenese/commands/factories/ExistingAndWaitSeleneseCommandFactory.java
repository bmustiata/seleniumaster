package com.ciplogic.seleniumaster.selenese.commands.factories;

import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;

public interface ExistingAndWaitSeleneseCommandFactory {
    SeleneseCommandBase create(String methodName);
}
