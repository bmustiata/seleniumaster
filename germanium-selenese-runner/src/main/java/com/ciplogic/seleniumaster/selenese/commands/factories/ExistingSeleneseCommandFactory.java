package com.ciplogic.seleniumaster.selenese.commands.factories;

import com.ciplogic.seleniumaster.selenese.commands.ExistingSeleneseCommand;

public interface ExistingSeleneseCommandFactory {
    ExistingSeleneseCommand create(String actionName);
}
