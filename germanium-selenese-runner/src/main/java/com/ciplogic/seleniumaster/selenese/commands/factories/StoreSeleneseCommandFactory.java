package com.ciplogic.seleniumaster.selenese.commands.factories;

import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;

public interface StoreSeleneseCommandFactory {
    SeleneseCommandBase create();
}
