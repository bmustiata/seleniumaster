package com.ciplogic.seleniumaster.selenese.commands.factories;

import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.ciplogic.seleniumaster.selenese.metadata.SeleniumPropertyName;

public interface UserExtensionActionFactory {
    SeleneseCommandBase create(String jsActionName);
}
