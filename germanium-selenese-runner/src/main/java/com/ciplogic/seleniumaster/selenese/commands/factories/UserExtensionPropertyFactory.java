package com.ciplogic.seleniumaster.selenese.commands.factories;

import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.google.inject.assistedinject.Assisted;

public interface UserExtensionPropertyFactory {
    SeleneseCommandBase create(String methodPrefix,
                               @Assisted("propertyName") String propertyName);
}
