package com.ciplogic.seleniumaster.selenese.commands.factories;

import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;

public interface WaitForCheckingConditionSeleneseCommandFactory {
    SeleneseCommandBase create(CheckConditionBase checkingCondition);
}
