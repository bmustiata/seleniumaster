package com.ciplogic.seleniumaster.selenese.conditions;

import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;

import static java.lang.String.format;

public class CheckBooleanCondition extends CheckConditionBase {
    private final String property;
    private final static Logger log = Logger.getLogger(CheckBooleanCondition.class);

    @Inject
    public CheckBooleanCondition(@Assisted String property) {
        this.property = property;
    }

    public boolean test(String target, String value) {
        log.debug(format("is%s(%s, %s)", property, target, value));
        Object booleanResult = command(SeleneseRunnerEnhancer.COMMAND_PREFIX_IS + property).execute(target, value);

        return booleanResult instanceof Boolean && (Boolean) booleanResult;
    }
}
