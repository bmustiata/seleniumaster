package com.ciplogic.seleniumaster.selenese.conditions;

import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.ciplogic.seleniumaster.selenese.commands.SeleneseCommandBase;
import com.google.inject.Inject;

/**
 * In selenium a lot of commands are based on evaluating if an expression is true or not. These expressions
 * vary depending on the selenium property they are called against, or if the method reading the property
 * needs locator parameters as well.<br/>
 * Depending if the method has parameters, and the return type (array, string or boolean) this evaluation
 * is implemented differently.<br/>
 * For example array results need to have their values joined with a comma character (,) before testing against
 * the expected result.
 */
public abstract class CheckConditionBase {
    @Inject
    private SeleneseExecutor seleneseExecutor;

    public abstract boolean test(String target, String value);

    protected SeleneseCommandBase command(String commandName) {
        return seleneseExecutor.getCommandMap().get(commandName);
    }
}
