package com.ciplogic.seleniumaster.selenese.conditions;

import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;

public class CheckProperty0 extends CheckConditionBase {
    private final String property;
    private final static Logger log = Logger.getLogger(CheckProperty0.class);

    @Inject
    public CheckProperty0(@Assisted String property) {
        this.property = property;
    }

    public boolean test(String target, String value) {
        log.debug(String.format("%s == get%s", target, property));
        return target.equals(command(SeleneseRunnerEnhancer.COMMAND_PREFIX_GET + property).execute(target, value).toString());
    }
}
