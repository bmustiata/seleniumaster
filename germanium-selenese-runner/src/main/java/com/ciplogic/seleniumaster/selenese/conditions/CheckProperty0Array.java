package com.ciplogic.seleniumaster.selenese.conditions;

import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;

import static org.apache.commons.lang.StringUtils.join;

public class CheckProperty0Array extends CheckConditionBase {
    private final String property;
    private final static Logger log = Logger.getLogger(CheckProperty0Array.class);

    @Inject
    public CheckProperty0Array(@Assisted String property) {
        this.property = property;
    }

    public boolean test(String target, String value) {
        log.debug(String.format("%s == get%s().join", target, property));
        Object[] arrayResult = (Object[]) command(SeleneseRunnerEnhancer.COMMAND_PREFIX_GET + property).execute(target, value);

        return target.equals(join(arrayResult, ","));
    }
}
