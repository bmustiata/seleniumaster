package com.ciplogic.seleniumaster.selenese.conditions;

import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;

import static org.apache.commons.lang.StringUtils.join;

public class CheckProperty1Array extends CheckConditionBase {
    private final String property;
    private final static Logger log = Logger.getLogger(CheckProperty1Array.class);

    @Inject
    public CheckProperty1Array(@Assisted String property) {
        this.property = property;
    }

    public boolean test(String target, String value) {
        log.debug(String.format("%s == get%s(%s).join", value, property, target));
        Object[] arrayResult = (Object[]) command(SeleneseRunnerEnhancer.COMMAND_PREFIX_GET + property).execute(target, value);

        return value.equals(join(arrayResult, ","));
    }
}
