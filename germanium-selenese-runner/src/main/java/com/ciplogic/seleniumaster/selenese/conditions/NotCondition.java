package com.ciplogic.seleniumaster.selenese.conditions;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class NotCondition extends CheckConditionBase {
    private final CheckConditionBase checkingCondition;

    @Inject
    public NotCondition(@Assisted CheckConditionBase checkingCondition) {
        this.checkingCondition = checkingCondition;
    }

    @Override
    public boolean test(String target, String value) {
        return ! checkingCondition.test(target, value);
    }
}
