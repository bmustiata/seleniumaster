package com.ciplogic.seleniumaster.selenese.conditions;

import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.javascript.JavaScriptExecutionException;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;

import static com.ciplogic.seleniumaster.javascript.util.JavaScriptParser.seleniumCallString;
import static com.ciplogic.seleniumaster.javascript.util.JavaScriptParser.userExtensionTryCatch;
import static com.ciplogic.seleniumaster.selenese.UserExtensionsEnhancer.SELENIUM_NOT_LOADED;

public class UserExtensionCheckCondition extends CheckConditionBase {
    private static final Logger log = Logger.getLogger(UserExtensionCheckCondition.class);

    private Germanium germanium;
    private final String methodPrefix;
    private final String propertyName;

    @Inject
    public UserExtensionCheckCondition(Germanium germanium,
                                       @Assisted String methodPrefix,
                                       @Assisted("propertyName") String propertyName) {
        this.germanium = germanium;
        this.methodPrefix = methodPrefix;
        this.propertyName = propertyName;
    }

    @Override
    public boolean test(String target, String value) {
        try {
            return executeCode(target, value);
        } catch (JavaScriptExecutionException e) {
            if (SELENIUM_NOT_LOADED.equals(e.getName())) {
                germanium.rewireUserExtensionsIfMissing();
                return executeCode(target, value);
            }
            throw e;
        }
    }

    private boolean executeCode(String target, String value) {
        String scriptCode = "return !!" + seleniumCallString(methodPrefix, propertyName, target, value);
        scriptCode = userExtensionTryCatch(scriptCode);
        String jsResult = germanium.callScript(
                scriptCode
        ).toString();

        return Boolean.valueOf(jsResult);
    }
}
