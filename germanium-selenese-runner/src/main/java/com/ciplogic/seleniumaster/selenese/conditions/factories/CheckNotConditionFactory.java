package com.ciplogic.seleniumaster.selenese.conditions.factories;

import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;

public interface CheckNotConditionFactory {
    CheckConditionBase create(CheckConditionBase baseCondition);
}
