package com.ciplogic.seleniumaster.selenese.conditions.factories;

import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;

public interface CheckProperty0ArrayFactory {
    CheckConditionBase create(String propertyName);
}
