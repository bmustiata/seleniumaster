package com.ciplogic.seleniumaster.selenese.conditions.factories;

import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;

public interface CheckProperty1ArrayFactory {
    CheckConditionBase create(String propertyName);
}
