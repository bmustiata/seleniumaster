package com.ciplogic.seleniumaster.selenese.conditions.factories;

import com.ciplogic.seleniumaster.selenese.conditions.CheckConditionBase;
import com.google.inject.assistedinject.Assisted;

public interface UserExtensionCheckConditionFactory {
    CheckConditionBase create(String methodPrefix,
                              @Assisted("propertyName") String propertyName);
}
