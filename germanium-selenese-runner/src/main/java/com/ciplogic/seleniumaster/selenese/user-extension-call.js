if (typeof selenium === "undefined") {
    throw {
        name : "SeleniumNotLoaded",
        message : "Selenium is not loaded.",
        description : "The Selenium object is not yet loaded. User extensions can not be called yet."
    }
} else {
    %s
}