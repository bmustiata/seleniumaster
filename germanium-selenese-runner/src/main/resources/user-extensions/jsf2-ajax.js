(function () {

/* // Groovy code to build the eval string.
// FIXME: move this to the test folder, since it should not be bundled in the final application.
@Grab(group="commons-lang", module="commons-lang", version="2.6")
import org.apache.commons.lang.StringEscapeUtils

println StringEscapeUtils.escapeJavaScript("""
(function() {
    if (typeof jsf.ajax.getAjaxCount === "undefined") {
        var ajaxCount = 0;

        jsf.ajax.addOnEvent(function(ajaxEvent) {
            if (ajaxEvent.status === "begin") {
                ajaxCount++;
            }
            if (ajaxEvent.status === "complete") {
                if (ajaxCount > 0) {
                    ajaxCount--;
                }
            }
        });

        jsf.ajax.getAjaxCount = function() {
            return ajaxCount;
        };
    }
})()
""".replaceAll(/\s+/, / /))

// script to monitor the unloading of the page.
@Grab(group="commons-lang", module="commons-lang", version="2.6")
import org.apache.commons.lang.StringEscapeUtils

println StringEscapeUtils.escapeJavaScript("""
(function() {
    var oldOnUnload = document.body.onbeforeunload;
    var pageUnloading = false;

    document.body.onbeforeunload = function() {
        pageUnloading = true;
        if (typeof oldOnUnload !== "undefined") {
            return oldOnUnload.apply(document, arguments);
        }
    };

   document.isPageUnloading = function() {
       return pageUnloading;
   }
})();
""".replaceAll(/\s+/, / /))
*/
    // third time is the charm.
    var DEFAULT_THROTTLE_COUNT = 3;
    var THROTTLE_DECREMENT_TIME = 400; // in milliseconds

    var throttle = DEFAULT_THROTTLE_COUNT;
    var lastThrottleDecrementTime = new Date().getTime();

    // This is for debug purposes only
    var LOG_FIRST_X_MESSAGES = 100000000;
    var messagesLogged = 0;

    var logMessage = function(message) {
        LOG.info(message);
        /*
        if (messagesLogged < LOG_FIRST_X_MESSAGES) {
            LOG.info(message);
            //getWindow().console.log(message);
            messagesLogged++;
        } else {
            logMessage = function() {};
        }
        */
    };

    /**
     * This needs to be predone on each page, since otherwise there is no way to tell when the first request takes place.
     */
    Selenium.prototype.doWireJsfMonitoring = function () {
        if (isJsfLoaded() && ! isTheJsfMonitoringWiredIn()) {
            wireInJsfMonitoring();
        }
    };

    Selenium.prototype.isActionComplete = function() {
        try {
            selenium.doWireJsfMonitoring();

            var pageLoaded = isPageLoaded();
            var ajaxComplete = isAjaxComplete();

            logMessage("ajaxComplete: " + ajaxComplete + " pageLoaded: " + pageLoaded);

            return recomputeThrottling(ajaxComplete && pageLoaded);
        } catch (e) {
            logMessage(e);
            return false;
        }
    };

    /**
     * In case there is no JSF AJAX support, we return that the AJAX is complete
     */
    var isAjaxComplete = function() {
        if (!isTheJsfMonitoringWiredIn()) {
            logMessage("JSF monitoring is not wired in. Call wireJsfMonitoring before" +
                " attempting to verify if the AJAX is complete.");
            return true;
        }

        return getJsfCount() == 0;
    };

    function recomputeThrottling(result) {
        // if the result is not ok, but the throttle is actually smaller, reset it again.
        if ((!result) && (throttle < DEFAULT_THROTTLE_COUNT)) {
            throttle = DEFAULT_THROTTLE_COUNT;
        }

        if (result && (throttle > 0)) {
            decrementThrottle();
            result = false;
        }

        // finished the counting for the current throttling
        if (result && (throttle == 0)) {
            throttle = DEFAULT_THROTTLE_COUNT;
        }

        return result;
    }

    // selenium IDE is calling those is*/get* methods all the time when doing waitFor*.
    var decrementThrottle = function () {
        var currentTime = new Date().getTime();
        if (currentTime - lastThrottleDecrementTime > THROTTLE_DECREMENT_TIME) { // more than throttle decrement time has passed?
            throttle--;
            lastThrottleDecrementTime = currentTime;
        }
    };

    var wireInJsfMonitoring = function() {
        getWindow().eval("(function() { if (typeof jsf.ajax.getAjaxCount === \"undefined\") { var ajaxCount = 0; jsf.ajax.addOnEvent(function(ajaxEvent) { if (ajaxEvent.status === \"begin\") { ajaxCount++; } if (ajaxEvent.status === \"complete\") { if (ajaxCount > 0) { ajaxCount--; } } }); jsf.ajax.getAjaxCount = function() { return ajaxCount; }; } })()");
    };

    var isTheJsfMonitoringWiredIn = function() {
        var userWindow = selenium.browserbot.getUserWindow();

        return !!userWindow &&
            !!userWindow.jsf &&
            !!userWindow.jsf.ajax &&
            !!userWindow.jsf.ajax.getAjaxCount;
    };

    var isPageLoaded = function() {
        var window = getWindow();

        return !!window &&
            !!window.document &&
            !!window.document.readyState &&
            ('complete' == getWindow().document.readyState) &&
            ! isPageUnloading();
    };

    var isPageUnloading = function() {
        if (typeof getWindow().document.isPageUnloading === "undefined") {
            getWindow().eval("(function() { var oldOnUnload = document.body.onbeforeunload; var pageUnloading = false; document.body.onbeforeunload = function() { pageUnloading = true; if (typeof oldOnUnload !== \"undefined\") { return oldOnUnload.apply(document, arguments); } }; document.isPageUnloading = function() { return pageUnloading; } })()");
        }

        return getWindow().document.isPageUnloading();
    };

    var isJsfLoaded = function() {
        return !! getWindow().jsf;
    };

    var getJsfCount = function() {
        return getWindow().jsf.ajax.getAjaxCount();
    };

    var getWindow = function() {
        return selenium.browserbot.getUserWindow();
    }
})();
