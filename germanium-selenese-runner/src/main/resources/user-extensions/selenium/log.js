var noop = function() {};

LOG = {
    log   : noop,
    info  : noop,
    warn  : noop,
    error : noop
};

if (!!window.console) {
    LOG.log  = function() {
        window.console.log.apply(window.console, arguments);
    };
    LOG.info  = function() {
        window.console.info.apply(window.console, arguments);
    };
    LOG.warn  = function() {
        window.console.warn.apply(window.console, arguments);
    };
    LOG.error  = function() {
        window.console.error.apply(window.console, arguments);
    };
}
