/**
 * Returns the text content of the found element.
 * @param locator
 * @param other
 */
Selenium.prototype.doClick = function(locator, other) {
    LOG.info("received " + arguments.length + " arguments.");
    for (var i = 0; i < arguments.length; i++) {
        LOG.info("argument[" + i + "] = " + arguments[i]);
    }

    var element = this.page().findElement(locator);

    // might not work on all browsers.
    element.click();
};
