package com.ciplogic.seleniumaster;

import com.ciplogic.germanium.selenium.BinaryDriverModule;
import com.ciplogic.seleniumaster.profiles.ProfilesModule;
import com.ciplogic.util.guice.PostConstructModule;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;

public abstract class BaseTest extends AbstractModule {
    protected Injector injector;

    @Before
    public void setupTest() {
        this.injector = Guice.createInjector(this,
            new PostConstructModule(),
            new BinaryDriverModule(),
            new ProfilesModule()
        );
    }
}
