package com.ciplogic.seleniumaster.formatter;

import com.ciplogic.seleniumaster.BaseTest;
import com.ciplogic.seleniumaster.runner.io.ClasspathStreamResolver;
import com.ciplogic.seleniumaster.runner.io.StreamResolver;
import com.ciplogic.seleniumaster.runner.io.StreamResolverBasePath;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.SeleniumTestReader;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;

import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.*;

public class OutputResultFormatterTest extends BaseTest {
    @Test
    public void testVelocityStartup() {
        SeleniumTest test = buildTestData();

        new OutputResultFormatter().writeToStream(test, System.out, "text");
    }

    @Test
    public void testVelocityStartup2() throws IOException {
        SeleniumTest test = buildTestData();

        FileOutputStream stream = new FileOutputStream("target/testVelocityStartup2.html");
        new OutputResultFormatter().writeToStream(test, stream, "html");

        stream.close();
    }

    private SeleniumTest buildTestData() {
        SeleniumTest test = injector.getInstance(SeleniumTestReader.class).readTest("simple-browse-google-test.html");

        for (int i = 0; i < 3; i++) {
            test.getCommandList().get(i).setCommandResult(new SeleniumCommandResult(SUCCESS));
        }

        test.getCommandList().get(4).setCommandResult(new SeleniumCommandResult(ERROR));
        test.getCommandList().get(4).getCommandResult().setErrorName("some error");
        test.getCommandList().get(4).getCommandResult().setErrorDescription("some\nreally\nnasty\nstack\ntrace");

        test.getCommandList().get(5).setCommandResult(new SeleniumCommandResult(FAILED));
        test.getCommandList().get(5).getCommandResult().setErrorName("some fatal error");
        test.getCommandList().get(5).getCommandResult().setErrorDescription("some\nreally\nreally\nreally\nreally\nreally\nreally\nnasty\nstack\ntrace");

        return test;
    }

    @Override
    protected void configure() {
        bind(String.class).annotatedWith(StreamResolverBasePath.class).toInstance("/");
        bind(StreamResolver.class).to(ClasspathStreamResolver.class);
    }
}
