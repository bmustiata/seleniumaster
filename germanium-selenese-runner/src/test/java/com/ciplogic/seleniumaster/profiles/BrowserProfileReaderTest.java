package com.ciplogic.seleniumaster.profiles;

import com.ciplogic.seleniumaster.BaseTest;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import org.junit.Test;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.List;

import static org.junit.Assert.*;

public class BrowserProfileReaderTest extends BaseTest {
    @Override
    protected void configure() {
        bind(String.class).annotatedWith(ProfilesJsonName.class).toInstance("profiles-test.json");
    }

    @Test
    public void testReadingProfiles() {
        List<BrowserProfile> result = injector.getInstance(BrowserProfileReader.class).readBrowserProfiles(
                BrowserProfileReaderTest.class.getResourceAsStream("/profiles-test.json")
        ).getProfiles();

        assertEquals(5, result.size());

        checkFirstProfile(result.get(0));
        checkSecondProfile(result.get(1));
        checkThirdProfile(result.get(2));
        checkFourthProfile(result.get(3));
        checkFifthProfile(result.get(4));
    }

    @Test
    public void testReadingProfilesViaManager() {
        BrowserProfileManager browserProfileManager = injector.getInstance(BrowserProfileManager.class);
        assertEquals("ie7-remote", browserProfileManager.getDefaultProfile().getId());
    }

    /*
    <profile>
        <id>firefox</id>
        <browser>firefox</browser>
        <proxy>
            <host>10.1.2.1</host>
            <port>3128</port>
        </proxy>
    </profile>
     */
    private void checkFirstProfile(BrowserProfile profile) {
        assertTrue(profile.isLocalHost());
        assertEquals("firefox", profile.getId());
        assertEquals("firefox", profile.getBrowser());
        assertNotNull(profile.getProxy());
        assertEquals("10.1.2.1", profile.getProxy().getHost());
        assertEquals((Long) 3128L, profile.getProxy().getPort());
    }

    /*
    <profile>
        <id>ie</id>
        <browser>ie</browser>
        <capabilities>
            <capability name="ignoreProtectedModeSettings" value="true"/>
        </capabilities>
    </profile>
     */
    private void checkSecondProfile(BrowserProfile profile) {
        assertTrue(profile.isLocalHost());
        assertEquals("ie", profile.getId());
        assertEquals("ie", profile.getBrowser());
        assertNull(profile.getProxy());
        assertNotNull(profile.getCapabilities());
        assertEquals(1, profile.getCapabilities().size());
        assertEquals(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, profile.getCapabilities().get(0).getName());
        assertEquals("true", profile.getCapabilities().get(0).getValue());
    }

    /*
    <profile>
        <id>chrome</id>
        <browser>chrome</browser>
        <capabilities>
            <binary>c:\foo\moo\chrome.exe</binary>
        </capabilities>
    </profile>
     */
    private void checkThirdProfile(BrowserProfile profile) {
        assertEquals("chrome", profile.getId());
        assertEquals("chrome", profile.getBrowser());
        assertTrue(profile.isLocalHost());
        assertNull(profile.getProxy());
        assertNotNull(profile.getCapabilities());
        assertEquals(1, profile.getCapabilities().size());
        assertEquals("binary", profile.getCapabilities().get(0).getName());
        assertEquals("c:\\foo\\moo\\chrome.exe", profile.getCapabilities().get(0).getValue());
    }

    /*
    <profile>
        <id>ie7-remote</id>
        <browser>ie</browser>
        <remote>192.168.14.103</remote>
    </profile>
     */
    private void checkFourthProfile(BrowserProfile profile) {
        assertEquals("ie7-remote", profile.getId());
        assertEquals("ie", profile.getBrowser());
        assertTrue(profile.isRemoteHost());
        assertEquals("192.168.14.103", profile.getRemoteIp());
        assertEquals((Long) 4444L, profile.getRemotePort());
    }

    /*
    <profile>
        <id>ie8-remote</id>
        <browser>ie</browser>
        <remote>192.168.14.104:1234</remote>
    </profile>
     */
    private void checkFifthProfile(BrowserProfile profile) {
        assertEquals("ie8-remote", profile.getId());
        assertEquals("ie", profile.getBrowser());
        assertTrue(profile.isRemoteHost());
        assertEquals("192.168.14.104", profile.getRemoteIp());
        assertEquals((Long) 1234L, profile.getRemotePort());
    }
}
