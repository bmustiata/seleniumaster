package com.ciplogic.seleniumaster.runner.userextensions;

import com.ciplogic.seleniumaster.BaseTest;
import com.ciplogic.seleniumaster.runner.userextensions.scriptfetchers.SeleniumJSFetcher;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class SeleniumJSFetcherTest extends BaseTest {
    @Override
    protected void configure() {
    }

    @Test
    public void testSeleniumResourceExisting() {
        SeleniumJSFetcher seleniumJSFetcher = injector.getInstance(SeleniumJSFetcher.class);

        assertNotNull("Unable to load the sizzle library.", seleniumJSFetcher.load("sizzle"));
        assertNotNull("Unable to load the findElement method.", seleniumJSFetcher.load("findElement"));
    }
}
