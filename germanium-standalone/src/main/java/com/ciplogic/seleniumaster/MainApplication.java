package com.ciplogic.seleniumaster;

import com.ciplogic.seleniumaster.metadata.ProgramArguments;
import com.ciplogic.seleniumaster.modules.StandaloneGermaniumModule;
import com.ciplogic.util.guice.PostConstructModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.apache.log4j.Logger;

import java.io.IOException;

public class MainApplication {
    private Logger log = Logger.getLogger(MainApplication.class);

    @Inject
    private Injector injector;

    @Inject @ProgramArguments
    private String[] programArguments;

    public static void main(String[] args) throws IOException {
        Injector injector = Guice.createInjector(new StandaloneGermaniumModule(args), new PostConstructModule());
        injector.getInstance(MainApplication.class).run();
    }

    void run() throws IOException {
        if (isDisplayUsage()) {
            injector.getInstance(UsageDisplay.class).displayUsage();
            System.exit(0);
        } else if (isListProfiles()) {
            injector.getInstance(ProfileListDisplay.class).displayList();
            System.exit(0);
        } else {
            injector.getInstance(TestSuiteExecutor.class).executeTest();
        }
    }

    private boolean isDisplayUsage() {
        return isFlag("-h") || isFlag("-help") || isFlag("--help") || programArguments.length == 0;
    }

    private boolean isListProfiles() {
        return isFlag("--list-profiles");
    }

    private boolean isFlag(String name) {
        for (String argument : programArguments) {
            if (name.equals(argument)) {
                return true;
            }
        }
        return false;
    }
}
