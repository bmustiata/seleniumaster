package com.ciplogic.seleniumaster;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import com.google.inject.Inject;

/**
 * Display the list of available profiles in the output stream.
 */
public class ProfileListDisplay {
    private BrowserProfileManager browserProfileManager;

    @Inject
    public ProfileListDisplay(BrowserProfileManager browserProfileManager) {
        this.browserProfileManager = browserProfileManager;
    }

    public void displayList() {
        System.out.println("\nProfile list:\n");
        for (BrowserProfile profile : browserProfileManager.getProfiles()) {
            System.out.println(String.format("%s (%s %s)",
                    profile.getId(),
                    profile.isLocalHost() ? "local" : "remote",
                    profile.getBrowser()
            ));
        }
    }
}
