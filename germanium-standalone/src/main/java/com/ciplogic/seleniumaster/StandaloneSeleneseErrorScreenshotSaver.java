package com.ciplogic.seleniumaster;

import com.ciplogic.seleniumaster.germanium.SeleneseErrorScreenshotSaver;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.metadata.ReportFileName;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.Inject;

public class StandaloneSeleneseErrorScreenshotSaver implements SeleneseErrorScreenshotSaver {
    private Germanium germanium;
    private String reportFileName;

    @Inject
    public StandaloneSeleneseErrorScreenshotSaver(Germanium germanium, @ReportFileName String reportFileName) {
        this.germanium = germanium;
        this.reportFileName = reportFileName;
    }

    @Override
    public void saveScreenshot(SeleniumTestSuite suite, SeleniumTest test, SeleniumCommand command) {
        germanium.captureScreenshot(reportFileName + ".png");
    }
}
