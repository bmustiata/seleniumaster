package com.ciplogic.seleniumaster;

import com.ciplogic.seleniumaster.formatter.OutputResultFormatter;
import com.ciplogic.seleniumaster.metadata.KeepBrowserOpenOnError;
import com.ciplogic.seleniumaster.metadata.ReportFileName;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.Inject;
import org.openqa.selenium.internal.WrapsDriver;

import java.io.FileOutputStream;
import java.io.IOException;

import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.SUCCESS;

public class TestSuiteExecutor {
    private SeleniumTestSuite testSuite;
    private DefaultSeleneseExecutor defaultSeleneseExecutor;
    private WrapsDriver wrapsDriver;
    private String reportFileName;
    private Boolean keepBrowserOpen;

    @Inject
    public TestSuiteExecutor(SeleniumTestSuite testSuite,
                             DefaultSeleneseExecutor defaultSeleneseExecutor,
                             WrapsDriver wrapsDriver,
                             @ReportFileName String reportFileName,
                             @KeepBrowserOpenOnError Boolean keepBrowserOpen) {
        this.testSuite = testSuite;
        this.defaultSeleneseExecutor = defaultSeleneseExecutor;
        this.wrapsDriver = wrapsDriver;
        this.reportFileName = reportFileName;
        this.keepBrowserOpen = keepBrowserOpen;
    }

    public void executeTest() throws IOException {
        executeTestSuite();
        writeOutputLogs(testSuite);
        if (! (isSuiteFailed() && isKeepBrowserOnError())) {
            closeBrowser();
        }
    }

    private void executeTestSuite() {
        defaultSeleneseExecutor.executeSuite(testSuite);
    }

    private void writeOutputLogs(SeleniumTestSuite testSuite) throws IOException {
        FileOutputStream fileOutput = new FileOutputStream(reportFileName);
        new OutputResultFormatter().writeToStream(testSuite, fileOutput, "html");
        new OutputResultFormatter().writeToStream(testSuite, System.out, "text");

        fileOutput.close();
    }

    private void closeBrowser() {
        wrapsDriver.getWrappedDriver().quit();
    }

    private boolean isSuiteFailed() {
        return testSuite.getCommandResult().getStatus() != SUCCESS ;
    }

    public boolean isKeepBrowserOnError() {
        return keepBrowserOpen;
    }
}
