package com.ciplogic.seleniumaster;

public class UsageDisplay {
    public void displayUsage() {
        System.out.println(
                "java -jar app.jar [-h|-help|--help] [-k] [-Pprofile_id] [--user-extensions=extension1.js,extension2.js] [--report=report.html] suite_name.html\n" +
                        "    -h|-help|--help   - display this message and exits the application.\n" +
                        "    --list-profiles   - list the profiles that are configured and exit the application.\n" +
                        "    -k                - keep the browser open in case of error.\n" +
                        "    -Pprofile_id      - pick a browser from profiles.xml. Implicitly is the first browser from the config file.\n" +
                        "    --report          - filename where to output the result. Implicitly is `report.html`\n" +
                        "    --user-extensions - A CSV list of the user extensions that you want to use.\n" +
                        "    suite_name.html   - the suite you want ran.\n"
        );
    }
}
