package com.ciplogic.seleniumaster.modules;

import com.ciplogic.seleniumaster.StandaloneSeleneseErrorScreenshotSaver;
import com.ciplogic.seleniumaster.germanium.*;
import com.ciplogic.seleniumaster.metadata.KeepBrowserOpenOnError;
import com.ciplogic.seleniumaster.metadata.ProgramArguments;
import com.ciplogic.seleniumaster.metadata.ReportFileName;
import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;
import com.ciplogic.seleniumaster.runner.io.FileStreamResolver;
import com.ciplogic.seleniumaster.runner.io.StreamResolver;
import com.ciplogic.seleniumaster.runner.io.StreamResolverBasePath;
import com.ciplogic.seleniumaster.runner.userextensions.ClasspathScriptLoader;
import com.ciplogic.seleniumaster.runner.userextensions.FileScriptLoader;
import com.ciplogic.seleniumaster.runner.userextensions.Script;
import com.ciplogic.seleniumaster.runner.userextensions.ScriptLoader;
import com.ciplogic.seleniumaster.runner.vo.SeleniumTestReader;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.google.inject.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.internal.WrapsDriver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.util.collections.ArrayUtils.cloneArray;

public class StandaloneGermaniumModule extends AbstractModule {
    public static final String USER_EXTENSIONS = "--user-extensions=";
    public static final String REPORT_FILE_NAME = "--report=";

    private String[] args;

    public StandaloneGermaniumModule(String[] args) {
        this.args = cloneArray(args);
    }

    @Override
    protected void configure() {
        bind(DefaultSeleneseExecutor.class).in(Singleton.class);
        bind(StreamResolver.class).to(FileStreamResolver.class).in(Singleton.class);
        bind(SeleneseExecutor.class).to(DefaultSeleneseExecutor.class).in(Singleton.class);
        bind(WrapsDriver.class).to(Germanium.class).in(Singleton.class);
        bind(File.class).annotatedWith(StreamResolverBasePath.class).to(Key.get(File.class, SuiteFile.class));
        bind(String[].class).annotatedWith(ProgramArguments.class).toInstance(args);
        bind(ScriptLoader.class).to(ClasspathScriptLoader.class).in(Singleton.class);
        bind(ScriptLoader.class).annotatedWith(UserExtensions.class).to(FileScriptLoader.class).in(Singleton.class);
        bind(Germanium.class).to(DefaultGermanium.class).in(Singleton.class);
        bind(SeleneseErrorScreenshotSaver.class).to(StandaloneSeleneseErrorScreenshotSaver.class);
    }

    @Provides @SuiteFile @Singleton
    private File getSuiteFile() {
        if (args.length > 0) {
            String fileName = args[args.length - 1];
            return new File(fileName);
        }

        return null;
    }

    @Provides @Inject @Singleton
    private SeleniumTestSuite getTestSuite(SeleniumTestReader seleniumTestReader, @SuiteFile File suiteFile) {
        return seleniumTestReader.readTestSuite(suiteFile.getName());
    }

    @Provides @TestBaseSite @Inject @Singleton
    private String getTestBaseSite(SeleniumTestSuite testSuite) {
        return testSuite.getBaseSite();
    }

    @Provides @Inject @Singleton
    private WebDriver getSelectedWebDriver(BrowserProfileManager browserProfileManager) {
        for (String arg : args) {
            if (arg.startsWith("-P")) { // profile
                return browserProfileManager.getWebDriver(arg.substring(2));
            }
        }

        return browserProfileManager.getDefaultWebDriver();
    }

    @Provides @Singleton @UserExtensions
    private String[] provideUserExtensionNames() {
        for (String arg : args) {
            if (arg.startsWith(USER_EXTENSIONS)) {
                return readValue(arg, USER_EXTENSIONS).split(",");
            }
        }
        return new String[0];
    }

    @Provides @Singleton @ReportFileName
    private String provideReportFileName() {
        for (String arg : args) {
            if (arg.startsWith(REPORT_FILE_NAME)) {
                return readValue(arg, REPORT_FILE_NAME);
            }
        }
        return "report.html";
    }

    @Provides @UserExtensions @Inject
    private List<Script> provideUserExtensions(@UserExtensions String[] userExtensions, @UserExtensions ScriptLoader scriptLoader) {
        List<Script> result = new ArrayList<Script>();

        for (String userExtension : userExtensions) {
            Script script = scriptLoader.load(userExtension);
            result.add(script);
        }

        return result;
    }

    @Provides @KeepBrowserOpenOnError @Singleton
    private Boolean provideKeepBrowserOpenOnError() {
        for (String arg : args) {
            if ("-k".equals(arg)) {
                return true;
            }
        }

        return false;
    }

    private String readValue(String argument, String prefix) {
        return argument.substring(prefix.length());
    }
}
