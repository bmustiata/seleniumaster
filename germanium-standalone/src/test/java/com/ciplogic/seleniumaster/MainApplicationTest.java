package com.ciplogic.seleniumaster;

import org.junit.Test;

import java.io.IOException;

public class MainApplicationTest {
    @Test
    public void testMainApplicationListProfiles() throws IOException {
        MainApplication.main(new String[] { "--list-profiles" });
    }
}
