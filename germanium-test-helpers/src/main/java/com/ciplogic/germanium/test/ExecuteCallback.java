package com.ciplogic.germanium.test;

public interface ExecuteCallback {
    Object execute(GermaniumTestExecution execution) throws GermaniumExecutionFailed;
}
