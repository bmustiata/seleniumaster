package com.ciplogic.germanium.test;

public class GermaniumExecutionFailed extends RuntimeException {
    public GermaniumExecutionFailed() {
    }

    public GermaniumExecutionFailed(String message) {
        super(message);
    }

    public GermaniumExecutionFailed(String message, Throwable cause) {
        super(message, cause);
    }

    public GermaniumExecutionFailed(Throwable cause) {
        super(cause);
    }
}
