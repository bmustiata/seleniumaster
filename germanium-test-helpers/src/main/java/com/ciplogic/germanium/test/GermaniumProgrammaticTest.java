package com.ciplogic.germanium.test;

import com.ciplogic.germanium.test.modules.GermaniumExecutionModule;
import com.ciplogic.germanium.test.modules.GermaniumTestModule;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.ciplogic.seleniumaster.selenese.UserExtensionsEnhancer;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class GermaniumProgrammaticTest implements ExecuteCallback {
    private final Injector injector;

    private final String testResultsFolder;

    public GermaniumProgrammaticTest()  {
        this("target");
    }

    public GermaniumProgrammaticTest(String testResultsFolder) {
        this.testResultsFolder = testResultsFolder;

        injector = Guice.createInjector(new GermaniumTestModule());
    }

    @Override
    public Object execute(GermaniumTestExecution execution) throws GermaniumExecutionFailed {
        Injector germaniumInjector = injector.createChildInjector(new GermaniumExecutionModule(execution.getProfileNames()[0], execution));

        Germanium instance = germaniumInjector.getInstance(Germanium.class);

        // FIXME: terrible.
        germaniumInjector.getInstance(SeleneseRunnerEnhancer.class);
        germaniumInjector.getInstance(UserExtensionsEnhancer.class);

        return instance;
    }

    public GermaniumTestExecution getGermanium() {
        return injector.getInstance(TestFactory.class).createTestExecution(testResultsFolder, false, this, new String[]{}, false);
    }
}
