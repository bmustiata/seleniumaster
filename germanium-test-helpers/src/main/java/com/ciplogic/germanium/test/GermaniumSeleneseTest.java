package com.ciplogic.germanium.test;

import com.ciplogic.germanium.test.modules.GermaniumTestModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class GermaniumSeleneseTest implements ExecuteCallback {
    private final Injector injector;

    private final String testResultsFolder;
    private final boolean keepBrowserOpen;

    public GermaniumSeleneseTest()  {
        this(false);
    }

    public GermaniumSeleneseTest(boolean keepBrowserOpen)  {
        this("target", keepBrowserOpen);
    }

    public GermaniumSeleneseTest(String testResultsFolder, boolean keepBrowserOpen) {
        try {
            injector = Guice.createInjector(new GermaniumTestModule());

            this.testResultsFolder = testResultsFolder;
            this.keepBrowserOpen = keepBrowserOpen;
        } catch (Exception e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    /**
     * Run one or more suites. The suites must be on the same domain. The domain is set when building the
     * selenium instance so it can not be changed if the suites point to a different domain.
     *
     * @param suiteNames
     * @return
     */
    public GermaniumTestExecution runClasspathSuites(String... suiteNames) {
        return createTestExecution(suiteNames, false);
    }

    public GermaniumTestExecution runFileSuites(String... suiteNames) {
        return createTestExecution(suiteNames, true);
    }

    private GermaniumTestExecution createTestExecution(String[] suiteNames, boolean fileSuite) {
        TestFactory testFactory = injector.getInstance(TestFactory.class);

        GermaniumTestExecution execution = testFactory.createTestExecution(testResultsFolder, keepBrowserOpen, this, suiteNames, fileSuite);
        execution.setTargetSite("http://localhost/"); // so it's not null, tests will rewrite this.

        return execution;
    }


    @Override
    public Object execute(GermaniumTestExecution execution) throws GermaniumExecutionFailed {
        return injector.getInstance(GermaniumSeleneseTestRunner.class).execute(execution);
    }
}
