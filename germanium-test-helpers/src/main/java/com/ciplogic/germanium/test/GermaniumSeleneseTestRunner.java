package com.ciplogic.germanium.test;

import com.ciplogic.germanium.test.modules.GermaniumExecutionModule;
import com.ciplogic.germanium.test.modules.SingleTestSuiteExecutionModule;
import com.ciplogic.seleniumaster.formatter.OutputResultFormatter;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.ciplogic.seleniumaster.selenese.UserExtensionsEnhancer;
import com.ciplogic.util.io.PathUtil;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;

import static java.lang.String.format;

/**
 * This class just has a bunch of sensible defaults and detections
 * in order to make test running a breeze.
 */
public class GermaniumSeleneseTestRunner implements ExecuteCallback {
    private static Logger log = Logger.getLogger(GermaniumSeleneseTestRunner.class);
    private final Injector parentInjector;
    private final BrowserProfileManager browserProfileManager;

    @Inject
    public GermaniumSeleneseTestRunner(Injector injector,
                                       BrowserProfileManager browserProfileManager) {
        this.parentInjector = injector;
        this.browserProfileManager = browserProfileManager;
    }

    // this method is package visible since it should only be called from the germanium test execution.
    @Override
    public Object execute(GermaniumTestExecution execution) throws GermaniumExecutionFailed {
        // FIXME: the stream resolver should be initialized for each test.
        String[] selectedProfileNames = getSelectedProfiles(execution);
        SeleneseExecutionError error = null;

        for (String profileName : selectedProfileNames) {
            Germanium germanium = null;

            try {
                Injector germaniumInjector = parentInjector.createChildInjector(new GermaniumExecutionModule(profileName, execution));
                germanium = germaniumInjector.getInstance(Germanium.class);

                germaniumInjector.getInstance(SeleneseRunnerEnhancer.class);
                germaniumInjector.getInstance(UserExtensionsEnhancer.class);

                DefaultSeleneseExecutor defaultSeleneseExecutor = (DefaultSeleneseExecutor) germanium.getSeleneseExecutor();

                for (String suiteName : execution.getSuiteNames()) {
                    try {
                        Injector suiteInjector = germaniumInjector.createChildInjector(new SingleTestSuiteExecutionModule(execution, suiteName));

                        SeleniumTestSuite suite = suiteInjector.getInstance(SeleniumTestSuite.class);
                        germanium.setBaseUrl( suite.getBaseSite() );
                        SeleniumTestSuite result = defaultSeleneseExecutor.executeSuite(suite);

                        String fileName = format("result-%s-%s-%s.html",
                                profileName,
                                result.getName(),
                                result.getCommandResult().getStatus());

                        fileName = PathUtil.concatPath(execution.getTestResultsFolder(), fileName);

                        writeReportToFile(result, fileName);

                        log.info("wrote result " + fileName);

                        if (result.getCommandResult().getStatus() != SeleniumCommandResult.Status.SUCCESS) {
                            error = new SeleneseExecutionError(result.getCommandResult().getErrorName(),
                                            result.getCommandResult().getErrorDescription());
                        }
                    } catch (Exception e) {
                        String message = format("Failed executing suite %s on profile %s.", suiteName, profileName);
                        log.error(message, e);
                        error = new SeleneseExecutionError(message, e.getMessage());
                    }
                }
            } catch (Exception e) {
                String message = format("Failed starting up profile %s.", profileName);
                log.error(message, e);
                error = new SeleneseExecutionError(message, e.getMessage());
            } finally {
                if (germanium != null) {
                    if (!(execution.isKeepBrowserOpenOnError() && error != null)) {
                        germanium.getWrappedDriver().quit();
                    }
                }
            }
        }

        if (error != null) {
            throw new GermaniumExecutionFailed( error.toString() );
        }

        return null;
    }

    private String[] getSelectedProfiles(GermaniumTestExecution execution) {
        if (execution.getProfileNames() == null || execution.getProfileNames().length == 0) {
            return new String[] { browserProfileManager.getDefaultProfile().getId() };
        }

        return execution.getProfileNames();
    }

    private void writeReportToFile(SeleniumTestSuite result, String fileName) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(fileName);
        new OutputResultFormatter().writeToStream(result, outputStream, "html");
        outputStream.close();
    }
}
