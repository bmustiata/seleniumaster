package com.ciplogic.germanium.test;

import com.ciplogic.util.collections.ArrayUtils;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;

import static com.ciplogic.util.collections.ArrayUtils.cloneArray;

/**
 * <p>
 *     Run one or several test suites on top of one or more profiles, using or not user-extensions.js. In case no
 * profiles are given, the default profile from the profiles.xml will be used.
 * </p>
 */
public class GermaniumTestExecution {
    private static Logger log = Logger.getLogger(GermaniumTestExecution.class);

    private final String testResultsFolder;
    private final boolean keepBrowserOpenOnError;

    private boolean fileSuite;

    private String[] suiteNames;
    private String[] profileNames;
    private String[] userExtensions = new String[] {};

    private String targetSite;

    private ExecuteCallback executeCallback;

    @Inject
    protected GermaniumTestExecution(@Assisted("testResultsFolder") String testResultsFolder,
                                     @Assisted("keepBrowserOpen") boolean keepBrowserOpenOnError,
                                     @Assisted ExecuteCallback executeCallback,
                                     @Assisted String[] suiteNames,
                                     @Assisted boolean fileSuite) {
        this.executeCallback = executeCallback;
        this.suiteNames = suiteNames;
        this.fileSuite = fileSuite;

        this.testResultsFolder = testResultsFolder;
        this.keepBrowserOpenOnError = keepBrowserOpenOnError;
    }

    /**
     * Pick one ore more profiles from the profiles.xml in order to execute them.
     * @param profileNames
     * @return
     */
    public GermaniumTestExecution onProfiles(String ... profileNames) {
        setProfileNames(profileNames);

        return this;
    }

    /**
     * Specify a set of selenese user extensions to use when running this test.
     * @param userExtensions
     * @return
     */
    public GermaniumTestExecution withExtensions(String ... userExtensions) {
        this.userExtensions = userExtensions;

        return this;
    }

    /**
     * Run the test on a different target site than the one specified in the first test
     * of the suite.
     *
     * @param targetSite
     * @return
     */
    public GermaniumTestExecution onTargetSite(String targetSite) {
        this.targetSite = targetSite;

        return this;
    }

    /**
     * Signifies the end of configuration, and the actual launch of the test.
     * @param <T>
     * @return
     * @throws GermaniumExecutionFailed
     */
    public <T> T now() throws GermaniumExecutionFailed {
        return (T) executeCallback.execute(this);
    }

    public String getTestResultsFolder() {
        return testResultsFolder;
    }

    public boolean isKeepBrowserOpenOnError() {
        return keepBrowserOpenOnError;
    }

    public boolean isFileSuite() {
        return fileSuite;
    }

    public void setFileSuite(boolean fileSuite) {
        this.fileSuite = fileSuite;
    }

    public String[] getSuiteNames() {
        return suiteNames;
    }

    public void setSuiteNames(String[] suiteNames) {
        this.suiteNames = ArrayUtils.cloneArray(suiteNames);
    }

    public String[] getProfileNames() {
        return profileNames;
    }

    public void setProfileNames(String[] profileNames) {
        this.profileNames = cloneArray(profileNames);
    }

    public String[] getUserExtensions() {
        return userExtensions;
    }

    public void setUserExtensions(String[] userExtensions) {
        this.userExtensions = cloneArray(userExtensions);
    }

    public String getTargetSite() {
        return targetSite;
    }

    public void setTargetSite(String targetSite) {
        this.targetSite = targetSite;
    }
}
