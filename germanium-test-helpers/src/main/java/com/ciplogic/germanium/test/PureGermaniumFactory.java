package com.ciplogic.germanium.test;

import com.ciplogic.seleniumaster.germanium.DefaultGermanium;
import org.openqa.selenium.WebDriver;

import java.util.List;

public interface PureGermaniumFactory  {
    DefaultGermanium createDefaultGermanium(WebDriver webDriver, String profileName, List<String> userExtensions);
}
