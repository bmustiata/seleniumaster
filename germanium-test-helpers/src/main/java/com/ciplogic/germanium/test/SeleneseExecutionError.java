package com.ciplogic.germanium.test;

public class SeleneseExecutionError {
    private String message;
    private String detail;

    public SeleneseExecutionError(String message, String detail) {
        this.message = message;
        this.detail = detail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Error executing Selenese: " +
                "message='" + message + '\'' +
                ", detail='" + detail + '\'';
    }
}
