package com.ciplogic.germanium.test;

import com.ciplogic.seleniumaster.runner.vo.SeleniumTestReader;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.Inject;

public class SeleniumTestFactory {
    private final SeleniumTestReader seleniumTestReader;

    @Inject
    public SeleniumTestFactory(SeleniumTestReader seleniumTestReader) {
        this.seleniumTestReader = seleniumTestReader;
    }

    public SeleniumTestSuite readSuite(String name) {
        return seleniumTestReader.readTestSuite(name);
    }
}
