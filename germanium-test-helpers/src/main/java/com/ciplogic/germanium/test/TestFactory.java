package com.ciplogic.germanium.test;

import com.ciplogic.germanium.test.metadata.KeepBrowserOpen;
import com.ciplogic.germanium.test.metadata.TestResultsFolder;
import com.ciplogic.seleniumaster.runner.io.StreamResolver;
import com.ciplogic.seleniumaster.runner.vo.SeleniumTestReader;
import com.google.inject.assistedinject.Assisted;

public interface TestFactory {
    GermaniumTestExecution createTestExecution(
        @Assisted("testResultsFolder") String testResultsFolder,
        @Assisted("keepBrowserOpen") boolean keepBrowserOpen,
        ExecuteCallback germaniumSeleneseTest,
        String[] suiteNames,
        boolean fileSuite
    );
}
