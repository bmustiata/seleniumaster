package com.ciplogic.germanium.test;

import com.ciplogic.germanium.test.metadata.CurrentProfileName;
import com.ciplogic.germanium.test.metadata.TestResultsFolder;
import com.ciplogic.seleniumaster.germanium.SeleneseErrorScreenshotSaver;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.Inject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.FileOutputStream;
import java.io.IOException;

import static com.ciplogic.util.io.PathUtil.concatPath;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.io.IOUtils.write;

public class TestHelperScreenshotSaverSelenese implements SeleneseErrorScreenshotSaver {
    private String testResultsFolder;
    private Germanium germanium;
    private String profileName;

    @Inject
    public TestHelperScreenshotSaverSelenese(Germanium germanium,
                                             @TestResultsFolder String testResultsFolder,
                                             @CurrentProfileName String profileName) {
        this.germanium = germanium;
        this.testResultsFolder = testResultsFolder;
        this.profileName = profileName;
    }

    @Override
    public void saveScreenshot(SeleniumTestSuite suite, SeleniumTest test, SeleniumCommand command) {
        FileOutputStream output = null;
        try {
            byte[] screenshot = getScreenshot();
            output = writeScreenshot(suite.getName(), output, screenshot);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        } finally {
            closeQuietly(output);
        }
    }

    private FileOutputStream writeScreenshot(String suiteName, FileOutputStream output, byte[] screenshot) throws IOException {
        String fileName = concatPath(testResultsFolder, "result-" + profileName + "-" + suiteName + ".png");
        output = new FileOutputStream(fileName);
        write(screenshot, output);
        return output;
    }

    private byte[] getScreenshot() {
        return ((TakesScreenshot) germanium.getWrappedDriver()).getScreenshotAs(OutputType.BYTES);
    }
}
