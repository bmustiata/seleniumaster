package com.ciplogic.germanium.test.modules;

import com.ciplogic.germanium.test.GermaniumTestExecution;
import com.ciplogic.germanium.test.TestHelperScreenshotSaverSelenese;
import com.ciplogic.germanium.test.metadata.CurrentProfileName;
import com.ciplogic.germanium.test.metadata.TestResultsFolder;
import com.ciplogic.seleniumaster.germanium.*;
import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;
import com.ciplogic.seleniumaster.runner.userextensions.ClasspathScriptLoader;
import com.ciplogic.seleniumaster.runner.userextensions.Script;
import com.ciplogic.seleniumaster.runner.userextensions.ScriptLoader;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.ciplogic.seleniumaster.selenese.SeleneseModule;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

/**
 * A module that specifies a Germanium execution - bound to the life of a single browser instance.
 */
public class GermaniumExecutionModule extends AbstractModule {
    private final String profileName;
    private final GermaniumTestExecution execution;

    /**
     *
     * @param profileName The profile name from the BrowserProfileManager for which this execution will be built.
     * @param execution The parameters of the execution (like user extension scripts and co).
     */
    public GermaniumExecutionModule(String profileName, GermaniumTestExecution execution) {
        this.profileName = profileName;
        this.execution = execution;
    }

    @Override
    protected void configure() {
        // FIXME: this should delegate to SingleTestSuite scopes
        bind(SeleneseErrorScreenshotSaver.class).to(TestHelperScreenshotSaverSelenese.class).in(Singleton.class);

        bind(String.class).annotatedWith(CurrentProfileName.class).toInstance(this.profileName);
        bind(String.class).annotatedWith(TestBaseSite.class).toInstance(execution.getTargetSite());
        bind(String.class).annotatedWith(TestResultsFolder.class).toInstance("target/");

        bind(SeleneseExecutor.class).to(DefaultSeleneseExecutor.class).in(Singleton.class);
        bind(ScriptLoader.class).to(ClasspathScriptLoader.class).in(Singleton.class);

        // FIXME: produce the same thing?
        bind(DefaultGermanium.class).in(Singleton.class);
        bind(Germanium.class).to(DefaultGermanium.class).in(Singleton.class);
        bind(Selenium.class).to(DefaultGermanium.class).in(Singleton.class);

        bind(ScreenshotSaver.class).to(DefaultScreenshotSaver.class).in(Singleton.class);

        install(new SeleneseModule());
    }

    @Provides @Singleton
    public WebDriver getWebDriver(BrowserProfileManager browserProfileManager,
                                  @CurrentProfileName String profileName) {
        return browserProfileManager.getWebDriver(profileName);
    }

    @Provides @UserExtensions @Singleton
    public List<Script> getUserExtensions(ScriptLoader scriptLoader) {
        List<Script> result = new ArrayList<Script>();

        for (String userExtension : execution.getUserExtensions()) {
            Script script = scriptLoader.load(userExtension);
            result.add(script);
        }

        return result;
    }
}
