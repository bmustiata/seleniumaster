package com.ciplogic.germanium.test.modules;

import com.ciplogic.germanium.selenium.BinaryDriverModule;
import com.ciplogic.germanium.test.TestFactory;
import com.ciplogic.seleniumaster.profiles.ProfilesModule;
import com.ciplogic.seleniumaster.runner.io.ClasspathStreamResolver;
import com.ciplogic.seleniumaster.runner.io.StreamResolver;
import com.ciplogic.seleniumaster.runner.io.StreamResolverBasePath;
import com.ciplogic.util.guice.PostConstructModule;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;

/**
 * A full GermaniumSeleneseTest, or a GermaniumProgrammaticTest. Thus this module contains only
 * the configuration for factories, and the PostConstructModule.
 */
public class GermaniumTestModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new PostConstructModule());
        install(new BinaryDriverModule());
        install(new FactoryModuleBuilder().build(TestFactory.class));
        install(new ProfilesModule());

        bind(String.class).annotatedWith(StreamResolverBasePath.class).toInstance("");
        bind(StreamResolver.class).to(ClasspathStreamResolver.class).in(Singleton.class);
    }
}
