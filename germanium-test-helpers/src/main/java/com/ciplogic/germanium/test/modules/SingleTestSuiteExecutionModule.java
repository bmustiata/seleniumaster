package com.ciplogic.germanium.test.modules;

import com.ciplogic.germanium.test.GermaniumTestExecution;
import com.ciplogic.seleniumaster.runner.vo.SeleniumTestReader;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class SingleTestSuiteExecutionModule extends AbstractModule {
    private final GermaniumTestExecution execution;
    private final String suiteName;

    public SingleTestSuiteExecutionModule(GermaniumTestExecution execution, String suiteName) {
        this.execution = execution;
        this.suiteName = suiteName;
    }

    @Override
    protected void configure() {
    }

    @Provides @Singleton
    public SeleniumTestSuite getSeleniumTestSuite(SeleniumTestReader seleniumTestReader) {
        return seleniumTestReader.readTestSuite(suiteName);
    }
}
