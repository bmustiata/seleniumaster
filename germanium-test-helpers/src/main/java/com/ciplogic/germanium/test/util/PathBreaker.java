package com.ciplogic.germanium.test.util;

import com.ciplogic.util.collections.Tuple;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PathBreaker {
    private static Pattern BASE_PATH_BREAKER = Pattern.compile("^(.*[\\\\/])(.*?)$");

    private PathBreaker() {}

    public static Tuple<String, String> deconstructPath(String suiteName) {
        Matcher matcher = BASE_PATH_BREAKER.matcher(suiteName);

        // path/file
        if (matcher.matches()) {
            return new Tuple<String, String>(matcher.group(1), matcher.group(2));
        }

        // simple file
        return new Tuple<String, String>("", suiteName);
    }
}
