package com.ciplogic.seleniumaster.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class XmlUtil {
    private XmlUtil() {}

    static XPathFactory xPathFactory = XPathFactory.newInstance();
    static DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    public static List<Node> xpathEvalList(Node e, String expression) {
        Object items = xpathEval(e, expression, XPathConstants.NODESET);

        if (items instanceof NodeList) {
            List<Node> result = new ArrayList<Node>();

            NodeList nodeListItems = (NodeList) items;
            for (int i = 0; i < nodeListItems.getLength(); i++) {
                result.add( nodeListItems.item(i) );
            }

            return result;
        }

        return Collections.emptyList();
    }

    public static Element xpathEvalElement(Node e, String expression) {
        Object resultElement = xpathEval(e, expression, XPathConstants.NODESET);

        if (resultElement instanceof NodeList) {
            NodeList resultElementList = (NodeList) resultElement;

            for (int i = 0; i < resultElementList.getLength(); i++) {
                return (Element) resultElementList.item(0);
            }

            return null;
        }

        return (Element) resultElement;
    }

    private static Object xpathEval(Node node, String expression, QName qName) {
        try {
            XPath xpath = xPathFactory.newXPath();
            return xpath.evaluate(expression, node, qName);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public static String xpathValue(Node e, String expression) {
        Object result = xpathEval(e, expression, XPathConstants.STRING);

        return result != null ? result.toString() : null;
    }

    public static Document getDocumentFromStream(InputStream xmlDocument) {
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            documentBuilder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    return new InputSource(new StringReader(""));
                }
            });

            return documentBuilder.parse(xmlDocument);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

}
