package com.ciplogic.germanium.web.client;

import com.ciplogic.germanium.test.GermaniumSeleneseTest;
import org.junit.Ignore;
import org.junit.Test;

@SuppressWarnings("NonJREEmulationClassesInClientCode")
public class ProfileCreationIntegrationTest extends BaseJettyServerTest {
    @Test
    public void testApplicationOpens() throws InterruptedException {
        new GermaniumSeleneseTest()
             .runClasspathSuites("/tests/Test Simple Profile Creation.html")
             .withExtensions("userextension-simple-locator.js")
             .onProfiles("firefox", "chrome", "ie")
             .now();
    }

    @Ignore
    @Test
    public void testServer() throws InterruptedException {
        new GermaniumSeleneseTest()
                .runClasspathSuites("/tests/Pause Test.html")
                .withExtensions("userextension-simple-locator.js")
                .now();
    }
}