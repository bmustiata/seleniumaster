package com.ciplogic.germanium.web.client;

import com.ciplogic.germanium.web.client.modules.notifier.NotifierAtmosphere;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.RootPanel;
import org.fusesource.restygwt.client.Defaults;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GermaniumWeb implements EntryPoint, UncaughtExceptionHandler {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";


    /**
	 * This is the entry point method.
	 */
    @Override
	public void onModuleLoad() {
		try {
			GWT.setUncaughtExceptionHandler(this);
	
			Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
				@Override
				public void execute() {
					moduleInitialization();
				}
			});
		} catch(Exception e) {
			GWT.log(e.getMessage(), e);
			logMessage(e.getMessage());
		}
	}

	public void moduleInitialization() {
        // notifier bridge
        new NotifierAtmosphere().registerChannel();

        // resty-gwt config
        Defaults.setServiceRoot(GWT.getHostPageBaseURL() + "service/");

        GermaniumWebGInjector injector = GWT.create(GermaniumWebGInjector.class);

        RootPanel.get("mainContainer").add(injector.globalLayout());
		
		History.addValueChangeHandler(new HistoryHandler());
		History.newItem("Home");
	}

	@Override
	public void onUncaughtException(Throwable e) {
		GWT.log(e.getMessage(), e);

		logMessage("Exception " + e.getClass().getName() + " caught:" + e.getMessage());

        for (StackTraceElement stackElement : e.getStackTrace()) {
            logMessage(
                stackElement.getClassName() + "." +
                stackElement.getMethodName() + " (" +
                stackElement.getFileName() + ":" +
                stackElement.getLineNumber() + ")"
            );
        }

        if (e.getCause() != null) {
            logMessage(" caused by ");
            onUncaughtException(e.getCause());
        }
	}
	
	public static native void logMessage(String message) /*-{
		if ($wnd.console) {
			$wnd.console.log(message);
		}
	}-*/;
}
