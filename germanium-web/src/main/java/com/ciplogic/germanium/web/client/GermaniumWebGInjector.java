package com.ciplogic.germanium.web.client;

import com.ciplogic.germanium.web.client.command.CommandModule;
import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfileModule;
import com.ciplogic.germanium.web.client.modules.documents.DocumentModule;
import com.ciplogic.germanium.web.client.modules.livebrowser.LiveBrowserModule;
import com.ciplogic.germanium.web.client.modules.projects.ProjectDocumentModule;
import com.ciplogic.germanium.web.client.modules.reports.ReportsModule;
import com.ciplogic.germanium.web.client.modules.resources.ResourceModule;
import com.ciplogic.germanium.web.client.modules.validators.ValidatorModule;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;

@GinModules({
    ValidatorModule.class,
    ProjectDocumentModule.class,
    DocumentModule.class,
    CommandModule.class,
    BrowserProfileModule.class,
    ResourceModule.class,
    LiveBrowserModule.class,
    ReportsModule.class
})
public interface GermaniumWebGInjector extends Ginjector {
    GlobalLayout globalLayout();
}
