package com.ciplogic.germanium.web.client;

import com.ciplogic.germanium.web.client.modules.browserprofiles.view.BrowserProfilesView;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.projects.view.TestProjectsView;
import com.ciplogic.germanium.web.client.modules.reports.view.ReportsView;
import com.ciplogic.germanium.web.client.modules.tests.view.TestBrowserView;
import com.ciplogic.germanium.web.client.views.UserExtensionsView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;

public class GlobalLayout extends Composite {
    interface MyUiBinder extends UiBinder<DockLayoutPanel, GlobalLayout> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    MenuBar menuBar;

    @UiField(provided = true)
    TabLayoutPanel documentsPanel;

    @UiField
    StackLayoutPanel viewsPanel;

    @Inject
    public GlobalLayout(@DocumentPanel DocumentHolder documentsPanel,
                        // views
                        TestProjectsView testProjectsView,
                        ReportsView reportsView,
                        TestBrowserView testBrowserView,
                        BrowserProfilesView browserProfilesView,
                        UserExtensionsView userExtensionsView
    ) {
        // This place needs to know that the documents panel is a tab.
        this.documentsPanel = (TabLayoutPanel) documentsPanel;

        initWidget(uiBinder.createAndBindUi(this));

        createMenu();
        createViews(testProjectsView, reportsView, testBrowserView, browserProfilesView, userExtensionsView);

        initializeEvents();
    }

    private void createMenu() {
        MenuItem runMenuEntry = new MenuItem("Run", false, (Command) null);
        menuBar.addItem(runMenuEntry);

        MenuItem executionsMenuEntry = new MenuItem("0 Executions", false, (Command) null);
        menuBar.addItem(executionsMenuEntry);
    }

    private StackLayoutPanel createViews(TestProjectsView testProjectsView, ReportsView reportsView,
                                         TestBrowserView testBrowserView, BrowserProfilesView browserProfilesView,
                                         UserExtensionsView userExtensionsView) {
        viewsPanel.add(testProjectsView, new HTML("Projects"), 32.0);
        viewsPanel.add(reportsView, new HTML("Reports"), 32.0);
        viewsPanel.add(testBrowserView, new HTML("Tests"), 32.0);
        viewsPanel.add(browserProfilesView, new HTML("Browser Profiles"), 32.0);
        viewsPanel.add(userExtensionsView, new HTML("User Extensions"), 32.0);

        viewsPanel.setSize("100%", "100%");

        return viewsPanel;
    }

    private void initializeEvents() {
    }
}
