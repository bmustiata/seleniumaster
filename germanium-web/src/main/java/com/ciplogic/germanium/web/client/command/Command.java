package com.ciplogic.germanium.web.client.command;

public interface Command {
    void execute();
}
