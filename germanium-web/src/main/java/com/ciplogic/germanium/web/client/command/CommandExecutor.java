package com.ciplogic.germanium.web.client.command;

public class CommandExecutor {
    public void execute(Command command) {
        command.execute();
    }
}
