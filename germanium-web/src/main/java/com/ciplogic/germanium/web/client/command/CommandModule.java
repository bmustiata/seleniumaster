package com.ciplogic.germanium.web.client.command;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;

public class CommandModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind(CommandExecutor.class).in(Singleton.class);
    }
}
