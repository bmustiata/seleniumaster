package com.ciplogic.germanium.web.client.modules.browserprofiles;

import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.*;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.impl.*;
import com.ciplogic.germanium.web.client.modules.browserprofiles.document.BrowserProfileDocumentFactory;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.GinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.inject.Singleton;

public class BrowserProfileModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind(BrowserProfilesHolder.class).in(Singleton.class);

        GinModule commandsFactoryModule = new GinFactoryModuleBuilder()
                .implement(ProfileDeleteCommand.class, ProfileDeleteCommandImpl.class)
                .implement(ProfileRefreshCommand.class, ProfileRefreshCommandImpl.class)
                .implement(ProfileEditCommand.class, ProfileEditCommandImpl.class)
                .implement(ProfileNewCommand.class, ProfileNewCommandImpl.class)
                .implement(ProfileSaveCommand.class, ProfileSaveCommandImpl.class)
                .build(BrowserProfileCommandFactory.class);

        install(commandsFactoryModule);

        GinModule documentFactoryModule = new GinFactoryModuleBuilder()
                .build(BrowserProfileDocumentFactory.class);

        install(documentFactoryModule);
    }
}
