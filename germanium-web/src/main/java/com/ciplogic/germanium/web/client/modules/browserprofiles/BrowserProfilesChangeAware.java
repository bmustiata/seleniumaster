package com.ciplogic.germanium.web.client.modules.browserprofiles;

public interface BrowserProfilesChangeAware {
    void onBrowserProfilesChange();
}
