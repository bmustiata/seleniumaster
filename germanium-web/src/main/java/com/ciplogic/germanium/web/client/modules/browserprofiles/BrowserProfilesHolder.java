package com.ciplogic.germanium.web.client.modules.browserprofiles;

import com.ciplogic.germanium.web.shared.vo.event.CustomEventBase;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrowserProfilesHolder extends CustomEventBase {
    private List<BrowserProfile> browserProfiles = new ArrayList<BrowserProfile>();
    private List<BrowserProfilesChangeAware> changeListeners = new ArrayList<BrowserProfilesChangeAware>();

    private Map<String, BrowserProfile> browserProfileMap = new HashMap<String, BrowserProfile>();

    public List<BrowserProfile> getBrowserProfiles() {
        return browserProfiles;
    }

    public void setBrowserProfiles(List<BrowserProfile> browserProfiles) {
        this.browserProfiles = browserProfiles;
        updateProfileMap();
        notifyListeners();
    }

    private void updateProfileMap() {
        for (BrowserProfile browserProfile : browserProfiles) {
            browserProfileMap.put(browserProfile.getId(), browserProfile);
        }
    }

    public List<BrowserProfile> fromStringList(List<String> browsers) {
        List<BrowserProfile> result = new ArrayList<BrowserProfile>();

        for (String browserId : browsers) {
            BrowserProfile browser = browserProfileMap.get(browserId);
            if (browser == null) {
                browser = new BrowserProfile("unknwon", browserId);
            }

            result.add(browser);
        }

        return result;
    }

    public List<String> toStringList(List<BrowserProfile> browsers) {
        List<String> result = new ArrayList<String>();

        for (BrowserProfile browserProfile : browsers) {
            result.add( browserProfile.getId() );
        }

        return result;
    }

    public void addListener(BrowserProfilesChangeAware listener) {
        changeListeners.add(listener);
    }

    public void removeListener(BrowserProfilesChangeAware listener) {
        changeListeners.remove(listener);
    }

    private void notifyListeners() {
        for (BrowserProfilesChangeAware browserProfilesChangeAware : changeListeners) {
            browserProfilesChangeAware.onBrowserProfilesChange();
        }
    }
}
