package com.ciplogic.germanium.web.client.modules.browserprofiles.commands;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

public interface BrowserProfileCommandFactory {
    ProfileDeleteCommand createProfileDeleteCommand(BrowserProfile selectedProfile);
    ProfileRefreshCommand createProfileRefreshCommand();
    ProfileEditCommand createProfileEditCommand(BrowserProfile selectedProfile);
    ProfileNewCommand createProfileNewCommand();
    ProfileSaveCommand createProfileSaveCommand(BrowserProfile browserProfile, String lastSuccessfulySavedId);
}
