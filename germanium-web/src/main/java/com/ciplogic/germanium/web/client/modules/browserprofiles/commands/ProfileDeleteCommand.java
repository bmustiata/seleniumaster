package com.ciplogic.germanium.web.client.modules.browserprofiles.commands;

import com.ciplogic.germanium.web.client.command.Command;

public interface ProfileDeleteCommand extends Command {
}
