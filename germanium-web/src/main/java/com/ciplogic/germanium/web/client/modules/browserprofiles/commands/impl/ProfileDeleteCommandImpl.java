package com.ciplogic.germanium.web.client.modules.browserprofiles.commands.impl;

import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesHolder;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileDeleteCommand;
import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.BrowserProfileService;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

import java.util.List;

public class ProfileDeleteCommandImpl implements ProfileDeleteCommand {
    private BrowserProfileService browserProfileService;
    private BrowserProfilesHolder browserProfilesHolder;
    private BrowserProfile browserProfile;

    @Inject
    public ProfileDeleteCommandImpl(BrowserProfileService browserProfileService,
                                    BrowserProfilesHolder browserProfilesHolder,
                                    @Assisted BrowserProfile browserProfile) {
        this.browserProfileService = browserProfileService;
        this.browserProfilesHolder = browserProfilesHolder;
        this.browserProfile = browserProfile;
    }

    @Override
    public void execute() {
        List<BrowserProfile> noData = REST.withCallback(new DefaultMethod<List<BrowserProfile>>() {
            @Override
            public void onSuccess(Method method, List<BrowserProfile> browserProfiles) {
                Notifier.showInfo("Profile `" + browserProfile.getId() + "` was deleted.");
                browserProfilesHolder.setBrowserProfiles(browserProfiles);
            }
        }).call(browserProfileService).deleteProfile(browserProfile.getId());
    }
}