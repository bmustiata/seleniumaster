package com.ciplogic.germanium.web.client.modules.browserprofiles.commands.impl;

import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileEditCommand;
import com.ciplogic.germanium.web.client.modules.browserprofiles.document.BrowserProfileDocumentFactory;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class ProfileEditCommandImpl implements ProfileEditCommand {
    private DocumentHolder documentHolder;
    private BrowserProfileDocumentFactory browserProfileDocumentFactory;

    private BrowserProfile profile;

    @Inject
    public ProfileEditCommandImpl(@DocumentPanel DocumentHolder documentHolder,
                                  BrowserProfileDocumentFactory browserProfileDocumentFactory,
                                  @Assisted BrowserProfile profile) {
        this.documentHolder = documentHolder;
        this.browserProfileDocumentFactory = browserProfileDocumentFactory;
        this.profile = profile;
    }

    @Override
    public void execute() {
        documentHolder.add(browserProfileDocumentFactory.createBrowserProfileDocument(profile));
    }
}
