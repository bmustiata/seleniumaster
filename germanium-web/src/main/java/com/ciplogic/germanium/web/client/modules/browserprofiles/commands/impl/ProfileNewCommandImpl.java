package com.ciplogic.germanium.web.client.modules.browserprofiles.commands.impl;

import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileNewCommand;
import com.ciplogic.germanium.web.client.modules.browserprofiles.document.BrowserProfileDocumentFactory;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;

public class ProfileNewCommandImpl implements ProfileNewCommand {
    private DocumentHolder documentHolder;
    private BrowserProfileDocumentFactory browserProfileDocumentFactory;

    @Inject
    public ProfileNewCommandImpl(@DocumentPanel DocumentHolder documentHolder,
                                 BrowserProfileDocumentFactory browserProfileDocumentFactory) {
        this.documentHolder = documentHolder;
        this.browserProfileDocumentFactory = browserProfileDocumentFactory;
    }

    @Override
    public void execute() {
        documentHolder.add(
            browserProfileDocumentFactory.createBrowserProfileDocument(new BrowserProfile())
        );
    }
}
