package com.ciplogic.germanium.web.client.modules.browserprofiles.commands.impl;

import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesHolder;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileRefreshCommand;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.BrowserProfileService;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

import java.util.List;

public class ProfileRefreshCommandImpl implements ProfileRefreshCommand {
    private BrowserProfileService browserProfileService;
    private BrowserProfilesHolder browserProfilesHolder;

    @Inject
    public ProfileRefreshCommandImpl(BrowserProfileService browserProfileService,
                                     BrowserProfilesHolder browserProfilesHolder) {
        this.browserProfileService = browserProfileService;
        this.browserProfilesHolder = browserProfilesHolder;
    }

    @Override
    public void execute() {
        REST.withCallback(new DefaultMethod<List<BrowserProfile>>() {
            @Override
            public void onSuccess(Method method, List<BrowserProfile> browserProfiles) {
                browserProfilesHolder.setBrowserProfiles(browserProfiles);
            }
        }).call(browserProfileService).listAllProfiles();
    }
}
