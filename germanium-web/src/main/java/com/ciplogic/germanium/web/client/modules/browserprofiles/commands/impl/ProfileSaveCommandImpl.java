package com.ciplogic.germanium.web.client.modules.browserprofiles.commands.impl;

import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesHolder;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileSaveCommand;
import com.ciplogic.germanium.web.client.modules.browserprofiles.event.SaveEvent;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.BrowserProfileService;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

import java.util.List;

public class ProfileSaveCommandImpl implements ProfileSaveCommand {
    private BrowserProfileService browserProfileService;
    private BrowserProfilesHolder browserProfilesHolder;

    private BrowserProfile newProfile;
    private String oldProfileId;

    @Inject
    public ProfileSaveCommandImpl(BrowserProfileService browserProfileService,
                                  BrowserProfilesHolder browserProfilesHolder,
                                  @Assisted BrowserProfile newProfile,
                                  @Assisted String oldProfileId) {
        this.browserProfileService = browserProfileService;
        this.browserProfilesHolder = browserProfilesHolder;

        this.newProfile = newProfile;
        this.oldProfileId = oldProfileId;
    }

    @Override
    public void execute() {
        List<BrowserProfile> noData = REST.withCallback(new DefaultMethod<List<BrowserProfile>>() {
            @Override
            public void onSuccess(Method method, List<BrowserProfile> browserProfiles) {
                Notifier.showInfo("Profile was saved successfuly.");
                browserProfilesHolder.notifyEvent(new SaveEvent(newProfile));
                browserProfilesHolder.setBrowserProfiles(browserProfiles);
            }
        }).call(browserProfileService).saveProfile(newProfile, oldProfileId);
    }
}
