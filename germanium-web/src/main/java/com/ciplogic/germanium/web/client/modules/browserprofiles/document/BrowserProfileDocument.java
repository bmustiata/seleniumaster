package com.ciplogic.germanium.web.client.modules.browserprofiles.document;

import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.gwtui.datagrid.SingleSelectionDataGrid;
import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesHolder;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.BrowserProfileCommandFactory;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileSaveCommand;
import com.ciplogic.germanium.web.client.modules.browserprofiles.event.SaveEvent;
import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.livebrowser.commands.LiveBrowserCommandFactory;
import com.ciplogic.germanium.web.client.modules.livebrowser.commands.StartLiveBrowserCommand;
import com.ciplogic.germanium.web.client.modules.validators.UIValidation;
import com.ciplogic.germanium.web.client.modules.validators.ValidatorFactory;
import com.ciplogic.germanium.web.shared.vo.event.CustomEventAware;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.Capability;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class BrowserProfileDocument extends AbstractDocument implements CapabilityChangeAware {
    interface MyUiBinder extends UiBinder<VerticalPanel, BrowserProfileDocument> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private RegExp IP_PORT_REGEXP = RegExp.compile("^(.*?)(:(\\d+))?$");

    public static final int REGEXP_PORT_GROUP_INDEX = 3;
    public static final int REGEXP_IP_GROUP_INDEX = 1;

    private BrowserProfile browserProfile;

    private UserExtensionsStringHolder userExtensionsHolder;

    private BrowserProfileCommandFactory commandFactory;
    private BrowserProfileDocumentFactory documentFactory;
    private LiveBrowserCommandFactory liveBrowserCommandFactory;
    private CommandExecutor commandExecutor;

    private BrowserProfilesHolder browserProfilesHolder;
    private String lastSuccessfulySavedId;

    private CustomEventAware profileSavedEventHandler;

    private ValidatorFactory validators;

    @UiField TextBox nameTextBox;
    @UiField(provided = true) ListBox browserComboBox;
    @UiField SimpleCheckBox isRemoteCheckbox;
    @UiField TextBox remoteIpTextBox;
    @UiField TextBox versionTextBox;

    @UiField Button btnLive;
    @UiField Button btnSave;
    @UiField Button btnCancel;

    // capabilities
    @UiField Button btnAddCapability;
    @UiField Button btnEditCapability;
    @UiField Button btnRemoveCapability;

    @UiField(provided = true)
    SingleSelectionDataGrid<Capability> capabilitiesCellTable;

    @UiField Label editedResourceLabel = new Label();

    @UiField(provided = true)
    UserExtensionsEditor userExtensionsEditor;

    @Inject
    public BrowserProfileDocument(@DocumentPanel DocumentHolder documentHolder,
                                  BrowserProfileCommandFactory commandFactory,
                                  BrowserProfileDocumentFactory documentFactory,
                                  LiveBrowserCommandFactory liveBrowserCommandFactory,
                                  BrowserProfilesHolder browserProfilesHolder,
                                  CommandExecutor commandExecutor,
                                  ValidatorFactory validators,
                                  @Assisted BrowserProfile browserProfile) {
        super(documentHolder);

        this.browserProfile = browserProfile;
        this.commandFactory = commandFactory;
        this.documentFactory = documentFactory;
        this.liveBrowserCommandFactory = liveBrowserCommandFactory;
        this.browserProfilesHolder = browserProfilesHolder;
        this.commandExecutor = commandExecutor;
        this.validators = validators;

        this.lastSuccessfulySavedId = browserProfile.getId();

        // FIXME: these settings should be set in the template
        createBrowserListBox();
        createCapabilitiesCellTable();
        createUserExtensionsEditor(browserProfile);

        initWidget(uiBinder.createAndBindUi(this));

        writeObjectToInputs();

        editedResourceLabel.setText( this.lastSuccessfulySavedId );

        initializeEvents();
    }

    private void createUserExtensionsEditor(final BrowserProfile browserProfile) {
        userExtensionsHolder = new UserExtensionsStringHolder(browserProfile.getUserExtensions());
        userExtensionsEditor = documentFactory.createUserExtensionsEditor(userExtensionsHolder);
    }

    private void createBrowserListBox() {
        browserComboBox = new ListBox();

        browserComboBox.addItem("Internet Explorer", "ie");
        browserComboBox.addItem("Firefox", "firefox");
        browserComboBox.addItem("Chrome", "chrome");
        browserComboBox.addItem("Opera", "opera");
        browserComboBox.addItem("Android", "android");
        browserComboBox.addItem("Safari", "safari");
    }

    private void createCapabilitiesCellTable() {
        capabilitiesCellTable = new SingleSelectionDataGrid<Capability>(browserProfile.getCapabilities());
        capabilitiesCellTable.setHeight("100%");

        Column<Capability, String> nameColumn = new Column<Capability, String>(
                new TextCell()) {
            @Override
            public String getValue(Capability object) {
                return object.getName();
            }
        };
        capabilitiesCellTable.addColumn(nameColumn, "Name");

        Column<Capability, String> valueColumn = new Column<Capability, String>(
                new TextCell()) {
            @Override
            public String getValue(Capability object) {
                return object.getValue();
            }
        };
        capabilitiesCellTable.addColumn(valueColumn, "Value");
    }

    private void initializeEvents() {
        profileSavedEventHandler = new CustomEventAware<SaveEvent>() {
            @Override
            public void onCustomEvent(SaveEvent event) {
                if (browserProfile == event.getProfile()) {
                    lastSuccessfulySavedId = browserProfile.getId();
                    editedResourceLabel.setText( lastSuccessfulySavedId );
                    updateDocumentLabel();
                }
            }
        };

        browserProfilesHolder.addListener(SaveEvent.class, profileSavedEventHandler);

        isRemoteCheckbox.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (isRemoteCheckbox.getValue()) {
                    remoteIpTextBox.setEnabled(true);
                    remoteIpTextBox.setFocus(true);
                } else {
                    remoteIpTextBox.setEnabled(false);
                }
            }
        });

        btnSave.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                readObjectFromInputs(false);

                ProfileSaveCommand command = commandFactory.createProfileSaveCommand(browserProfile, lastSuccessfulySavedId);
                commandExecutor.execute(command);
            }
        });

        btnCancel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BrowserProfileDocument.this.close();
            }
        });

        btnAddCapability.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                documentFactory.createCapabilityDialogBox(
                        BrowserProfileDocument.this,
                        browserProfile,
                        null).show();
            }
        });

        capabilitiesCellTable.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent selectionChangeEvent) {
                btnEditCapability.setEnabled(true);
                btnRemoveCapability.setEnabled(true);
            }
        });

        btnLive.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                readObjectFromInputs(true);

                StartLiveBrowserCommand startLiveBrowserCommand =
                        liveBrowserCommandFactory.createStartLiveBrowserCommand(browserProfile);
                commandExecutor.execute(startLiveBrowserCommand);
            }
        });

        btnEditCapability.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                documentFactory.createCapabilityDialogBox(
                        BrowserProfileDocument.this,
                        browserProfile,
                        getSelectedCapability()).show();
            }
        });

        btnRemoveCapability.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                removeSelectedCapability();
                refreshCapabilities();
                btnEditCapability.setEnabled(false);
                btnRemoveCapability.setEnabled(false);
            }
        });
    }

    @Override
    public void preDestroy() {
        browserProfilesHolder.removeListener(SaveEvent.class, profileSavedEventHandler);
    }

    @Override
    public String getDocumentName() {
        if (lastSuccessfulySavedId == null) {
            return "New Profile";
        } else {
            return lastSuccessfulySavedId;
        }
    }

    private Long getRemotePort(String value) {
        MatchResult matchResult = IP_PORT_REGEXP.exec(value);

        if (matchResult == null || isEmpty(matchResult.getGroup(REGEXP_PORT_GROUP_INDEX))) {
            return null;
        }

        return Long.parseLong(matchResult.getGroup(REGEXP_PORT_GROUP_INDEX));
    }

    private boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    private void readObjectFromInputs(boolean excludeNameValidation) {
        UIValidation validations = validators.createNewValidation();

        if (!excludeNameValidation) {
            validations.on(nameTextBox)
                    .check(validators.generic(), nameTextBox.getValue(), "Name of the profile should not be empty.")
                    .done();
        }

        validations.on(versionTextBox)
                .check(validators.version(), versionTextBox.getValue(), "Invalid version for the profile.")
                .done();

        if (isRemoteCheckbox.getValue()) {
            validations.on(remoteIpTextBox)
                    .check(validators.remoteIp(), remoteIpTextBox.getValue(), "Invalid remote address.")
                    .done();
        } else {
            validations.on(remoteIpTextBox).done();
        }

        if (validations.isFailed()) {
            throw new IllegalArgumentException("Invalid profile.");
        }

        browserProfile.setId(nameTextBox.getValue());
        browserProfile.setBrowser(getSelectedBrowser());
        browserProfile.setVersion(versionTextBox.getValue());
        if (isRemoteCheckbox.getValue()) {
            browserProfile
                    .setRemoteIp(getRemoteIp(remoteIpTextBox.getValue()));
            browserProfile.setRemotePort(getRemotePort(remoteIpTextBox
                    .getValue()));
        } else {
            browserProfile.setRemoteIp(null);
            browserProfile.setRemotePort(null);
        }

        userExtensionsHolder.writeToTargetList();
    }

    private void writeObjectToInputs() {
        nameTextBox.setText(browserProfile.getId());
        browserComboBox.setSelectedIndex(getBrowserIndex());
        versionTextBox.setText(browserProfile.getVersion());
        if (browserProfile.isLocalHost()) {
            isRemoteCheckbox.setValue(false);
            remoteIpTextBox.setEnabled(false);
        } else {
            isRemoteCheckbox.setValue(true);
            remoteIpTextBox.setEnabled(true);
            remoteIpTextBox.setValue(getRemoteAddress(browserProfile));
        }
    }

    private String getSelectedBrowser() {
        if (browserComboBox.getSelectedIndex() < 0) {
            return null;
        }
        return browserComboBox.getValue(browserComboBox.getSelectedIndex());
    }

    private int getBrowserIndex() {
        String browserValue = browserProfile.getBrowser();

        for (int i = 0; i < browserComboBox.getItemCount(); i++) {
            String comboValue = browserComboBox.getValue(i);
            if (comboValue.equals(browserValue)) {
                return i;
            }
        }

        return -1;
    }

    private String getRemoteIp(String value) {
        MatchResult matchResult = IP_PORT_REGEXP.exec(value);

        return matchResult != null ? matchResult
                .getGroup(REGEXP_IP_GROUP_INDEX) : null;
    }

    private String getRemoteAddress(BrowserProfile browserProfile) {
        if (browserProfile.isLocalHost()) {
            return "";
        }

        if (browserProfile.getRemotePort() != null) {
            return browserProfile.getRemoteIp() + ":"
                    + browserProfile.getRemotePort();
        }

        return browserProfile.getRemoteIp();
    }

    @Override
    public void refreshCapabilities() {
        capabilitiesCellTable.setRowData( browserProfile.getCapabilities() );
    }

    private Capability getSelectedCapability() {
        return capabilitiesCellTable.getSelectedObject();
    }

    private void removeSelectedCapability() {
        browserProfile.getCapabilities().remove(getSelectedCapability());
    }
}
