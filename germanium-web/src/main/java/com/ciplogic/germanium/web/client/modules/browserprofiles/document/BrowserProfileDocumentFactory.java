package com.ciplogic.germanium.web.client.modules.browserprofiles.document;

import com.ciplogic.germanium.web.shared.vo.UserExtensionsHolder;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.Capability;

public interface BrowserProfileDocumentFactory {
    BrowserProfileDocument createBrowserProfileDocument(BrowserProfile browserProfile);

    // FIXME: this should be in its own package?
    UserExtensionsEditor createUserExtensionsEditor(UserExtensionsHolder userExtensionsHolder);

    CreateCapabilityDialogBox createCapabilityDialogBox(CapabilityChangeAware browserProfileDocument, BrowserProfile browserProfile, Capability selectedCapability);
}
