package com.ciplogic.germanium.web.client.modules.browserprofiles.document;

public interface CapabilityChangeAware {
    public void refreshCapabilities();
}
