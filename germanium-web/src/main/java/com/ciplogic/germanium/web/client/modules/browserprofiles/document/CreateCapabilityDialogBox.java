package com.ciplogic.germanium.web.client.modules.browserprofiles.document;

import com.ciplogic.germanium.web.client.modules.validators.UIValidation;
import com.ciplogic.germanium.web.client.modules.validators.ValidatorFactory;
import com.ciplogic.gwtui.dialog.ResizableDialogBox;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.Capability;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class CreateCapabilityDialogBox extends ResizableDialogBox {

    private Button btnAdd;
    private Button btnCancel;

    private TextBox nameTextBox;
    private TextBox valueTextBox;

    private BrowserProfile browserProfile;
    private Capability oldCapability;
    private Capability editedCapability;

    private CapabilityChangeAware parent;
    private ValidatorFactory validators;

    @Inject
    public CreateCapabilityDialogBox(
            ValidatorFactory validators,
            @Assisted CapabilityChangeAware parent,
            @Assisted BrowserProfile browserProfile,
            @Assisted Capability oldCapability) {
    	setGlassEnabled(true);
    	setHTML("Capability...");

        this.validators = validators;

        this.browserProfile = browserProfile;
        this.oldCapability = oldCapability;
        this.parent = parent;

        if (oldCapability != null) {
            this.editedCapability = oldCapability.copy();
        } else {
            this.editedCapability = new Capability();
        }

        createWidgets();
        writeObjectToInputs();
        createActions();

        center();
    }

    private void createWidgets() {
        FlexTable flexTable = new FlexTable();
        setWidget(flexTable);
        flexTable.setSize("100%", "100%");

        Label lblName = new Label("Name:");
        lblName.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        flexTable.setWidget(0, 0, lblName);

        nameTextBox = new TextBox();
        flexTable.setWidget(0, 1, nameTextBox);

        Label lblValue = new Label("Value:");
        lblValue.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        flexTable.setWidget(1, 0, lblValue);

        valueTextBox = new TextBox();
        flexTable.setWidget(1, 1, valueTextBox);

        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.setSpacing(2);
        flexTable.setWidget(2, 1, horizontalPanel);

        btnAdd = new Button(oldCapability != null ? "Change" : "Add");
        horizontalPanel.add(btnAdd);

        btnCancel = new Button("Cancel");
        horizontalPanel.add(btnCancel);
    }

    private void createActions() {
        btnAdd.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                readObjectFromInputs();
                updateCapabilitiesList();
                hideDialog();
            }
        });

        btnCancel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                hideDialog();
            }
        });
    }

    private void writeObjectToInputs() {
        nameTextBox.setText( editedCapability.getName() );
        valueTextBox.setText( editedCapability.getValue() );
    }

    private void readObjectFromInputs() {
        UIValidation validation = validators.createNewValidation();

        validation.on(nameTextBox)
                .check(validators.generic(), nameTextBox.getText(), "The name of the capability should not be empty.")
                .done();

        if (validation.isFailed()) {
            throw new IllegalArgumentException();
        }

        editedCapability.setName(nameTextBox.getText());
        editedCapability.setValue(valueTextBox.getText());
    }

    private void updateCapabilitiesList() {
        if (oldCapability != null) {
            int index = browserProfile.getCapabilities().indexOf(oldCapability);
            browserProfile.getCapabilities().set(index, editedCapability);
        } else {
            browserProfile.getCapabilities().add(editedCapability);
        }
        parent.refreshCapabilities();
    }

    private void hideDialog() {
        this.removeFromParent();
        this.hide();
    }

}
