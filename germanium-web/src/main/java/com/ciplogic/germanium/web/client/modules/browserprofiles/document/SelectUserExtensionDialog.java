package com.ciplogic.germanium.web.client.modules.browserprofiles.document;

import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.gwtui.tree.AsynchronousTree;
import com.ciplogic.germanium.web.client.views.ResourceTreeViewModel;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;

public class SelectUserExtensionDialog extends DialogBox {
    interface MyUiBinder extends UiBinder<VerticalPanel, SelectUserExtensionDialog> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private UserExtensionsEditor userExtensionsEditor;

    @UiField(provided = true)
    AsynchronousTree<FileVO> userExtensionsTree;

    @UiField
    Button addButton;

    @UiField
    Button cancelButton;

    public SelectUserExtensionDialog(UserExtensionsEditor userExtensionsEditor) {
        this.userExtensionsEditor = userExtensionsEditor;

        setGlassEnabled(true);
        setHTML("Add user-extension...");

        createUserExtensionsTree();
        setWidget(uiBinder.createAndBindUi(this));

        center();

        initializeEvents();
    }

    private void createUserExtensionsTree() {
        FileVO userExtensionRoot = new FileVO(null, "/", FileVO.Category.USER_EXTENSION);
        userExtensionsTree = new AsynchronousTree<FileVO>(new ResourceTreeViewModel(userExtensionRoot));
    }

    private void initializeEvents() {
        userExtensionsTree.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent selectionChangeEvent) {
                FileVO selectedNode = userExtensionsTree.getSelectedNode();
                if (selectedNode != null) {
                    boolean folderSelected = selectedNode.isDirectory();
                    addButton.setEnabled(! folderSelected);
                }
            }
        });

        addButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                FileVO selectedFile = userExtensionsTree.getSelectedNode();

                if (selectedFile.isDirectory()) {
                    Notifier.showError("Unable to add user extension since it's not a folder.");
                    return;
                }

                userExtensionsEditor.addUserExtension(selectedFile);

                closeDialog();
            }
        });

        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                closeDialog();
            }
        });
    }

    private void closeDialog() {
        this.removeFromParent();
    }
}
