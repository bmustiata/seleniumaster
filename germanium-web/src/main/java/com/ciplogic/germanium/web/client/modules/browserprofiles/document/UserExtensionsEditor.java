package com.ciplogic.germanium.web.client.modules.browserprofiles.document;

import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.gwtui.datagrid.SingleSelectionDataGrid;
import com.ciplogic.germanium.web.client.modules.resources.commands.ResourceEditCommand;
import com.ciplogic.germanium.web.client.modules.resources.commands.ResourceEditCommandFactory;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.UserExtensionsHolder;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import java.util.List;

public class UserExtensionsEditor extends Composite {
    private UserExtensionsHolder userExtensionsHolder;
    private ResourceEditCommandFactory resourceEditCommandFactory;
    private CommandExecutor commandExecutor;

    interface MyUiBinder extends UiBinder<HorizontalPanel, UserExtensionsEditor> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField(provided = true)
    SingleSelectionDataGrid<FileVO> userExtensionsDataGrid;

    @UiField
    Button addButton;

    @UiField
    Button removeButton;

    @UiField
    Button moveUpButton;

    @UiField
    Button moveDownButton;

    @UiField
    Button editButton;

    @Inject
    public UserExtensionsEditor(
            ResourceEditCommandFactory resourceEditCommandFactory,
            CommandExecutor commandExecutor,
            @Assisted UserExtensionsHolder userExtensionsHolder) {
        this.userExtensionsHolder = userExtensionsHolder;
        this.resourceEditCommandFactory = resourceEditCommandFactory;
        this.commandExecutor = commandExecutor;

        createUserExtensionsDataGrid();

        initWidget(uiBinder.createAndBindUi(this));

        initializeEvents();
    }

    public void createUserExtensionsDataGrid() {
        userExtensionsDataGrid = new SingleSelectionDataGrid<FileVO>(userExtensionsHolder.getUserExtensions());

        Column<FileVO, String> nameColumn = new Column<FileVO, String>(new TextCell()) {
            @Override
            public String getValue(FileVO object) {
                return object.getName();
            }
        };
        userExtensionsDataGrid.addColumn(nameColumn, "Name");

        refreshRowData();
    }

    private void refreshRowData() {
        userExtensionsDataGrid.setRowData(userExtensionsHolder.getUserExtensions());
    }

    private void initializeEvents() {
        userExtensionsDataGrid.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent selectionChangeEvent) {
                boolean selectedFile = userExtensionsDataGrid.getSelectedObject() != null;
                setButtonsEnabled(selectedFile);
            }
        });

        addButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
            new SelectUserExtensionDialog(UserExtensionsEditor.this).show();
            }
        });

        removeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                int selectedFileIndex = userExtensionsDataGrid.getSelectedIndex();

                if (selectedFileIndex >= 0) {
                    userExtensionsHolder.getUserExtensions().remove(selectedFileIndex);
                }

                refreshRowData();

                setButtonsEnabled(false);
            }
        });

        moveUpButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if (isFirstFileSelected()) {
                    Notifier.showInfo("Unable to move up the first element of the user extensions.");
                    return;
                }

                moveUserExtensionByOffset(-1);
            }
        });

        moveDownButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if (isLastFileSelected()) {
                    Notifier.showInfo("Unable to move down the last element of the user extensions.");
                    return;
                }

                moveUserExtensionByOffset(1);
            }
        });



        editButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ResourceEditCommand editResourceCommand = resourceEditCommandFactory.createResourceEditCommand(
                        userExtensionsDataGrid.getSelectedObject());
                commandExecutor.execute(editResourceCommand);
            }
        });
    }

    private void moveUserExtensionByOffset(int offset) {
        List<FileVO> userExtensions = userExtensionsHolder.getUserExtensions();

        swapCurrentUserExtensionByOffset(userExtensions, offset);

        refreshRowData();

        setButtonsEnabled(true);
    }

    private void swapCurrentUserExtensionByOffset(List<FileVO> userExtensions, int offset) {
        int index = userExtensionsDataGrid.getSelectedIndex();
        FileVO temp = userExtensions.get(index);
        userExtensions.set(index, userExtensions.get(index + offset));
        userExtensions.set(index + offset, temp);
    }

    private void setButtonsEnabled(boolean selectedFile) {
        removeButton.setEnabled( selectedFile );
        moveUpButton.setEnabled( selectedFile && ! isFirstFileSelected() );
        moveDownButton.setEnabled( selectedFile && ! isLastFileSelected() );
        editButton.setEnabled( selectedFile );
    }

    private boolean isLastFileSelected() {

        return userExtensionsDataGrid.getSelectedIndex() ==
                userExtensionsDataGrid.getRowCount() - 1;
    }

    private boolean isFirstFileSelected() {
        return userExtensionsDataGrid.getSelectedIndex() == 0;
    }

    public void addUserExtension(FileVO selectedFile) {
        userExtensionsHolder.getUserExtensions().add(selectedFile);
        refreshRowData();

        setButtonsEnabled( userExtensionsDataGrid.getSelectedObject() != null );
    }

}
