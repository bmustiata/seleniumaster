package com.ciplogic.germanium.web.client.modules.browserprofiles.document;

import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.UserExtensionsHolder;

import java.util.ArrayList;
import java.util.List;

public class UserExtensionsStringHolder implements UserExtensionsHolder {
    private List<FileVO> userExtensions;
    private List<String> targetList;

    public UserExtensionsStringHolder(List<String> targetList) {
        this.targetList = targetList;
        userExtensions = new ArrayList<FileVO>();

        for (String fileName : targetList) {
            userExtensions.add(new FileVO(fileName, FileVO.Category.USER_EXTENSION));
        }
    }

    @Override
    public List<FileVO> getUserExtensions() {
        return userExtensions;
    }

    public void writeToTargetList() {
        targetList.clear();

        for (FileVO file : userExtensions) {
            targetList.add( file.fullPath() );
        }
    }
}
