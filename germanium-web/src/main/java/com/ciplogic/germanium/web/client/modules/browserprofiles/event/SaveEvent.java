package com.ciplogic.germanium.web.client.modules.browserprofiles.event;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

public class SaveEvent {
    private BrowserProfile profile;

    public SaveEvent(BrowserProfile newProfile) {
        this.profile = newProfile;
    }

    public BrowserProfile getProfile() {
        return profile;
    }

    public void setProfile(BrowserProfile profile) {
        this.profile = profile;
    }
}
