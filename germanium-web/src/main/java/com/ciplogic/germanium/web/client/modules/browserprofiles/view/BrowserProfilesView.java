package com.ciplogic.germanium.web.client.modules.browserprofiles.view;

import com.ciplogic.germanium.web.client.command.Command;
import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.gwtui.datagrid.SingleSelectionDataGrid;
import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesChangeAware;
import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesHolder;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.BrowserProfileCommandFactory;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileEditCommand;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileNewCommand;
import com.ciplogic.germanium.web.client.modules.browserprofiles.commands.ProfileRefreshCommand;
import com.ciplogic.germanium.web.client.modules.browserprofiles.view.util.ImageColumn;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.livebrowser.commands.LiveBrowserCommandFactory;
import com.ciplogic.germanium.web.client.modules.livebrowser.commands.StartLiveBrowserCommand;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

public class BrowserProfilesView extends Composite {
    private DocumentHolder documentHolder;
    private BrowserProfilesHolder browserProfilesHolder;
    private BrowserProfileCommandFactory browserProfileCommandFactory;
    private LiveBrowserCommandFactory liveBrowserCommandFactory;
    private CommandExecutor commandExecutor;

    interface MyUiBinder extends UiBinder<VerticalPanel, BrowserProfilesView> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField(provided = true)
    SingleSelectionDataGrid<BrowserProfile> cellTable;

    @UiField Button btnNew;

    @UiField Button btnEdit;

    @UiField Button btnLive;

    @UiField Button btnDelete;

    @Inject
    public BrowserProfilesView(@DocumentPanel DocumentHolder documentHolder,
                               BrowserProfilesHolder browserProfilesHolder,
                               BrowserProfileCommandFactory browserProfileCommandFactory,
                               LiveBrowserCommandFactory liveBrowserCommandFactory,
                               CommandExecutor commandExecutor) {
        this.documentHolder = documentHolder;
        this.browserProfilesHolder = browserProfilesHolder;
        this.browserProfileCommandFactory = browserProfileCommandFactory;
        this.liveBrowserCommandFactory = liveBrowserCommandFactory;
        this.commandExecutor = commandExecutor;

        createDataTable();

        initWidget(uiBinder.createAndBindUi(this));

        initializeEvents();
    }

    private Composite createDataTable() {
        cellTable = new SingleSelectionDataGrid<BrowserProfile>(createDataList());

        ImageColumn<BrowserProfile> imageColumn = new ImageColumn<BrowserProfile>() {
            @Override
            protected Object getImage(BrowserProfile item) {
                return "images/icons/" + item.getBrowser() + "24.png";
            }
        };
        cellTable.addColumn(imageColumn, "", "44px");
        imageColumn.setCellStyleNames("noPadding");

        cellTable.addColumn(new TextColumn<BrowserProfile>() {
            @Override
            public String getValue(BrowserProfile object) {
                return object.getId();
            }
        }, "Name");

        return cellTable;
    }

    private List<BrowserProfile> createDataList() {
        List<BrowserProfile> data = new ArrayList<BrowserProfile>();

        data.add(new BrowserProfile("ie", "Loading..."));

        return data;
    }


    private void initializeEvents() {
        browserProfilesHolder.addListener(new BrowserProfilesChangeAware() {
            @Override
            public void onBrowserProfilesChange() {
                cellTable.setRowData(browserProfilesHolder.getBrowserProfiles());
            }
        });

        ProfileRefreshCommand profileRefreshCommand = browserProfileCommandFactory.createProfileRefreshCommand();
        commandExecutor.execute(profileRefreshCommand);

        btnNew.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
            ProfileNewCommand profileNewCommand = browserProfileCommandFactory.createProfileNewCommand();
            commandExecutor.execute(profileNewCommand);
            }
        });

        btnDelete.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BrowserProfile selectedProfile = getSelectedProfile();
                Command deleteCommand = browserProfileCommandFactory.createProfileDeleteCommand(selectedProfile);
                commandExecutor.execute(deleteCommand);
            }
        });

        btnEdit.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BrowserProfile selectedProfile = getSelectedProfile();
                ProfileEditCommand profileEditCommand = browserProfileCommandFactory.createProfileEditCommand(selectedProfile);
                commandExecutor.execute(profileEditCommand);
            }
        });

        btnLive.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BrowserProfile selectedProfile = getSelectedProfile();
                StartLiveBrowserCommand startLiveBrowserCommand =
                        liveBrowserCommandFactory.createStartLiveBrowserCommand(selectedProfile);
                commandExecutor.execute(startLiveBrowserCommand);
            }
        });
    }

    private BrowserProfile getSelectedProfile() {
        return cellTable.getSelectedObject();
    }
}