package com.ciplogic.germanium.web.client.modules.browserprofiles.view.util;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.ImageResourceRenderer;

class ClickableImageResourceCell extends AbstractCell<ImageResource> {
    private static ImageResourceRenderer renderer;

    private ImageResourceColumn parent;

    ClickableImageResourceCell() {
        super("click");

        if (renderer == null) {
            renderer = new ImageResourceRenderer();
        }
    }

    @Override
    public void onBrowserEvent(Context context, Element parentElement, ImageResource value, NativeEvent event, ValueUpdater<ImageResource> valueUpdater) {
        if ("click".equals(event.getType())) {
            parent.onClick(context);
        }
    }

    @Override
    public void render(Context context, ImageResource value, SafeHtmlBuilder sb) {
        if (value != null) {
            sb.append(renderer.render(value));
        }
    }

    public ImageResourceColumn getParent() {
        return parent;
    }

    public void setParent(ImageResourceColumn parent) {
        this.parent = parent;
    }
}
