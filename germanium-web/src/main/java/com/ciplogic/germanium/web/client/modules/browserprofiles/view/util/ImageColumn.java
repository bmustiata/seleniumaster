package com.ciplogic.germanium.web.client.modules.browserprofiles.view.util;

import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.user.cellview.client.Column;

public abstract class ImageColumn<T> extends Column {
	public ImageColumn() {
		super(new ImageCell());
	}

	@Override
	public Object getValue(Object object) {
		return getImage((T) object);
	}
	
	protected abstract Object getImage(T item);
}
