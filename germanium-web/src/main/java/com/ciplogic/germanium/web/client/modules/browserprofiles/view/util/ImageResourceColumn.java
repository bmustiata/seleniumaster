package com.ciplogic.germanium.web.client.modules.browserprofiles.view.util;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.Column;

public abstract class ImageResourceColumn<T> extends Column {

    public ImageResourceColumn() {
        super(new ClickableImageResourceCell());
        ((ClickableImageResourceCell)this.getCell()).setParent(this);
    }

    protected void onClick(Cell.Context context) {
    }

    @Override
    public Object getValue(Object object) {
        return getImage((T) object);
    }

    protected abstract ImageResource getImage(T item);
}
