package com.ciplogic.germanium.web.client.modules.documents;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;

/**
 * A document that removes the tab when the document is being closed.
 */
public abstract class AbstractDocument extends Composite implements Document {
    protected DocumentHolder documentHolder;
    protected boolean changed;

    protected AbstractDocument(@DocumentPanel DocumentHolder documentHolder) {
        this.documentHolder = documentHolder;
    }

    @Override
    public void close() {
        if (isChanged()) {
            if (Window.confirm("Document was changed. Confirm closing without saving?")) {
                closeTab();
            }
        } else {
            closeTab();
        }
    }

    public abstract void preDestroy();

    @Override
    public void save() {
        setChanged(false);
    }

    @Override
    public boolean isChanged() {
        return changed;
    }

    @Override
    public void setChanged(boolean changed) {
        this.changed = changed;
        updateDocumentLabel();
    }

    private void closeTab() {
        documentHolder.remove(this);
        preDestroy();
    }

    protected void updateDocumentLabel() {
        documentHolder.setDocumentTitle(this, computeLabel());
    }

    private String computeLabel() {
        return getDocumentName() + (isChanged() ? "*" : "");
    }
}
