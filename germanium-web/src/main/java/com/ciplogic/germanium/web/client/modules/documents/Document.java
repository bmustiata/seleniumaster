package com.ciplogic.germanium.web.client.modules.documents;

public interface Document {
    /**
     * This method is called when the document is supposed to close.
     */
    void close();

    /**
     * This method is called when the document is supposed to be saved.
     */
    void save();

    /**
     * This property returns true when the document is changed.
     * @return is document changed or not since the last save.
     */
    boolean isChanged();

    void setChanged(boolean changed);

    /**
     * Retuns the document name used for displaying purposes.
     * @return
     */
    String getDocumentName();
}
