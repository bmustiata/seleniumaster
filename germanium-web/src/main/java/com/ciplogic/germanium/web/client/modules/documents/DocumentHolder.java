package com.ciplogic.germanium.web.client.modules.documents;

public interface DocumentHolder {
    void remove(Document document);
    void setDocumentTitle(Document document, String title);

    void add(Document document);
}
