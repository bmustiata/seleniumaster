package com.ciplogic.germanium.web.client.modules.documents;

import com.ciplogic.germanium.web.client.modules.documents.impl.DocumentHolderImpl;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;

public class DocumentModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind(DocumentHolder.class)
                .annotatedWith(DocumentPanel.class)
                .to(DocumentHolderImpl.class)
                .in(Singleton.class);
    }
}
