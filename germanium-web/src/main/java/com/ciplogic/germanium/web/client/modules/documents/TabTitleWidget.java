package com.ciplogic.germanium.web.client.modules.documents;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class TabTitleWidget extends Widget {
    interface MyUiBinder extends UiBinder<HTMLPanel, TabTitleWidget> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    interface Resources extends ClientBundle {
        @Source("close-tab.png")
        ImageResource closeTab();
    }

    @UiField
    SpanElement tabTitleTextSpan;

    @UiField(provided = true)
    Image tabTitleClose;

    public static void addDocument(TabLayoutPanel tabPanel, AbstractDocument document) {
        TabTitleWidget tabWidget = new TabTitleWidget(document, document.getDocumentName());
        tabPanel.add(document, tabWidget);
    }

    private TabTitleWidget(final Document document, String title) {
        Resources resources = GWT.create(Resources.class);

        tabTitleClose = new Image( resources.closeTab() );

        setElement( uiBinder.createAndBindUi(this).getElement() );

        tabTitleTextSpan.setInnerText(title);

        initializeEvents(document);
    }

    public void setTitle(String newTitle) {
        tabTitleTextSpan.setInnerText(newTitle);
    }

    private void initializeEvents(final Document document) {
        Event.sinkEvents(tabTitleClose.getElement(), Event.ONCLICK);
        Event.setEventListener(tabTitleClose.getElement(), new EventListener() {
            @Override
            public void onBrowserEvent(Event event) {
                if (Event.ONCLICK == event.getTypeInt()) {
                    document.close();
                }
            }
        });
    }
}
