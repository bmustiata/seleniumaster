package com.ciplogic.germanium.web.client.modules.documents.impl;

import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.Document;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.TabTitleWidget;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

public class DocumentHolderImpl extends TabLayoutPanel implements DocumentHolder {
    @Inject
    public DocumentHolderImpl() {
        super(32, Style.Unit.PX);
    }

    @Override
    public void add(Document document) {
        TabTitleWidget.addDocument(this, (AbstractDocument) document);
        ((Widget) document).setSize("100%", "100%");

        this.selectTab((Widget) document);
    }

    @Override
    public void remove(Document document) {
        this.remove((Widget) document);
    }

    @Override
    public void setDocumentTitle(Document document, String title) {
        TabTitleWidget titleWidget = (TabTitleWidget) getTabWidget((Widget) document);
        titleWidget.setTitle(title);
    }
}
