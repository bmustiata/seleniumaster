package com.ciplogic.germanium.web.client.modules.livebrowser;

import com.ciplogic.germanium.web.client.modules.livebrowser.commands.LiveBrowserCommandFactory;
import com.ciplogic.germanium.web.client.modules.livebrowser.commands.StartLiveBrowserCommand;
import com.ciplogic.germanium.web.client.modules.livebrowser.commands.impl.StartLiveBrowserCommandImpl;
import com.ciplogic.germanium.web.client.modules.livebrowser.document.LiveBrowserDocumentFactory;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.GinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;

public class LiveBrowserModule extends AbstractGinModule {
    @Override
    protected void configure() {
        GinModule commandsFactoryModule = new GinFactoryModuleBuilder()
                .implement(StartLiveBrowserCommand.class, StartLiveBrowserCommandImpl.class)
                .build(LiveBrowserCommandFactory.class);

        install(commandsFactoryModule);

        GinModule documentFactoryModule = new GinFactoryModuleBuilder()
                .build(LiveBrowserDocumentFactory.class);

        install(documentFactoryModule);}
}
