package com.ciplogic.germanium.web.client.modules.livebrowser.commands;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

public interface LiveBrowserCommandFactory {
    StartLiveBrowserCommand createStartLiveBrowserCommand(BrowserProfile browserProfile);
}
