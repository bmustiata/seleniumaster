package com.ciplogic.germanium.web.client.modules.livebrowser.commands;

import com.ciplogic.germanium.web.client.command.Command;

public interface StartLiveBrowserCommand extends Command {
}
