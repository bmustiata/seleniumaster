package com.ciplogic.germanium.web.client.modules.livebrowser.commands.impl;

import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.livebrowser.commands.StartLiveBrowserCommand;
import com.ciplogic.germanium.web.client.modules.livebrowser.document.LiveBrowserDocumentFactory;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.LiveBrowserService;
import com.ciplogic.germanium.web.shared.vo.LiveSessionVO;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class StartLiveBrowserCommandImpl implements StartLiveBrowserCommand {
    private DocumentHolder documentHolder;
    private LiveBrowserDocumentFactory liveBrowserDocumentFactory;
    private BrowserProfile browserProfile;
    private LiveBrowserService liveBrowserService;

    @Inject
    public StartLiveBrowserCommandImpl(
            @DocumentPanel DocumentHolder documentHolder,
            LiveBrowserDocumentFactory liveBrowserDocumentFactory,
            LiveBrowserService liveBrowserService,
            @Assisted BrowserProfile browserProfile) {
        this.documentHolder = documentHolder;
        this.liveBrowserDocumentFactory = liveBrowserDocumentFactory;
        this.liveBrowserService = liveBrowserService;
        this.browserProfile = browserProfile;
    }

    @Override
    public void execute() {
        REST.withCallback(new DefaultMethod<LiveSessionVO>() {
            @Override
            public void onSuccess(Method method, LiveSessionVO liveSessionVO) {
                documentHolder.add(liveBrowserDocumentFactory.createLiveBrowserDocument(liveSessionVO));
            }
        }).call(liveBrowserService).startSession(browserProfile);

    }
}
