package com.ciplogic.germanium.web.client.modules.livebrowser.document;

import java.util.HashMap;
import java.util.Map;

public class IdGenerator {
    private String prefix;

    private static Map<String, Integer> generatedIndexes = new HashMap<String, Integer>();

    public IdGenerator(String prefix) {
        this.prefix = prefix;
    }

    public String getNextIndex() {
        Integer currentIndex = generatedIndexes.get(prefix);
        if (currentIndex == null) {
            currentIndex = 0;
        }

        currentIndex++;

        generatedIndexes.put(prefix, currentIndex);

        return prefix + currentIndex;
    }

    public void releaseIndex() {
    }
}
