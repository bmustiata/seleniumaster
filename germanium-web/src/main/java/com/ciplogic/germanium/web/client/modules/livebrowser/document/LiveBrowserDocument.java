package com.ciplogic.germanium.web.client.modules.livebrowser.document;

import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.gwtui.editor.CodeTextEditor;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.LiveBrowserService;
import com.ciplogic.germanium.web.shared.vo.JSCallResultVO;
import com.ciplogic.germanium.web.shared.vo.LiveSessionVO;
import com.ciplogic.gwtui.ProgrammingLanguage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.REST;

import static com.ciplogic.gwtui.ProgrammingLanguage.*;

public class LiveBrowserDocument extends AbstractDocument {
    public static final int WEBDRIVER_SCRIPT_CALL = 0;
    public static final int JS_EVAL_CALL = 1;

    interface MyUiBinder extends UiBinder<SplitLayoutPanel, LiveBrowserDocument> {}
	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private Integer selectedCodeTabPanel = WEBDRIVER_SCRIPT_CALL;

    @UiField LogView logView;

    @UiField Button closeButton;

    @UiField Button executeButton;

    @UiField Button clearLog;

    @UiField CodeTextEditor webDriverCode;

    @UiField CodeTextEditor jsEvalCode;

    @UiField ListBox programmingListBox;

    @UiField TabLayoutPanel codeTabPanel;

    private LiveSessionVO liveSessionVO;

    private LiveBrowserService liveBrowserService = GWT.create(LiveBrowserService.class);

    private IdGenerator idGenerator;

    @Inject
    public LiveBrowserDocument(
            @DocumentPanel DocumentHolder documentHolder,
            @Assisted LiveSessionVO liveSessionVO) {
		super(documentHolder);

        this.liveSessionVO = liveSessionVO;
		initWidget(uiBinder.createAndBindUi(this));

        // FIXME: normally this should be done with a server call, and an application bean
        // that knows what languages are supported by the server.
        initializeProgrammingLanguageSupport();

        initializeEvents();

        idGenerator = new IdGenerator(liveSessionVO.getBrowserProfile().getId() + " #");
	}

    private void initializeProgrammingLanguageSupport() {
        programmingListBox.addItem("Groovy", GROOVY.toString());
        programmingListBox.addItem("JavaScript", JAVASCRIPT.toString());
        programmingListBox.addItem("Python", PYTHON.toString());
        programmingListBox.addItem("Ruby", RUBY.toString());
    }

    @Override
    public void close() {
        REST.withCallback(new DefaultMethod<Object>() {
            @Override
            public void onSuccess(Method method, Object o) {
                LiveBrowserDocument.super.close();
            }
        }).call(liveBrowserService).stopSession(liveSessionVO.getId());
    }

    private void initializeEvents() {
        executeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                logExecutedCode();

                MethodCallback<JSCallResultVO> callback = createSuccessCallback();

                if (selectedCodeTabPanel == WEBDRIVER_SCRIPT_CALL) {
                    callWebDriverScript(callback);
                } else if (selectedCodeTabPanel == JS_EVAL_CALL) {
                    callJSEval(callback);
                }
            }
        });

        clearLog.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                logView.clear();
            }
        });

        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                LiveBrowserDocument.this.close();
            }
        });

        programmingListBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                String selectedProgrammingLanguageString = programmingListBox.getValue(
                        programmingListBox.getSelectedIndex());
                webDriverCode.setProgrammingLanguage(
                        ProgrammingLanguage.valueOf(
                            selectedProgrammingLanguageString));
            }
        });

        codeTabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
            @Override
            public void onSelection(SelectionEvent<Integer> integerSelectionEvent) {
                selectedCodeTabPanel = integerSelectionEvent.getSelectedItem();
            }
        });
    }

    private void logExecutedCode() {
        String code = selectedCodeTabPanel == WEBDRIVER_SCRIPT_CALL ? webDriverCode.getValue() : jsEvalCode.getValue();
        logView.logHtmlContent("<pre style='color : gray'>" + code + "</pre>");
    }

    private MethodCallback<JSCallResultVO> createSuccessCallback() {
        return new MethodCallback<JSCallResultVO>() {
            @Override
            public void onFailure(Method method, Throwable throwable) {
                logView.logHtmlContent("<span style='color : red'>Error: </span>" + throwable.getMessage());
            }

            @Override
            public void onSuccess(Method method, JSCallResultVO result) {
                if (result.getValue() == null) {
                    logView.logHtmlContent("<i>null</i> or <i>undefined</i>");
                } else {
                    logView.logTextContent( result.getValue() );
                }
            }
        };
    }

    private void callJSEval(MethodCallback<JSCallResultVO> callback) {
        JSCallResultVO noop = REST.withCallback(callback)
                .call(liveBrowserService)
                .executeJavaScript(liveSessionVO.getId(), jsEvalCode.getValue());
    }

    private void callWebDriverScript(MethodCallback<JSCallResultVO> callback) {
        String stringLanguage = programmingListBox.getValue(
                        programmingListBox.getSelectedIndex() );
        ProgrammingLanguage programmingLanguage = ProgrammingLanguage.valueOf(stringLanguage);

        JSCallResultVO noop = REST.withCallback(callback)
                .call(liveBrowserService)
                .executeWebDriver(liveSessionVO.getId(),
                        webDriverCode.getValue(),
                        programmingLanguage);
    }

    @Override
	public String getDocumentName() {
        return idGenerator.getNextIndex();
	}

    @Override
	public void preDestroy() {
        idGenerator.releaseIndex();
	}
}
