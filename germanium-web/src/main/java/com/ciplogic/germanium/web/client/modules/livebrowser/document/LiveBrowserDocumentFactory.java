package com.ciplogic.germanium.web.client.modules.livebrowser.document;

import com.ciplogic.germanium.web.shared.vo.LiveSessionVO;

public interface LiveBrowserDocumentFactory {
    LiveBrowserDocument createLiveBrowserDocument(LiveSessionVO liveSessionVO);
}
