package com.ciplogic.germanium.web.client.modules.livebrowser.document;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ScrollPanel;

public class LogView extends Composite {
    interface MyUiBinder extends UiBinder<ScrollPanel, LogView>{}
    private MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField ScrollPanel logContent;

    public LogView() {
        initWidget(uiBinder.createAndBindUi(this));
    }

    public void logTextContent(String textContent) {
        ParagraphElement paragraphElement = Document.get().createPElement();
        paragraphElement.setInnerText(textContent);

        logContent.getElement().appendChild(paragraphElement);
    }

    public void logHtmlContent(String htmlContent) {
        ParagraphElement paragraphElement = Document.get().createPElement();
        paragraphElement.setInnerHTML(htmlContent);

        logContent.getElement().appendChild(paragraphElement);
    }

    public void clear() {
        Element element = logContent.getElement();
        while (element.getFirstChild() != null) {
            element.getFirstChild().removeFromParent();
        }
    }
}
