package com.ciplogic.germanium.web.client.modules.notifier;

import com.ciplogic.gwtui.notifier.Notifier;
import com.google.gwt.core.client.GWT;
import org.atmosphere.gwt20.client.*;

import java.util.List;

public class NotifierAtmosphere {
    public void registerChannel() {
        RPCSerializer rpc_serializer = GWT.create(RPCSerializer.class);

        AtmosphereRequestConfig rpcRequestConfig = AtmosphereRequestConfig.create(rpc_serializer);
        rpcRequestConfig.setUrl(GWT.getModuleBaseURL() + "notify/rpc");
        rpcRequestConfig.setTransport(AtmosphereRequestConfig.Transport.STREAMING);
        rpcRequestConfig.setFallbackTransport(AtmosphereRequestConfig.Transport.LONG_POLLING);
        rpcRequestConfig.setOpenHandler(new AtmosphereOpenHandler() {
            @Override
            public void onOpen(AtmosphereResponse response) {
                Notifier.showInfo("RPC Connection opened");
            }
        });
        rpcRequestConfig.setReopenHandler(new AtmosphereReopenHandler() {
            @Override
            public void onReopen(AtmosphereResponse response) {
                Notifier.showInfo("RPC Connection reopened");
            }
        });
        rpcRequestConfig.setCloseHandler(new AtmosphereCloseHandler() {
            @Override
            public void onClose(AtmosphereResponse response) {
                Notifier.showInfo("RPC Connection closed");
            }
        });
        rpcRequestConfig.setMessageHandler(new AtmosphereMessageHandler() {
            @Override
            public void onMessage(AtmosphereResponse response) {
                List<RPCEvent> messages = response.getMessages();
                for (RPCEvent event : messages) {
                    Notifier.showInfo("received message through RPC: " + event.getData());
                }
            }
        });

        Atmosphere atmosphere = Atmosphere.create();
        final AtmosphereRequest rpcRequest = atmosphere.subscribe(rpcRequestConfig);
    }
}
