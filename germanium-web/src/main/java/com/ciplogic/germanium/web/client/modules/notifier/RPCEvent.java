package com.ciplogic.germanium.web.client.modules.notifier;

import java.io.Serializable;

public class RPCEvent implements Serializable {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
