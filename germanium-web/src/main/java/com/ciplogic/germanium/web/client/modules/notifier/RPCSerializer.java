package com.ciplogic.germanium.web.client.modules.notifier;

import org.atmosphere.gwt20.client.GwtRpcClientSerializer;
import org.atmosphere.gwt20.client.GwtRpcSerialTypes;

@GwtRpcSerialTypes(RPCEvent.class)
abstract public class RPCSerializer extends GwtRpcClientSerializer {
}
