package com.ciplogic.germanium.web.client.modules.projects;

import com.ciplogic.germanium.web.client.modules.projects.command.EditProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.command.ExecuteProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.command.NewProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.command.ProjectCommandFactory;
import com.ciplogic.germanium.web.client.modules.projects.command.impl.EditProjectCommandImpl;
import com.ciplogic.germanium.web.client.modules.projects.command.impl.ExecuteProjectCommandImpl;
import com.ciplogic.germanium.web.client.modules.projects.command.impl.NewProjectCommandImpl;
import com.ciplogic.germanium.web.client.modules.projects.document.ProjectDocumentFactory;
import com.ciplogic.germanium.web.client.views.ResourceTreeViewModel;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.GinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.TEST_PROJECT;

public class ProjectDocumentModule extends AbstractGinModule {
    @Override
    protected void configure() {
        GinModule commandFactoryModule = new GinFactoryModuleBuilder()
                .implement(NewProjectCommand.class, NewProjectCommandImpl.class)
                .implement(EditProjectCommand.class, EditProjectCommandImpl.class)
                .implement(ExecuteProjectCommand.class, ExecuteProjectCommandImpl.class)
                .build(ProjectCommandFactory.class);

        install(commandFactoryModule);

        GinModule documentFactoryModule = new GinFactoryModuleBuilder()
                .build(ProjectDocumentFactory.class);

        install(documentFactoryModule);
    }

    @Provides @Singleton
    public ResourceTreeViewModel provideProjectsTree() {
        return new ResourceTreeViewModel(new FileVO("/", TEST_PROJECT));
    }
}
