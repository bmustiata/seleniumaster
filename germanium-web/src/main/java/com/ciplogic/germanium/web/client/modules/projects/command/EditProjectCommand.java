package com.ciplogic.germanium.web.client.modules.projects.command;

import com.ciplogic.germanium.web.client.command.Command;

public interface EditProjectCommand extends Command {
}
