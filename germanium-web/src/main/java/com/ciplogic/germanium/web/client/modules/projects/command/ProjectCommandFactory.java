package com.ciplogic.germanium.web.client.modules.projects.command;

import com.ciplogic.germanium.web.shared.vo.FileVO;

public interface ProjectCommandFactory {
    NewProjectCommand createNewProjectCommand(FileVO selectedFile);
    EditProjectCommand createEditProjectCommand(FileVO projectVO);
    ExecuteProjectCommand createExecuteProjectCommand(FileVO projectVO);
}
