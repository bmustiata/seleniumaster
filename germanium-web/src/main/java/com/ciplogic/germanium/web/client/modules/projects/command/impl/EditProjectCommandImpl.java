package com.ciplogic.germanium.web.client.modules.projects.command.impl;

import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.projects.command.EditProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.document.ProjectDocumentFactory;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.ProjectService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class EditProjectCommandImpl implements EditProjectCommand {
    private ProjectService projectService;
    private DocumentHolder documentPanel;
    private ProjectDocumentFactory projectDocumentFactory;

    private FileVO projectPath;

    @Inject
    public EditProjectCommandImpl( ProjectService projectService,
                                   @DocumentPanel DocumentHolder documentPanel,
                                   ProjectDocumentFactory projectDocumentFactory,
                                   @Assisted FileVO projectPath ) {
        this.projectService = projectService;
        this.projectPath = projectPath;
        this.documentPanel = documentPanel;
        this.projectDocumentFactory = projectDocumentFactory;
    }

    @Override
    public void execute() {
        REST.withCallback(new DefaultMethod<ProjectVO>() {
            @Override
            public void onSuccess(Method method, ProjectVO projectVO) {
                documentPanel.add( projectDocumentFactory.editProjectDocument(projectVO, projectPath.getParentNode()) );
            }
        }).call(projectService).load(projectPath.fullPath());
    }
}
