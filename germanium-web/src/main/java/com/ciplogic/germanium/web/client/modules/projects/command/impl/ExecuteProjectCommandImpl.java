package com.ciplogic.germanium.web.client.modules.projects.command.impl;

import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.germanium.web.client.modules.projects.command.ExecuteProjectCommand;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.ExecutionService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class ExecuteProjectCommandImpl implements ExecuteProjectCommand {
    private ExecutionService executionService;
    private FileVO fileVO;

    @Inject
    public ExecuteProjectCommandImpl(
            ExecutionService executionService,
            @Assisted FileVO fileVO
    ) {
        this.executionService = executionService;
        this.fileVO = fileVO;
    }

    @Override
    public void execute() {
        REST.withCallback(new DefaultMethod<Object>() {
            @Override
            public void onSuccess(Method method, Object o) {
                Notifier.showInfo("Execution started.");
            }
        }).call(executionService).startExecuton(fileVO);
    }
}
