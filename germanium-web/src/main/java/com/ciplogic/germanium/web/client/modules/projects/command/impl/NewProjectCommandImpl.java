package com.ciplogic.germanium.web.client.modules.projects.command.impl;

import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.projects.command.NewProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.document.ProjectDocumentFactory;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class NewProjectCommandImpl implements NewProjectCommand {
    private DocumentHolder documentPanel;
    private ProjectDocumentFactory projectDocumentFactory;
    private FileVO parentFolder;

    @Inject
    public NewProjectCommandImpl(
            @DocumentPanel DocumentHolder documentPanel,
            ProjectDocumentFactory projectDocumentFactory,
            @Assisted FileVO parentFolder
    ) {
        this.documentPanel = documentPanel;
        this.projectDocumentFactory = projectDocumentFactory;
        this.parentFolder = parentFolder;
    }

    @Override
    public void execute() {
        documentPanel.add( projectDocumentFactory.editProjectDocument(new ProjectVO("New Project"), parentFolder) );
    }
}
