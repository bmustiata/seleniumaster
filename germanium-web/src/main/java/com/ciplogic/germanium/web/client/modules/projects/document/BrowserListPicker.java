package com.ciplogic.germanium.web.client.modules.projects.document;

import com.ciplogic.gwtui.ItemListHolder;
import com.ciplogic.gwtui.composite.ListToListItemPicker;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

public class BrowserListPicker extends ListToListItemPicker<BrowserProfile> {
    public BrowserListPicker(ItemListHolder<BrowserProfile> availableItems,
                             ItemListHolder<BrowserProfile> selectedItems) {
        super(availableItems, selectedItems);
    }

    @Override
    protected boolean isPickable(BrowserProfile selectedItem) {
        return true;
    }

    @Override
    protected String getLabel(BrowserProfile item) {
        return item.getId();
    }
}
