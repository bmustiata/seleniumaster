package com.ciplogic.germanium.web.client.modules.projects.document;

import com.ciplogic.germanium.web.client.views.ResourceTreeViewModel;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.gwtui.ItemListHolder;
import com.ciplogic.gwtui.composite.TreeToListItemPicker;

public class FileListPicker extends TreeToListItemPicker<FileVO> {
    public FileListPicker(ItemListHolder<FileVO> fileListHolder, FileVO rootFile) {
        super(fileListHolder,
              new ResourceTreeViewModel(rootFile)
        );
    }

    protected FileVO processAddedItem(FileVO item) {
        return item.toAbsoluteFile();
    }

    @Override
    protected boolean isPickable(FileVO selectedFile) {
        return ! selectedFile.isDirectory();
    }

    @Override
    protected String getLabel(FileVO file) {
        return file.getName();
    }
}
