package com.ciplogic.germanium.web.client.modules.projects.document;

import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesChangeAware;
import com.ciplogic.germanium.web.client.modules.browserprofiles.BrowserProfilesHolder;
import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.validators.UIValidation;
import com.ciplogic.germanium.web.client.modules.validators.ValidatorFactory;
import com.ciplogic.germanium.web.client.views.ResourceTreeViewModel;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.ProjectService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.ciplogic.gwtui.ItemListHolder;
import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

import java.util.List;

public class ProjectDocument extends AbstractDocument {
    private ResourceTreeViewModel resourceTreeViewModel;

    interface MyUiBinder extends UiBinder<DockLayoutPanel, ProjectDocument> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private BrowserProfilesHolder browserProfilesHolder;

    private ProjectVO projectVO;
    private FileVO projectPath;

    private BrowserProfilesChangeAware browserProfilesChangeListener;

    private ValidatorFactory validators;

    @UiField
    Button closeButton;

    @UiField
    Button saveButton;

    @UiField
    TextBox nameTextBox;

    @UiField
    TextBox baseUrlTextBox;

    @UiField(provided = true)
    FileListPicker testFileListPicker;

    @UiField(provided = true)
    FileListPicker userExtensionsFileListPicker;

    @UiField(provided = true)
    BrowserListPicker browserListPicker;

    private ProjectService projectService;

    private List<BrowserProfile> browserProfilesSelected;

    @Inject
    public ProjectDocument(@DocumentPanel DocumentHolder documentHolder,
                           ProjectService projectService,
                           BrowserProfilesHolder browserProfilesHolder,
                           ValidatorFactory validators,
                           ResourceTreeViewModel resourceTreeViewModel,

                           @Assisted FileVO projectPath,
                           @Assisted ProjectVO projectVO) {
        super(documentHolder);
        this.projectService = projectService;
        this.browserProfilesHolder = browserProfilesHolder;
        this.validators = validators;
        this.resourceTreeViewModel = resourceTreeViewModel;

        this.projectPath = projectPath;
        this.projectVO = projectVO;

        createTestFileListPicker();
        createBrowserListPicker();
        createUserExtensionsFileListPicker();

        initWidget(uiBinder.createAndBindUi(this));

        writeObjectToInputs();

        initializeEvents();
    }

    private void createTestFileListPicker() {
        testFileListPicker = new FileListPicker(new ItemListHolder<FileVO>() {
            @Override
            public List<FileVO> getItemList() {
                return projectVO.getTestFiles();
            }
        }, new FileVO("/", FileVO.Category.TEST));
    }

    private void createUserExtensionsFileListPicker() {
        userExtensionsFileListPicker = new FileListPicker(new ItemListHolder<FileVO>() {
            @Override
            public List<FileVO> getItemList() {
                return projectVO.getUserExtensions();
            }
        }, new FileVO("/", FileVO.Category.USER_EXTENSION));
    }

    private void createBrowserListPicker() {
        browserProfilesSelected = browserProfilesHolder.fromStringList(projectVO.getBrowserProfiles());

        this.browserListPicker = new BrowserListPicker(
                new ItemListHolder<BrowserProfile>() {
                    @Override
                    public List<BrowserProfile> getItemList() {
                        return browserProfilesHolder.getBrowserProfiles();
                    }
                },
                new ItemListHolder<BrowserProfile>() {
                    @Override
                    public List<BrowserProfile> getItemList() {
                        return browserProfilesSelected;
                    }
                }
        );
    }

    private void writeObjectToInputs() {
        // since the pickers keep the data in sync themselves, just copy the name.
        nameTextBox.setText(projectVO.getName());
        baseUrlTextBox.setText(projectVO.getBaseUrl());
    }

    private void initializeEvents() {
        browserProfilesChangeListener = new BrowserProfilesChangeAware() {
            @Override
            public void onBrowserProfilesChange() {
                browserListPicker.updateAvailableItems();
            }
        };
        browserProfilesHolder.addListener(browserProfilesChangeListener);

        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                readObjectFromInputs();
                REST.withCallback(new DefaultMethod<Object>() {
                    @Override
                    public void onSuccess(Method method, Object o) {
                        resourceTreeViewModel.refreshNode(projectPath);
                        Notifier.showInfo("Project saved.");
                    }
                }).call(projectService).save(projectPath.fullPath(), projectVO);
            }
        });

        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ProjectDocument.this.close();
            }
        });
    }

    private void readObjectFromInputs() {
        String name = nameTextBox.getText();

        UIValidation validations = validators.createNewValidation();

        validations.on(nameTextBox)
                .check(validators.generic(), name, "The name of the project can't be empty.")
                .done();

        validations.on(baseUrlTextBox)
                .check(validators.generic(), baseUrlTextBox.getText(), "There should be a base URL for the project.")
                .done();

        validations.on(testFileListPicker)
                .check(validators.collectionNotEmpty(), projectVO.getTestFiles(),
                        "The project needs at least one test to run.")
                .done();

        validations.on(browserListPicker)
                .check(validators.collectionNotEmpty(), browserProfilesSelected,
                        "The project needs at least a browser where to run.")
                .done();

        if (validations.isFailed()) {
            throw new IllegalArgumentException("Invalid ProjectVO.");
        }

        projectVO.setName(nameTextBox.getText());
        projectVO.setBaseUrl(baseUrlTextBox.getText());
        projectVO.setBrowserProfiles(browserProfilesHolder.toStringList(browserProfilesSelected));
    }

    @Override
    public void preDestroy() {
        browserProfilesHolder.removeListener(browserProfilesChangeListener);
    }

    @Override
    public String getDocumentName() {
        return projectVO.getName();
    }
}
