package com.ciplogic.germanium.web.client.modules.projects.document;

import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;

public interface ProjectDocumentFactory {
    ProjectDocument editProjectDocument(ProjectVO projectVO, FileVO projectPath);
}
