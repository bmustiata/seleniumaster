package com.ciplogic.germanium.web.client.modules.projects.view;

import com.ciplogic.germanium.web.client.views.ResourceTreeViewModel;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;

public class TestProjectsTreeModel extends ResourceTreeViewModel {
    @Inject
    public TestProjectsTreeModel(FileVO rootNode) {
        super(rootNode);
    }

    @Override
    public String getIcon(FileVO item) {
        if (item.isDirectory()) {
            return getIconPath("directory");
        } else {
            return getIconPath("project");
        }
    }
}
