package com.ciplogic.germanium.web.client.modules.projects.view;

import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.gwtui.tree.AsynchronousTree;
import com.ciplogic.germanium.web.client.modules.projects.command.EditProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.command.ExecuteProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.command.NewProjectCommand;
import com.ciplogic.germanium.web.client.modules.projects.command.ProjectCommandFactory;
import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.client.modules.resources.commands.ResourceEditCommandFactory;
import com.ciplogic.germanium.web.client.modules.resources.commands.ShowCreateFolderDialogCommand;
import com.ciplogic.germanium.web.client.modules.resources.commands.ShowDeleteFileDialogCommand;
import com.ciplogic.germanium.web.client.views.ResourceTreeViewModel;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.inject.Inject;

import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.TEST_PROJECT;

public class TestProjectsView extends Composite {
    interface MyUiBinder extends UiBinder<DockLayoutPanel, TestProjectsView> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    Button btnNew;

    @UiField
    Button btnNewFolder;

    @UiField
    Button btnEdit;

    @UiField
    Button btnRun;

    @UiField
    Button btnDelete;

    @UiField(provided = true)
    AsynchronousTree<FileVO> testProjectsTree;

    private CommandExecutor commandExecutor;
    private ProjectCommandFactory commandFactory;
    private ResourceEditCommandFactory resourceCommandFactory;

    private TestProjectsTreeModel resourceTreeViewModel;

    @Inject
    public TestProjectsView(
        CommandExecutor commandExecutor,
        ProjectCommandFactory commandFactory,
        ResourceEditCommandFactory resourceCommandFactory
    ) {
        this.commandExecutor = commandExecutor;
        this.commandFactory = commandFactory;
        this.resourceCommandFactory = resourceCommandFactory;
        this.resourceTreeViewModel = new TestProjectsTreeModel(new FileVO("/", TEST_PROJECT));

        createTestProjectsTree(resourceTreeViewModel);

        initWidget(uiBinder.createAndBindUi(this));

        initializeEvents();
    }

    private void createTestProjectsTree(ResourceTreeViewModel resourceTreeViewModel) {
        testProjectsTree = new AsynchronousTree<FileVO>(resourceTreeViewModel);
    }

    private void initializeEvents() {
        btnNew.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                NewProjectCommand newProjectCommand = commandFactory.createNewProjectCommand(getSelectedFolder());
                commandExecutor.execute(newProjectCommand);
            }
        });

        btnEdit.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EditProjectCommand editProjectCommand = commandFactory.createEditProjectCommand(getSelectedFile());
                commandExecutor.execute(editProjectCommand);
            }
        });

        btnNewFolder.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                FileVO selectedNode = getSelectedFile();

                final FileVO finalSelectedNode = selectedNode;
                ShowCreateFolderDialogCommand showCreateFolderDialogCommand =
                        resourceCommandFactory.createShowCreateFolderDialogCommand(selectedNode,
                                new FolderChangedAware() {
                                    @Override
                                    public void onFolderChange() {
                                        Notifier.showInfo("Folder created successfuly.");
                                        resourceTreeViewModel.refreshNode(finalSelectedNode);
                                    }
                                });
                commandExecutor.execute(showCreateFolderDialogCommand);
            }
        });

        btnRun.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final FileVO selectedNode = testProjectsTree.getSelectedNode();

                ExecuteProjectCommand executeProjectCommand = commandFactory.createExecuteProjectCommand(selectedNode);
                commandExecutor.execute(executeProjectCommand);
            }
        });

        btnDelete.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final FileVO selectedNode = testProjectsTree.getSelectedNode();
                if (selectedNode == null) {
                    return;
                }

                final FileVO parentNode = selectedNode.getParentNode();

                ShowDeleteFileDialogCommand showDeleteFileDialogCommand =
                        resourceCommandFactory.createDeleteFileDialogCommand(selectedNode,
                                new FolderChangedAware() {
                                    @Override
                                    public void onFolderChange() {
                                        Notifier.showInfo("File removed successfuly.");
                                        testProjectsTree.selectAndRefresh(parentNode);
                                    }
                                });
                commandExecutor.execute(showDeleteFileDialogCommand);

            }
        });
    }

    private FileVO getSelectedFolder() {
        FileVO result = getSelectedFile();

        if (result.isDirectory()) {
            return result;
        } else {
            return result.getParentNode();
        }
    }

    private FileVO getSelectedFile() {
        FileVO selectedNode = testProjectsTree.getSelectedNode();
        selectedNode = selectedNode == null ? resourceTreeViewModel.getRoot() : selectedNode;

        return selectedNode;
    }

}
