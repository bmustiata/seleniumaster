package com.ciplogic.germanium.web.client.modules.reports;

import com.ciplogic.germanium.web.client.modules.reports.commands.ReportsCommandFactory;
import com.ciplogic.germanium.web.client.modules.reports.commands.ShowExecutionResultsCommand;
import com.ciplogic.germanium.web.client.modules.reports.commands.ShowSingleTestSuiteResultsCommand;
import com.ciplogic.germanium.web.client.modules.reports.commands.impl.ShowExecutionResultsCommandImpl;
import com.ciplogic.germanium.web.client.modules.reports.commands.impl.ShowSingleTestSuiteResultsCommandImpl;
import com.ciplogic.germanium.web.client.modules.reports.document.ReportsDocumentFactory;
import com.ciplogic.germanium.web.shared.remote.ReportsService;
import com.google.gwt.core.client.GWT;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.GinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class ReportsModule extends AbstractGinModule {
    @Override
    protected void configure() {
        GinModule commandsFactoryModule = new GinFactoryModuleBuilder()
                .implement(ShowExecutionResultsCommand.class, ShowExecutionResultsCommandImpl.class)
                .implement(ShowSingleTestSuiteResultsCommand.class, ShowSingleTestSuiteResultsCommandImpl.class)
                .build(ReportsCommandFactory.class);

        install(commandsFactoryModule);

        GinModule documentFactoryModule = new GinFactoryModuleBuilder()
                .build(ReportsDocumentFactory.class);

        install(documentFactoryModule);
    }

    @Provides @Singleton
    public ReportsService createReportsService() {
        return GWT.create(ReportsService.class);
    }
}
