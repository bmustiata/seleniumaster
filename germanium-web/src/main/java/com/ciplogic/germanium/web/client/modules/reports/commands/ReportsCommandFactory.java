package com.ciplogic.germanium.web.client.modules.reports.commands;

import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;

public interface ReportsCommandFactory {
    ShowExecutionResultsCommand createShowExecutionResultsCommand(ReportTreeProjectExecution item);

    ShowSingleTestSuiteResultsCommand createShowSingleTestSuiteResultsCommand(SeleniumTestSuite seleniumTestSuite);
}
