package com.ciplogic.germanium.web.client.modules.reports.commands;

import com.ciplogic.germanium.web.client.command.Command;

public interface ShowSingleTestSuiteResultsCommand extends Command {
}
