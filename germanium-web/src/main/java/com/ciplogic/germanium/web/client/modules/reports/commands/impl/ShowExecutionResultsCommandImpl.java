package com.ciplogic.germanium.web.client.modules.reports.commands.impl;

import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.reports.commands.ShowExecutionResultsCommand;
import com.ciplogic.germanium.web.client.modules.reports.document.ReportsDocumentFactory;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.ReportsService;
import com.ciplogic.germanium.web.shared.vo.ProjectExecutionVO;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class ShowExecutionResultsCommandImpl implements ShowExecutionResultsCommand {
    private ReportsService reportsService;
    private ReportTreeProjectExecution treeProjectExecution;
    private DocumentHolder documentHolder;
    private ReportsDocumentFactory reportsDocumentFactory;

    @Inject
    public ShowExecutionResultsCommandImpl(@DocumentPanel DocumentHolder documentHolder,
                                           ReportsDocumentFactory reportsDocumentFactory,
                                           ReportsService reportsService,
                                           @Assisted ReportTreeProjectExecution treeProjectExecution) {
        this.reportsService = reportsService;
        this.reportsDocumentFactory = reportsDocumentFactory;
        this.documentHolder = documentHolder;
        this.treeProjectExecution = treeProjectExecution;
    }

    @Override
    public void execute() {
        ProjectExecutionVO noop = REST.withCallback(new DefaultMethod<ProjectExecutionVO>() {
            @Override
            public void onSuccess(Method method, ProjectExecutionVO projectExecutionVO) {
                documentHolder.add(
                    reportsDocumentFactory.createProjectResultsDocument(projectExecutionVO)
                );
            }
        }).call(reportsService).fetchExecution(treeProjectExecution.fullPath());
    }
}
