package com.ciplogic.germanium.web.client.modules.reports.commands.impl;

import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.reports.commands.ShowSingleTestSuiteResultsCommand;
import com.ciplogic.germanium.web.client.modules.reports.document.ReportsDocumentFactory;
import com.ciplogic.germanium.web.client.modules.reports.document.SingleSeleneseTestSuiteResultsDocument;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class ShowSingleTestSuiteResultsCommandImpl implements ShowSingleTestSuiteResultsCommand {
    private DocumentHolder documentHolder;
    private ReportsDocumentFactory reportsDocumentFactory;

    private SeleniumTestSuite seleniumTestSuite;

    @Inject
    public ShowSingleTestSuiteResultsCommandImpl(@DocumentPanel DocumentHolder documentHolder,
                                                 ReportsDocumentFactory reportsDocumentFactory,
                                                 @Assisted SeleniumTestSuite seleniumTestSuite) {
        this.documentHolder = documentHolder;
        this.reportsDocumentFactory = reportsDocumentFactory;
        this.seleniumTestSuite = seleniumTestSuite;
    }

    @Override
    public void execute() {
        SingleSeleneseTestSuiteResultsDocument resultsDocumentSelenese = reportsDocumentFactory.createSingleTestSuiteResultsDocument(seleniumTestSuite);
        documentHolder.add(resultsDocumentSelenese);
    }
}
