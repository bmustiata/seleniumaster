package com.ciplogic.germanium.web.client.modules.reports.document;

import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.germanium.web.client.modules.browserprofiles.view.util.ImageResourceColumn;
import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.reports.commands.ReportsCommandFactory;
import com.ciplogic.germanium.web.shared.vo.ExecutionVO;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectExecutionVO;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution;
import com.ciplogic.gwtui.datagrid.CiplogicDataGrid;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import java.util.ArrayList;
import java.util.List;

public class ProjectResultsDocument extends AbstractDocument {
    interface MyUiBinder extends UiBinder<DockLayoutPanel, ProjectResultsDocument> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private ReportsCommandFactory reportsCommandFactory;

    private CommandExecutor commandExecutor;

    private ProjectExecutionVO projectExecutionVO;

    interface Resources extends ClientBundle {
        @Source("img/running.png")
        ImageResource running();

        @Source("img/failure.png")
        ImageResource failed();

        @Source("img/success.png")
        ImageResource success();
    }

    Resources resources = GWT.create(Resources.class);

    @UiField(provided = true)
    CiplogicDataGrid<TestResultVO> testResults;

    @Inject
    public ProjectResultsDocument(@DocumentPanel DocumentHolder documentHolder,
                                  ReportsCommandFactory reportsCommandFactory,
                                  CommandExecutor commandExecutor,
                                  @Assisted ProjectExecutionVO projectExecutionVO) {
        super(documentHolder);

        this.reportsCommandFactory = reportsCommandFactory;
        this.commandExecutor = commandExecutor;

        this.projectExecutionVO = projectExecutionVO;

        testResults = createTestResults(getTestResultsAsList(projectExecutionVO));

        initWidget(uiBinder.createAndBindUi(this));
    }

    private List<TestResultVO> getTestResultsAsList(ProjectExecutionVO projectExecutionVO) {
        List<TestResultVO> result = new ArrayList<TestResultVO>();

        List<FileVO> testFiles = projectExecutionVO.getProjectVO().getTestFiles();
        for (int i = 0; i < testFiles.size(); i++) {
            result.add( createTestResultVO(projectExecutionVO, i) );
        }

        return result;
    }

    private TestResultVO createTestResultVO(ProjectExecutionVO projectExecutionVO, int testIndex) {
        TestResultVO result = new TestResultVO();

        String name = projectExecutionVO.getProjectVO().getTestFiles().get(testIndex).getName();
        result.setName(name);

        for (int profileIndex = 0; profileIndex < projectExecutionVO.getBrowserProfiles().size(); profileIndex++) {
            List<com.ciplogic.germanium.web.shared.vo.TestResultVO> testResultVOs = projectExecutionVO.getExecutionResultsVO().get(profileIndex).getTestResultVOs();
            if (testResultVOs.size() > testIndex) {
                SeleniumCommandResult commandResult = testResultVOs.get(testIndex).getSeleniumTestSuite().getCommandResult();

                ReportTreeProjectExecution.Status status = commandResult.getStatus() == SeleniumCommandResult.Status.SUCCESS ?
                        ReportTreeProjectExecution.Status.SUCCESS : ReportTreeProjectExecution.Status.FAILED;

                result.getTestStatuses().add(status);
            } else {
                result.getTestStatuses().add(ReportTreeProjectExecution.Status.RUNNING);
            }
        }

        return result;
    }

    private CiplogicDataGrid<TestResultVO> createTestResults(List<TestResultVO> testResultsAsList) {
        CiplogicDataGrid<TestResultVO> result = new CiplogicDataGrid<TestResultVO>();

        result.addColumn(createNameColumn(), "Test");
        List<BrowserProfile> browserProfiles = this.projectExecutionVO.getBrowserProfiles();
        for (int i = 0; i < browserProfiles.size(); i++) {
            result.addColumn(createTestResultColumn(i), browserProfiles.get(i).getId());
        }

        result.setRowData(testResultsAsList);

        return result;
    }

    private Column<TestResultVO, ?> createNameColumn() {
        return new TextColumn<TestResultVO>() {
            @Override
            public String getValue(TestResultVO object) {
                return object.getName();
            }
        };
    }

    private ImageResourceColumn<TestResultVO> createTestResultColumn(final int i) {
        return new ImageResourceColumn<TestResultVO>() {
            @Override
            protected ImageResource getImage(TestResultVO item) {
                switch (item.getTestStatuses().get(i)) {
                    case RUNNING: return resources.running();
                    case FAILED: return resources.failed();
                    case SUCCESS: return resources.success();
                }

                return null;
            }

            @Override
            protected void onClick(Cell.Context context) {
                ExecutionVO executionVO = projectExecutionVO.getExecutionResultsVO().get(context.getColumn() - 1);
                SeleniumTestSuite testSuite = executionVO.getTestResultVOs().get( context.getIndex() ).getSeleniumTestSuite();

                commandExecutor.execute(reportsCommandFactory.createShowSingleTestSuiteResultsCommand(testSuite));
            }
        };
    }

    @Override
    public void preDestroy() {
    }

    @Override
    public String getDocumentName() {
        return "results";
    }
}
