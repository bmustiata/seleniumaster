package com.ciplogic.germanium.web.client.modules.reports.document;

import com.ciplogic.germanium.web.shared.vo.ProjectExecutionVO;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;

public interface ReportsDocumentFactory {
    ProjectResultsDocument createProjectResultsDocument(ProjectExecutionVO projectExecutionVO);

    SingleSeleneseTestSuiteResultsDocument createSingleTestSuiteResultsDocument(SeleniumTestSuite seleniumTestSuite);

    SingleSeleneseTestResultsDocument createSingleTestResultsDocument(SeleniumTest seleniumTest);
}
