package com.ciplogic.germanium.web.client.modules.reports.document;

import com.ciplogic.gwtui.datagrid.CiplogicDataGrid;
import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class SingleSeleneseTestResultsDocument extends AbstractDocument {
    interface MyUiBinder extends UiBinder<DockLayoutPanel, SingleSeleneseTestResultsDocument> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private SeleniumTest seleniumTest;

    @UiField(provided = true)
    CiplogicDataGrid testResults;

    @Inject
    public SingleSeleneseTestResultsDocument(@DocumentPanel DocumentHolder documentHolder,
                                             @Assisted SeleniumTest seleniumTest) {
        super(documentHolder);

        this.seleniumTest = seleniumTest;

        createDataGrid();

        initWidget( uiBinder.createAndBindUi(this) );
    }

    private void createDataGrid() {
        testResults = new CiplogicDataGrid<SeleniumCommand>();

        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand command) {
                return "" + command.getAction();
            }
        }, "Action");

        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand command) {
                return "" + command.getTarget();
            }
        }, "Target");

        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand command) {
                return "" + command.getValue();
            }
        }, "Value");

        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand command) {
                return "" + command.getCommandResult().getStatus();
            }
        }, "Status");

        testResults.setRowData(seleniumTest.getCommandList());
    }

    @Override
    public void preDestroy() {
    }

    @Override
    public String getDocumentName() {
        return "Test suite results";
    }
}
