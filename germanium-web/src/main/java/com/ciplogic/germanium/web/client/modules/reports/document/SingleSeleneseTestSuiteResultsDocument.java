package com.ciplogic.germanium.web.client.modules.reports.document;

import com.ciplogic.gwtui.datagrid.CiplogicDataGrid;
import com.ciplogic.germanium.web.client.modules.browserprofiles.view.util.ImageResourceColumn;
import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.reports.document.util.SeleneseTestSuiteTableBuilder;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import java.util.ArrayList;
import java.util.List;

public class SingleSeleneseTestSuiteResultsDocument extends AbstractDocument {
    interface MyUiBinder extends UiBinder<DockLayoutPanel, SingleSeleneseTestSuiteResultsDocument> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    interface Resources extends ClientBundle {
        @Source("img/notrun.png")
        ImageResource notrun();

        @Source("img/failure.png")
        ImageResource failed();

        @Source("img/success.png")
        ImageResource success();
    }
    private Resources resources = GWT.create( Resources.class );

    private SeleniumTestSuite seleniumTestSuite;

    @UiField(provided = true)
    CiplogicDataGrid testResults;

    @Inject
    public SingleSeleneseTestSuiteResultsDocument(@DocumentPanel DocumentHolder documentHolder,
                                                  @Assisted SeleniumTestSuite seleniumTestSuite) {
        super(documentHolder);

        this.seleniumTestSuite = seleniumTestSuite;

        createDataGrid();

        initWidget( uiBinder.createAndBindUi(this) );
    }

    private void createDataGrid() {
        testResults = new CiplogicDataGrid<SeleniumCommand>();

        testResults.setTableBuilder(new SeleneseTestSuiteTableBuilder(testResults, seleniumTestSuite));

        testResults.addColumn(new ImageResourceColumn<SeleniumCommand>() {
            @Override
            protected ImageResource getImage(SeleniumCommand item) {
                SeleniumCommandResult commandResult = item.getCommandResult();

                if (commandResult == null || commandResult.getStatus() == null) {
                    return resources.notrun();
                } else if (commandResult.getStatus() == SeleniumCommandResult.Status.SUCCESS) {
                    return resources.success();
                } else if (commandResult.getStatus() == SeleniumCommandResult.Status.ERROR ||
                        commandResult.getStatus() == SeleniumCommandResult.Status.FAILED) {
                    return resources.failed();
                }
                return null;
            }
        }, "");
        testResults.setColumnWidth(0, "36px");

        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand command) {
                return "" + command.getAction();
            }
        }, "Action");

        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand command) {
                return "" + command.getTarget();
            }
        }, "Target");

        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand command) {
                return "" + command.getValue();
            }
        }, "Value");


        testResults.addColumn(new TextColumn<SeleniumCommand>() {
            @Override
            public String getValue(SeleniumCommand object) {
                SeleniumCommandResult commandResult = object.getCommandResult();

                if (commandResult != null && commandResult.getErrorName() != null) {
                    return commandResult.getErrorName() + " : " + commandResult.getErrorDescription();
                }

                return "";
            }
        }, "Status");

        testResults.setRowData(getCombinedCommandList());
    }

    private List<SeleniumCommand> getCombinedCommandList() {
        List<SeleniumCommand> result = new ArrayList<SeleniumCommand>();

        for (SeleniumTest test : seleniumTestSuite.getTestList()) {
            result.addAll( test.getCommandList() );
        }

        return result;
    }

    @Override
    public void preDestroy() {
    }

    @Override
    public String getDocumentName() {
        return "Test suite results";
    }
}
