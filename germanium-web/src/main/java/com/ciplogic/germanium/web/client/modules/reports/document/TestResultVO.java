package com.ciplogic.germanium.web.client.modules.reports.document;

import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution.Status;

public class TestResultVO {
    private String name;
    private List<Status> testStatuses = new ArrayList<Status>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Status> getTestStatuses() {
        return testStatuses;
    }

    public void setTestStatuses(List<Status> testStatuses) {
        this.testStatuses = testStatuses;
    }
}
