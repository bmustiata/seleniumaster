package com.ciplogic.germanium.web.client.modules.reports.document.util;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.DefaultCellTableBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SeleneseTestSuiteTableBuilder extends DefaultCellTableBuilder<SeleniumCommand> {
    private SeleniumTestSuite seleniumTestSuite;

    // Is a List of tests, in case some tests are empty, and a single command would be needed for multiple tests.
    // FIXME: if all the tests from the suite are empty then nothing will appear (?!)
    private Map<SeleniumCommand, List<SeleniumTest>> firstCommandMap = new HashMap<SeleniumCommand, List<SeleniumTest>>();

    public SeleneseTestSuiteTableBuilder(AbstractCellTable<SeleniumCommand> cellTable, SeleniumTestSuite seleniumTestSuite) {
        super(cellTable);

        this.seleniumTestSuite = seleniumTestSuite;

        buildFirstCommandMap();
    }

    @Override
    public void buildRowImpl(SeleniumCommand seleniumCommand, int i) {
        if (isFirstCommand(seleniumCommand)) {
            buildTestNamesList(seleniumCommand);
        }

        super.buildRowImpl(seleniumCommand, i);
    }

    private boolean isFirstCommand(SeleniumCommand seleniumCommand) {
        return firstCommandMap.get(seleniumCommand) != null;
    }

    private void buildTestNamesList(SeleniumCommand seleniumCommand) {
        List<SeleniumTest> tests = firstCommandMap.get(seleniumCommand);

        for (SeleniumTest test : tests) {
            buildRowForTest(test);
        }
    }

    private void buildRowForTest(SeleniumTest test) {
        TableRowBuilder row = startRow();
        row.className("SeleneseTestSuiteRow");

        row.startTD().colSpan(6).text(test.getName()).endTD();

        row.end();
    }

    private void buildFirstCommandMap() {
        List<SeleniumTest> testList = new ArrayList<SeleniumTest>();

        for (SeleniumTest test : seleniumTestSuite.getTestList()) {
            testList.add(test);

            if (! test.getCommandList().isEmpty()) {
                firstCommandMap.put( test.getCommandList().get(0), testList );
                testList = new ArrayList<SeleniumTest>();
            }
        }
    }
}
