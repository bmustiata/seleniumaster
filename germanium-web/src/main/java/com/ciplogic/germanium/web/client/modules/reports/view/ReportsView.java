package com.ciplogic.germanium.web.client.modules.reports.view;

import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.gwtui.tree.AsynchronousTree;
import com.ciplogic.germanium.web.client.modules.reports.commands.ReportsCommandFactory;
import com.ciplogic.germanium.web.client.modules.reports.commands.ShowExecutionResultsCommand;
import com.ciplogic.germanium.web.shared.vo.reports.ReportRootNode;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeItem;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.inject.Inject;

public class ReportsView extends Composite {
    interface MyUiBinder extends UiBinder<DockLayoutPanel, ReportsView> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private ReportsCommandFactory reportsCommandFactory;
    private CommandExecutor commandExecutor;

    @UiField
    Button viewTestButton;

    @UiField(provided = true)
    AsynchronousTree<ReportTreeItem> reportsTree;

    @Inject
    public ReportsView(ReportsCommandFactory reportsCommandFactory,
                       CommandExecutor commandExecutor) {
        this.reportsCommandFactory = reportsCommandFactory;
        this.commandExecutor = commandExecutor;

        createTree();

        initWidget(uiBinder.createAndBindUi(this));

        initializeEvents();
    }

    private void initializeEvents() {
        viewTestButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ReportTreeItem item = reportsTree.getSelectedNode();
                if (item instanceof ReportTreeProjectExecution) {
                    ReportTreeProjectExecution treeProjectExecution = (ReportTreeProjectExecution) item;
                    ShowExecutionResultsCommand showExecutionResultsCommand =
                            reportsCommandFactory.createShowExecutionResultsCommand(treeProjectExecution);

                    commandExecutor.execute(showExecutionResultsCommand);
                }
            }
        });
    }

    private void createTree() {
        reportsTree = new AsynchronousTree<ReportTreeItem>(new ReportsViewTreeModel(new ReportRootNode()));
    }
}
