package com.ciplogic.germanium.web.client.modules.reports.view;

import com.ciplogic.gwtui.tree.AsynchronousTreeModelBase;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.ReportsService;
import com.ciplogic.germanium.web.shared.vo.reports.*;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReportsViewTreeModel extends AsynchronousTreeModelBase<ReportTreeItem> {
    private ReportsService reportsService = GWT.create(ReportsService.class);

    protected ReportsViewTreeModel(ReportTreeItem rootNode) {
        super(rootNode);
    }

    @Override
    public void getChildren(final ReportTreeItem parent, final Callback<List<ReportTreeItem>, Exception> callback) {
        if (parent instanceof ReportRootNode) {
            callback.onSuccess(Collections.<ReportTreeItem>singletonList(new ReportTreeFolder(parent, "/")));
        } else if (parent instanceof ReportTreeFolder) {
            getFoldersChildrenFromServer((ReportTreeFolder) parent, callback);
        } else if (parent instanceof ReportTreeProject) {
            getProjectExecutionsFromServer(parent, callback);
        } else {
            callback.onSuccess(Collections.<ReportTreeItem>emptyList());
        }
    }

    private void getFoldersChildrenFromServer(final ReportTreeFolder parent, final Callback<List<ReportTreeItem>, Exception> callback) {
        List<ReportTreeItem> noop = REST.withCallback(new DefaultMethod<List<ReportTreeItem>>() {
            @Override
            public void onSuccess(Method method, List<ReportTreeItem> items) {
                reparentItems(items, parent);
                callback.onSuccess(items);
            }
        }).call(reportsService).listFolder(parent.fullPath());
    }

    private void getProjectExecutionsFromServer(final ReportTreeItem parent, final Callback<List<ReportTreeItem>, Exception> callback) {
        List<ReportTreeProjectExecution> noop = REST.withCallback(new DefaultMethod<List<ReportTreeProjectExecution>>() {
            @Override
            public void onSuccess(Method method, List<ReportTreeProjectExecution> executionsList) {
                reparentItems(executionsList, parent);
                List<ReportTreeItem> itemsList = new ArrayList<ReportTreeItem>(executionsList);
                callback.onSuccess(itemsList);
            }
        }).call(reportsService).listExecutions(parent.fullPath());
    }

    private <T extends ReportTreeItem> void reparentItems(List<? extends ReportTreeItem> items, T parent) {
        for (ReportTreeItem item : items) {
            item.setParent(parent);
        }
    }

    @Override
    public String getLabel(ReportTreeItem item) {
        return item.getName();
    }

    @Override
    public String getIcon(ReportTreeItem item) {
        if (item instanceof ReportTreeFolder) {
            return getIconPath("directory");
        } else if (item instanceof ReportTreeProjectExecution) {
            ReportTreeProjectExecution execution = (ReportTreeProjectExecution) item;
            switch (execution.getStatus()) {
                case FAILED: return getIconPath("failure");
                case SUCCESS: return getIconPath("success");
                case RUNNING: return getIconPath("running");
            }
        } else if (item instanceof ReportTreeProject) {
            return getIconPath("project");
        }

        return getIconPath("directory");
    }

    @Override
    public String getKey(ReportTreeItem item) {
        return item.getKey();
    }

    @Override
    public boolean isLeaf(ReportTreeItem item) {
        return item instanceof ReportTreeProjectExecution;
    }

    private String getIconPath(final String name) {
        return "/images/browse/" + name + ".png";
    }
}
