package com.ciplogic.germanium.web.client.modules.resources;

import com.ciplogic.germanium.web.client.modules.resources.commands.*;
import com.ciplogic.germanium.web.client.modules.resources.commands.impl.ResourceEditCommandImpl;
import com.ciplogic.germanium.web.client.modules.resources.commands.impl.ShowCreateFileDialogCommandImpl;
import com.ciplogic.germanium.web.client.modules.resources.commands.impl.ShowCreateFolderDialogCommandImpl;
import com.ciplogic.germanium.web.client.modules.resources.commands.impl.ShowDeleteFileDialogCommandImpl;
import com.ciplogic.germanium.web.client.modules.resources.dialog.CreateFolderDialogFactory;
import com.ciplogic.germanium.web.client.modules.resources.document.EditResourceDocumentFactory;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.GinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;

public class ResourceModule extends AbstractGinModule {
    @Override
    protected void configure() {
        GinModule dialogsModule = new GinFactoryModuleBuilder()
                .build(CreateFolderDialogFactory.class);

        install(dialogsModule);

        GinModule commandsFactoryModule = new GinFactoryModuleBuilder()
                .implement(ResourceEditCommand.class, ResourceEditCommandImpl.class)
                .implement(ShowCreateFolderDialogCommand.class, ShowCreateFolderDialogCommandImpl.class)
                .implement(ShowCreateFileDialogCommand.class, ShowCreateFileDialogCommandImpl.class)
                .implement(ShowDeleteFileDialogCommand.class, ShowDeleteFileDialogCommandImpl.class)
                .build(ResourceEditCommandFactory.class);

        install(commandsFactoryModule);

        GinModule documentFactoryModule = new GinFactoryModuleBuilder()
                .build(EditResourceDocumentFactory.class);

        install(documentFactoryModule);}
}
