package com.ciplogic.germanium.web.client.modules.resources.commands;

public interface FolderChangedAware {
    public void onFolderChange();
}
