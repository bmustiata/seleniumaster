package com.ciplogic.germanium.web.client.modules.resources.commands;

import com.ciplogic.germanium.web.shared.vo.FileVO;

public interface ResourceEditCommandFactory {
    ResourceEditCommand createResourceEditCommand(FileVO documentVO);
    ShowCreateFolderDialogCommand createShowCreateFolderDialogCommand(FileVO fileVO, FolderChangedAware folderCreatedAware);
    ShowCreateFileDialogCommand createShowCreateFileDialogCommand(FileVO fileVO, FolderChangedAware folderCreatedAware);
    ShowDeleteFileDialogCommand createDeleteFileDialogCommand(FileVO fileVO, FolderChangedAware folderCreatedAware);
}
