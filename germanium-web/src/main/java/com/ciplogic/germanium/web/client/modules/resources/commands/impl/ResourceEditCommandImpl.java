package com.ciplogic.germanium.web.client.modules.resources.commands.impl;

import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.modules.resources.commands.ResourceEditCommand;
import com.ciplogic.germanium.web.client.modules.resources.document.EditResourceDocumentFactory;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.FileListService;
import com.ciplogic.germanium.web.shared.vo.DocumentVO;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class ResourceEditCommandImpl implements ResourceEditCommand {
    private FileListService fileListService;
    private DocumentHolder documentHolder;
    private EditResourceDocumentFactory editResourceDocumentFactory;

    private FileVO fileVO;

    @Inject
    public ResourceEditCommandImpl(
            FileListService fileListService,
            @DocumentPanel DocumentHolder documentHolder,
            EditResourceDocumentFactory editResourceDocumentFactory,

            @Assisted FileVO fileVO) {
        this.fileListService = fileListService;
        this.documentHolder = documentHolder;
        this.editResourceDocumentFactory = editResourceDocumentFactory;

        this.fileVO = fileVO;
    }

    @Override
    public void execute() {
        DocumentVO noop = REST.withCallback(new DefaultMethod<DocumentVO>() {
            @Override
            public void onSuccess(Method method, DocumentVO documentVO) {
                // make sure the full path stays consistent.
                documentVO.getFileVO().setParentNode(fileVO.getParentNode());

                documentHolder.add( editResourceDocumentFactory.createEditResourceDocument(documentVO) );
            }
        }).call(fileListService).loadContent(fileVO.fullPath(), fileVO.getCategory());
    }
}
