package com.ciplogic.germanium.web.client.modules.resources.commands.impl;

import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.client.modules.resources.commands.ShowCreateFileDialogCommand;
import com.ciplogic.germanium.web.client.modules.resources.dialog.CreateFolderDialogFactory;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class ShowCreateFileDialogCommandImpl implements ShowCreateFileDialogCommand {
    private FileVO fileVO;
    private CreateFolderDialogFactory createFolderDialogFactory;
    private FolderChangedAware folderCreatedAware;

    @Inject
    public ShowCreateFileDialogCommandImpl(
            CreateFolderDialogFactory createFolderDialogFactory,
            @Assisted FileVO fileVO,
            @Assisted FolderChangedAware folderChangedAware
    ) {
        this.createFolderDialogFactory = createFolderDialogFactory;
        this.fileVO = fileVO;
        this.folderCreatedAware = folderChangedAware;
    }

    @Override
    public void execute() {
        createFolderDialogFactory.createFileDialog(fileVO, folderCreatedAware).show();
    }
}
