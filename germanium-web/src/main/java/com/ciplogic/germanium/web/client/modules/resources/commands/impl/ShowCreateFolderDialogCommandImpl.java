package com.ciplogic.germanium.web.client.modules.resources.commands.impl;

import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.client.modules.resources.commands.ShowCreateFolderDialogCommand;
import com.ciplogic.germanium.web.client.modules.resources.dialog.CreateFolderDialogFactory;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class ShowCreateFolderDialogCommandImpl implements ShowCreateFolderDialogCommand {
    private FileVO fileVO;
    private CreateFolderDialogFactory createFolderDialogFactory;
    private FolderChangedAware folderCreatedAware;

    @Inject
    public ShowCreateFolderDialogCommandImpl(
            CreateFolderDialogFactory createFolderDialogFactory,
            @Assisted FileVO fileVO,
            @Assisted FolderChangedAware folderCreatedAware
    ) {
        this.createFolderDialogFactory = createFolderDialogFactory;
        this.fileVO = fileVO;
        this.folderCreatedAware = folderCreatedAware;
    }

    @Override
    public void execute() {
        createFolderDialogFactory.createFolderDialog(fileVO, folderCreatedAware).show();
    }
}
