package com.ciplogic.germanium.web.client.modules.resources.commands.impl;

import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.client.modules.resources.commands.ShowDeleteFileDialogCommand;
import com.ciplogic.germanium.web.client.modules.resources.dialog.CreateFolderDialogFactory;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class ShowDeleteFileDialogCommandImpl implements ShowDeleteFileDialogCommand {
    private FileVO fileVO;
    private FolderChangedAware folderCreatedAware;
    private CreateFolderDialogFactory createFolderDialogFactory;

    @Inject
    public ShowDeleteFileDialogCommandImpl(
                    CreateFolderDialogFactory createFolderDialogFactory,
                    @Assisted FileVO fileVO,
                    @Assisted FolderChangedAware folderCreatedAware) {
        this.createFolderDialogFactory = createFolderDialogFactory;
        this.fileVO = fileVO;
        this.folderCreatedAware = folderCreatedAware;
    }

    @Override
    public void execute() {
        createFolderDialogFactory.createDeleteFileDialog(fileVO, folderCreatedAware).show();
    }
}
