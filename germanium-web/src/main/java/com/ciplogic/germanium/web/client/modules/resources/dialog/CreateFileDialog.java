package com.ciplogic.germanium.web.client.modules.resources.dialog;

import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.client.modules.resources.commands.ResourceEditCommand;
import com.ciplogic.germanium.web.client.modules.resources.commands.ResourceEditCommandFactory;
import com.ciplogic.germanium.web.client.modules.validators.UIValidation;
import com.ciplogic.germanium.web.client.modules.validators.ValidatorFactory;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.FileListService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class CreateFileDialog extends DialogBox {
    interface MyUiBinder extends UiBinder<VerticalPanel, CreateFileDialog> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private FileListService fileListService;
    private ValidatorFactory validators;

    private ResourceEditCommandFactory resourceEditCommandFactory;
    private CommandExecutor commandExecutor;

    private FolderChangedAware folderCreatedAware;
    private FileVO fileVO;

    @UiField
    Button createButton;

    @UiField
    Button cancelButton;

    @UiField
    TextBox fileTextBox;

    @Inject
    public CreateFileDialog(
            ValidatorFactory validators,
            FileListService fileListService,
            ResourceEditCommandFactory resourceEditCommandFactory,
            CommandExecutor commandExecutor,
            @Assisted FileVO fileVO,
            @Assisted FolderChangedAware folderCreatedAware) {
        this.fileListService = fileListService;
        this.validators = validators;
        this.commandExecutor = commandExecutor;
        this.resourceEditCommandFactory = resourceEditCommandFactory;
        this.fileVO = fileVO;
        this.folderCreatedAware = folderCreatedAware;

        setGlassEnabled(true);
        setHTML("Create file...");

        setWidget(uiBinder.createAndBindUi(this));

        center();

        initializeEvents();
    }

    private void initializeEvents() {
        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CreateFileDialog.this.hide(true);
            }
        });

        createButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                UIValidation uiValidation = validators.createNewValidation();

                String fileName = fileTextBox.getText();

                uiValidation.on(fileTextBox)
                        .check(validators.fileName(), fileName, "Invalid file name")
                        .done();

                if (uiValidation.isFailed()) {
                    return;
                }

                callCreateResourceRestService(fileName);
            }
        });
    }

    private void callCreateResourceRestService(String fileName) {
        FileVO noop = REST.withCallback(new DefaultMethod<FileVO>() {
            @Override
            public void onSuccess(Method method, FileVO result) {
                CreateFileDialog.this.hide(true);
                folderCreatedAware.onFolderChange();

                ResourceEditCommand resourceEditCommand = resourceEditCommandFactory.createResourceEditCommand(result);
                commandExecutor.execute(resourceEditCommand);
            }
        }).call(fileListService).createEmptyFile(fileVO, fileName);
    }
}
