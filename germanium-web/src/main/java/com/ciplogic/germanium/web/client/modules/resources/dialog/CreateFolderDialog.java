package com.ciplogic.germanium.web.client.modules.resources.dialog;

import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.client.modules.validators.UIValidation;
import com.ciplogic.germanium.web.client.modules.validators.ValidatorFactory;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.FileListService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class CreateFolderDialog extends DialogBox {
    interface MyUiBinder extends UiBinder<VerticalPanel, CreateFolderDialog> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private FileListService fileListService;
    private ValidatorFactory validators;

    private FolderChangedAware folderCreatedAware;
    private FileVO fileVO;

    @UiField
    Button createButton;

    @UiField
    Button cancelButton;

    @UiField
    TextBox folderTextBox;

    @Inject
    public CreateFolderDialog(
            ValidatorFactory validators,
            FileListService fileListService,
            @Assisted FileVO fileVO,
            @Assisted FolderChangedAware folderCreatedAware) {
        this.fileListService = fileListService;
        this.validators = validators;
        this.fileVO = fileVO;
        this.folderCreatedAware = folderCreatedAware;

        setGlassEnabled(true);
        setHTML("Create folder...");

        setWidget(uiBinder.createAndBindUi(this));

        center();

        initializeEvents();
    }

    private void initializeEvents() {
        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CreateFolderDialog.this.hide(true);
            }
        });

        createButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                UIValidation uiValidation = validators.createNewValidation();

                String folderName = folderTextBox.getText();

                uiValidation.on(folderTextBox)
                        .check(validators.fileName(), folderName, "Invalid file name")
                        .done();

                if (uiValidation.isFailed()) {
                    return;
                }

                callMkdirRestService(folderName);
            }
        });
    }

    private void callMkdirRestService(String folderName) {
        REST.withCallback(new DefaultMethod<Object>() {
            @Override
            public void onSuccess(Method method, Object o) {
                CreateFolderDialog.this.hide(true);
                folderCreatedAware.onFolderChange();
            }
        }).call(fileListService).mkdir(fileVO, folderName);
    }
}
