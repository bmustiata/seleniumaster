package com.ciplogic.germanium.web.client.modules.resources.dialog;

import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.shared.vo.FileVO;

public interface CreateFolderDialogFactory {
    public CreateFolderDialog createFolderDialog(FileVO fileVO,
                                                 FolderChangedAware folderCreatedAware);

    public CreateFileDialog createFileDialog(FileVO fileVO,
                                                 FolderChangedAware folderCreatedAware);

    public DeleteFileDialog createDeleteFileDialog(FileVO fileVO,
                                                 FolderChangedAware folderCreatedAware);
}
