package com.ciplogic.germanium.web.client.modules.resources.dialog;

import com.ciplogic.germanium.web.client.modules.resources.commands.FolderChangedAware;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.FileListService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class DeleteFileDialog extends DialogBox {
    interface MyUiBinder extends UiBinder<VerticalPanel, DeleteFileDialog> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField(provided = true)
    Label removeFileQuestionLabel;

    @UiField
    Button removeButton;

    @UiField
    Button cancelButton;

    private FileVO fileVO;
    private FolderChangedAware folderCreatedAware;

    private FileListService fileListService;

    @Inject
    public DeleteFileDialog(@Assisted FileVO fileVO,
                            @Assisted FolderChangedAware folderCreatedAware,
                            FileListService fileListService) {
        this.fileVO = fileVO;
        this.folderCreatedAware = folderCreatedAware;
        this.fileListService = fileListService;

        setGlassEnabled(true);
        setHTML("Remove...");

        removeFileQuestionLabel = new Label("Are you sure you want to remove " + fileVO.getName() + "?");
        setWidget(uiBinder.createAndBindUi(this));

        center();

        initializeEvents();
    }

    private void initializeEvents() {
        removeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                REST.withCallback(new DefaultMethod<Object>() {
                    @Override
                    public void onSuccess(Method method, Object o) {
                        folderCreatedAware.onFolderChange();
                        DeleteFileDialog.this.hide(true);
                    }
                }).call(fileListService).remove(fileVO);
            }
        });

        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                DeleteFileDialog.this.hide(true);
            }
        });
    }
}
