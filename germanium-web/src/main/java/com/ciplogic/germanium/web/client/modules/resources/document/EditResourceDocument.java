package com.ciplogic.germanium.web.client.modules.resources.document;

import com.ciplogic.germanium.web.client.modules.documents.AbstractDocument;
import com.ciplogic.germanium.web.client.modules.documents.DocumentHolder;
import com.ciplogic.germanium.web.client.modules.documents.DocumentPanel;
import com.ciplogic.germanium.web.client.views.testbrowser.DefaultMethod;
import com.ciplogic.germanium.web.shared.remote.FileListService;
import com.ciplogic.germanium.web.shared.vo.DocumentVO;
import com.ciplogic.gwtui.ProgrammingLanguage;
import com.ciplogic.gwtui.editor.CodeTextEditor;
import com.ciplogic.gwtui.notifier.Notifier;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.REST;

public class EditResourceDocument extends AbstractDocument {
    private DocumentVO documentVO;

    interface MyUiBinder extends UiBinder<DockLayoutPanel, EditResourceDocument> {}
	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    private FileListService fileListService = GWT.create(FileListService.class);

    @UiField(provided = true) CodeTextEditor documentCodeEditor;

    @UiField Button closeButton;

    @UiField Button saveButton;

    @Inject
    public EditResourceDocument(@DocumentPanel DocumentHolder documentHolder,
                                @Assisted DocumentVO documentVO) {
        super(documentHolder);

        this.documentVO = documentVO;

        ProgrammingLanguage programmingLanguage = detectLanguage(documentVO);
        documentCodeEditor = new CodeTextEditor(programmingLanguage, true);

        initWidget(uiBinder.createAndBindUi(this));

        documentCodeEditor.setValue(documentVO.getContent());

        initializeEvents();
    }

    private ProgrammingLanguage detectLanguage(DocumentVO documentVO) {
        String fileExtension = documentVO.getFileVO().fileExtension();

        if ("js".equalsIgnoreCase(fileExtension)) {
            return ProgrammingLanguage.JAVASCRIPT;
        } else if ("groovy".equalsIgnoreCase(fileExtension)) {
            return ProgrammingLanguage.GROOVY;
        } else if ("py".equalsIgnoreCase(fileExtension)) {
            return ProgrammingLanguage.PYTHON;
        } else if ("rb".equalsIgnoreCase(fileExtension)) {
            return ProgrammingLanguage.RUBY;
        } else if ("htm".equalsIgnoreCase(fileExtension) || "html".equalsIgnoreCase(fileExtension) || "xhtml".equalsIgnoreCase(fileExtension)) {
            return ProgrammingLanguage.HTML;
        }

        return ProgrammingLanguage.TEXT;
    }

    private void initializeEvents() {
        // FIXME: this needs backup strategies (e.g. rename + recopy - e.g. in case the disk is full), and conflict
        // strategies. If two users edit the same files at the same time.
        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                REST.withCallback(new DefaultMethod<Object>() {
                    @Override
                    public void onSuccess(Method method, Object o) {
                        Notifier.showInfo("File `" + documentVO.getFileVO().getName() + "` was saved successfully.");
                    }
                }).call(fileListService).saveContent(
                    documentVO.getFileVO().fullPath(),
                        documentVO.getFileVO().getCategory(),
                        documentCodeEditor.getValue()
                );
            }
        });

        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                EditResourceDocument.this.close();
            }
        });
	}

    @Override
    public void preDestroy() {
    }

    @Override
    public String getDocumentName() {
        return documentVO.getFileVO().getName();
    }
}
