package com.ciplogic.germanium.web.client.modules.resources.document;

import com.ciplogic.germanium.web.shared.vo.DocumentVO;

public interface EditResourceDocumentFactory {
    EditResourceDocument createEditResourceDocument(DocumentVO documentVO);
}
