package com.ciplogic.germanium.web.client.modules.tests.view;

import com.ciplogic.germanium.web.client.command.CommandExecutor;
import com.ciplogic.gwtui.tree.AsynchronousTree;
import com.ciplogic.germanium.web.client.modules.resources.commands.*;
import com.ciplogic.germanium.web.client.views.ResourceTreeViewModel;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;

import javax.inject.Inject;

public class TestBrowserView extends Composite {
    interface MyUiBinder extends UiBinder<VerticalPanel, TestBrowserView> {}
    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    @UiField
    Button newButton;

    @UiField
    Button newFolderButton;

    @UiField
    Button editButton;

    @UiField
    Button removeButton;

    @UiField(provided = true)
    AsynchronousTree<FileVO> userExtensionsTree;
    private ResourceTreeViewModel treeModel;

    private CommandExecutor commandExecutor;
    private ResourceEditCommandFactory commandFactory;

    @Inject
    public TestBrowserView(
        CommandExecutor commandExecutor,
        ResourceEditCommandFactory resourceEditCommandFactory
    ) {
        this.commandExecutor = commandExecutor;
        this.commandFactory = resourceEditCommandFactory;

        createTree();

        initWidget(uiBinder.createAndBindUi(this));

        initializeEvents();
	}

    private void createTree() {
        treeModel = new ResourceTreeViewModel(new FileVO(null, "/", FileVO.Category.TEST));
        userExtensionsTree = new AsynchronousTree<FileVO>(
                treeModel
        );
    }

    private void initializeEvents() {
        newButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ShowCreateFileDialogCommand showCreateFileDialogCommand = commandFactory.createShowCreateFileDialogCommand(
                        userExtensionsTree.getSelectedNode(), new FolderChangedAware() {
                    @Override
                    public void onFolderChange() {
                        treeModel.refreshNode(userExtensionsTree.getSelectedNode());
                    }
                });
                commandExecutor.execute(showCreateFileDialogCommand);
            }
        });

        newFolderButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ShowCreateFolderDialogCommand showCreateFolderDialogCommand = commandFactory.createShowCreateFolderDialogCommand(
                        userExtensionsTree.getSelectedNode(), new FolderChangedAware() {
                    @Override
                    public void onFolderChange() {
                        treeModel.refreshNode(userExtensionsTree.getSelectedNode());
                    }
                });
                commandExecutor.execute(showCreateFolderDialogCommand);
            }
        });

        editButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                editSelectedFile();
            }
        });

        removeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                FileVO selectedNode = userExtensionsTree.getSelectedNode();

                if (selectedNode == null) {
                    return;
                }
                final FileVO parentNode = selectedNode.getParentNode();

                ShowDeleteFileDialogCommand showDeleteFileDialogCommand = commandFactory.createDeleteFileDialogCommand(
                        selectedNode, new FolderChangedAware() {
                    @Override
                    public void onFolderChange() {
                        userExtensionsTree.selectAndRefresh(parentNode);
                    }
                });

                commandExecutor.execute(showDeleteFileDialogCommand);
            }
        });
    }

    private void editSelectedFile() {
        FileVO selectedFile = userExtensionsTree.getSelectedNode();

        ResourceEditCommand command = commandFactory.createResourceEditCommand(selectedFile);
        commandExecutor.execute(command);
    }

}
