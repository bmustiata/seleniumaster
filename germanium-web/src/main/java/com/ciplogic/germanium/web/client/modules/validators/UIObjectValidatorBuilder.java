package com.ciplogic.germanium.web.client.modules.validators;

import com.ciplogic.germanium.web.shared.validator.Validator;

public interface UIObjectValidatorBuilder {
    <T> UIObjectValidatorBuilder check(Validator<T> validator, T value, String message);
    void done();
}
