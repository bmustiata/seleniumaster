package com.ciplogic.germanium.web.client.modules.validators;

import com.google.gwt.user.client.ui.UIObject;

public interface UIValidation {
    UIObjectValidatorBuilder on(UIObject object);

    boolean isFailed();
}
