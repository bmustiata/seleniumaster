package com.ciplogic.germanium.web.client.modules.validators;

import com.ciplogic.germanium.web.shared.validator.*;

public interface ValidatorFactory {
    FileNameValidator fileName();
    IdentifierValidator generic();
    RemoteIpValidator remoteIp();
    VersionValidator version();
    CollectionNotEmptyValidator collectionNotEmpty();

    UIValidation createNewValidation();
}
