package com.ciplogic.germanium.web.client.modules.validators;

import com.ciplogic.germanium.web.client.modules.validators.impl.UIValidationImpl;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.GinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;

public class ValidatorModule extends AbstractGinModule {
    @Override
    protected void configure() {
        GinModule validatorFactoryModule = new GinFactoryModuleBuilder()
                .implement(UIValidation.class, UIValidationImpl.class)
                .build(ValidatorFactory.class);

        install(validatorFactoryModule);
    }
}
