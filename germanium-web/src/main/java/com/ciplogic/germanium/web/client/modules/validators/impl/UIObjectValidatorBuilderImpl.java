package com.ciplogic.germanium.web.client.modules.validators.impl;

import com.ciplogic.germanium.web.client.modules.validators.UIObjectValidatorBuilder;
import com.ciplogic.germanium.web.shared.validator.Validator;
import com.google.gwt.user.client.ui.UIObject;

public class UIObjectValidatorBuilderImpl implements UIObjectValidatorBuilder {
    public static final String ERROR_CSS_CLASS = "ciplogicError";

    private UIValidationImpl uiObjectValidator;
    private UIObject uiObject;

    private boolean validationFailed;

    public UIObjectValidatorBuilderImpl(UIObject object, UIValidationImpl uiObjectValidator) {
        this.uiObjectValidator = uiObjectValidator;
        this.uiObject = object;
    }

    @Override
    public <T> UIObjectValidatorBuilder check(Validator<T> validator, T value, String message) {
        if (! validator.isValid(value)) {
            uiObjectValidator.notifyValidationFailed(message);
            validationFailed = true;
        }

        return this;
    }

    @Override
    public void done() {
        if (validationFailed) {
            uiObject.setStyleName(ERROR_CSS_CLASS, true);
        } else {
            uiObject.removeStyleName(ERROR_CSS_CLASS);
        }
    }
}
