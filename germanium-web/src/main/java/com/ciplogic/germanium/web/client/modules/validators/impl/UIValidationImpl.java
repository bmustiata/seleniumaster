package com.ciplogic.germanium.web.client.modules.validators.impl;

import com.ciplogic.gwtui.notifier.Notifier;
import com.ciplogic.germanium.web.client.modules.validators.UIValidation;
import com.ciplogic.germanium.web.client.modules.validators.UIObjectValidatorBuilder;
import com.google.gwt.user.client.ui.UIObject;

import javax.inject.Inject;

public class UIValidationImpl implements UIValidation {
    private boolean validationFailed;

    @Inject
    public UIValidationImpl() {
    }

    @Override
    public UIObjectValidatorBuilder on(UIObject object) {
        return new UIObjectValidatorBuilderImpl(object, this);
    }

    @Override
    public boolean isFailed() {
        return validationFailed;
    }

    public void notifyValidationFailed(String message) {
        Notifier.showError(message);
        validationFailed = true;
    }
}
