package com.ciplogic.germanium.web.client.views;

import com.ciplogic.gwtui.tree.AsynchronousTreeModelBase;
import com.ciplogic.germanium.web.client.views.testbrowser.DataMethod;
import com.ciplogic.germanium.web.shared.remote.FileListService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import org.fusesource.restygwt.client.REST;

import java.util.List;

import static java.util.Collections.singletonList;

public class ResourceTreeViewModel extends AsynchronousTreeModelBase<FileVO> {
    class RootMarkerFileVO extends FileVO {
        public RootMarkerFileVO() {
        }
    }

    private FileListService fileListService = GWT.create(FileListService.class);
    private RootMarkerFileVO markerNode;

    public ResourceTreeViewModel(FileVO rootNode) {
        super(rootNode);
        this.markerNode = new RootMarkerFileVO();
    }

    @Override
    public void getChildren(final FileVO parent, final Callback<List<FileVO>, Exception> callback) {
        if (parent == markerNode) {
            returnTheRootNode(callback);
        } else {
            fetchChildNodes(parent, callback);
        }
    }

    private void returnTheRootNode(Callback<List<FileVO>, Exception> callback) {
        callback.onSuccess(singletonList(super.getRoot()));
    }

    private void fetchChildNodes(final FileVO parent, final Callback<List<FileVO>, Exception> callback) {
        List<FileVO> noop = REST.withCallback(new DataMethod<List<FileVO>>(callback) {
            @Override
            public List<FileVO> postProcess(List<FileVO> data) {
                for (FileVO file : data) {
                    file.setParentNode(parent);
                }

                return data;
            }
        }).call(fileListService).listFilesAsJson(parent.fullPath(), parent.getCategory());
    }

    @Override
    public String getLabel(FileVO item) {
        if (item.getName().isEmpty()) {
            return item.fullPath();
        } else {
            return item.getName();
        }
    }

    @Override
    public String getIcon(FileVO item) {
        if (item.isDirectory()) {
            return getIconPath("directory");
        } else if (item.getName().endsWith(".groovy")) {
            return getIconPath("groovy");
        } else if (item.getName().endsWith(".html")) {
            return getIconPath("html");
        } else if (item.getName().endsWith(".js")) {
            return getIconPath("js");
        }

        return getIconPath("file");
    }

    @Override
    public String getKey(FileVO item) {
        if (item instanceof RootMarkerFileVO) {
            return "$ROOT$";
        } else {
            String fullPath = item.fullPath();

            if ("/".equals(fullPath)) {
                return "$ROOT$/" + item.getCategory().toString();
            } else {
                return "$ROOT$/" + item.getCategory() + fullPath;
            }
        }
    }

    protected String getIconPath(final String name) {
        return "/images/browse/" + name + ".png";
    }

    @Override
    public boolean isLeaf(FileVO item) {
        return !item.isDirectory();
    }

    @Override
    public FileVO getRoot() {
        return markerNode;
    }
}
