package com.ciplogic.germanium.web.client.views.testbrowser;

import com.google.gwt.core.client.Callback;
import org.fusesource.restygwt.client.Method;

/**
 * A method that delegates the failure to a callback, since most methods are used mostly in in association with callbacks,
 * and it would not make sense to delegate the failure to another method.
 */
public abstract class DataMethod<T> extends DefaultMethod<T> {
    private Callback delegate;

    public <F> DataMethod(Callback<T, F> callback) {
        this.delegate = callback;
    }

    @Override
    public void onSuccess(Method method, T response) {
        delegate.onSuccess( postProcess(response) );
    }

    public T postProcess(T data) {
        return data;
    }

    @Override
    public void onFailure(Method method, Throwable exception) {
        super.onFailure(method, exception);
        delegate.onFailure(exception);
    }
}
