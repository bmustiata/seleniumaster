package com.ciplogic.germanium.web.client.views.testbrowser;

import com.ciplogic.gwtui.notifier.Notifier;
import com.google.gwt.user.client.rpc.AsyncCallback;

public abstract class DefaultCallback<T> implements AsyncCallback<T> {
    @Override
    public void onSuccess(T result) {
    }

    @Override
    public void onFailure(Throwable caught) {
        String message = caught.getMessage();

        if (message.length() > 200) {
            message = message.substring(0, 195) + " ... ";
        }

        Notifier.showError(message, "Server call failed.");
    }
}

