package com.ciplogic.germanium.web.client.views.testbrowser;

import com.ciplogic.gwtui.notifier.Notifier;
import com.google.gwt.core.client.GWT;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

public abstract class DefaultMethod<T> implements MethodCallback<T> {
    @Override
    public void onFailure(Method method, Throwable exception) {
        String message = exception.getMessage();

        if (message.length() > 200) {
            message = message.substring(0, 195) + " ... ";
        }

        Notifier.showError(message, "Server call failed.");

        GWT.log("Server call failed.", exception);
    }
}
