package com.ciplogic.germanium.web.client.views.testbrowser;

import java.util.HashMap;
import java.util.Map;

public class MapMaker {
    public static MapEntry entry(String key, String value) {
        return new MapEntry(key, value);
    }

    public static Map<String, String> map(MapEntry ... path) {
        Map<String, String> result = new HashMap<String, String>();

        for (MapEntry entry : path) {
            result.put(entry.key, entry.value);
        }

        return result;
    }

    public static Map<String, String> map(String key, String value) {
        return map(entry(key, value));
    }

    public static class MapEntry {
        private String key;
        private String value;

        public MapEntry(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
