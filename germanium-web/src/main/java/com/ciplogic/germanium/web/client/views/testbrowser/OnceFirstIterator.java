package com.ciplogic.germanium.web.client.views.testbrowser;

import java.util.Iterator;

public class OnceFirstIterator<T> implements Iterator<T> {
    private T firstValue;
    private T nextValue;
    private boolean firstReturned;

    public OnceFirstIterator(T firstValue, T nextValue) {
        this.firstValue = firstValue;
        this.nextValue = nextValue;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public T next() {
        if (firstReturned) {
            return nextValue;
        } else {
            firstReturned = true;
            return firstValue;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
