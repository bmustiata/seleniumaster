package com.ciplogic.germanium.web.server;

import com.ciplogic.germanium.web.server.cache.ClearClientCacheFilter;
import com.ciplogic.germanium.web.server.rest.FileListServiceImpl;
import com.ciplogic.germanium.web.server.rest.executions.ExecutionServiceImpl;
import com.ciplogic.germanium.web.server.rest.executions.ExecutionsModule;
import com.ciplogic.germanium.web.server.rest.live.LiveBrowserServiceImpl;
import com.ciplogic.germanium.web.server.rest.profiles.BrowserProfileServiceImpl;
import com.ciplogic.germanium.web.server.rest.projects.ProjectServiceImpl;
import com.ciplogic.germanium.web.server.rest.reports.ReportsServiceImpl;
import com.ciplogic.germanium.web.server.util.LogErrorsModule;
import com.ciplogic.util.guice.PostConstructModule;
import com.google.inject.*;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.atmosphere.cpr.AtmosphereServlet;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import java.util.HashMap;
import java.util.Map;

public class GermaniumGuiceServletConfig extends GuiceServletContextListener {
    @Override
    protected Injector getInjector() {
        return Guice.createInjector(
        new LogErrorsModule(),
        new JerseyServletModule() {
            @Override
            protected void configureServlets() {
                bindJacksonIntegration();
                bindAndServeAtmosphereServices();
                bindAndServeRestServices();
                bindAndFilterCaching();
            }

            private void bindJacksonIntegration() {
                bind(JacksonJsonProvider.class).in(Singleton.class);
            }

            private void bindAndServeAtmosphereServices() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("org.atmosphere.cpr.AtmosphereInterceptor", "org.atmosphere.gwt20.server.GwtRpcInterceptor");

                bind(AtmosphereServlet.class).in(Singleton.class);
                serve("/notify/rpc").with(AtmosphereServlet.class, params);
            }

            private void bindAndServeRestServices() {
                bind(FileListServiceImpl.class);
                bind(BrowserProfileServiceImpl.class);
                bind(LiveBrowserServiceImpl.class);
                bind(ProjectServiceImpl.class);
                bind(ExecutionServiceImpl.class);
                bind(ReportsServiceImpl.class);

                serve("/service/*").with(GuiceContainer.class);
            }

            private void bindAndFilterCaching() {
                bind(ClearClientCacheFilter.class).in(Singleton.class);
                filter("*/clear.cache.gif", "*.nocache.js").through(ClearClientCacheFilter.class);
            }

            @Provides
            @Singleton
            @Inject
            public GuiceContainer creatGuiceContainer(Injector injector) {
                GuiceContainer container = new GuiceContainer(injector);
                return container;
            }
        }, new ExecutionsModule(), new GermaniumWebModule(), new PostConstructModule());
    }
}
