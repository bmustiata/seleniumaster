package com.ciplogic.germanium.web.server;

import com.ciplogic.germanium.web.server.config.ReportsPath;
import com.ciplogic.germanium.web.server.config.TestProjectsPath;
import com.ciplogic.germanium.web.server.config.TestsPath;
import com.ciplogic.germanium.web.server.config.UserExtensionsPath;
import com.ciplogic.seleniumaster.profiles.ProfilesJsonName;
import com.google.inject.AbstractModule;

public class GermaniumWebModule extends AbstractModule {
    @Override
    protected void configure() {
        // FIXME: thiese bindings should be read from a configuration file of some sort.
        bind(String.class).annotatedWith(ReportsPath.class)
                .toInstance("c:/tmp/germanium/reports");
        bind(String.class).annotatedWith(TestsPath.class)
                .toInstance("c:/tmp/germanium/tests");
        bind(String.class).annotatedWith(UserExtensionsPath.class)
                .toInstance("c:/tmp/germanium/user-extensions");
        bind(String.class).annotatedWith(TestProjectsPath.class)
                .toInstance("c:/tmp/germanium/projects");

        bind(String.class).annotatedWith(ProfilesJsonName.class)
                .toInstance("c:/tmp/germanium/profiles.json");
    }
}
