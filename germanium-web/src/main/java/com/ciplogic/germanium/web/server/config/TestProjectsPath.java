package com.ciplogic.germanium.web.server.config;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Designates where the test projects are located.
 */
@BindingAnnotation
@Retention(RUNTIME)
@Target(value = {PARAMETER, LOCAL_VARIABLE, FIELD})
public @interface TestProjectsPath {
}
