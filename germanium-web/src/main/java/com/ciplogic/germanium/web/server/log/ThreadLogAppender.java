package com.ciplogic.germanium.web.server.log;

import com.ciplogic.germanium.web.server.log.impl.ThreadLogWriter;
import org.apache.log4j.WriterAppender;

/**
 * This appender simply uses a writer that picks out where to write, depending on the writer that is assigned
 * for the current thread.
 */
public class ThreadLogAppender extends WriterAppender {
    public ThreadLogAppender() {
        encoding = "utf-8";
        immediateFlush = true;

        activateOptions();
    }

    @Override
    public void activateOptions() {
        setWriter(new ThreadLogWriter() );
    }
}
