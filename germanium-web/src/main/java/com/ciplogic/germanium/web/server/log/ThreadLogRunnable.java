package com.ciplogic.germanium.web.server.log;

import com.ciplogic.germanium.web.server.log.impl.ThreadLogData;

import java.io.Writer;

/**
 * Runs a runnable and set the thread log data for the duration of the execution of the runnable.
 */
public class ThreadLogRunnable implements Runnable {
    private Runnable delegate;
    private Writer writer;

    public ThreadLogRunnable(Runnable delegate, Writer writer) {
        this.delegate = delegate;
        this.writer = writer;
    }

    public void run() {
        try {
            ThreadLogData.setData(new ThreadLogData(writer));

            delegate.run();
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage(), e);
        } finally {
            ThreadLogData.setData(null);
        }

    }
}
