package com.ciplogic.germanium.web.server.log;

import java.io.FileWriter;
import java.io.IOException;

public class ThreadLogRunnableFactory {
    public static ThreadLogRunnable createThreadLogRunnable(String name, Runnable delegate) {
        try {
            return new ThreadLogRunnable(delegate, createWriter(name));
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private static FileWriter createWriter(String name) throws IOException {
        return new FileWriter(name + ".txt");
    }
}
