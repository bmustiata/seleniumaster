package com.ciplogic.germanium.web.server.log.impl;

import java.io.IOException;
import java.io.Writer;

/**
 * Data assigned for a thread that keeps the output stream where to log to.
 */
public class ThreadLogData {
    private static InheritableThreadLocal<ThreadLogData> data = new InheritableThreadLocal<ThreadLogData>();

    private Writer writer;

    public ThreadLogData(Writer writer) {
        this.writer = writer;
    }

    public static ThreadLogData getData() {
        return data.get();
    }

    public static void setData(ThreadLogData logData) {
        if (logData == null) {
            try {
                data.get().getWriter().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        data.set( logData );
    }

    public Writer getWriter() {
        return writer;
    }
}
