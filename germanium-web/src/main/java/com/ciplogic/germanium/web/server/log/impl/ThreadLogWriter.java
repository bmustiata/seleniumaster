package com.ciplogic.germanium.web.server.log.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * A writer that accesses the ThreadLogData and writes there its log messages.
 */
public class ThreadLogWriter extends Writer {
    public static PrintWriter sysOutWriter = new PrintWriter(System.out);

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        ThreadLogData logData = ThreadLogData.getData();
        if (logData != null) {
            logData.getWriter().write(cbuf, off, len);
        } else {
            sysOutWriter.write(cbuf, off, len);
        }
    }

    @Override
    public void flush() throws IOException {
        ThreadLogData logData = ThreadLogData.getData();
        if (logData != null) {
            logData.getWriter().flush();
        } else {
            sysOutWriter.flush();
        }
    }

    @Override
    public void close() throws IOException {
        ThreadLogData logData = ThreadLogData.getData();
        if (logData != null) {
            logData.getWriter().close();
        }
    }
}
