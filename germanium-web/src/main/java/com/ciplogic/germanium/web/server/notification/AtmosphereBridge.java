package com.ciplogic.germanium.web.server.notification;

import org.apache.log4j.Logger;
import org.atmosphere.cpr.AtmosphereHandler;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.DefaultBroadcasterFactory;
import org.atmosphere.gwt20.shared.Constants;

import java.io.IOException;

public class AtmosphereBridge implements AtmosphereHandler {
    static final Logger logger = Logger.getLogger("AtmosphereHandler");
    @Override
    public void onRequest(AtmosphereResource ar) throws IOException {
        if (ar.getRequest().getMethod().equals("GET") ) {
            doGet(ar);
        } else if (ar.getRequest().getMethod().equals("POST") ) {
            doPost(ar);
        }
    }

    @Override
    public void onStateChange(AtmosphereResourceEvent atmosphereResourceEvent) throws IOException {
        // FIXME:
    }

    public void doGet(AtmosphereResource ar) {
        // lookup the broadcaster, if not found create it. Name is arbitrary
        ar.setBroadcaster(DefaultBroadcasterFactory.getDefault().lookup("MyBroadcaster", true));

        ar.suspend();
    }

    /**
     * receive push message from client
     **/
    public void doPost(AtmosphereResource ar) {
        Object msg = ar.getRequest().getAttribute(Constants.MESSAGE_OBJECT);
        if (msg != null) {
            logger.info("received RPC post: " + msg.toString());
            // for demonstration purposes we will broadcast the message to all connections
            DefaultBroadcasterFactory.getDefault().lookup("MyBroadcaster").broadcast(msg);
        }
    }


    @Override
    public void destroy() {
    }
}
