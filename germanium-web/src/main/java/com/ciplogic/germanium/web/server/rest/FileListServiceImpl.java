package com.ciplogic.germanium.web.server.rest;

import com.ciplogic.germanium.web.server.util.LogErrors;
import com.ciplogic.germanium.web.shared.vo.DocumentVO;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ciplogic.germanium.web.shared.FileUtil.concatPath;
import static org.apache.commons.lang.StringUtils.isEmpty;

public class FileListServiceImpl implements com.ciplogic.germanium.web.shared.remote.FileListService {
    @Inject
    private FilePathResolver pathResolver;

    public List<FileVO> listFilesAsJson(String path, FileVO.Category category) {
        return getFilesForPath(pathResolver.getPrefixPath(category) + path, category);
    }

    @LogErrors
    @Override
    public DocumentVO loadContent(String fullPath, FileVO.Category category) {
        FileReader reader = null;
        try {
            File targetFile = new File(pathResolver.getPrefixPath(category) + fullPath);
            reader = new FileReader(targetFile);
            String content = IOUtils.toString(reader);

            DocumentVO result = new DocumentVO();

            result.setFileVO(new FileVO(fullPath, category));
            result.setContent(content);

            return result;
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    @LogErrors
    @Override
    public void saveContent(String fullPath, FileVO.Category category, String content) {
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(concatPath( pathResolver.getPrefixPath(category), fullPath));
            IOUtils.copy(IOUtils.toInputStream(content), outputStream);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }

    @LogErrors
    @Override
    public FileVO createEmptyFile(FileVO parentFolder, String fileName) {
        try {
            String categoryPath = pathResolver.getPrefixPath(parentFolder.getCategory());
            String fullFolderName = concatPath(categoryPath, parentFolder.fullPath());
            new File(concatPath(fullFolderName, fileName)).createNewFile();

            return new FileVO(concatPath(parentFolder.fullPath(), fileName), parentFolder.getCategory());
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    @LogErrors
    @Override
    public void mkdir(FileVO parentFolder, String folderName) {
        String categoryPath = pathResolver.getPrefixPath(parentFolder.getCategory());
        String fullFolderName = concatPath(categoryPath, parentFolder.fullPath());
        boolean result = new File(concatPath(fullFolderName, folderName)).mkdirs();

        if (!result) {
            throw new IllegalArgumentException("Unable to create folder `" + folderName + "` in " + parentFolder.getCategory());
        }
    }

    @LogErrors
    @Override
    public void remove(FileVO fileVO) {
        try {
            String categoryPath = pathResolver.getPrefixPath(fileVO.getCategory());
            String fullFileName = concatPath(categoryPath, fileVO.fullPath());

            FileUtils.deleteQuietly(new File(fullFileName));
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private List<FileVO> getFilesForPath(String path, FileVO.Category category) {
        List<FileVO> files;

        if (! isEmpty(path)) {
            files = listFiles(path, category);
        } else {
            files = new ArrayList<FileVO>();
        }
        return files;
    }

    public List<FileVO> listFiles(String name, FileVO.Category category) throws IllegalArgumentException {
        List<FileVO> result = new ArrayList<FileVO>();

        File targetFile = new File(name);
        if (targetFile.isFile()) {
            return Collections.emptyList();
        }

        File[] files = targetFile.listFiles();

        if (files != null) {
            for (File file: files) {
                result.add(createFileVO(file, category));
            }
        }

        return result;
    }

    private FileVO createFileVO(File file, FileVO.Category category) {
        FileVO result = new FileVO(null, file.getName(), category);

        result.setDirectory( file.isDirectory() );

        return result;
    }
}
