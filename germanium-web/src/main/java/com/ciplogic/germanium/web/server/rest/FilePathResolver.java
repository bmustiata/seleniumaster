package com.ciplogic.germanium.web.server.rest;

import com.ciplogic.germanium.web.server.config.ReportsPath;
import com.ciplogic.germanium.web.server.config.TestProjectsPath;
import com.ciplogic.germanium.web.server.config.TestsPath;
import com.ciplogic.germanium.web.server.config.UserExtensionsPath;
import com.ciplogic.germanium.web.shared.FileUtil;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;

public class FilePathResolver {
    @Inject
    @UserExtensionsPath
    private String userExtensionsPath;

    @Inject @ReportsPath
    private String reportsPath;

    @Inject @TestsPath
    private String testsPath;

    @Inject @TestProjectsPath
    private String testProjectsPath;

    public String getPrefixPath(FileVO.Category category) {
        if (category == FileVO.Category.USER_EXTENSION) {
            return userExtensionsPath;
        } else if (category == FileVO.Category.REPORT) {
            return reportsPath;
        } else if (category == FileVO.Category.TEST) {
            return testsPath;
        } else if (category == FileVO.Category.TEST_PROJECT) {
            return testProjectsPath;
        }

        throw new NullPointerException("Category can't be null");
    }

    public String getPath(FileVO fileVO) {
        return FileUtil.concatPath(getPrefixPath(fileVO.getCategory()), fileVO.fullPath());
    }
}
