package com.ciplogic.germanium.web.server.rest;

import com.ciplogic.germanium.web.shared.FileUtil;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;

import java.io.*;

/**
 * Reusable file service, that works mostly with FileVOs in order to abstract work.
 */
public class FileService {
    private FilePathResolver filePathResolver;

    @Inject
    public FileService(FilePathResolver filePathResolver) {
        this.filePathResolver = filePathResolver;
    }

    /**
     * Creates a folder and all the necessary subfolders to that folder.
     * @param category
     * @param path
     */
    public void mkdir(FileVO.Category category, String path) {
        File file = getFile(category, path);
        if (! file.mkdirs()) {
            throw new IllegalArgumentException(String.format("Unable to create folder `%s` in category %s.", path, category));
        }
    }

    public Writer getWriter(FileVO.Category category, String path) {
        try{
            String fullPath = getFullPath(category, path);

            // FIXME: mkdir the parent path?
            return new FileWriter(fullPath);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public Reader getReader(FileVO.Category category, String path) {
        try{
            String fullPath = getFullPath(category, path);

            return new FileReader(fullPath);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private String getFullPath(FileVO.Category category, String path) {
        String categoryPath = filePathResolver.getPrefixPath(category);
        return FileUtil.concatPath(categoryPath, path);
    }

    public void copyFile(FileVO testFile, FileVO fileVO) {
        try {
            FileUtils.copyFile(getFile(testFile), getFile(fileVO));
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private File getFile(FileVO fileVO) {
        return getFile(fileVO.getCategory(), fileVO.fullPath());
    }

    private File getFile(FileVO.Category category, String path) {
        String fullPath = getFullPath(category, path);

        return new File(fullPath);
    }

    public void touch(FileVO.Category category, String name) {
        try {
            String fullPath = getFullPath(category, name);
            File fullPathFile = new File(fullPath);

            FileUtils.touch(fullPathFile);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
