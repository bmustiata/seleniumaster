package com.ciplogic.germanium.web.server.rest.executions;

public class CurrentBrowserExecution extends CurrentProjectExecution {
    private String browserProfile;

    public CurrentBrowserExecution(CurrentProjectExecution currentProjectExecution, String browserProfile) {
        this.setBaseFolder( currentProjectExecution.getBaseFolder() );
        this.setExecutionId( currentProjectExecution.getExecutionId() );

        this.browserProfile = browserProfile;
    }

    public String getBrowserProfile() {
        return browserProfile;
    }

    public void setBrowserProfile(String browserProfile) {
        this.browserProfile = browserProfile;
    }
}
