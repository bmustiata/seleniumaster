package com.ciplogic.germanium.web.server.rest.executions;

public class CurrentProjectExecution {
    private String executionId;

    private String baseFolder;

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    /**
     * This path is the path of a project, in its "unresolved" form, e.g. '/cool-project/Regressions', and not
     * an actual filesystem location like 'c:/germanium/tests/cool-project/Regression'.
     * @return
     */
    public String getBaseFolder() {
        return baseFolder;
    }

    public void setBaseFolder(String baseFolder) {
        this.baseFolder = baseFolder;
    }
}
