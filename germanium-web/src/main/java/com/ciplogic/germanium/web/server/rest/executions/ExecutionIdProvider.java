package com.ciplogic.germanium.web.server.rest.executions;

import com.ciplogic.germanium.web.server.rest.FilePathResolver;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

import java.io.File;

import static com.ciplogic.germanium.web.shared.FileUtil.concatPath;
import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.REPORT;

public class ExecutionIdProvider {
    private FilePathResolver filePathResolver;
    private Logger logger = Logger.getLogger(ExecutionIdProvider.class);

    @Inject
    public ExecutionIdProvider(FilePathResolver filePathResolver) {
        this.filePathResolver = filePathResolver;
    }

    public String getNextExecutionId(ProjectVO projectVO) {
        File reportFolder = getProjectReportsFolder(projectVO);
        int maxId = findMaxId(reportFolder);

        return ++maxId + "";
    }

    private File getProjectReportsFolder(ProjectVO projectVO) {
        String categoryPrefix = filePathResolver.getPrefixPath(REPORT);
        String projectLocation = projectVO.getProjectLocation();
        String projectReportPath = concatPath(categoryPrefix, projectLocation);

        return new File(projectReportPath);
    }

    private int findMaxId(File reportFolder) {
        int maxNumber = 1;
        File[] files = reportFolder.listFiles();

        if (files == null) {
            logger.warn("Report folder is not existing or some I/O Error occurred when listing files. " + reportFolder.getAbsolutePath());
            return maxNumber;
        }

        for (File file : files) {
            if (file.isDirectory() && isNameANumber(file)) {
                int actualNumber = getNumberFromName(file);
                if (actualNumber > maxNumber) {
                    maxNumber = actualNumber;
                }
            }
        }

        return maxNumber;
    }

    private int getNumberFromName(File file) {
        return Integer.parseInt( file.getName() );
    }

    private boolean isNameANumber(File file) {
        return file.getName().matches("\\d+");
    }
}
