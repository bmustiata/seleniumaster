package com.ciplogic.germanium.web.server.rest.executions;

import com.ciplogic.germanium.web.server.rest.FilePathResolver;
import com.ciplogic.germanium.web.shared.remote.ExecutionService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.google.inject.Inject;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.FileInputStream;

public class ExecutionServiceImpl implements ExecutionService {
    @Inject
    private TestExecutorFactory testExecutorFactory;

    @Inject
    private FilePathResolver filePathResolver;

    @Override
    public void startExecuton(FileVO fileVO) {
        ProjectVO projectVO = loadProjectVO(fileVO);
        testExecutorFactory.createProjectExecution(projectVO).start();
    }

    private ProjectVO loadProjectVO(FileVO projectLocationVO) {
        try {
            ProjectVO projectVO = new ObjectMapper().readValue(new FileInputStream(filePathResolver.getPath(projectLocationVO)), ProjectVO.class);
            projectVO.setProjectLocation( projectLocationVO.fullPath() );

            return projectVO;
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
