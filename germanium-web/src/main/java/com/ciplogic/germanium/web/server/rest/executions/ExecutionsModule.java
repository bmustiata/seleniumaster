package com.ciplogic.germanium.web.server.rest.executions;

import com.ciplogic.germanium.selenium.BinaryDriverModule;
import com.ciplogic.germanium.web.server.rest.executions.browser.TestFileStreamResolver;
import com.ciplogic.seleniumaster.profiles.ProfilesModule;
import com.ciplogic.seleniumaster.runner.io.StreamResolver;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;

public class ExecutionsModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(StreamResolver.class).to(TestFileStreamResolver.class).in(Singleton.class);

        install(new BinaryDriverModule());
        install(new ProfilesModule());
        install(
            new FactoryModuleBuilder().build(TestExecutorFactory.class)
        );
    }
}
