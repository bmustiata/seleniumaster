package com.ciplogic.germanium.web.server.rest.executions;

import com.ciplogic.germanium.web.server.rest.FileService;
import com.ciplogic.germanium.web.shared.FileUtil;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.germanium.web.shared.FileUtil.concatPath;
import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.REPORT;
import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.FAILED;
import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.SUCCESS;

public class ProjectExecution implements Runnable {
    private Logger log = Logger.getLogger(ProjectExecution.class);

    private ProjectVO projectVO;

    private TestExecutorFactory testExecutorFactory;
    private ProjectExecutionFoldersCreator projectExecutionFoldersCreator;

    private Provider<CurrentProjectExecution> currentProjectExecutionProvider;

    private ExecutionIdProvider executionIdProvider;
    private FileService fileService;

    @Inject
    public ProjectExecution(
            TestExecutorFactory testExecutorFactory,
            ProjectExecutionFoldersCreator projectExecutionFoldersCreator,
            Provider<CurrentProjectExecution> currentProjectExecutionProvider,
            FileService fileService,
            ExecutionIdProvider executionIdProvider,
            @Assisted ProjectVO projectVO) {
        this.testExecutorFactory = testExecutorFactory;
        this.projectExecutionFoldersCreator = projectExecutionFoldersCreator;
        this.currentProjectExecutionProvider = currentProjectExecutionProvider;
        this.fileService = fileService;
        this.executionIdProvider = executionIdProvider;

        this.projectVO = projectVO;
    }

    public void start() {
        new Thread(this).start();
    }

    public void run() {
        try {
            CurrentProjectExecution currentProjectExecution = createCurrentProjectExecution(projectVO);
            projectExecutionFoldersCreator.createStorage(currentProjectExecution, projectVO);

            ThreadGroup threadGroup = new ThreadGroup(currentProjectExecution.getExecutionId());
            List<Thread> threadList = new ArrayList<Thread>();

            createThreads(threadGroup, threadList, currentProjectExecution);
            startThreads(threadList);
            joinThreads(threadList);

            writeFinalResult(currentProjectExecution, projectVO);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void createThreads(ThreadGroup threadGroup, List<Thread> threadList, CurrentProjectExecution currentProjectExecution) {
        for (String browserProfile : projectVO.getBrowserProfiles()) {
            Thread thread = new Thread(threadGroup, testExecutorFactory.createLoggingProfileExecution(
                    new CurrentBrowserExecution(currentProjectExecution, browserProfile), projectVO));
            threadList.add(thread);
        }
    }

    private CurrentProjectExecution createCurrentProjectExecution(ProjectVO projectVO) {
        CurrentProjectExecution result = currentProjectExecutionProvider.get();

        String executionId = executionIdProvider.getNextExecutionId(projectVO);
        result.setExecutionId(executionId);

        String projectFolder = concatPath(projectVO.getProjectLocation(), result.getExecutionId());
        result.setBaseFolder(projectFolder);

        return result;
    }

    private void startThreads(List<Thread> threadList) {
        for (Thread thread : threadList) {
            thread.start();
        }
    }

    private void joinThreads(List<Thread> threadList) {
        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeFinalResult(CurrentProjectExecution currentProjectExecution, ProjectVO projectVO) {
        SeleniumCommandResult.Status projectStatus = SUCCESS;

        for (String browserProfile : projectVO.getBrowserProfiles()) {
            SeleniumCommandResult.Status browserStatus = readBrowserStatus(currentProjectExecution, browserProfile);
            if (browserStatus != SUCCESS) {
                projectStatus = FAILED;
                break;
            }
        }

        writeProjectStatus(currentProjectExecution, projectStatus);
    }

    private SeleniumCommandResult.Status readBrowserStatus(CurrentProjectExecution currentProjectExecution, String browserProfile) {
        String path = FileUtil.concatPaths(
                currentProjectExecution.getBaseFolder(),
                "executions",
                browserProfile,
                ".status"
        );

        try {
            return new ObjectMapper().readValue( fileService.getReader(REPORT, path), SeleniumCommandResult.Status.class );
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return FAILED;
        }
    }

    private void writeProjectStatus(CurrentProjectExecution currentProjectExecution, SeleniumCommandResult.Status projectStatus) {
        String path = FileUtil.concatPath(
                currentProjectExecution.getBaseFolder(),
                ".status"
        );

        try {
            new ObjectMapper().writeValue( fileService.getWriter(REPORT, path), projectStatus );
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
