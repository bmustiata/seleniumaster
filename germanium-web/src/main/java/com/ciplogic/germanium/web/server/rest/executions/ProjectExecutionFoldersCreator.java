package com.ciplogic.germanium.web.server.rest.executions;

import com.ciplogic.germanium.web.server.rest.FileService;
import com.ciplogic.germanium.web.shared.FileUtil;
import com.ciplogic.germanium.web.server.rest.profiles.BrowserProfileService;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.ciplogic.seleniumaster.profiles.BrowserProfileWriter;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfileConfig;
import com.ciplogic.util.collections.Algorithms;
import com.ciplogic.util.collections.Function;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.io.Writer;
import java.util.*;

import static com.ciplogic.germanium.web.shared.FileUtil.concatPath;
import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.*;

public class ProjectExecutionFoldersCreator {
    private Provider<BrowserProfileWriter> browserProfileWriterProvider;
    private FileService fileService;
    private BrowserProfileService browserProfileService;

    @Inject
    public ProjectExecutionFoldersCreator(
                BrowserProfileService browserProfileService,
                Provider<BrowserProfileWriter> browserProfileWriterProvider,
                FileService fileService) {
        this.fileService = fileService;
        this.browserProfileService = browserProfileService;
        this.browserProfileWriterProvider = browserProfileWriterProvider;
    }

    public void createStorage(CurrentProjectExecution currentProjectExecution, ProjectVO projectVO) {
        createFolders(projectVO, currentProjectExecution);
    }

    private void createFolders(ProjectVO projectVO, CurrentProjectExecution result) {
        touchProjectMarkerFile(result);
        makeBaseFolder(result);

        List<BrowserProfile> projectBrowserProfiles = readProjectBrowserProfiles(projectVO);

        copyProjectFile(projectVO, result);

        copySelectedTests(projectVO, result);
        copyBrowserProfiles(projectBrowserProfiles, result);
        copyUserExtensions(projectVO, projectBrowserProfiles, result);
    }

    private void touchProjectMarkerFile(CurrentProjectExecution result) {
        FileVO baseFolder = new FileVO(result.getBaseFolder(), REPORT);

        fileService.touch(REPORT,
            concatPath( baseFolder.getParentPath(), ".project-marker" )
        );
    }

    private void makeBaseFolder(CurrentProjectExecution result) {
        fileService.mkdir(REPORT, result.getBaseFolder());
    }

    private void copyProjectFile(ProjectVO projectVO, CurrentProjectExecution result) {
        FileVO projectLocation = new FileVO(projectVO.getProjectLocation(), TEST_PROJECT);
        FileVO targetFile = new FileVO( concatPath(result.getBaseFolder(), ".project"), REPORT );

        fileService.copyFile(projectLocation, targetFile);
    }

    private void copySelectedTests(ProjectVO projectVO, CurrentProjectExecution result) {
        Set<FileVO> uniqueTestFiles = new HashSet<FileVO>(projectVO.getTestFiles());
        String basePath = FileUtil.concatPath(result.getBaseFolder(), "tests");

        copyFilesToReports(uniqueTestFiles, basePath);
    }

    private void copyBrowserProfiles(List<BrowserProfile> projectProfiles, CurrentProjectExecution result) {
        writeBrowserProfiles(result, projectProfiles);
    }

    private void copyUserExtensions(ProjectVO projectVO, List<BrowserProfile> projectBrowserProfiles, CurrentProjectExecution result) {
        Set<FileVO> allUserExtensions = new HashSet<FileVO>(projectVO.getUserExtensions());
        for (BrowserProfile browserProfile : projectBrowserProfiles) {
            allUserExtensions.addAll(getBrowserUserExtensions(browserProfile));
        }

        String basePath = FileUtil.concatPath(result.getBaseFolder(), "user-extensions");

        copyFilesToReports(allUserExtensions, basePath);
    }

    private Collection<? extends FileVO> getBrowserUserExtensions(BrowserProfile browserProfile) {
        return Algorithms.collect(browserProfile.getUserExtensions(), new Function<FileVO, String>() {
            @Override
            public FileVO call(String item) {
                return new FileVO(item, USER_EXTENSION);
            }
        });
    }

    private void copyFilesToReports(Set<FileVO> uniqueTestFiles, String targetBasePath) {
        for (FileVO actualTestSourceFile : uniqueTestFiles) {
            FileVO reportsTargetTestFile = getReportsTestVOLocation(actualTestSourceFile, targetBasePath);
            fileService.copyFile( actualTestSourceFile, reportsTargetTestFile);
        }
    }

    private List<BrowserProfile> readProjectBrowserProfiles(ProjectVO projectVO) {
        BrowserProfileConfig browserProfileConfig = readBrowserProfileConfig();
        List<BrowserProfile> projectProfiles = new ArrayList<BrowserProfile>();

        for (String profileName : projectVO.getBrowserProfiles()) {
            projectProfiles.add(browserProfileConfig.getProfile(profileName));
        }
        return projectProfiles;
    }

    private FileVO getReportsTestVOLocation(FileVO actualTestSourceFile, String targetBasePath) {
        return new FileVO(concatPath(targetBasePath, actualTestSourceFile.fullPath()), REPORT);
    }

    private BrowserProfileConfig readBrowserProfileConfig() {
        return browserProfileService.readBrowserProfilesConfiguration();
    }

    private void writeBrowserProfiles(CurrentProjectExecution result, List<BrowserProfile> projectProfiles) {
        browserProfileWriterProvider.get().writeBrowserProfiles(
                getBrowserProfilesWriter(result),
                new BrowserProfileConfig(projectProfiles));
    }

    private Writer getBrowserProfilesWriter(CurrentProjectExecution result) {
        String profilesPath = FileUtil.concatPath(result.getBaseFolder(), "profiles.json");
        return fileService.getWriter(REPORT,  profilesPath);
    }
}
