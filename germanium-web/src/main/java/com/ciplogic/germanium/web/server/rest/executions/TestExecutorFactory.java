package com.ciplogic.germanium.web.server.rest.executions;

import com.ciplogic.germanium.web.server.rest.executions.browser.BrowserProjectExecution;
import com.ciplogic.germanium.web.server.rest.executions.browser.GermaniumTestExecutor;
import com.ciplogic.germanium.web.server.rest.executions.browser.LoggingBrowserProjectExecution;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;

public interface TestExecutorFactory {
    ProjectExecution createProjectExecution(ProjectVO projectVO);

    LoggingBrowserProjectExecution createLoggingProfileExecution(
            CurrentBrowserExecution currentBrowserExecution,
            ProjectVO projectVO);

    BrowserProjectExecution createProfileExecution(CurrentBrowserExecution currentBrowserExecution, ProjectVO projectVO);

    GermaniumTestExecutor createGermaniumTestExecutor(CurrentBrowserExecution currentBrowserExecution,
                                                      Germanium germanium,
                                                      DefaultSeleneseExecutor seleneseExecutor);
}
