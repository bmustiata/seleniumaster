package com.ciplogic.germanium.web.server.rest.executions.browser;

import com.ciplogic.germanium.web.server.rest.FileService;
import com.ciplogic.germanium.web.shared.FileUtil;
import com.ciplogic.germanium.web.server.rest.executions.CurrentBrowserExecution;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.Writer;

import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.REPORT;
import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.FAILED;
import static com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult.Status.SUCCESS;

public class BrowserProjectExecution implements Runnable {
    private Logger log = Logger.getLogger(BrowserProjectExecution.class);

    private GermaniumTestExecutorFactory testExecutorFactory;
    private FileService fileService;

    private CurrentBrowserExecution currentBrowserExecution;
    private ProjectVO projectVO;

    @Inject
    public BrowserProjectExecution(
            GermaniumTestExecutorFactory testExecutorFactory,
            FileService fileService,
            @Assisted CurrentBrowserExecution currentBrowserExecution,
            @Assisted ProjectVO projectVO
    ) {
        this.testExecutorFactory = testExecutorFactory;
        this.fileService = fileService;

        this.currentBrowserExecution = currentBrowserExecution;
        this.projectVO = projectVO;
    }

    @Override
    public void run() {
        SeleniumCommandResult.Status status = executeTests();
        logAggregatedBrowserResult(status);
    }

    private SeleniumCommandResult.Status executeTests() {
        SeleniumCommandResult.Status status = SUCCESS;

        GermaniumTestExecutor testExecutor = null;
        try {
            testExecutor = testExecutorFactory.createTestExecutor(currentBrowserExecution, projectVO);

            for (int i = 0; i < projectVO.getTestFiles().size(); i++) {
                SeleniumCommandResult.Status currentStatus = testExecutor.execute(i, projectVO.getTestFiles().get(i));

                if (currentStatus != SUCCESS) {
                    status = FAILED;
                }
            }

            log.debug("Execution finished for `" + currentBrowserExecution.getBrowserProfile() + "`");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (testExecutor != null) {
                testExecutor.close();
            }
        }
        return status;
    }

    private void logAggregatedBrowserResult(SeleniumCommandResult.Status status) {
        Writer writer = getWriter();
        writeValue(status, writer);
    }

    private Writer getWriter() {
        String path = FileUtil.concatPaths(
                currentBrowserExecution.getBaseFolder(),
                "executions",
                currentBrowserExecution.getBrowserProfile(),
                ".status"
        );

        return fileService.getWriter(REPORT, path);
    }

    private void writeValue(SeleniumCommandResult.Status status, Writer writer) {
        try {
            new ObjectMapper().writeValue(writer, status);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
