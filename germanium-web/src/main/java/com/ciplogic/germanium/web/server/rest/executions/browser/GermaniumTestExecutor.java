package com.ciplogic.germanium.web.server.rest.executions.browser;

import com.ciplogic.germanium.web.server.rest.FileService;
import com.ciplogic.germanium.web.server.rest.executions.CurrentBrowserExecution;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommandResult;
import com.ciplogic.seleniumaster.runner.vo.SeleniumTestReader;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.Writer;

import static com.ciplogic.germanium.web.shared.FileUtil.concatPaths;
import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.REPORT;

public class GermaniumTestExecutor {
    private DefaultSeleneseExecutor seleneseExecutor;
    private SeleniumTestReader seleniumTestReader;
    private Germanium germanium;
    private FileService fileService;
    private CurrentBrowserExecution currentBrowserExecution;

    @Inject
    public GermaniumTestExecutor(SeleniumTestReader seleniumTestReader,
                                 FileService fileService,
                                 @Assisted CurrentBrowserExecution currentBrowserExecution,
                                 @Assisted Germanium germanium,
                                 @Assisted DefaultSeleneseExecutor seleneseExecutor) {
        this.seleneseExecutor = seleneseExecutor;
        this.germanium = germanium;
        this.seleniumTestReader = seleniumTestReader;
        this.fileService = fileService;
        this.currentBrowserExecution = currentBrowserExecution;
    }

    public SeleniumCommandResult.Status execute(int i, FileVO fileVO) {
        SeleniumTestSuite suiteResult = seleneseExecutor.executeSuite( buildSuite(fileVO) );
        logResults(i, suiteResult);

        return suiteResult.getCommandResult().getStatus();
    }

    private SeleniumTestSuite buildSuite(FileVO fileVO) {
        return seleniumTestReader.readTestSuite(fileVO.fullPath());
    }

    private void logResults(int i, SeleniumTestSuite suiteResult) {
        try {
            String name = getFileName(i, suiteResult);

            String resultLocation = concatPaths(
                currentBrowserExecution.getBaseFolder(),
                "executions",
                currentBrowserExecution.getBrowserProfile(),
                name
            );
            Writer writer = fileService.getWriter(REPORT, resultLocation);

            new ObjectMapper().writeValue(writer, suiteResult);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private String getFileName(int i, SeleniumTestSuite suiteResult) {
        // FIXME: strategy?
        return i + ". " + suiteResult.getCommandResult().getStatus() + " " + suiteResult.getName();
    }

    public void close() {
        germanium.getWrappedDriver().quit();
    }
}
