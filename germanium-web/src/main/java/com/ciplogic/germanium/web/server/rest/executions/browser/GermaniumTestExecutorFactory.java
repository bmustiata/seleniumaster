package com.ciplogic.germanium.web.server.rest.executions.browser;

import com.ciplogic.germanium.web.server.GermaniumWebModule;
import com.ciplogic.germanium.web.server.rest.executions.CurrentBrowserExecution;
import com.ciplogic.germanium.web.server.rest.executions.CurrentProjectExecution;
import com.ciplogic.germanium.web.server.rest.executions.TestExecutorFactory;
import com.ciplogic.germanium.web.server.rest.live.EnhancerFactory;
import com.ciplogic.germanium.web.server.rest.live.ProfileBasedModule;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.ciplogic.seleniumaster.germanium.DefaultGermanium;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;
import com.ciplogic.seleniumaster.selenese.SeleneseModule;
import com.ciplogic.util.collections.Algorithms;
import com.ciplogic.util.collections.Function;
import com.ciplogic.util.guice.PostConstructModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import java.util.List;

public class GermaniumTestExecutorFactory {
    private TestExecutorFactory testExecutorFactory;
    private BrowserProfileManager browserProfileManager;

    @Inject
    public GermaniumTestExecutorFactory(TestExecutorFactory testExecutorFactory, BrowserProfileManager browserProfileManager) {
        this.testExecutorFactory = testExecutorFactory;
        this.browserProfileManager = browserProfileManager;
    }

    public GermaniumTestExecutor createTestExecutor(CurrentBrowserExecution currentBrowserExecution, ProjectVO projectVO) {
        // FIXME: terrible, terrible, and more terrible.
        DefaultGermanium defaultGermanium = (DefaultGermanium) getGermanium(currentBrowserExecution.getBrowserProfile(), projectVO);
        DefaultSeleneseExecutor seleneseExecutor = (DefaultSeleneseExecutor) defaultGermanium.getSeleneseExecutor();

        return testExecutorFactory.createGermaniumTestExecutor(currentBrowserExecution, defaultGermanium, seleneseExecutor);
    }

    private Germanium getGermanium(String profileName, ProjectVO projectVO) {
        BrowserProfile browserProfile = browserProfileManager.getProfile(profileName);

        Injector childInjector = Guice.createInjector(
                new GermaniumWebModule(),
                new ProfileBasedModule(browserProfile, getUserExtensions(projectVO.getUserExtensions()), projectVO.getBaseUrl()),
                new SeleneseModule(),
                new PostConstructModule()
        );

        Germanium result = childInjector.getInstance(Germanium.class);

        // FIXME: these looks like the enhancers should be arguments to the DefaultGermanium construction. API for @PostConstruct?
        childInjector.getInstance(EnhancerFactory.class).createSeleneseRunnerEnhancer();
        childInjector.getInstance(EnhancerFactory.class).createUserExtensionsEnhancer();

        return result;
    }

    private List<String> getUserExtensions(List<FileVO> userExtensions) {
        return Algorithms.collect(userExtensions, new Function<String, FileVO>() {
            @Override
            public String call(FileVO file) {
                return file.fullPath();
            }
        });
    }
}
