package com.ciplogic.germanium.web.server.rest.executions.browser;

import com.ciplogic.germanium.web.server.log.ThreadLogRunnable;
import com.ciplogic.germanium.web.server.rest.FilePathResolver;
import com.ciplogic.germanium.web.server.rest.executions.CurrentBrowserExecution;
import com.ciplogic.germanium.web.server.rest.executions.TestExecutorFactory;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import static com.ciplogic.germanium.web.shared.FileUtil.concatPaths;
import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.REPORT;

/**
 * Builds the logging writer.
 */
public class LoggingBrowserProjectExecution implements Runnable {
    private Runnable loggingDelegate;

    @Inject
    public LoggingBrowserProjectExecution(
                    TestExecutorFactory testExecutorFactory,
                    FilePathResolver filePathResolver,
                    @Assisted CurrentBrowserExecution currentBrowserExecution,
                    @Assisted ProjectVO projectVO) {
        BrowserProjectExecution profileExecution = testExecutorFactory.createProfileExecution(currentBrowserExecution, projectVO);
        Writer logWriter = createLogWriter(currentBrowserExecution, filePathResolver);

        loggingDelegate = new ThreadLogRunnable(profileExecution, logWriter);
    }

    @Override
    public void run() {
        loggingDelegate.run();
    }

    private Writer createLogWriter(CurrentBrowserExecution currentBrowserExecution, FilePathResolver filePathResolver) {
        String path = concatPaths(
                filePathResolver.getPrefixPath(REPORT),
                currentBrowserExecution.getBaseFolder(),
                "executions",
                currentBrowserExecution.getBrowserProfile(),
                "execution.log"
        );

        try {
            FileUtils.touch(new File(path));
            return new FileWriter(path);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
