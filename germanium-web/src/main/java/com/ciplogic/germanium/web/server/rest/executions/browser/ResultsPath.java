package com.ciplogic.germanium.web.server.rest.executions.browser;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({FIELD, METHOD, PARAMETER, TYPE})
@BindingAnnotation
public @interface ResultsPath {
}
