package com.ciplogic.germanium.web.server.rest.executions.browser;

import com.ciplogic.germanium.web.server.rest.FilePathResolver;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import com.ciplogic.seleniumaster.runner.io.StreamResolver;
import com.google.inject.Inject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class TestFileStreamResolver implements StreamResolver {
    @Inject
    private FilePathResolver pathResolver;

    @Override
    public InputStream getInputStream(String resourceName) {
        try {
            return new FileInputStream(pathResolver.getPath(new FileVO(resourceName, FileVO.Category.TEST)) );
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
}
