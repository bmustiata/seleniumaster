package com.ciplogic.germanium.web.server.rest.live;

import com.ciplogic.seleniumaster.selenese.SeleneseRunnerEnhancer;
import com.ciplogic.seleniumaster.selenese.UserExtensionsEnhancer;

public interface EnhancerFactory {
    SeleneseRunnerEnhancer createSeleneseRunnerEnhancer();

    UserExtensionsEnhancer createUserExtensionsEnhancer();
}
