package com.ciplogic.germanium.web.server.rest.live;

import com.ciplogic.germanium.web.server.GermaniumWebModule;
import com.ciplogic.germanium.web.server.rest.profiles.BrowserProfileServiceImpl;
import com.ciplogic.germanium.web.server.util.LogErrors;
import com.ciplogic.germanium.web.shared.vo.JSCallResultVO;
import com.ciplogic.germanium.web.shared.vo.LiveSessionVO;
import com.ciplogic.gwtui.ProgrammingLanguage;
import com.ciplogic.seleniumaster.germanium.Germanium;
import com.ciplogic.seleniumaster.javascript.JavascriptRunner;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.selenese.SeleneseModule;
import com.ciplogic.util.guice.PostConstructModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.apache.log4j.Logger;

import javax.script.ScriptEngine;
import javax.script.SimpleBindings;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.ciplogic.util.collections.MapMaker.entry;
import static com.ciplogic.util.collections.MapMaker.map;

public class LiveBrowserServiceImpl implements com.ciplogic.germanium.web.shared.remote.LiveBrowserService {
    private Logger LOG = Logger.getLogger(LiveBrowserServiceImpl.class);

    // FIXME: obviously a bad idea.
    private static Map<String, Germanium> germaniumInstances = new HashMap<String, Germanium>();

    private BrowserProfileServiceImpl browserProfileServiceImpl;

    private ScriptEngineProvider scriptEngineProvider;

    @Inject
    public LiveBrowserServiceImpl(
            BrowserProfileServiceImpl browserProfileServiceImpl, // FIXME: use interface
            ScriptEngineProvider scriptEngineProvider
    ) {
        this.browserProfileServiceImpl = browserProfileServiceImpl;
        this.scriptEngineProvider = scriptEngineProvider;
    }

    @LogErrors
    @Override
    public LiveSessionVO startSession(BrowserProfile browserProfile) {
        String germaniumId = UUID.randomUUID().toString();

        Injector childInjector = Guice.createInjector(
                new GermaniumWebModule(),
                new ProfileBasedModule(browserProfile, Collections.<String>emptyList(), "http://ciplogic.com"),
                new SeleneseModule(),
                new PostConstructModule()
        );
        Germanium germanium = childInjector.getInstance(Germanium.class);
        germaniumInstances.put(germaniumId, germanium);

        return new LiveSessionVO(germaniumId, browserProfile);
    }

    @LogErrors
    @Override
    public void stopSession(String liveBrowserId) {
        germaniumInstances.get(liveBrowserId).getWrappedDriver().quit();
    }

    @LogErrors
    @Override
    public JSCallResultVO executeJavaScript(String liveBrowserId, String code) {
        JavascriptRunner jsRunner = (JavascriptRunner) germaniumInstances.get(liveBrowserId);
        Object result = jsRunner.callScript(code);

        return outputScriptValueToString(result);
    }

    @LogErrors
    @Override
    public JSCallResultVO executeWebDriver(String liveBrowserId, String code, ProgrammingLanguage language) {
        try {
            Germanium germanium = germaniumInstances.get(liveBrowserId);
            ScriptEngine engine = scriptEngineProvider.getEngine(language);

            SimpleBindings bindings = new SimpleBindings(map(
                entry("webdriver", germanium.getWrappedDriver()),
                entry("germanium", germanium),
                entry("selenium", germanium)
            ));

            Object result = engine.eval(code, bindings);

            return outputScriptValueToString(result);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private JSCallResultVO outputScriptValueToString(Object result) {
        // FIXME: undefined should be represented as undefined.
        if (result == null) {
            return new JSCallResultVO(null);
        } else {
            return new JSCallResultVO(result.toString());
        }
    }
}
