package com.ciplogic.germanium.web.server.rest.live;

import com.ciplogic.seleniumaster.germanium.ScreenshotSaver;
import org.apache.log4j.Logger;

public class NullScreenshotSaver implements ScreenshotSaver {
    private Logger log = Logger.getLogger(NullScreenshotSaver.class);

    @Override
    public void saveScreenshot(String fileName) {
        log.info("Null save screenshot call:"  + fileName);
    }
}
