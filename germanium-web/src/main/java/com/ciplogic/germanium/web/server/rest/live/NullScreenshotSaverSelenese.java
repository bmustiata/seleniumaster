package com.ciplogic.germanium.web.server.rest.live;

import com.ciplogic.seleniumaster.germanium.SeleneseErrorScreenshotSaver;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumCommand;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTest;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;

public class NullScreenshotSaverSelenese implements SeleneseErrorScreenshotSaver {
    @Override
    public void saveScreenshot(SeleniumTestSuite suite, SeleniumTest test, SeleniumCommand command) {
    }
}
