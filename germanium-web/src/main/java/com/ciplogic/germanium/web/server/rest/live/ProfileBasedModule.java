package com.ciplogic.germanium.web.server.rest.live;

import com.ciplogic.germanium.web.server.config.UserExtensionsPath;
import com.ciplogic.seleniumaster.germanium.*;
import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.runner.DefaultSeleneseExecutor;
import com.ciplogic.seleniumaster.runner.userextensions.ClasspathScriptLoader;
import com.ciplogic.seleniumaster.runner.userextensions.FileScriptLoader;
import com.ciplogic.seleniumaster.runner.userextensions.Script;
import com.ciplogic.seleniumaster.runner.userextensions.ScriptLoader;
import com.ciplogic.seleniumaster.selenese.SeleneseExecutor;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.util.io.PathUtil.concatPath;

public class ProfileBasedModule extends AbstractModule {
    private BrowserProfile browserProfile;
    private List<String> additionalUserExtensions;
    private String baseUrl;

    public ProfileBasedModule(BrowserProfile browserProfile, List<String> additionalUserExtensions, String baseUrl) {
        this.browserProfile = browserProfile;
        this.additionalUserExtensions = additionalUserExtensions;
        this.baseUrl = baseUrl;
    }

    @Override
    protected void configure() {
        // FIXME: why there is a script loader required to load stuff, since the user extensions are also required?
        bind(ScriptLoader.class).to(ClasspathScriptLoader.class).in(Singleton.class);

        // FIXME: since the reporting is a completely different beast, a different module should exist for it.
        // FIXME: this should either be read from the project (recommended), or the first test.
        bind(String.class).annotatedWith(TestBaseSite.class).toInstance(baseUrl);
        bind(SeleneseExecutor.class).to(DefaultSeleneseExecutor.class).in(Singleton.class);
        bind(SeleneseErrorScreenshotSaver.class).to(NullScreenshotSaverSelenese.class).in(Singleton.class);
        bind(ScreenshotSaver.class).to(NullScreenshotSaver.class).in(Singleton.class);
        bind(Germanium.class).to(DefaultGermanium.class).in(Singleton.class);

        // FIXME: should be in the selenese package.
        this.install(new FactoryModuleBuilder().build(EnhancerFactory.class));
    }

    // FIXME: Set?
    @Provides @UserExtensions @Inject @Singleton
    private List<Script> getUserExtensions(@UserExtensionsPath String userExtensionsPath) {
        ScriptLoader scriptLoader = new FileScriptLoader();

        List<Script> result = new ArrayList<Script>();

        for (String userExtension : browserProfile.getUserExtensions()) {
            result.add(scriptLoader.load(concatPath(userExtensionsPath, userExtension)));
        }

        if (additionalUserExtensions != null) {
            for (String userExtension : additionalUserExtensions) {
                result.add(scriptLoader.load(concatPath(userExtensionsPath, userExtension)));
            }
        }

        return result;
    }

    @Provides @Inject @Singleton
    private WebDriver getWebDriver(BrowserProfileManager profileManager) {
        return profileManager.getWebDriver(browserProfile);
    }
}
