package com.ciplogic.germanium.web.server.rest.live;

import com.ciplogic.gwtui.ProgrammingLanguage;
import com.google.inject.Singleton;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

@Singleton
public class ScriptEngineProvider {
    public ScriptEngine getEngine(ProgrammingLanguage language) {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager(this.getClass().getClassLoader());

        String languageName = null;

        switch (language) {
            case GROOVY: languageName = "groovy"; break;
            case JAVASCRIPT: languageName = "ecmascript"; break;
            case PYTHON: languageName = "jython"; break;
            case RUBY: languageName = "jruby"; break;
        }

        return scriptEngineManager.getEngineByName(languageName);
    }
}
