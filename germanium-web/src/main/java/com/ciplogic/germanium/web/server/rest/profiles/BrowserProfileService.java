package com.ciplogic.germanium.web.server.rest.profiles;

import com.ciplogic.seleniumaster.profiles.BrowserProfileManager;
import com.ciplogic.seleniumaster.profiles.BrowserProfileReader;
import com.ciplogic.seleniumaster.profiles.BrowserProfileWriter;
import com.ciplogic.seleniumaster.profiles.ProfilesJsonName;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfileConfig;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileWriter;

/**
 * Utility service that allows reading the browser profiles.
 */
public class BrowserProfileService {
    private static Logger LOG = Logger.getLogger(BrowserProfileService.class);

    private Provider<BrowserProfileReader> browserProfileReaderProvider;
    private Provider<BrowserProfileWriter> browserProfileWriterProvider;
    private Provider<BrowserProfileManager> browserProfileManagerProvider;

    private String profilesLocation;

    @Inject
    public BrowserProfileService(Provider<BrowserProfileReader> browserProfileReaderProvider,
                Provider<BrowserProfileWriter> browserProfileWriterProvider,
                Provider<BrowserProfileManager> browserProfileManagerProvider,
                @ProfilesJsonName String profilesLocation) {
        this.browserProfileReaderProvider = browserProfileReaderProvider;
        this.browserProfileWriterProvider = browserProfileWriterProvider;
        this.browserProfileManagerProvider = browserProfileManagerProvider;
        this.profilesLocation = profilesLocation;
    }

    public BrowserProfileConfig readBrowserProfilesConfiguration() {
        try {
            BrowserProfileReader browserProfileReader = browserProfileReaderProvider.get();
            return browserProfileReader.readBrowserProfiles(new FileInputStream(profilesLocation));
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new BrowserProfileConfig();
        }
    }

    public void writeProfiles(BrowserProfileConfig profileConfig) {
        try {
            BrowserProfileWriter browserProfileWriter = browserProfileWriterProvider.get();
            browserProfileWriter.writeBrowserProfiles(new FileWriter(profilesLocation), profileConfig);

            browserProfileManagerProvider.get().reloadProfiles();
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to write profiles on `" + profilesLocation + "`", e);
        }
    }
}
