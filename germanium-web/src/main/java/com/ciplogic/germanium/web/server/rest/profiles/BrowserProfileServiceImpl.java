package com.ciplogic.germanium.web.server.rest.profiles;

import com.ciplogic.germanium.web.server.util.LogErrors;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfileConfig;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

import java.util.List;

public class BrowserProfileServiceImpl implements com.ciplogic.germanium.web.shared.remote.BrowserProfileService {
    private static Logger LOG = Logger.getLogger(BrowserProfileServiceImpl.class);

    private BrowserProfileService browserProfileService;

    @Inject
    public BrowserProfileServiceImpl(BrowserProfileService browserProfileService) {
        this.browserProfileService = browserProfileService;
    }

    @LogErrors
    public List<BrowserProfile> deleteProfile( String targetId ) {
        BrowserProfileConfig config = browserProfileService.readBrowserProfilesConfiguration();
        removeProfileById(config, targetId);
        browserProfileService.writeProfiles(config);

        return config.getProfiles();
    }

    @LogErrors
    public List<BrowserProfile> listAllProfiles() {
        BrowserProfileConfig config = browserProfileService.readBrowserProfilesConfiguration();
        return config.getProfiles();
    }

    @LogErrors
    public List<BrowserProfile> saveProfile(BrowserProfile browserProfile, String oldProfileId) {
        BrowserProfileConfig config = browserProfileService.readBrowserProfilesConfiguration();

        removeProfileIfExists(config, oldProfileId);
        updateConfig(config, browserProfile);
        browserProfileService.writeProfiles(config);

        return config.getProfiles();
    }

    private void removeProfileById(BrowserProfileConfig config, String targetId) {
        for (int i = 0; i < config.getProfiles().size(); i++) {
            BrowserProfile browserProfile = config.getProfiles().get(i);
            if (targetId.equals( browserProfile.getId() )) {
                config.getProfiles().remove(i);
                break;
            }
        }
    }

    private void updateConfig(BrowserProfileConfig profileConfig, BrowserProfile browserProfileVO) {
        if (findProfileById(profileConfig, browserProfileVO.getId()) >= 0) {
            throw new IllegalArgumentException("Profile id `" + browserProfileVO.getId() + "` already exists.");
        }

        profileConfig.getProfiles().add(browserProfileVO);
    }

    private void removeProfileIfExists(BrowserProfileConfig profileConfig, String oldProfileId) {
        int index = findProfileById(profileConfig, oldProfileId);
        if (index >= 0) {
            removeProfileByIndex(profileConfig, index);
        }
    }

    private int findProfileById(BrowserProfileConfig profileConfig, String oldProfileId) {
        for (int i = 0; i < profileConfig.getProfiles().size(); i++) {
            BrowserProfile profile = profileConfig.getProfiles().get(i);
            if (profile.getId().equals(oldProfileId)) {
                return i;
            }
        }

        return -1;
    }

    private void removeProfileByIndex(BrowserProfileConfig profileConfig, int index) {
        profileConfig.getProfiles().remove(index);
    }
}
