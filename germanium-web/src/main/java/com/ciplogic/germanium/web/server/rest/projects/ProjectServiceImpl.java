package com.ciplogic.germanium.web.server.rest.projects;

import com.ciplogic.germanium.web.server.rest.FilePathResolver;
import com.ciplogic.germanium.web.server.util.LogErrors;
import com.ciplogic.germanium.web.shared.remote.ProjectService;
import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import com.google.inject.Inject;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Writer;

import static com.ciplogic.germanium.web.shared.FileUtil.concatPath;
import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.TEST_PROJECT;

public class ProjectServiceImpl implements ProjectService {
    @Inject
    private FilePathResolver pathResolver;

    @LogErrors
    @Override
    public void save(String path, ProjectVO projectVO) {
        try {
            String projectFolder = concatPath(pathResolver.getPrefixPath(TEST_PROJECT), path);
            Writer writer = getTargetWriter(concatPath(projectFolder, projectVO.getName()));
            new ObjectMapper().writeValue(writer, projectVO);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    @LogErrors
    @Override
    public ProjectVO load(String projectLocation) {
        String projectFolder = concatPath(pathResolver.getPrefixPath(TEST_PROJECT), projectLocation);

        try {
            ProjectVO result = new ObjectMapper().readValue(new File(projectFolder), ProjectVO.class);
            result.setProjectLocation(projectLocation);

            return result;
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    private Writer getTargetWriter(String name) throws IOException {
            return new BufferedWriter(
                    new FileWriterWithEncoding(name, "utf-8")
            );
    }
}
