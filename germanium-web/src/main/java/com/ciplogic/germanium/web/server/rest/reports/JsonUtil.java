package com.ciplogic.germanium.web.server.rest.reports;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.CollectionType;

import java.io.File;
import java.util.List;

public class JsonUtil {
    public static <T> T readObject(String fileName, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(new File(fileName), clazz);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public static <T> T readObject(File file, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(file, clazz);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public static <T> List<T> readList(String fileName, Class<T> clazz) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            CollectionType valueType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);

            return mapper.readValue(new File(fileName), valueType);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

}
