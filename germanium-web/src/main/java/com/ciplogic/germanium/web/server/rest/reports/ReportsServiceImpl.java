package com.ciplogic.germanium.web.server.rest.reports;

import com.ciplogic.germanium.web.server.rest.FilePathResolver;
import com.ciplogic.germanium.web.server.util.LogErrors;
import com.ciplogic.germanium.web.shared.remote.ReportsService;
import com.ciplogic.germanium.web.shared.vo.*;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeFolder;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeItem;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProject;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfileConfig;
import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;
import com.google.inject.Inject;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ciplogic.germanium.web.server.rest.reports.JsonUtil.readObject;
import static com.ciplogic.germanium.web.shared.FileUtil.concatPath;
import static com.ciplogic.germanium.web.shared.FileUtil.concatPaths;
import static com.ciplogic.germanium.web.shared.vo.ExecutionStatus.*;
import static com.ciplogic.germanium.web.shared.vo.FileVO.Category.REPORT;
import static com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution.Status.FAILED;
import static com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution.Status.RUNNING;

public class ReportsServiceImpl implements ReportsService {
    private FilePathResolver filePathResolver;

    @Inject
    public ReportsServiceImpl(FilePathResolver filePathResolver) {
        this.filePathResolver = filePathResolver;
    }

    @LogErrors
    @Override
    public List<ReportTreeItem> listFolder(String parentPath) {
        String fullPath = getFullReportsPath(parentPath);

        List<ReportTreeItem> result = new ArrayList<ReportTreeItem>();

        for (File file : new File(fullPath).listFiles()) {
            if (isProject(file)) {
                addProject(result, file);
            } else if (isProjectsFolder(file)) {
                addProjectsFolder(result, file);
            }
        }

        return result;
    }

    @LogErrors
    @Override
    public List<ReportTreeProjectExecution> listExecutions(String projectPath) {
        String fullPath = getFullReportsPath(projectPath);

        List<ReportTreeProjectExecution> result = new ArrayList<ReportTreeProjectExecution>();

        for (File file : new File(fullPath).listFiles()) {
            if (isExecutionsFolder(file)) {
                addExecutionsFolder(result, file);
            }
        }

        return result;
    }

    @Override
    public ProjectExecutionVO fetchExecution(String executionPath) {
        String fullPath = getFullReportsPath(executionPath);

        ProjectExecutionVO result = readProjectExecutionVO(fullPath);

        return result;
    }

    private ProjectExecutionVO readProjectExecutionVO(String fullPath) {
        ProjectExecutionVO result = new ProjectExecutionVO();

        result.setProjectVO(readExecutionProjectVO(fullPath));
        result.setBrowserProfiles(readBrowserProfilesListVO(fullPath));
        result.setExecutionResultsVO(readExecutionResults(fullPath, result.getBrowserProfiles()));

        return result;
    }

    private ProjectVO readExecutionProjectVO(String fullPath) {
        return readObject(concatPath(fullPath, ".project"), ProjectVO.class);
    }

    private List<BrowserProfile> readBrowserProfilesListVO(String fullPath) {
        BrowserProfileConfig browserProfileConfig =
                JsonUtil.readObject(concatPath(fullPath, "profiles.json"), BrowserProfileConfig.class);

        return browserProfileConfig.getProfiles();
    }

    private List<ExecutionVO> readExecutionResults(String fullPath, List<BrowserProfile> browserProfiles) {
        List<ExecutionVO> result = new ArrayList<ExecutionVO>();

        for (BrowserProfile browserProfile : browserProfiles) {
            result.add(readExecutionVO(fullPath, browserProfile));
        }

        return result;
    }

    private ExecutionVO readExecutionVO(String fullPath, BrowserProfile browserProfile) {
        ExecutionVO result = new ExecutionVO();

        result.setProfileName( browserProfile.getId() );
        result.setExecutionStatus( readExecutionStatus(fullPath, browserProfile) );
        result.setTestResultVOs( readTestResultsVO(fullPath, browserProfile) );

        return result;
    }

    private ExecutionStatus readExecutionStatus(String fullPath, BrowserProfile browserProfile) {
        try {
            String statusPath = concatPaths(fullPath, "executions", browserProfile.getId(), ".status");
            String result = readObject(statusPath, String.class);
            if ("SUCCESS".equals(result)) {
                return SUCCESS;
            } else {
                return FAILURE;
            }
        } catch (IllegalArgumentException e) {
            return EXECUTING;
        }
    }

    private List<TestResultVO> readTestResultsVO(String fullPath, BrowserProfile browserProfile) {
        List<TestResultVO> testResultVOs = new ArrayList<TestResultVO>();

        String testResultsPath = concatPaths(fullPath, "executions", browserProfile.getId());

        Map<Integer, TestResultVO> testResultVOMap = new TreeMap<Integer, TestResultVO>();

        for (File file : new File(testResultsPath).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.matches("^\\d+\\. .*");
            }
        })) {
            Matcher matcher = Pattern.compile("^(\\d+).*$").matcher(file.getName());
            matcher.matches();
            int testNumber = Integer.parseInt(matcher.group(1));

            testResultVOMap.put(testNumber, readTestResultsVO(file));
        }

        testResultVOs.addAll( testResultVOMap.values() );

        return testResultVOs;
    }

    private TestResultVO readTestResultsVO(File file) {
        TestResultVO resultVO = new TestResultVO();

        resultVO.setSeleniumTestSuite( JsonUtil.readObject(file, SeleniumTestSuite.class) );
        resultVO.setName( file.getName() ); // FIXME: the name should be extracted from the filename.

        return resultVO;
    }

    private String getFullReportsPath(String projectPath) {
        String prefixPath = filePathResolver.getPrefixPath(REPORT);
        return concatPath(prefixPath, projectPath);
    }

    private boolean isExecutionsFolder(File file) {
        return new File(file, ".project").exists();
    }

    private void addExecutionsFolder(List<ReportTreeProjectExecution> result, File file) {
        result.add( createProjectExecution(file) );
    }

    private ReportTreeProjectExecution createProjectExecution(File file) {
        ReportTreeProjectExecution result = new ReportTreeProjectExecution(null, file.getName());

        fillExecutionStatus(result, file);

        return result;
    }

    private void fillExecutionStatus(ReportTreeProjectExecution result, File file) {
        File statusFile = new File(file, ".status");

        if (!statusFile.exists()) {
            result.setStatus(RUNNING);
            return;
        }

        String status = readStatusValue(statusFile);

        if ("SUCCESS".equals(status)) {
            result.setStatus(ReportTreeProjectExecution.Status.SUCCESS);
        } else {
            result.setStatus(FAILED);
        }
    }

    private String readStatusValue(File statusFile) {
        return readObject(statusFile, String.class);
    }

    private void addProjectsFolder(List<ReportTreeItem> result, File file) {
        result.add(createReportFolder(file));
    }

    private ReportTreeFolder createReportFolder(File file) {
        return new ReportTreeFolder(null, file.getName());
    }

    private boolean isProjectsFolder(File file) {
        return isFolder(file);
    }

    private boolean isProject(File file) {
        return isFolder(file) && containsProjectMarker(file);
    }

    private boolean containsProjectMarker(File file) {
        return new File(file, ".project-marker").exists();
    }

    private boolean isFolder(File file) {
        return file.isDirectory();
    }

    private void addProject(List<ReportTreeItem> result, File file) {
        result.add(createProject(file));
    }

    private ReportTreeProject createProject(File file) {
        ReportTreeProject result = new ReportTreeProject(null);

        result.setName(file.getName());

        return result;
    }
}
