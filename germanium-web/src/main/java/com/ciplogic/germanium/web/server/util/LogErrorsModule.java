package com.ciplogic.germanium.web.server.util;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

public class LogErrorsModule extends AbstractModule {
    @Override
    protected void configure() {
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(LogErrors.class),
                new LogErrorsInterceptor());
    }
}
