package com.ciplogic.germanium.web.shared;

public class FileUtil {
    private final static String FILE_SEPARATOR = "/"; // In Germanium all paths are `/` separated.

    /**
     * Concatenate two paths checking if the name of the base file has or not the ending slash, or if both
     * the file name and the base file name have a starting and ending slash accordingly.
     * @param basePath
     * @param filePath
     * @return
     */
    public static String concatPath(String basePath, String filePath) {
        if (isEmpty(filePath)) {
            throw new IllegalArgumentException("The file name cannot be empty: `" + filePath + "`");
        }

        if (isEmpty(basePath)) {
            return filePath;
        }

        if (basePath.endsWith(FILE_SEPARATOR) && filePath.startsWith(FILE_SEPARATOR)) {
            return basePath + filePath.substring(1);
        }

        if (basePath.endsWith(FILE_SEPARATOR) || filePath.startsWith(FILE_SEPARATOR)) {
            return basePath + filePath;
        }

        return basePath + FILE_SEPARATOR + filePath;
    }

    public static String concatPaths(String ... paths) {
        if (paths == null || paths.length < 2) {
            throw new IllegalArgumentException("You need at least two parameters");
        }

        String currentPath = paths[0];
        for (int i = 1; i < paths.length; i++) {
            currentPath = concatPath( currentPath, paths[i] );
        }

        return currentPath;
    }

    private static boolean isEmpty(String filePath) {
        return filePath == null || "".equals(filePath);
    }
}
