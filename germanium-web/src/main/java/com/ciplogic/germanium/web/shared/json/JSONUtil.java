package com.ciplogic.germanium.web.shared.json;

import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

public class JSONUtil {
    public static String getJSONStringValue(JSONObject javaScriptObject, String name) {
        JSONString result = (JSONString) javaScriptObject.get(name);

        if (result == null) {
            return "";
        }

        return result.stringValue();
    }

    public static Long getJSONLongValue(JSONObject javaScriptObject, String name) {
        JSONNumber result = (JSONNumber) javaScriptObject.get(name);

        if (result == null) {
            return null;
        }

        return (long) result.doubleValue();
    }

    public static boolean getJSONBooleanValue(JSONObject javaScriptObject, String name) {
        JSONBoolean result = (JSONBoolean) javaScriptObject.get(name);

        return result == null ? false : result.booleanValue();
    }
}
