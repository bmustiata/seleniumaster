package com.ciplogic.germanium.web.shared.remote;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import org.fusesource.restygwt.client.DirectRestService;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.List;

@Path("/browserProfile")
public interface BrowserProfileService extends DirectRestService {
    @POST
    @Path("/delete")
    public List<BrowserProfile> deleteProfile(@QueryParam("id") String targetId);

    @POST
    @Path("/list")
    public List<BrowserProfile> listAllProfiles();

    @POST
    @Path("/save")
    public List<BrowserProfile> saveProfile(BrowserProfile browserProfile,
                                              @QueryParam("oldProfileId") String oldProfileId);
}
