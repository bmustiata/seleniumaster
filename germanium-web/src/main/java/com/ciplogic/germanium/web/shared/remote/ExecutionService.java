package com.ciplogic.germanium.web.shared.remote;

import com.ciplogic.germanium.web.shared.vo.FileVO;
import org.fusesource.restygwt.client.DirectRestService;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/executions")
public interface ExecutionService extends DirectRestService {
    @Path("execute")
    @POST
    public void startExecuton(FileVO projectVO);
}
