package com.ciplogic.germanium.web.shared.remote;

import com.ciplogic.germanium.web.shared.vo.DocumentVO;
import com.ciplogic.germanium.web.shared.vo.FileVO;
import org.fusesource.restygwt.client.DirectRestService;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.List;

@Path("/files")
public interface FileListService extends DirectRestService {
    @POST
    @Path("/list")
    public List<FileVO> listFilesAsJson(@QueryParam("path") String path,
                                        @QueryParam("category") FileVO.Category category);

    @POST
    @Path("/get")
    public DocumentVO loadContent(@QueryParam("file") String fullPath,
                                  @QueryParam("category") FileVO.Category category);

    @POST
    @Path("/save")
    public void saveContent(@QueryParam("file") String fullPath,
                            @QueryParam("category") FileVO.Category category,
                            @FormParam("content") String content);

    @POST
    @Path("/create")
    public FileVO createEmptyFile(FileVO parentFolder,
                                   @QueryParam("newfile") String fileName);

    @POST
    @Path("/mkdir")
    public void mkdir(FileVO fileVO,
                      @QueryParam("newfolder") String folderName);

    @POST
    @Path("/remove")
    void remove(FileVO fileVO);
}
