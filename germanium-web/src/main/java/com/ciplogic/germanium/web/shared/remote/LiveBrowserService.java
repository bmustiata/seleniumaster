package com.ciplogic.germanium.web.shared.remote;


import com.ciplogic.germanium.web.shared.vo.JSCallResultVO;
import com.ciplogic.germanium.web.shared.vo.LiveSessionVO;
import com.ciplogic.gwtui.ProgrammingLanguage;
import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;
import org.fusesource.restygwt.client.DirectRestService;

import javax.ws.rs.*;


@Path("/liveBrowser")
public interface LiveBrowserService extends DirectRestService {
    @POST
    public LiveSessionVO startSession(BrowserProfile browserProfile);

    @DELETE
    public void stopSession(@QueryParam("id") String liveBrowserId);

    /**
     * Execute some javascript code in the context of the browser that the webdriver is controlling.
     * Thus the script is actually sent as is into the browser's eval.
     * @param liveBrowserId
     * @param code
     * @return
     */
    @POST
    @Path("executeJS")
    public JSCallResultVO executeJavaScript(@QueryParam("id") String liveBrowserId,
                                            @QueryParam("code") String code);

    /**
     * Execute some script in the context of the server, having the webdriver and germanium
     * objects available in the context.
     * @param liveBrowserId The current session id.
     * @param code Code to execute.
     * @param language The programming language where is written into (javascript/groovy/python/ruby).
     * @return the output value of the script, serialized to String.
     */
    @POST
    @Path("executeWebDriver")
    public JSCallResultVO executeWebDriver(@QueryParam("id") String liveBrowserId,
                                           @QueryParam("code") String code,
                                           @QueryParam("lang") ProgrammingLanguage language);
}
