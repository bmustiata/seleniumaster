package com.ciplogic.germanium.web.shared.remote;

import com.ciplogic.germanium.web.shared.vo.ProjectVO;
import org.fusesource.restygwt.client.DirectRestService;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/projects")
public interface ProjectService extends DirectRestService {
    @POST
    @Path("/save")
    public void save(@QueryParam("path") String path, ProjectVO projectVO);

    @GET
    @Path("/load")
    public ProjectVO load(@QueryParam("path") String path);
}
