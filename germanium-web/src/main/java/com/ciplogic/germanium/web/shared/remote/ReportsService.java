package com.ciplogic.germanium.web.shared.remote;

import com.ciplogic.germanium.web.shared.vo.ProjectExecutionVO;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeItem;
import com.ciplogic.germanium.web.shared.vo.reports.ReportTreeProjectExecution;
import org.fusesource.restygwt.client.DirectRestService;

import javax.ws.rs.*;
import java.util.List;

@Path("/reports")
public interface ReportsService extends DirectRestService {
    @GET
    @Path("/list")
    @Produces("application/json")
    public List<ReportTreeItem> listFolder(@QueryParam("parentPath") String parentPath);

    @POST
    @Path("/executions/list")
    @Produces("application/json")
    public List<ReportTreeProjectExecution> listExecutions(@QueryParam("project") String projectPath);

    @GET
    @Path("/executions/get")
    @Produces("application/json")
    public ProjectExecutionVO fetchExecution(@QueryParam("executionPath") String executionPath);
}
