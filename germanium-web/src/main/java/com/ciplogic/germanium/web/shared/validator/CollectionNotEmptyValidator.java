package com.ciplogic.germanium.web.shared.validator;

import java.util.Collection;

public class CollectionNotEmptyValidator implements Validator<Collection> {
    @Override
    public boolean isValid(Collection value) {
        return value != null && !value.isEmpty();
    }
}
