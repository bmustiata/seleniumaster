package com.ciplogic.germanium.web.shared.validator;

public class ComposedValidator<T> implements Validator<T> {
    private Validator<T>[] validators;

    public ComposedValidator(Validator<T> ... validators) {
        this.validators = validators;
    }

    @Override
    public boolean isValid(T value) {
        for (Validator<T> validator : validators) {
            if (!validator.isValid(value)) {
                return false;
            }
        }

        return true;
    }
}
