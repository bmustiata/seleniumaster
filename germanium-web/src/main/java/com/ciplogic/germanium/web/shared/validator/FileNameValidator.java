package com.ciplogic.germanium.web.shared.validator;

public class FileNameValidator implements Validator<String> {
    @Override
    public boolean isValid(String value) {
        return !isEmpty(value) &&
                !isInvalidCharPresent(value);
    }

    private boolean isInvalidCharPresent(String text) {
        return text.matches("[\\\\\\/\\:\\*\\?\\\"\\<\\>]");
    }

    private boolean isEmpty(String text) {
        return text == null || "".equals(text);
    }

}
