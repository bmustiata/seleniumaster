package com.ciplogic.germanium.web.shared.validator;

public class IdentifierValidator implements Validator<String> {
    @Override
    public boolean isValid(String value) {
        return value != null &&
                !value.trim().isEmpty();
    }
}
