package com.ciplogic.germanium.web.shared.validator;

public class RemoteIpValidator implements Validator<String> {
    @Override
    public boolean isValid(String text) {
        return text != null && !text.trim().isEmpty();
    }
}
