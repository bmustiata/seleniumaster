package com.ciplogic.germanium.web.shared.validator;

public interface Validator<T> {
    boolean isValid(T value);
}
