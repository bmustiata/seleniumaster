package com.ciplogic.germanium.web.shared.validator;

public class VersionValidator implements Validator<String> {
    public static final String REGEXP_VERSION = "^[\\d\\w\\.\\-]+$";

    @Override
    public boolean isValid(String text) {
        return text == null ||
                "".equals(text) ||
                text.matches(REGEXP_VERSION);
    }
}
