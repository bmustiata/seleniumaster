package com.ciplogic.germanium.web.shared.vo;

/**
 * A document is a file with its associated content.
 */
public class DocumentVO {
    private FileVO fileVO;

    private String content;

    public FileVO getFileVO() {
        return fileVO;
    }

    public void setFileVO(FileVO fileVO) {
        this.fileVO = fileVO;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
