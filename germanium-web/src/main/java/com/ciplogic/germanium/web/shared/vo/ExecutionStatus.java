package com.ciplogic.germanium.web.shared.vo;

public enum ExecutionStatus {
    SUCCESS,
    FAILURE,
    EXECUTING
}
