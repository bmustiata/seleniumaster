package com.ciplogic.germanium.web.shared.vo;

import java.util.ArrayList;
import java.util.List;

public class ExecutionVO {
    private String profileName;
    private ExecutionStatus executionStatus;
    private List<TestResultVO> testResultVOs = new ArrayList<TestResultVO>();

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public ExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(ExecutionStatus executionStatus) {
        this.executionStatus = executionStatus;
    }

    public List<TestResultVO> getTestResultVOs() {
        return testResultVOs;
    }

    public void setTestResultVOs(List<TestResultVO> testResultVOs) {
        this.testResultVOs = testResultVOs;
    }
}
