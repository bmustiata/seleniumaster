package com.ciplogic.germanium.web.shared.vo;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import org.codehaus.jackson.annotate.JsonIgnore;

public class FileVO {
    // Files exist on the serverside in specific categories.
    public enum Category {
        TEST_PROJECT,
        TEST,
        USER_EXTENSION,
        REPORT
    }

    private FileVO parentNode;
    private String parentPath;

    private Category category;
    private String name;
	private boolean directory = true;

    private static RegExp FILE_EXTENSION_REGEXP = RegExp.compile("^.*\\.(.*?)$");
    private static RegExp ABSOLUTE_PATH_REGEXP = RegExp.compile("^(.*\\/)(.*?)$");

    public FileVO() {
    }

	public FileVO(FileVO parentNode, String name, Category category) {
        this.parentNode = parentNode;
        this.name = name;
        this.category = category;
	}

    public FileVO(String absolutePath, Category category) {
        MatchResult matchResult = ABSOLUTE_PATH_REGEXP.exec(absolutePath);
        if (matchResult == null) {
            throw new IllegalArgumentException("`" + absolutePath + "` is an invalid file path.");
        }

        this.parentPath = matchResult.getGroup(1);
        this.name = matchResult.getGroup(2);

        this.category = category;
    }

    public FileVO toAbsoluteFile() {
        FileVO fileVO = new FileVO(fullPath(), getCategory());
        fileVO.setDirectory( isDirectory() );

        return fileVO;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public boolean isDirectory() {
		return directory;
	}

	public void setDirectory(boolean directory) {
		this.directory = directory;
	}

    @JsonIgnore
    public FileVO getParentNode() {
        return parentNode;
    }

    public Category getCategory() {
        if (category == null) {
            category = getParentNode().getCategory();
        }
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setParentNode(FileVO parentNode) {
        this.parentNode = parentNode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileVO fileVO = (FileVO) o;

        if (directory != fileVO.directory) return false;
        if (name != null ? !name.equals(fileVO.name) : fileVO.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (directory ? 1 : 0);
        return result;
    }

    public String fullPath() {
        if (parentNode == null) {
            return concatPath(getParentPath(), getName());
        }

        return concatPath(parentNode.fullPath(), getName());
    }

    private String concatPath(String parentPath, String localPath) {
        if (parentPath == null) {
            return localPath;
        }

        if (parentPath.endsWith("/") || localPath.startsWith("/")) {
            return parentPath + localPath;
        }

        return parentPath + "/" + getName();
    }

    public String fileExtension() {
        MatchResult matchResult = FILE_EXTENSION_REGEXP.exec(name);

        if (matchResult == null) {
            return null;
        }

        return matchResult.getGroup(1);
    }

    @Override
    public String toString() {
        return "FileVO{" +
                "directory=" + directory +
                ", fullpath='" + fullPath() + '\'' +
                ", category=" + category +
                '}';
    }

    public String getParentPath() {
        if (parentPath == null && parentNode != null) {
            return parentNode.fullPath();
        }

        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }
}
