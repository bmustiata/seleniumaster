package com.ciplogic.germanium.web.shared.vo;

public class JSCallResultVO {
    private String value;

    public JSCallResultVO() {
    }

    public JSCallResultVO(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
