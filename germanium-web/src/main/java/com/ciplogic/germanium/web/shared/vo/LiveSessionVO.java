package com.ciplogic.germanium.web.shared.vo;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

public class LiveSessionVO {
    private String id;

    // the browser profile on which this live session is executing against.
    private BrowserProfile browserProfile;

    public LiveSessionVO() {
    }

    public LiveSessionVO(String germaniumId, BrowserProfile browserProfile) {
        this.id = germaniumId;
        this.browserProfile = browserProfile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BrowserProfile getBrowserProfile() {
        return browserProfile;
    }

    public void setBrowserProfile(BrowserProfile browserProfile) {
        this.browserProfile = browserProfile;
    }
}
