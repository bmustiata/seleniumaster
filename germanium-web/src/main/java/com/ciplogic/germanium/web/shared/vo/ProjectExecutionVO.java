package com.ciplogic.germanium.web.shared.vo;

import com.ciplogic.seleniumaster.profiles.client.BrowserProfile;

import java.util.ArrayList;
import java.util.List;

public class ProjectExecutionVO {
    private ProjectVO projectVO;
    private List<BrowserProfile> browserProfiles;
    private List<ExecutionVO> executionResultsVO = new ArrayList<ExecutionVO>();

    public ProjectVO getProjectVO() {
        return projectVO;
    }

    public void setProjectVO(ProjectVO projectVO) {
        this.projectVO = projectVO;
    }

    public List<BrowserProfile> getBrowserProfiles() {
        return browserProfiles;
    }

    public void setBrowserProfiles(List<BrowserProfile> browserProfiles) {
        this.browserProfiles = browserProfiles;
    }

    public List<ExecutionVO> getExecutionResultsVO() {
        return executionResultsVO;
    }

    public void setExecutionResultsVO(List<ExecutionVO> executionResultsVO) {
        this.executionResultsVO = executionResultsVO;
    }
}
