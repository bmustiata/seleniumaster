package com.ciplogic.germanium.web.shared.vo;

import com.ciplogic.germanium.web.shared.json.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class ProjectVO {
    private String name;
    private String baseUrl;

    /**
     * The location of the project, from where it was read.
     */
    @JsonIgnore
    private String projectLocation;

    private List<FileVO> testFiles = new ArrayList<FileVO>();
    private List<String> browserProfiles = new ArrayList<String>();
    private List<FileVO> userExtensions= new ArrayList<FileVO>();

    public ProjectVO() {
    }

    public ProjectVO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public List<FileVO> getTestFiles() {
        return testFiles;
    }

    public void setTestFiles(List<FileVO> testFiles) {
        this.testFiles = testFiles;
    }

    public List<String> getBrowserProfiles() {
        return browserProfiles;
    }

    public void setBrowserProfiles(List<String> browserProfiles) {
        this.browserProfiles = browserProfiles;
    }

    public List<FileVO> getUserExtensions() {
        return userExtensions;
    }

    public void setUserExtensions(List<FileVO> userExtensions) {
        this.userExtensions = userExtensions;
    }

    public String getProjectLocation() {
        return projectLocation;
    }

    public void setProjectLocation(String projectLocation) {
        this.projectLocation = projectLocation;
    }
}
