package com.ciplogic.germanium.web.shared.vo;

import com.ciplogic.seleniumaster.runner.vo.client.SeleniumTestSuite;

public class TestResultVO {
    private String name;
    private SeleniumTestSuite seleniumTestSuite;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SeleniumTestSuite getSeleniumTestSuite() {
        return seleniumTestSuite;
    }

    public void setSeleniumTestSuite(SeleniumTestSuite seleniumTestSuite) {
        this.seleniumTestSuite = seleniumTestSuite;
    }
}
