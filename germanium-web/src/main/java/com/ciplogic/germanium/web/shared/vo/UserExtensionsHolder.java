package com.ciplogic.germanium.web.shared.vo;

import java.util.List;

public interface UserExtensionsHolder {
    List<FileVO> getUserExtensions();
}
