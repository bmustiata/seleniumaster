package com.ciplogic.germanium.web.shared.vo.event;

public interface CustomEventAware<T> {
    void onCustomEvent(T event);
}
