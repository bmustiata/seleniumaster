package com.ciplogic.germanium.web.shared.vo.event;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomEventBase {
    @JsonIgnore
    private Map<Class, List<CustomEventAware>> listeners = new HashMap<Class, List<CustomEventAware>>();

    public void addListener(Class eventType, CustomEventAware listener) {
        List<CustomEventAware> listenerList = listeners.get(eventType);

        if (listenerList == null) {
            listenerList = new ArrayList<CustomEventAware>();
            listeners.put(eventType, listenerList);
        }

        listenerList.add(listener);
    }

    public void removeListener(Class eventType, CustomEventAware listener) {
        listeners.get(eventType).remove(listener);
    }

    public void notifyEvent(Object event) {
        List<CustomEventAware> listenerList = listeners.get(event.getClass());
        if (listenerList != null) {
            for (CustomEventAware listener : listenerList) {
                listener.onCustomEvent(event);
            }
        }
    }
}
