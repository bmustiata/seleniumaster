package com.ciplogic.germanium.web.shared.vo.reports;

public class ReportRootNode extends ReportTreeItem {
    public ReportRootNode() {
        super(null);
    }

    @Override
    public String getKey() {
        return "$ROOT$";
    }

    @Override
    public String getName() {
        return "";
    }
}
