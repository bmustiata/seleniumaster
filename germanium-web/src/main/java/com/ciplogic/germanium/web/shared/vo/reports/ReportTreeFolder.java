package com.ciplogic.germanium.web.shared.vo.reports;

import com.ciplogic.germanium.web.shared.FileUtil;

public class ReportTreeFolder extends ReportTreeItem {
    private String name;

    /**
     * Used for serialization.
     */
    @Deprecated
    public ReportTreeFolder() {
    }

    public ReportTreeFolder(ReportTreeItem parent, String name) {
        super(parent);

        this.name = name;
    }

    @Override
    public String getKey() {
        if ("/".equals(name)) {
            return getParent().getKey() + "/" + "$SLASH$";
        } else {
            return getParent().getKey() + "/" + name;
        }
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
