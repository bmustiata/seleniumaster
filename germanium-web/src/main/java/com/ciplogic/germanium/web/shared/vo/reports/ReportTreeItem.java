package com.ciplogic.germanium.web.shared.vo.reports;

import com.ciplogic.germanium.web.shared.FileUtil;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import static org.codehaus.jackson.annotate.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@JsonSubTypes({
    @Type(name = "ReportRootNode", value = ReportRootNode.class),
    @Type(name = "ReportTreeFolder", value = ReportTreeFolder.class),
    @Type(name = "ReportTreeProject", value = ReportTreeProject.class),
    @Type(name = "ReportTreeProjectExecution",  value = ReportTreeProjectExecution.class)
})
@JsonIgnoreProperties({"parent", "key"})
public abstract class ReportTreeItem {
    private ReportTreeItem parent;

    /**
     * Used for serialization.
      */
    @Deprecated
    public ReportTreeItem() {
    }

    protected ReportTreeItem(ReportTreeItem parent) {
        this.parent = parent;
    }

    public ReportTreeItem getParent() {
        return parent;
    }

    public void setParent(ReportTreeItem parent) {
        this.parent = parent;
    }

    public abstract String getKey();

    public abstract String getName();

    /**
     * Builds the full path of a folder by concatenating the paths from all its parents.
     * @return
     */
    public String fullPath() {
        ReportTreeItem parent = this.getParent();
        String currentName = getName();

        while (parent != null) {
            currentName = FileUtil.concatPath(parent.getName(), currentName);
            parent = parent.getParent();
        }

        return currentName;
    }
}
