package com.ciplogic.germanium.web.shared.vo.reports;

public class ReportTreeProject extends ReportTreeItem {
    private String name;

    /**
     * Used for serialization.
     */
    @Deprecated
    public ReportTreeProject() {
    }

    public ReportTreeProject(ReportTreeItem parent) {
        super(parent);
    }

    @Override
    public String getKey() {
        return getParent().getKey() + "/" + name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
