package com.ciplogic.germanium.web.shared.vo.reports;

import com.ciplogic.germanium.web.shared.FileUtil;

public class ReportTreeProjectExecution extends ReportTreeItem {
    private String name;

    public enum Status {
        RUNNING,
        SUCCESS,
        FAILED
    }

    private Status status;

    /**
     * Deprecated, used only for serialization.
      */
    @Deprecated
    public ReportTreeProjectExecution() {
    }

    public ReportTreeProjectExecution(ReportTreeItem parent, String name) {
        super(parent);
        this.name = name;
    }

    @Override
    public String getKey() {
        return FileUtil.concatPath(getParent().getKey(), name);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
