package com.ciplogic.germanium.web.server.rest.live;

import com.ciplogic.gwtui.ProgrammingLanguage;
import org.junit.Test;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import static com.ciplogic.gwtui.ProgrammingLanguage.*;
import static junit.framework.Assert.assertEquals;

public class EmbeddedScriptingLanguagesTest {
    private ScriptEngineProvider scriptEngineProvider = new ScriptEngineProvider();

    @Test
    public void testGroovyExists() throws ScriptException {
        testLanguageExists(GROOVY);
    }

    @Test
    public void testJavaScriptExists() throws ScriptException {
        testLanguageExists(JAVASCRIPT);
    }

    @Test
    public void testPythonExists() throws ScriptException {
        testLanguageExists(PYTHON);
    }

    @Test
    public void testRubyExists() throws ScriptException {
        testLanguageExists(RUBY);
    }

    private void testLanguageExists(ProgrammingLanguage programmingLanguage) throws ScriptException {
        ScriptEngine scriptEngine = scriptEngineProvider.getEngine(programmingLanguage);

        Object result = scriptEngine.eval("2 + 2");

        assertEquals(4,  (int) Double.parseDouble(result.toString()) );
    }
}
