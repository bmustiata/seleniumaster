package com.ciplogic.germanium.selenium;

import com.ciplogic.germanium.selenium.driver.DriverFileStorage;
import com.google.inject.Inject;
import com.google.inject.internal.util.$Nullable;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import static com.ciplogic.germanium.selenium.OperatingSystem.LINUX;
import static com.ciplogic.germanium.selenium.OperatingSystem.MACOSX;
import static java.lang.String.format;

public class BinaryDriver {
    private static Logger log = Logger.getLogger( BinaryDriver.class );

    private final OperatingSystem operatingSystem;
    private final Architecture architecture;
    private final DriverFileStorage driverFileStorage;

    @Inject
    public BinaryDriver(DriverFileStorage driverFileStorage,
                        OperatingSystem operatingSystem,
                        Architecture architecture) {
        this.driverFileStorage = driverFileStorage;
        this.operatingSystem = operatingSystem;
        this.architecture = architecture;
    }

    public synchronized void ensureLoaded(String browser, String version) {
        if (Browser.IE.equals(browser)) {
            loadBrowserIfMissing("webdriver.ie.driver", Browser.IE);
        } else if (Browser.CHROME.equals(browser)) {
            loadBrowserIfMissing("webdriver.chrome.driver", Browser.CHROME);
        }
    }

    private void loadBrowserIfMissing(String propertyName, String browser) {
        if (System.getProperty(propertyName) != null) {
            return;
        }

        System.setProperty(propertyName, storeDriver(browser));
    }

    private String storeDriver(String browser) {
        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            String architecture = getArchitecture(browser);
            String osSuffix = getOsSuffix(browser);

            String classpathFileName = format("/drivers/%s_driver_%s%s", browser, architecture, osSuffix);
            String tempFileName = format("%s_driver_%s", browser, architecture);

            inputStream = BinaryDriver.class.getResourceAsStream(classpathFileName);

            if (inputStream == null) {
                throw new IllegalStateException(format("Unable to find binary driver (%s, %s) at %s",
                        this.operatingSystem, this.architecture, classpathFileName));
            }


            return driverFileStorage.storeFile(tempFileName, osSuffix, inputStream);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

    private String getOsSuffix(String browser) {
        switch (operatingSystem) {
            case WINDOWS: return ".exe";
            case LINUX: return "_linux";
            case MACOSX: return "_macosx";

            default: return ".exe";
        }
    }

    private String getArchitecture(String browser) {
        if (Browser.CHROME.equals(browser)) { // chrome provides only 32 bit drivers for Windows and MacOSX
            switch (operatingSystem) {
                case WINDOWS: return "x86";
                case MACOSX: return "x86";
            }
        }

        switch (architecture) {
            case X86: return "x86";
            case AMD64: return "amd64";

            default: return "amd64";
        }
    }
}
