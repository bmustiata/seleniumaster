package com.ciplogic.germanium.selenium;

import com.ciplogic.germanium.selenium.driver.DriverFileStorage;
import com.ciplogic.germanium.selenium.driver.TargetFolderDriverFileStorage;
import com.ciplogic.germanium.selenium.driver.TargetSeleniumBinaryFolder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class BinaryDriverModule extends AbstractModule {
    @Override
    protected void configure() {
        // FIXME: parametrize.
        bind(String.class).annotatedWith(TargetSeleniumBinaryFolder.class).toInstance("target/");
        bind(DriverFileStorage.class).to(TargetFolderDriverFileStorage.class);
    }

    @Provides
    public Architecture getDetectedArchitecture() {
        return Architecture.AMD64; // FIXME: implement
    }

    @Provides
    public OperatingSystem getDetectedOperatingSystem() {
        return OperatingSystem.WINDOWS; // FIXME: implement
    }
}
