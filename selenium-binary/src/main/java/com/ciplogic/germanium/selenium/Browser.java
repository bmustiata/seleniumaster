package com.ciplogic.germanium.selenium;

// "android", "chrome", "firefox", "htmlUnit", "ie", "iphone", "opera"
public final class Browser {
    public static final String ANDROID = "android";
    public static final String CHROME = "chrome";
    public static final String FIREFOX = "firefox";
    public static final String HTML_UNIT = "htmlUnit";
    public static final String IE = "ie";
    public static final String IPHONE = "iphone";
    public static final String OPERA = "opera";

    private Browser() {}
}
