package com.ciplogic.germanium.selenium;

public enum OperatingSystem {
    WINDOWS, LINUX, MACOSX
}
