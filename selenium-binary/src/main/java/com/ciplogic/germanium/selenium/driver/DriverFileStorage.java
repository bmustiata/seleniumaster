package com.ciplogic.germanium.selenium.driver;

import java.io.InputStream;

public interface DriverFileStorage {
    public String storeFile(String tempFileName, String osSuffix, InputStream inputStream);
}
