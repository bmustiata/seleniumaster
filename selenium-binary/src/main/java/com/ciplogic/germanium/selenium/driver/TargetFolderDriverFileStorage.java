package com.ciplogic.germanium.selenium.driver;

import com.ciplogic.germanium.selenium.OperatingSystem;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import static com.ciplogic.germanium.selenium.OperatingSystem.LINUX;
import static com.ciplogic.germanium.selenium.OperatingSystem.MACOSX;
import static com.ciplogic.util.io.PathUtil.concatPath;

public class TargetFolderDriverFileStorage implements DriverFileStorage {
    private final String targetFolder;
    private Logger log = Logger.getLogger(TargetFolderDriverFileStorage.class);

    private final OperatingSystem operatingSystem;

    @Inject
    public TargetFolderDriverFileStorage(
            @TargetSeleniumBinaryFolder String targetFolder,
            OperatingSystem operatingSystem) {
        this.targetFolder = targetFolder;
        this.operatingSystem = operatingSystem;
    }

    @Override
    public String storeFile(String tempFileName, String osSuffix, InputStream inputStream) {
        FileOutputStream outputStream = null;
        try {
            File driverFile = new File(concatPath(targetFolder, tempFileName + osSuffix));
            outputStream = new FileOutputStream(driverFile);

            IOUtils.copy(inputStream, outputStream);
            setExecutableBit(driverFile);

            return driverFile.getAbsolutePath();
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

    private void setExecutableBit(File driverFile) {
        if (operatingSystem == MACOSX || operatingSystem == LINUX) {
            boolean executableSet = driverFile.setExecutable(true);
            if (!executableSet) {
                log.warn("Unable to set the executable bit for: " + driverFile.getAbsolutePath());
            }
        }
    }
}
