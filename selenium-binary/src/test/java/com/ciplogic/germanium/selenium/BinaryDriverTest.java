package com.ciplogic.germanium.selenium;

import com.ciplogic.germanium.selenium.driver.DriverFileStorage;
import com.ciplogic.germanium.selenium.driver.TargetFolderDriverFileStorage;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.ciplogic.germanium.selenium.Architecture.AMD64;
import static com.ciplogic.germanium.selenium.Architecture.X86;
import static com.ciplogic.germanium.selenium.Browser.CHROME;
import static com.ciplogic.germanium.selenium.Browser.IE;
import static com.ciplogic.germanium.selenium.OperatingSystem.*;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.junit.Assert.*;

public class BinaryDriverTest {
    @Test
    public void testIe32BitLoading() throws IOException {
        BinaryDriver binaryDriver = new BinaryDriver(fileStorage(WINDOWS), WINDOWS, X86);
        testDriverLoading(binaryDriver, "webdriver.ie.driver", "ie_driver_x86.exe", IE);
    }

    @Test
    public void testIe64BitLoading() throws IOException {
        BinaryDriver binaryDriver = new BinaryDriver(fileStorage(WINDOWS), WINDOWS, AMD64);
        testDriverLoading(binaryDriver, "webdriver.ie.driver", "ie_driver_amd64.exe", IE);
    }

    @Test
    public void testChrome64BitLinuxLoading() throws IOException {
        BinaryDriver binaryDriver = new BinaryDriver(fileStorage(LINUX), LINUX, AMD64);
        testDriverLoading(binaryDriver, "webdriver.chrome.driver", "chrome_driver_amd64_linux", CHROME);
    }

    @Test
    public void testChrome32BitLinuxLoading() throws IOException {
        BinaryDriver binaryDriver = new BinaryDriver(fileStorage(LINUX), LINUX, X86);
        testDriverLoading(binaryDriver, "webdriver.chrome.driver", "chrome_driver_x86_linux", CHROME);
    }

    @Test
    public void testChrome32BitWindowsLoading() throws IOException {
        BinaryDriver binaryDriver = new BinaryDriver(fileStorage(WINDOWS), WINDOWS, X86);
        testDriverLoading(binaryDriver, "webdriver.chrome.driver", "chrome_driver_x86.exe", CHROME);
    }

    @Test
    public void testChrome64BitWindowsLoading() throws IOException {
        BinaryDriver binaryDriver = new BinaryDriver(fileStorage(WINDOWS), WINDOWS, AMD64);
        // x86 on purpose, since no amd64 driver is available.
        testDriverLoading(binaryDriver, "webdriver.chrome.driver", "chrome_driver_x86.exe", CHROME);
    }

    @Test
    public void testChrome32BitMacOSXLoading() throws IOException {
        BinaryDriver binaryDriver = new BinaryDriver(fileStorage(MACOSX), MACOSX, X86);
        testDriverLoading(binaryDriver, "webdriver.chrome.driver", "chrome_driver_x86_macosx", CHROME);
    }

    private void testDriverLoading(BinaryDriver binaryDriver, String systemProperty, String classpathResource, String browser) throws IOException {
        System.clearProperty(systemProperty);
        assertNull(System.getProperty(systemProperty));

        binaryDriver.ensureLoaded(browser, null);

        String tempFileDriverName = System.getProperty(systemProperty);

        assertNotNull(tempFileDriverName);

        File tempFileDriver = new File(tempFileDriverName);
        FileInputStream tempFileStream = new FileInputStream(tempFileDriver);
        InputStream classPathStream = BinaryDriverTest.class.getResourceAsStream("/drivers/" + classpathResource);

        String expectedHash = sha256Hex(classPathStream);
        String actualHash = sha256Hex(tempFileStream);

        tempFileStream.close();

        assertTrue("File is not executable.", tempFileDriver.canExecute());

        assertEquals("Hash values for the " + browser + " driver are different. It means the file it's corrupted or invalid.",
                expectedHash, actualHash);

        assertTrue("Unable to delete temp file: " + tempFileDriverName, tempFileDriver.delete());
    }

    private DriverFileStorage fileStorage(OperatingSystem x86) {
        return new TargetFolderDriverFileStorage("target/", x86);
    }
}
