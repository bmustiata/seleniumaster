package com.ciplogic.velocity.formatter;

/**
 * Exception thrown when rendering a template fails for some reason.
 */
public class CiplogicRenderTemplateError extends RuntimeException {
    public CiplogicRenderTemplateError() {
    }

    public CiplogicRenderTemplateError(String message) {
        super(message);
    }

    public CiplogicRenderTemplateError(String message, Throwable cause) {
        super(message, cause);
    }

    public CiplogicRenderTemplateError(Throwable cause) {
        super(cause);
    }
}
