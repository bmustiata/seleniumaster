package com.ciplogic.velocity.formatter;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;

import java.io.InputStream;

/**
 * The reason for this class is made by the inability of the normal resource loaders to be registered
 * and unregistered cleanly from the VelocityContext in a threadsafe way.
 *
 * This class stores the base path in a threadlocal variable making it perfect for a servlet container
 * usage.
 *
 * Implicitly the resources are searched in /formatter/text/. In order to change the place from
 * where they are readed, use setBasePath.
 */
public class CiplogicVelocityClasspathResourceLoader extends ResourceLoader {
    private static ThreadLocal<String> basePath = new InheritableThreadLocal<String>();

    /**
     * Sets the base path against which the #includes will be resolved. The base path
     * must include the trailing /.
     * @param basePath
     */
    public static void setBasePath(String basePath) {
        CiplogicVelocityClasspathResourceLoader.basePath.set(basePath);
    }

    public static String getBasePath() {
        return CiplogicVelocityClasspathResourceLoader.basePath.get();
    }

    @Override
    public void init(ExtendedProperties configuration) {
        // set the base path only if it is not set already.
        if (CiplogicVelocityClasspathResourceLoader.basePath.get() == null) {
            setBasePath("/formatter/text/");
        }
    }

    @Override
    public InputStream getResourceStream(String source) throws ResourceNotFoundException {
        return getClass().getResourceAsStream(basePath.get() + source);
    }

    @Override
    public boolean isSourceModified(Resource resource) {
        return false;
    }

    @Override
    public long getLastModified(Resource resource) {
        return 0;
    }
}
