package com.ciplogic.velocity.formatter;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.Properties;

import static com.ciplogic.velocity.formatter.CiplogicVelocityClasspathResourceLoader.getBasePath;
import static com.ciplogic.velocity.formatter.CiplogicVelocityClasspathResourceLoader.setBasePath;

public final class VelocityTemplateRenderer {
    private static Logger log = Logger.getLogger(VelocityTemplateRenderer.class);

    private static volatile VelocityEngine velocityEngineInstance;

    /**
     * Render a template using the base path as the clazz package.
     * @param name
     * @param parameters
     * @return
     */
    public static String renderTemplate(Class clazz, String name, Map<String, Object> parameters) {
        String savedBasePath = null;
        try {
            savedBasePath = getBasePath();
            changeBasePath(clazz);

            return renderTemplate(name, parameters);
        } finally {
            setBasePath(savedBasePath);
        }
    }

    /**
     * Render a template, and return the result of the rendering as a String.
     * @param name
     * @param parameters
     * @return
     */
    public static String renderTemplate(String name, Map<String, Object> parameters) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        renderTemplate(name, outputStream, parameters);

        return outputStream.toString();
    }

    /**
     * Render a certain template to the given outputStream.
     * @param name
     * @param outputStream
     * @param parameters
     */
    public static void renderTemplate(String name, OutputStream outputStream, Map<String, Object> parameters) {
        try {
            Template template = getVelocityEngine().getTemplate(name);
            OutputStreamWriter writer = new OutputStreamWriter(outputStream);
            template.merge(new VelocityContext(parameters), writer);

            writer.flush();
        } catch (Exception e) {
            log.error("Unable to render template: " + name, e);
            throw new CiplogicRenderTemplateError(e);
        }
    }

    private static void changeBasePath(Class clazz) {
        String newPath = "/" + clazz.getPackage().getName().replaceAll("\\.", "/") + "/";
        setBasePath(newPath);
    }

    /**
     * A singleton instance of the velocity engine. It is ok to be singleton since the resource loader are actually
     * threadlocal.
     * @return
     */
    private static VelocityEngine getVelocityEngine() {
        VelocityEngine result = velocityEngineInstance;
        if (result == null) {
            synchronized (VelocityTemplateRenderer.class) {
                if (result == null) {
                    Properties p = new Properties();
                    p.put("resource.loader", "class");
                    p.put("class.resource.loader.class",
                            "com.ciplogic.velocity.formatter.CiplogicVelocityClasspathResourceLoader");

                    result = velocityEngineInstance = new VelocityEngine();
                    velocityEngineInstance.init(p);
                }
            }
        }

        return result;
    }

}
