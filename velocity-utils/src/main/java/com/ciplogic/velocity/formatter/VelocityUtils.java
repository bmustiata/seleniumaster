package com.ciplogic.velocity.formatter;

/**
 * A bunch of methods that are probably needed when creating velocity
 * templates.
 */
public class VelocityUtils {
    public boolean isNull(Object obj) {
        return obj == null;
    }

    public boolean isNotNull(Object obj) {
        return obj != null;
    }

    public String print(Object obj) {
        if (!isNull(obj)) {
            return obj.toString();
        }
        return "";
    }
}
