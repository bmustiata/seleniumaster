package com.ciplogic.velocity.formatter;

import org.junit.Test;

import java.io.IOException;

import static com.ciplogic.util.collections.MapMaker.entry;
import static com.ciplogic.util.collections.MapMaker.map;
import static java.lang.String.format;
import static junit.framework.Assert.assertEquals;

public class VelocityTemplateRendererTest {
    @Test
    public void testRenderingToString() throws IOException {
        CiplogicVelocityClasspathResourceLoader.setBasePath("/templates/");

        String result = VelocityTemplateRenderer.renderTemplate("StringTest.vtl", map(
            entry("param1", "param1test"),
            entry("param2", "param2test")
        ));
        
        assertEquals(format("param1 is %s, and param2 is %s", "param1test", "param2test"), result);
    }
}
